//
//  AccountsVC.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import MessageUI
import Crashlytics

class AccountsVC: UIViewController,MFMailComposeViewControllerDelegate {

   
    @IBOutlet weak var labelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    fileprivate var AccountsListAr = ["طلباتي","إدارة الموقع","تغيير كلمة المرور","إدارة البطاقات","العروض","ارسل رأيك","المزيد","تسجيل خروج"]
    
     fileprivate var AccountsList = ["My Orders","Manage Address","Change Password","Manage Cards","Offers","Rate us on AppStore","More","Logout"]
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var allDetailLbl: UILabel!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var appVersionLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.appVersionLbl.isUserInteractionEnabled = true
        // App version
        self.appVersionLbl.text = "App version \(AppUtility.methodForApplicationVersion())\nPowered by Creative Solutions"
        let text1 = (self.appVersionLbl.text)!
        let underlineAttriString1 = NSMutableAttributedString(string: text1)
        let range3 = (text1 as NSString).range(of: "Creative Solutions")
        underlineAttriString1.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range3)
        self.appVersionLbl.attributedText = underlineAttriString1
        // Label Action
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(tapCreativeSolutions(gesture:)))
        self.appVersionLbl.addGestureRecognizer(gesture1)
    }
    @IBAction func tapCreativeSolutions(gesture: UITapGestureRecognizer) {
        let text = (appVersionLbl.text)!
        let termsRange = (text as NSString).range(of: "Creative Solutions")
        if gesture.didTapAttributedTextInLabel(label: self.appVersionLbl, inRange: termsRange) {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            Obj.titleStr = "Creative Solutions"
            Obj.url = "http://www.creative-sols.com/"
            self.present(Obj, animated: true, completion: nil)
        } else {
            print("Tapped none")
        }
    }
    @objc func removeLoader(){
        tableView.hideLoader()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        if(isUserLogIn() == true) {
            labelTopConstraint.constant = -42
            self.loginView.isHidden = true
            let userDic = UserDef.getUserProfileDic(key: "userDetails")
            self.nameLbl.text = "\(userDic["Name"]!)"
            self.allDetailLbl.text = "+\(String(describing: userDic["Mobile"]!)) - \(userDic["Email"]!)"
                AccountsList = ["My Orders","Manage Address","Change Password","Manage Cards","Offers","Rate us","Contact Us","About Us","Logout"]
                AccountsListAr = ["طلباتي","إدارة الموقع","تغيير كلمة المرور","إدارة البطاقات","العروض","قيمنا","تواصل معنا","من نحن","تسجيل خروج"]
            
            // Crashlytics
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                Crashlytics.sharedInstance().setUserIdentifier("\(UserDef.getUserId())")
                Crashlytics.sharedInstance().setUserName("\(userDic["Name"]!)")
                Crashlytics.sharedInstance().setUserEmail("\(userDic["Mobile"]!)")
            }
        }else{
            labelTopConstraint.constant = 0
            self.loginView.isHidden = false
                AccountsList = ["Offers","Rate us","Contact Us","About Us"]
                AccountsListAr = ["العروض","قيمنا","تواصل معنا","من نحن"]
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    func userDetailSerCall() {
        if(Connectivity.isConnectedToInternet()){
            let userId = UserDef.getUserId()
            ANLoader.showLoading("Loading..", disableUI: true)
            AccountApiRouter.displayProfileService(userId: "\(userId)") { (data) in
                ANLoader.hide()
                if data!.failure != nil{
                    print(data!.failure!)
                }else{
                   // print(data!.successDic!.fullName!)
                    self.nameLbl.text = data!.successDic!.fullName!
                    self.allDetailLbl.text = "\(String(describing: data!.successDic!.mobile!)) - \(data!.successDic!.email!)"
                    let dic = ["Email":data!.successDic!.email!,"UserId":data!.successDic!.userId!,"Mobile":data!.successDic!.mobile!,"Name":data!.successDic!.fullName!] as [String : Any]
                    UserDef.storeUserProfileDic(dict: dic, withKey: "userDetails")
                }
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
        }
    }
    @IBAction func loginBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let loginObj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        loginObj.loginFrom = 2
        self.navigationController?.pushViewController(loginObj, animated: true)
    }
    @IBAction func editBtn_Tapped(_ sender: Any) {
        if(UserDefaults.standard.value(forKey: "UserId") == nil){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Login", value: "", table: nil))!)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
            self.navigationController?.pushViewController(Obj, animated: true)
        }
    }
}
extension AccountsVC: UITableViewDataSource, UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if AppDelegate.getDelegate().appLanguage == "English"{
            print(AccountsList.count)
            return AccountsList.count
        }else{
             return AccountsListAr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountsCell") as! AccountsCell
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.nameLbl.text = AccountsList[indexPath.row]
        }else{
            cell.nameLbl.text = AccountsListAr[indexPath.row]
        }
        //cell.nameImg.image = UIImage(named: "right_menu_" + String(indexPath.row + 1))
       // print("cell height :\(cell.bounds.size.height)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        if AccountsList[indexPath.row] == "My Orders" {
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "OrderHistoryVC") as! OrderHistoryVC
           // print(AccountsList[indexPath.row])
            //Obj.branchId = 0
            self.navigationController?.pushViewController(Obj, animated: true)
        }else if AccountsList[indexPath.row] == "Manage Address" {
            //print(AccountsList[1])
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageAddressVC") as! ManageAddressVC
            self.navigationController?.pushViewController(Obj, animated: true)
        }else if AccountsList[indexPath.row] == "Change Password" {
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.navigationController?.pushViewController(Obj, animated: true)
        }else if AccountsList[indexPath.row] == "Manage Cards" {
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "ManageCardsVC") as! ManageCardsVC
            self.navigationController?.pushViewController(Obj, animated: true)
        }else if AccountsList[indexPath.row] == "Offers" {
            DispatchQueue.main.async {
                let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "PromotionsVC") as! PromotionsVC
                Obj.obj_Offer = 1
                self.navigationController?.pushViewController(Obj, animated: true)
            }
        }else if AccountsList[indexPath.row] == "Rate us"  {
            self.openAppStore()
        }else if AccountsList[indexPath.row] == "Contact Us" {
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            self.navigationController?.pushViewController(Obj, animated: true)
            //self.SendFeedback()
        }else if AccountsList[indexPath.row] == "About Us"  {
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "MoreVC") as! MoreVC
            self.navigationController?.pushViewController(Obj, animated: true)
        }else if AccountsList[indexPath.row] == "Logout"{
            let alertController = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Are You Sure to Logout ?", value: "", table: nil))!, preferredStyle: .alert)
            let yesAction = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "YES", value: "", table: nil))!, style: UIAlertAction.Style.default) {
                UIAlertAction in
                if Connectivity.isConnectedToInternet() == true && isUserLogIn() == true {
                    let dic:[String:Any] = ["UserId":"\(UserDef.getUserId())", "UserType":1, "DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))"]
                    UserModuleService.logoutService(dic: dic,isLoader:false,isUIDisable:true, success: { (data) in
                       // print (data.Status)
                    }){ (error) in
                        print (error)
                    }
                }
                self.logOut()
            }
            let noAction = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NO", value: "", table: nil))!, style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(noAction)
            alertController.addAction(yesAction)

            self.present(alertController, animated: false, completion: nil)
        }
    }
    func openAppStore() {
        let url = URL(string: "itms-apps://itunes.apple.com/app/1464131797")
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        
//        if let url = URL(string: "itms://itunes.apple.com/us/app/id1464131797?mt=8&action=write-review"),
//            UIApplication.shared.canOpenURL(url){
//            UIApplication.shared.open(url, options: [:]) { (opened) in
//                if(opened){
//                    print("App Store Opened")
//                }
//            }
//        } else {
//            print("Can't Open URL on Simulator")
//        }
    }
    func logOut(){
        ANLoader.hide()
        labelTopConstraint.constant = 0
        UserDef.removeFromUserDefaultForKey(key: "UserId")
        UserDef.removeFromUserDefaultForKey(key: "userDetails")
        //self.navigationController?.popViewController(animated: false)

        self.loginView.isHidden = false
        AccountsList = ["Offers","Rate us","Contact Us","About Us"]
        AccountsListAr = ["العروض","قيمنا","تواصل معنا","من نحن"]
        tableView.reloadData()
    }
    func SendFeedback(){
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients(["info@arab-space.com"])
        composeVC.setSubject("About The Chef App")
        composeVC.setMessageBody("", isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError?) {
        controller.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)

    }
    
    func ContactUs()  {
        let whatsappURL = URL(string: "https://api.whatsapp.com/send?phone=966550364268&text=")
        if UIApplication.shared.canOpenURL(whatsappURL!) {
            UIApplication.shared.open(whatsappURL! as URL, options: [:], completionHandler: nil)
        }
    }
    
}


