//
//  WebViewVC.swift
//  Samkra
//
//  Created by Creative Solutions on 12/4/18.
//  Copyright © 2018 RAVI. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController,UIWebViewDelegate {
    var titleStr = String()
    var url = String()
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        titleLbl?.text = titleStr
        webView.delegate = self
        if(Connectivity.isConnectedToInternet()){
           webView.loadRequest(URLRequest(url: URL(string: url)!))
        }else{
        Alert.showToastAlert(on: self, message:(AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
        }
    }
    
    @IBAction func backBtn_tap(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
          ANLoader.hide()
        }
        self.dismiss(animated: false, completion: nil)
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
         ANLoader.showLoading("Loading..", disableUI: false)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
          ANLoader.hide()
        }
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
    }
}
