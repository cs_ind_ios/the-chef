//
//  MoreVC.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import CoreLocation

class MoreVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,LocationUpdateProtocol {
    func locationDidUpdateToLocation(location: CLLocation) {
        print(location.coordinate.latitude)
    }
    
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    //@IBOutlet weak var socialMediaLbl: UILabel!
    //@IBOutlet weak var aboutUsLbl: UILabel!
    //@IBOutlet weak var snapBtn: UIButton!
    //@IBOutlet weak var instaBtn: UIButton!
    //@IBOutlet weak var fbBtn: UIButton!
    //@IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var aboutUSBtn: UIButton!
//    @IBOutlet weak var faqBtn: UIButton!
//    @IBOutlet weak var feedBackBtn: UIButton!
//    @IBOutlet weak var contactUsBtn: UIButton!
    @IBOutlet weak var termsBtn: UIButton!
    @IBOutlet weak var privacyBtn: UIButton!
//    @IBOutlet weak var cvBGView: UIView!
//    @IBOutlet weak var optionCV: UICollectionView!
    
    @IBOutlet weak var termsOfUseLbl: UILabel!
    @IBOutlet weak var privacyPolicyTextLbl: UILabel!
    //@IBOutlet weak var appFeedbackTextLbl: UILabel!
    @IBOutlet weak var aboutUstextLbl: UILabel!
    //@IBOutlet weak var faqTextLbl: UILabel!
    //@IBOutlet weak var contactUsLbl: UILabel!
    @IBOutlet weak var companyNameLbl: UILabel!

    let optionArr = ["About Us","FAQ","Contact Us","App Feedback","Privacy Policy","Terms of Use"]
    let optionImg = ["about-us","faq","contact-us","app-feedback","privacy-policy","terms-of-use"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)

       termsOfUseLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms of Use", value: "", table: nil))!
        privacyPolicyTextLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Privacy Policy", value: "", table: nil))!
        //appFeedbackTextLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "App Feedback", value: "", table: nil))!
        aboutUstextLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "About Us", value: "", table: nil))!
        //faqTextLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "FAQ", value: "", table: nil))!
        //contactUsLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Contact Us", value: "", table: nil))!

//        self.btnBorderAndCornerRadius(button: fbBtn)
//        self.btnBorderAndCornerRadius(button: twitterBtn)
//        self.btnBorderAndCornerRadius(button: instaBtn)
//        self.btnBorderAndCornerRadius(button: snapBtn)
//        
        let LocationMgr = UserLocationManager.SharedManager
        LocationMgr.delegate = self
        
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "About Us", value: "", table: nil))!
        //aboutUsLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "About Us", value: "", table: nil))!
        //socialMediaLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Social Media", value: "", table: nil))!
        
        //contactUsBtn.addTarget(self, action: #selector(contactUsBtn_tapped(_:)), for: .touchUpInside)
        
        // Company name
        self.companyNameLbl.isUserInteractionEnabled = true
        // App version
        self.companyNameLbl.text = "This application is managed by Arab Space"
        let text1 = (self.companyNameLbl.text)!
        let underlineAttriString1 = NSMutableAttributedString(string: text1)
        let range3 = (text1 as NSString).range(of: "Arab Space")
        underlineAttriString1.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range3)
        self.companyNameLbl.attributedText = underlineAttriString1
        // Label Action
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(tapCreativeSolutions(gesture:)))
        self.companyNameLbl.addGestureRecognizer(gesture1)
        
    }
    @IBAction func AboutUsBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "About Us", value: "", table: nil))!
        Obj.url = "http://app.thechefapp.net/AboutUS"
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    @IBAction func privacyAndPolicyBtn_Tapped(_ sender: UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Privacy Policy", value: "", table: nil))!
        Obj.url = "http://app.thechefapp.net/PrivacyPolicy"
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    @IBAction func tapCreativeSolutions(gesture: UITapGestureRecognizer) {
        let text = (companyNameLbl.text)!
        let termsRange = (text as NSString).range(of: "Arab Space")
        if gesture.didTapAttributedTextInLabel(label: self.companyNameLbl, inRange: termsRange) {
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            Obj.titleStr = "Arab Space"
            Obj.url = "http://arab-space.com.sa"
            Obj.modalPresentationStyle = .overCurrentContext
            self.present(Obj, animated: true, completion: nil)
        } else {
            print("Tapped none")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    @IBAction func TwitterBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = "Twitter"
        Obj.url = "https://www.twitter.com/thechefapp"
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    @IBAction func backBtn_Tapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // social media button Round corner radius method
    func btnBorderAndCornerRadius(button:UIButton) {
        button.layer.cornerRadius = 12
        button.layer.masksToBounds = true
        button.layer.borderWidth = 2.0
        let borderColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
        button.layer.borderColor = borderColor.cgColor
    }
    
    // option button Round corner radius method
    func btnBorderAndCornerRadius1(button:UIButton) {
        //button.layer.cornerRadius = snapBtn.frame.size.width/2
        button.layer.masksToBounds = true
        button.layer.borderWidth = 1.5
        let borderColor = UIColor.init(red: 0.427, green: 0.639, blue: 0.749, alpha: 1.0)
        button.layer.borderColor = borderColor.cgColor
    }
    @IBAction func accountsBtn_tapped(_ sender: UIButton){
        if isUserLogIn() == true {
            var Obj = AccountsVC()
            Obj = self.storyboard?.instantiateViewController(withIdentifier: "AccountsVC") as! AccountsVC
            self.navigationController?.pushViewController(Obj, animated: true)
        }else{
            var Obj = LoginVC()
            Obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            Obj.modalPresentationStyle = .overCurrentContext
            self.present(Obj, animated: true, completion: nil)
        }
    }
    
    @IBAction func contactUsBtn_tapped(_ sender: UIButton){
//        var str = ""
//        str = str.addingPercentEncoding(withAllowedCharacters: (NSCharacterSet.urlQueryAllowed))!
//        let whatsappURL = NSURL(string: "whatsapp://send?text=\(str)")
//
//        if UIApplication.shared.canOpenURL(whatsappURL! as URL) {
//            UIApplication.shared.open(whatsappURL! as URL, options: [:], completionHandler: nil)
//        } else {
//            Alert.showAlert(on: self, title: Title, message: "Whatsapp is not installed on this device. Please install Whatsapp and try again.")
//        }
        let whatsappURL = URL(string: "https://api.whatsapp.com/send?phone=966550364268&text=")
        if UIApplication.shared.canOpenURL(whatsappURL!) {
            UIApplication.shared.open(whatsappURL! as URL, options: [:], completionHandler: nil)
        }
    }
    
    // MARK: - Collection view delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionCell", for: indexPath) as! OptionsCell
        cell.roundView.layer.cornerRadius = cell.roundView.bounds.size.height/2
        cell.roundView.layer.masksToBounds = true
        //cell.roundView.layer.borderWidth = 3.0
        // let borderColor = UIColor.init(red: 0.427, green: 0.639, blue: 0.749, alpha: 1.0)
        //cell.roundView.layer.borderColor = borderColor.cgColor
        cell.optionNameLbl?.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: optionArr[indexPath.row], value: "", table: nil))!
        cell.optImageVIew.image = UIImage(named: optionImg[indexPath.row])
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
