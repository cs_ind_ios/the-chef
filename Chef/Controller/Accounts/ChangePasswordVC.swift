//
//  ChangePasswordVC.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController, UITextFieldDelegate {

    
    @IBOutlet var textFields: [UITextField]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        textFields[0].delegate = self
        textFields[1].delegate = self
        textFields[2].delegate = self
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            textFields[0].textAlignment = .left
            textFields[0].semanticContentAttribute = .forceLeftToRight
            textFields[1].textAlignment = .left
            textFields[1].semanticContentAttribute = .forceLeftToRight
            textFields[2].textAlignment = .left
            textFields[2].semanticContentAttribute = .forceLeftToRight
        }else{
            textFields[0].textAlignment = .right
            textFields[0].semanticContentAttribute = .forceRightToLeft
            textFields[1].textAlignment = .right
            textFields[1].semanticContentAttribute = .forceRightToLeft
            textFields[2].textAlignment = .right
            textFields[2].semanticContentAttribute = .forceRightToLeft
        }
        
    }
    
    @IBAction func backBtn_Tapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submitBtn_Tapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if (self.validInputParams()==false){
            return
        }
        let userId = "\(String(describing: UserDefaults.standard.value(forKey: "UserId")!))"
        let dic = ["UserId":userId,"OldPassword":self.textFields[0].text!,"NewPassword":self.textFields[1].text!]
        let jsonDic = ["ChangePassword":dic]
        
        if(Connectivity.isConnectedToInternet()){
            //ANLoader.showLoading("Loading..", disableUI: true)
            AccountApiRouter.changePasswordService(dic: jsonDic) { (data) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if data?.staus == false{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    }
                }else{
                    self.navigationController?.popViewController(animated: false)
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showToastAlert(on: self, message:data!.message!)
                    }else{
                        Alert.showToastAlert(on: self, message:data!.messageAr!)
                    }
                    
//                    Alert.showAlertWithOkAction(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data!.message!, okAction: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Done", value: "", table: nil))!, style: .default, handler: { (UIAlertAction) in
//                        self.navigationController?.popViewController(animated: false)
//                    }))
                }
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.showToastAlert(on: self, message:(AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)

            //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
        }
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    func validInputParams() -> Bool {
        if textFields[0].text == nil || (textFields[0].text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Old Password", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "OldPassword", value: "", table: nil))!)
            return false
        }
        if textFields[1].text == nil || (textFields[1].text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "New Password", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NewPassword", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isPasswordValid(password: textFields[1].text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Invalid Password", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PasswordValidation", value: "", table: nil))!)
                return false
            }
        }
        if textFields[2].text == nil || (textFields[2].text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Re-Type Password", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "RetypePassword", value: "", table: nil))!)
            return false
        }else{
            if !(textFields[1].text == textFields[2].text){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Wrong Re-Type Password", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "RetypePasswordValid", value: "", table: nil))!)
                return false
            }
        }
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(string == "#"){
            return false
        }else{
            return true
        }
    }
    // Dismiss the keyboard when the user taps the "Return" key or its equivalent  while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.returnKeyType == .default)
        {
            textField.resignFirstResponder()
            return true
        }
        if (textField == textFields[0])
        {
            textFields[1].becomeFirstResponder()
        }
        if (textField==textFields[1])
        {
            textFields[2].becomeFirstResponder()
        }
        return true;
    }
}
