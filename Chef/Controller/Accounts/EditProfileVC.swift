//
//  EditProfileVC.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController, UITextFieldDelegate {

    @IBOutlet var textFields: [UITextField]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        let userDetails = UserDef.getUserProfileDic(key: "userDetails")
        self.textFields[0].text = "\((userDetails["Name"] as? String)!)"
        self.textFields[1].text = "\((userDetails["Email"] as? String)!)"
        self.textFields[2].text = "+\((userDetails["Mobile"] as? String)!)"
        
        self.textFields[0].delegate = self
        self.textFields[1].delegate = self
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            textFields[0].textAlignment = .left
            textFields[0].semanticContentAttribute = .forceLeftToRight
            textFields[1].textAlignment = .left
            textFields[1].semanticContentAttribute = .forceLeftToRight
        }else{
            textFields[0].textAlignment = .right
            textFields[0].semanticContentAttribute = .forceRightToLeft
            textFields[1].textAlignment = .right
            textFields[1].semanticContentAttribute = .forceRightToLeft
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    @IBAction func backBtn_Tapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submitBtn_Tapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validInputParams() == false){
            return
        }
        let userId = UserDef.getUserId()
        if(Connectivity.isConnectedToInternet()){
            UserModuleService.signUpService(name: self.textFields[0].text!, mobNum: self.textFields[2].text!, email: self.textFields[1].text!, paswd: "", lang: "En", otp: "", nickName: "", gender: "", userType: "1", userId: "\(userId)", deviceToken: "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))") { (data) in
                
                if data!.status == false{
                    
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    }                 
                }else{
                    UserDef.saveToUserDefault(value: data!.successDic!.userId!, key: "UserId")
                    let userDetailDic = ["Mobile":data!.successDic!.mobile!,"Email":data!.successDic!.email!,"Name":data!.successDic!.fullName!]
                    UserDef.storeUserProfileDic(dict: userDetailDic, withKey: "userDetails")
                    self.navigationController?.popViewController(animated: false)
                }
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
        }
    }
    // validating
    func validInputParams() -> Bool {
        if textFields[0].text == nil || (textFields[0].text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "YourName", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isNameValid(name: textFields[0].text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidCharacters", value: "", table: nil))!)
                return false
            }
        }
        if textFields[1].text == nil || (textFields[1].text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Email", value: "", table: nil))!)
            return false
        }else {
            if !Validations.isValidEmail(email: textFields[1].text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EmailValidation", value: "", table: nil))!)
                return false
            }
        }
        return true
    }
    // MARK: - TextField delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.textFields[0]  {
            if(string == " " && self.textFields[0].text?.count == 0){
                return false;
            }
            let nameStr:NSString = (self.textFields[0].text! as NSString)
            if(nameStr.length >= 1){
                let code:NSString = nameStr.substring(from: nameStr.length-1) as NSString
                if(code.isEqual(to: " ")){
                    if(string == " "){
                        return false
                    }
                }
            }
            let letters = "!~`@#$%^&*-+();:={}[],.<>?\\/\"\'_•¥£€|"
            let cs:CharacterSet = CharacterSet.init(charactersIn: letters)
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return string == filtered
        }
        return true
    }
}
