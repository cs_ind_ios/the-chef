//
//  ContactUsVC.swift
//  Chef
//
//  Created by Devbox on 27/09/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import MessageUI
class ContactUsVC: UIViewController,MFMailComposeViewControllerDelegate {

    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var emailBtn: UIButton!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var whatsAppBtn: UIButton!
    @IBOutlet weak var snapChatBtn: UIButton!
    
    @IBOutlet weak var mailNameLbl: UILabel!
    @IBOutlet weak var twitterNameLbl: UILabel!
    @IBOutlet weak var snapNameLbl: UILabel!
    @IBOutlet weak var whatsAppNameLbl: UILabel!
    @IBOutlet weak var vendorRegistrationLbl: UILabel!
    @IBOutlet weak var vendorRegistrationBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Contact Us", value: "", table: nil))!
        mailNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Mail", value: "", table: nil))!
        twitterNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Twitter", value: "", table: nil))!
        snapNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Snapchat", value: "", table: nil))!
        whatsAppNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "WhatsApp", value: "", table: nil))!
        vendorRegistrationLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Vendor Registration", value: "", table: nil))!

        emailBtn.addTarget(self, action: #selector(emailBtn_Tapped), for: .touchUpInside)
        twitterBtn.addTarget(self, action: #selector(twitterBtn_Tapped), for: .touchUpInside)
        snapChatBtn.addTarget(self, action: #selector(snapBtn_Tapped), for: .touchUpInside)
        whatsAppBtn.addTarget(self, action: #selector(whatsAppBtn_Tapped), for: .touchUpInside)
        vendorRegistrationBtn.addTarget(self, action: #selector(vendorRegistrationBtn_Tapped), for: .touchUpInside)

    }
    
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func emailBtn_Tapped(){
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["info@arab-space.com"])
            mail.setSubject("About The Chef App")
            mail.setMessageBody("", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    @IBAction func callBtn_Tapped(_ sender: Any) {
        if let url = URL(string: "tel://920009685"),
        UIApplication.shared.canOpenURL(url) {
           if #available(iOS 10, *) {
             UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
                 // add error message here
        }
    }
    
    @objc func twitterBtn_Tapped(){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Twitter", value: "", table: nil))!
        Obj.url = "https://www.twitter.com/thechefapp"
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    @objc func snapBtn_Tapped(){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Snapchat", value: "", table: nil))!
        Obj.url = "https://www.snapchat.com/add/thechefapp"
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    @objc func vendorRegistrationBtn_Tapped(){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Vendor Registration", value: "", table: nil))!
        Obj.url = "https://docs.google.com/forms/d/e/1FAIpQLSeKLQNwPqqlzBaq4_G-IQtoD6SqJmbBqercA8VpftkAT_wFGg/viewform?usp=sf_link"
        Obj.modalPresentationStyle = .overCurrentContext
        self.present(Obj, animated: true, completion: nil)
    }
    @objc func whatsAppBtn_Tapped(){
        let whatsappURL = URL(string: "https://api.whatsapp.com/send?phone=966550364268&text=")
        if UIApplication.shared.canOpenURL(whatsappURL!) {
            UIApplication.shared.open(whatsappURL! as URL, options: [:], completionHandler: nil)
        }
    }
    
}
