//
//  ManageAddressVC.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class ManageAddressVC: UIViewController {

    @IBOutlet var tableView:UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    var AddressList = [String]()
    var nameArr = [String]()
    var mainArr = [Any]()
    var landMarkArr = [String]()
    var houseNo = [String]()
    var selectedIndex:Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.tableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ManageAddressTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ManageAddressTVCell", value: "", table: nil))!)
        
        self.tableView.separatorStyle = .none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 104
    }
    override func viewWillAppear(_ animated: Bool) {
        if Connectivity.isConnectedToInternet() == true {
            getUserAddDetails()
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
        }
    }
    
    func getUserAddDetails(){
        ANLoader.showLoading("Loading...", disableUI: true)
        let userId = "\(String(describing: UserDefaults.standard.value(forKey: "UserId")!))"
        AccountApiRouter.getUserAdd(userId: userId) { (data) in
            if data?.status == false{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                   // Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    Alert.showToastAlert(on: self, message:data!.message!)

                }else{
                    //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    Alert.showToastAlert(on: self, message:data!.messageAr!)

                }
                
            }else{
                self.mainArr = (data?.successDic!)!
                self.AddressList.removeAll()
                self.nameArr.removeAll()
                self.houseNo.removeAll()
                self.landMarkArr.removeAll()
                for dic in (data?.successDic!)!{
                    self.AddressList.append(dic["Address"] as! String)
                    self.nameArr.append(dic["AddressName"] as! String)
                    self.houseNo.append(dic["HouseNo"] as! String)
                    self.landMarkArr.append(dic["LandMark"] as! String)
                }
                self.tableView.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
            }
        }
    }
    @IBAction func backBtn_tap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func addNewAddressBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var Obj = SearchAddressVC()
        Obj = mainStoryBoard.instantiateViewController(withIdentifier: "SearchAddressVC") as! SearchAddressVC
        Obj.isNewUser = true
        Obj.whereObj = 2
        self.navigationController?.pushViewController(Obj, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension ManageAddressVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AddressList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ManageAddressTVCell", value: "", table: nil))!) as! ManageAddressTVCell
        let userDetails = UserDef.getUserProfileDic(key: "userDetails")
        cell.NameLbl.text = nameArr[indexPath.row]
        cell.addressLbl.text = "\(houseNo[indexPath.row]), \(landMarkArr[indexPath.row]), \(AddressList[indexPath.row])"
        cell.mobileNumLbl.text = "+\((userDetails["Mobile"] as? String)!)"
        //self.tableViewHeight.constant = tableView.contentSize.height
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        var addressView = UIView(frame: CGRect(x: 10, y: 3, width: 150, height: 44))
        
        let addAddressName = UILabel(frame: CGRect(x: 5, y: 7, width: 140, height: 30))
        addAddressName.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add address", value: "", table: nil))!
        addAddressName.textAlignment = .left
        
        let addAddressBtn = UIButton(frame: CGRect(x: 5, y: 7, width: 140, height: 30))
        addAddressBtn.setImage(UIImage(named: "plus 1"), for: .normal)
        addAddressBtn.contentHorizontalAlignment = .right
        addAddressBtn.addTarget(self, action: #selector(addNewAddressBtn_Tapped(_:)), for: .touchUpInside)
        
        
        if AppDelegate.getDelegate().appLanguage != "English"{
            addressView = UIView(frame: CGRect(x: tableView.bounds.width-160, y: 3, width: 150, height: 44))
            addAddressName.textAlignment = .right
            addAddressBtn.contentHorizontalAlignment = .left
        }
        
        addressView.layer.cornerRadius = 8
        addressView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        addressView.layer.borderWidth = 1
        
        addressView.addSubview(addAddressName)
        addressView.addSubview(addAddressBtn)
        footerView.addSubview(addressView)
        footerView.backgroundColor = .white
        return footerView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let editAction = UITableViewRowAction(style: .destructive, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Edit", value: "", table: nil))!) { (rowAction, indexPath) in
            //TODO: edit the row at indexPath here
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var Obj = SearchAddressVC()
            Obj = mainStoryBoard.instantiateViewController(withIdentifier: "SearchAddressVC") as! SearchAddressVC
            Obj.isNewUser = false
            Obj.addDetails = self.mainArr[indexPath.row] as! [String : Any]
            self.navigationController?.pushViewController(Obj, animated: true)
        }
        editAction.backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
        
        let deleteAction = UITableViewRowAction(style: .normal, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delete", value: "", table: nil))!) { (rowAction, indexPath) in
            //TODO: Delete the row at indexPath here
            let dic1 = self.mainArr[indexPath.row] as! [String:Any]
            let userId = "\(String(describing: UserDefaults.standard.value(forKey: "UserId")!))"
            let addId = "\(dic1["AddressId"]!)"
            let dic = ["UserId":userId, "AddressId":addId,"IsActive":"false"]
            let alertController = UIAlertController(title: Title, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "are you sure, want to delete?", value: "", table: nil))!, preferredStyle: .alert)
            let yesAction = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "YES", value: "", table: nil))!, style: UIAlertAction.Style.default) {
                UIAlertAction in
                ANLoader.showLoading("Loading...", disableUI: true)
                if(Connectivity.isConnectedToInternet()){
                    ANLoader.hide()
                    AccountApiRouter.saveAddress(dic: dic) { (data) in
                        if data!.status == false{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                Alert.showToastAlert(on: self, message:data!.message!)
                            }else{
                                Alert.showToastAlert(on: self, message:data!.messageAr!)
                            }
                        }else{
                            self.getUserAddDetails()
                        }
                    }
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
                    ANLoader.hide()
                }
            }
            let noAction = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NO", value: "", table: nil))!, style: UIAlertAction.Style.default) {
                UIAlertAction in
                return
            }
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            self.present(alertController, animated: false, completion: nil)
        }
        editAction.backgroundColor = .red
        return [editAction,deleteAction]
    }
}
