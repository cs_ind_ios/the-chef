//
//  SearchAddressVC.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class SearchAddressVC: UIViewController, CLLocationManagerDelegate , GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate {
    
    var latitute = String()
    var longitute = String()
    var addDetails = [String:Any]()
    var isNewUser = Bool()
    var isSearch = Bool()
    
    var addressId : Int?
    
    @IBOutlet weak var hideAndShowView: UIView!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var MarkerImg: UIImageView!
    // OUTLETS
    @IBOutlet weak var googleMapsView: GMSMapView!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var saveAs: UILabel!
    //address type buttons
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var workBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    @IBOutlet weak var saveAsTextLbl: UILabel!
    @IBOutlet weak var locationDispTF: ACFloatingTextfield!
    @IBOutlet weak var houseNoTF: ACFloatingTextfield!
    @IBOutlet weak var landmarkTF: ACFloatingTextfield!
    @IBOutlet weak var addresstypeTf: ACFloatingTextfield!
    @IBOutlet weak var locViewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var locViewBottomConst: NSLayoutConstraint!
    
    @IBOutlet weak var setDeliveryLocationNameLbl: UILabel!
    @IBOutlet weak var saveAsLbl: UILabel!
    
    
    // VARIABLES
    var locationManager = CLLocationManager()
    var addTypeStr = String()
    var whereObj:Int = 0
    var Latitude:Double = 24.6815519
    var Longitude:Double = 46.6881631

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        setDeliveryLocationNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Set delivery location", value: "", table: nil))!
        saveAs.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "save as", value: "", table: nil))!
        
        //btnActions
        self.locViewHeightConst.constant = 195
        self.landmarkTF.isHidden = true
        self.confirmBtn.addTarget(self, action: #selector(addLocationBtn_tap), for: .touchUpInside)
        
        //initGoogleMaps()

        if isNewUser == true{
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
            
            //initGoogleMaps()

            addressId = 0
            self.locViewHeightConst.constant = 195
            self.hideAndShowView.isHidden = true
            self.houseNoTF.isHidden = true
            self.landmarkTF.isHidden = true
            self.addresstypeTf.isHidden = true
            self.saveAs.isHidden = true
            
            self.btnBorderAndCornerRadius(btn: self.workBtn, selected: false)
            self.btnBorderAndCornerRadius(btn: self.homeBtn, selected: true)
            self.btnBorderAndCornerRadius(btn: self.otherBtn, selected: false)
            if isSearch == true {
                self.openSearchAddressBtn_tapped(0)
            }
        }else{
            Latitude = (addDetails["Latitude"])! as! Double
            Longitude = (addDetails["Longitude"])! as! Double
            initGoogleMaps()
            
            
            
           // initGoogleMaps()
//            let camera = GMSCameraPosition.camera(withLatitude: (addDetails["Latitude"])! as! CLLocationDegrees, longitude: (addDetails["Longitude"])! as! CLLocationDegrees, zoom: 18.0)
//            self.googleMapsView.animate(to: camera)
//            self.googleMapsView.bringSubviewToFront(self.MarkerImg)

            self.locViewHeightConst.constant = 195+305
            self.hideAndShowView.isHidden = false
            self.hideAndShowView.isHidden = false
            self.houseNoTF.isHidden = false
            self.landmarkTF.isHidden = false
            self.addresstypeTf.isHidden = false
            self.saveAs.isHidden = false
            self.skipBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            self.confirmBtn.setTitle("\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Save Address", value: "", table: nil))!)", for: .normal)
            self.houseNoTF.text = "\(String(describing: addDetails["HouseNo"]!))"
            self.landmarkTF.text = "\(String(describing: addDetails["LandMark"]!))"
            self.addresstypeTf.text = "\(String(describing: addDetails["AddressName"]!))"
            addTypeStr = "My Home"
            addressId = addDetails["AddressId"] as? Int
            
            self.btnBorderAndCornerRadius(btn: self.workBtn, selected: false)
            self.btnBorderAndCornerRadius(btn: self.homeBtn, selected: true)
            self.btnBorderAndCornerRadius(btn: self.otherBtn, selected: false)
        }
    }
    func initGoogleMaps() {
        let camera = GMSCameraPosition.camera(withLatitude:Latitude, longitude: Longitude, zoom: 18.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true
        //mapView.settings.myLocationButton = true
        self.googleMapsView.camera = camera
        self.googleMapsView.delegate = self
        self.googleMapsView.isMyLocationEnabled = true
        self.googleMapsView.settings.myLocationButton = true
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Latitude, longitude: Longitude)
        marker.title = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Office", value: "", table: nil))!
        marker.snippet = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!
        marker.map = mapView
        self.googleMapsView.bringSubviewToFront(self.MarkerImg)
    }
    
    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {
        //ANLoader.showLoading("Loading..", disableUI: true)
        let latitude: Double? = mapView.camera.target.latitude
        let longitude: Double? = mapView.camera.target.longitude
        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2DMake(latitude!, longitude!), completionHandler: { response, error in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            
            for addressObj: GMSAddress? in response?.results() ?? [GMSAddress?]() {
                if let anObj = addressObj {
                    self.latitute = "\(anObj.coordinate.latitude)"
                    self.longitute = "\(anObj.coordinate.longitude)"
                }
                let addStr:[String] = (addressObj?.lines)!
                var str=NSString()
                for index in addStr {
                    str=str.appending(index) as NSString
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                self.locationDispTF?.text = str as String;
                return
            }
        })
    }
    // MARK: CLLocation Manager Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        //let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 16.0)
        //self.googleMapsView.animate(to: camera)
        self.locationManager.stopUpdatingLocation()
        //self.googleMapsView.bringSubviewToFront(self.MarkerImg)
        Latitude = (location?.coordinate.latitude)!
        Longitude = (location?.coordinate.longitude)!
        initGoogleMaps()
    }
    // MARK: GMSMapview Delegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.googleMapsView.isMyLocationEnabled = true
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.googleMapsView.isMyLocationEnabled = true
        if (gesture) {
            mapView.selectedMarker = nil
        }
    }
    // MARK: GOOGLE AUTO COMPLETE DELEGATE
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print(place)
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15.0)
        self.googleMapsView.camera = camera
        self.dismiss(animated: true, completion: nil) // dismiss after select place
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("ERROR AUTO COMPLETE \(error)")
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil) // when cancel search
    }
    @IBAction func openSearchAddressBtn_tapped(_ sender: Any) {
        let vc = LocationAutocompleteVC(nibName: "LocationAutocompleteVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.locationAutocompleteVCDelegate = self
        self.present(vc, animated: true, completion: nil)
//        let autoCompleteController = GMSAutocompleteViewController()
//        autoCompleteController.delegate = self
//        self.locationManager.startUpdatingLocation()
//        self.present(autoCompleteController, animated: true, completion: nil)
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func addLocationBtn_tap(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        self.view.endEditing(true)
        if self.hideAndShowView.isHidden == false {
            ANLoader.hide()
            if (self.validInputParams()==false){
                return
            }
            if(isUserLogIn() == true){
                let userId = "\(String(describing: UserDefaults.standard.value(forKey: "UserId")!))"
                let addId = "\(String(describing: addressId!))"
                var dic = [String:Any]()
                if AppDelegate.getDelegate().homeScreen == true{
                    dic = ["UserId":userId, "AddressId":addId, "HouseNo":self.houseNoTF.text!, "HouseName":self.addresstypeTf.text!, "LandMark":self.landmarkTF.text!, "AddressType":addTypeStr, "Address":self.locationDispTF.text!, "Latitude": self.latitute, "Longitude": self.longitute, "IsActive":"true"]
                }else{
                    let myLocation = CLLocation(latitude: Double(latitute)!, longitude: Double(longitute)!)
                    if whereObj == 1{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            ANLoader.hide()
                        }
                        let myBuddysLocation = CLLocation(latitude: SaveAddressClass.getBranchDetailsArray[0].Latitude!, longitude: SaveAddressClass.getBranchDetailsArray[0].Longitude!)
                        let distance = myLocation.distance(from: myBuddysLocation) / 1000
                        if distance > SaveAddressClass.getBranchDetailsArray[0].DeliveryDistance!{
                            Alert.showAlertWithOkAction(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "The Disatnce too far from Restaurent Please Seelect Near Restaurent", value: "", table: nil))!, okAction: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: .default, handler: { (UIAlertAction) in
                                self.navigationController?.popViewController(animated: true)
                            }))
                }else{
                            dic = ["UserId":userId, "AddressId":addId, "HouseNo":self.houseNoTF.text!, "HouseName":self.addresstypeTf.text!, "LandMark":self.landmarkTF.text!, "AddressType":addTypeStr, "Address":self.locationDispTF.text!, "Latitude": self.latitute, "Longitude": self.longitute, "IsActive":"true"]
                        }
                    }else{
                        dic = ["UserId":userId, "AddressId":addId, "HouseNo":self.houseNoTF.text!, "HouseName":self.addresstypeTf.text!, "LandMark":self.landmarkTF.text!, "AddressType":addTypeStr, "Address":self.locationDispTF.text!, "Latitude": self.latitute, "Longitude": self.longitute, "IsActive":"true"]
                    }
                }
                ANLoader.showLoading("Loading...", disableUI: true)
                if(Connectivity.isConnectedToInternet()){
                    AccountApiRouter.saveAddress(dic: dic) { (data) in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            ANLoader.hide()
                        }
                        if data!.status == false{
                            if AppDelegate.getDelegate().appLanguage == "English"{
                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                            }else{
                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                            }
                        }else{
                            NotificationCenter.default.post(name: .didReceiveData, object: nil)
                            self.navigationController?.popViewController(animated: false)
                            self.locViewHeightConst.constant = 195
                        }
                    }
                }else{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                    }
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
                }
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                let loginVCObj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                loginVCObj.loginFrom = 1
                self.navigationController?.pushViewController(loginVCObj, animated: true)
            }
        }else{
            self.locViewHeightConst.constant = 195+305
            self.houseNoTF.isHidden = false
            self.landmarkTF.isHidden = false
            self.addresstypeTf.isHidden = false
            self.saveAs.isHidden = false
            self.hideAndShowView.isHidden = false
            self.confirmBtn.setTitle("\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Save Address", value: "", table: nil))!)", for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
            }
            UIView.animate(withDuration: 0.75) {
                self.skipBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func skipBtn_tap(_ sender: Any) {
        self.hideAndShowView.isHidden = true
        self.locViewHeightConst.constant = 195
        self.confirmBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Confirm Location", value: "", table: nil))!, for: .normal)
        self.houseNoTF.isHidden = true
        self.landmarkTF.isHidden = true
        self.addresstypeTf.isHidden = true
        self.saveAs.isHidden = true
        UIView.animate(withDuration: 0.6) {
            self.skipBtn.transform = CGAffineTransform(rotationAngle: CGFloat(0))
            self.view.layoutIfNeeded()
        }
    }
    
    // home ,work , other Btn Action
    @IBAction func addressTypeBtn_tap(_ sender: UIButton) {
        if sender == self.homeBtn{
            self.btnBorderAndCornerRadius(btn: self.workBtn, selected: false)
            self.btnBorderAndCornerRadius(btn: self.homeBtn, selected: true)
            self.btnBorderAndCornerRadius(btn: self.otherBtn, selected: false)
            self.workBtn.layer.borderWidth = 0
            self.otherBtn.layer.borderWidth = 0
            
            self.addresstypeTf.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My home", value: "", table: nil))!
            self.addTypeStr = "My home"
        }else if sender == self.workBtn{
            self.btnBorderAndCornerRadius(btn: self.workBtn, selected: true)
            self.btnBorderAndCornerRadius(btn: self.homeBtn, selected: false)
            self.btnBorderAndCornerRadius(btn: self.otherBtn, selected: false)
            self.homeBtn.layer.borderWidth = 0
            self.otherBtn.layer.borderWidth = 0
            
            self.addresstypeTf.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My Office", value: "", table: nil))!
            self.addTypeStr = "My Office"
        }else{
            self.btnBorderAndCornerRadius(btn: self.workBtn, selected: false)
            self.btnBorderAndCornerRadius(btn: self.homeBtn, selected: false)
            self.btnBorderAndCornerRadius(btn: self.otherBtn, selected: true)
            self.workBtn.layer.borderWidth = 0
            self.homeBtn.layer.borderWidth = 0
            
            self.addresstypeTf.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "My friend home", value: "", table: nil))!
            self.addTypeStr = "My friend home"
        }
    }
    func btnBorderAndCornerRadius(btn:UIButton, selected:Bool) {
        btn.layer.cornerRadius = btn.frame.size.width/2
        btn.layer.masksToBounds = true
        if(selected == true){
            btn.layer.borderWidth = 4.0
        }else{
            btn.layer.borderWidth = 0
        }
        let borderColor = UIColor.init(red: 0.087, green: 0.705, blue: 1.0, alpha: 1.0)
        btn.layer.borderColor = borderColor.cgColor
    }
    // text fields validation
    func validInputParams() -> Bool {
        if self.houseNoTF.text == nil || (self.houseNoTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "House Name/ Flat No.", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "HouseNo", value: "", table: nil))!)
            return false
        }
        if self.landmarkTF.text == nil || (self.landmarkTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title LandMark", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Landmark", value: "", table: nil))!)
            return false
        }
        if self.addresstypeTf.text == nil || (self.addresstypeTf.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Address Name", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "AddressName", value: "", table: nil))!)
            return false
        }
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.houseNoTF || textField == self.landmarkTF || textField == self.addresstypeTf {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 50
        }
        return true
    }
    
    // Dismiss the keyboard when the user taps the "Return" key or its equivalent  while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension SearchAddressVC:LocationAutocompleteVCDelegate{
    func didTapAction(latitude: Double, longitude: Double, MainAddress: String, TotalAddress: String) {
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 15.0)
        self.googleMapsView.camera = camera
    }
}
