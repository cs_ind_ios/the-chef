//
//  DropDownVC.swift
//  Chef
//
//  Created by Creative Solutions on 5/7/20.
//  Copyright © 2020 Creative Solutions. All rights reserved.
//

import UIKit
protocol DropDownVCDelegate {
    func didTapAction(id:String,name:String,whereType:Int)
}
class DropDownVC: UIViewController {
    var dropDownVCDelegate:DropDownVCDelegate!

    @IBOutlet var DropDownTV: UITableView!
    var dropDownArray = [DropDownModel]()
    var whereObj:Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        DropDownTV.delegate = self
        DropDownTV.dataSource = self    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DropDownVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropDownArray.count

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = DropDownTVCell()
        DropDownTV.register(UINib(nibName: "DropDownTVCell", bundle: nil), forCellReuseIdentifier: "DropDownTVCell")
        cell = DropDownTV.dequeueReusableCell(withIdentifier: "DropDownTVCell", for: indexPath) as! DropDownTVCell
        cell.ListLbl.text = "\(dropDownArray[indexPath.row].Name) (\(dropDownArray[indexPath.row].ID))"
        return cell

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dropDownVCDelegate.didTapAction(id:dropDownArray[indexPath.row].ID,name: dropDownArray[indexPath.row].Name, whereType: whereObj)
        dismiss(animated: false, completion: nil)


    }
}
