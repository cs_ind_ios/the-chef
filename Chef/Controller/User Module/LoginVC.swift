//
//  LoginVC.swift
//  Chef
//
//  Created by RAVI on 16/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import Crashlytics

class LoginVC: UIViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate {

    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    var loginFrom = Int()
    var app = AppDelegate()
    var window: UIWindow?
    @IBOutlet var countryCodeTf: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        app = UIApplication.shared.delegate as! AppDelegate
        countryCodeTf.text = "+966"

        self.textFields[0].delegate = self
        self.textFields[1].delegate = self
        
        textFields[0].textAlignment = .left
        textFields[0].semanticContentAttribute = .forceLeftToRight
        textFields[0].addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)

        if AppDelegate.getDelegate().appLanguage == "English"{
            textFields[1].textAlignment = .left
            textFields[1].semanticContentAttribute = .forceLeftToRight
        }else{
            textFields[1].textAlignment = .right
            textFields[1].semanticContentAttribute = .forceRightToLeft
        }
        
        //self.makePrefix()
    }
    override func viewWillAppear(_ animated: Bool) {
        if(isUserLogIn() == true){
            self.dismiss(animated:false, completion: nil)
        }
    }
    
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func countryCodeBtn_Tapped(_ sender: UIButton) {
           self.view.endEditing(true)

           var dropDownArray = [DropDownModel]()
               dropDownArray.append(DropDownModel(ID: "+966", Name: "Saudi Arabia"))
               dropDownArray.append(DropDownModel(ID: "+973", Name: "Bahrain"))

            let obj : DropDownVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropDownVC") as! DropDownVC)
            let height = dropDownArray.count * 50
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.modalPresentationStyle = .popover
           obj.preferredContentSize = CGSize(width: 180, height: height)
            
            let pVC = obj.popoverPresentationController
            pVC?.permittedArrowDirections = .up
            pVC?.sourceView = sender
            pVC?.delegate = self
           pVC?.sourceRect = CGRect(x: sender.frame.width - 15 , y: 0, width: 0, height: sender.frame.size.height)
            obj.whereObj = 1
            obj.dropDownArray = dropDownArray
            obj.dropDownVCDelegate = self
            self.present(obj, animated: false, completion: nil)
       }
       func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
           return UIModalPresentationStyle.none
       }
       func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
           return true
       }
    @IBAction func forgotPasswordBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let forgetPSWDObj = mainStoryBoard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        forgetPSWDObj.forgotFrom = loginFrom
        self.navigationController?.pushViewController(forgetPSWDObj, animated: true)
    }
    
    @IBAction func signupNowBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let signInVCObj = mainStoryBoard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        signInVCObj.registerFrom = loginFrom
        self.navigationController?.pushViewController(signInVCObj, animated: true)
    }
    @IBAction func loginBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if (self.validInputParams()==false){
            return
        }else{
            let mobNum = "\(countryCodeTf.text!.removePlusValue())\(self.textFields[0].text!.removeStringValue())"

            if(Connectivity.isConnectedToInternet()){
                ANLoader.showLoading("Loading..", disableUI: true)
                UserModuleService.loginService(mobNum: mobNum, paswd: self.textFields[1].text!, language:"En", success: { (data) in
                    if(data?.status == false){
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            ANLoader.hide()
                        }
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                        }else{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                        }
                    }else{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            ANLoader.hide()
                        }
                        UserDef.saveToUserDefault(value: data!.successDic!.userId!, key: "UserId")
                        let userDetailDic:[String:Any] = ["Mobile":data!.successDic!.mobile!,"Email":data!.successDic!.email!,"Name":data!.successDic!.fullName!]
                        UserDef.storeUserProfileDic(dict: userDetailDic, withKey: "userDetails")
                        self.navigationController?.popViewController(animated: true)
                    }
                })
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
            }
        }
    }
    func validInputParams() -> Bool {
        if textFields[0].text == nil || (textFields[0].text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PhoneNo", value: "", table: nil))!)
            return false
        }else {
            if !Validations.isMobileNoValid(mobile:textFields[0].text!.removeStringValue(), country : countryCodeTf.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                return false
            }
            
        }
        if textFields[1].text == nil || (textFields[1].text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Password", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isPasswordValid(password: textFields[1].text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Invalid Password", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PasswordValidation", value: "", table: nil))!)
                return false
            }
        }
        return true
    }
    func makePrefix() {
        let attributedString = NSMutableAttributedString(string: "+966 ")
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0,5))
        textFields[0].attributedText = attributedString
    }
   
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if textField == self.textFields[0] {
//                let  char = string.cString(using: String.Encoding.utf8)!
//                let isBackSpace = strcmp(char, "\\b")
//                if (isBackSpace == -92) {
//                    if range.location == 0 {
//                        return false
//                    }
//                }
                
    //            if(self.textFields[0].text?.count == 4){
    //                let num = "123456789"
    //                let checNum = CharacterSet.init(charactersIn: num)
    //                if (string.rangeOfCharacter(from: checNum) != nil){
    //                    return true
    //                }else{
    //                    return false
    //                }
    //            }
                
                if(self.textFields[0].text?.count == 0){
                    let num = "123456789"
                    let checNum = CharacterSet.init(charactersIn: num)
                    if (string.rangeOfCharacter(from: checNum) != nil){
                        return true
                    }else{
                        return false
                    }
                }

                let currentText = textField.text ?? ""
                guard let stringRange = Range(range, in: currentText) else { return false }
                let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
                if countryCodeTf.text! == "+973"{
                    return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                    updatedText.safelyLimitedTo(length: 8)
                }else{
                    return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                        updatedText.safelyLimitedTo(length: 9)
                }
            }
            return true
        }
    
    @objc func textFieldDidChange(textField: UITextField){
        if textField == self.textFields[0] {
            if countryCodeTf.text! == "+973"{
                if self.textFields[0].text!.count == 8 {
                    self.textFields[1].becomeFirstResponder()
                }
            }else{
                if self.textFields[0].text!.count == 9 {
                    self.textFields[1].becomeFirstResponder()
                }
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.returnKeyType == .done {
            self.loginBtn_Tapped(0)
        }
        return true;
    }
}
extension String{
    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "")
    }
    func removeStringValue() -> String {
        return self.replace(string: "+966 ", replacement: "")
    }
    func removePlusValue() -> String {
        return self.replace(string: "+", replacement: "")
    }
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
}
extension LoginVC: DropDownVCDelegate{
    func didTapAction(id: String, name: String, whereType: Int) {
         countryCodeTf.text = id
        self.textFields[0].text = ""
    }
    
    
}
