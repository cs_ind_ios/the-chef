//
//  ForgotPasswordVC.swift
//  Chef
//
//  Created by RAVI on 16/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController, UITextFieldDelegate,UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var mobNumTF: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var mobileNoLbl: UILabel!
    @IBOutlet weak var firstDigitTf: UITextField!
    @IBOutlet weak var secondDigitTf: UITextField!
    @IBOutlet weak var thirdDigitTf: UITextField!
    @IBOutlet weak var fourthDigitTf: UITextField!
    @IBOutlet weak var resendOTPBtn: UIButton!
    @IBOutlet weak var otpSubView: UIView!
    @IBOutlet var newPasswordView: UIView!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var confNewPasswordTF: UITextField!
    @IBOutlet var countryCodeTf: UITextField!

    var forgotFrom = Int()
    var otpString = String()
    var timer:Timer?
    var countDown = 120
    var backSpaceTF = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true

        self.newPasswordView.frame = CGRect(x: 10, y: self.view.frame.size.height+10, width: self.view.frame.size.width-20, height: 0)
        self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        countryCodeTf.text = "+966"

        firstDigitTf?.delegate = self
        secondDigitTf?.delegate = self
        thirdDigitTf?.delegate = self
        fourthDigitTf?.delegate = self
        
        mobNumTF?.delegate = self
        newPasswordTF?.delegate = self
        confNewPasswordTF?.delegate = self
        mobNumTF.textAlignment = .left
        mobNumTF.semanticContentAttribute = .forceLeftToRight
        mobNumTF.becomeFirstResponder()
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            newPasswordTF.textAlignment = .left
            newPasswordTF.semanticContentAttribute = .forceLeftToRight
            confNewPasswordTF.textAlignment = .left
            confNewPasswordTF.semanticContentAttribute = .forceLeftToRight
        }else{
            newPasswordTF.textAlignment = .right
            newPasswordTF.semanticContentAttribute = .forceRightToLeft
            confNewPasswordTF.textAlignment = .right
            confNewPasswordTF.semanticContentAttribute = .forceRightToLeft
        }
        
        // changing  OTP text fields auto matically one by one
        firstDigitTf.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        secondDigitTf.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        thirdDigitTf.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fourthDigitTf.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        mobNumTF.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        //self.makePrefix()
        
    }
    @IBAction func backBtn_Tapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sendBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if(validInputParams() == false){
            return
        }
        if(Connectivity.isConnectedToInternet()){
            ANLoader.showLoading("Loading..", disableUI: true)
            let mobNum = "966\(self.mobNumTF.text!.removeStringValue())"
            self.mobileNoLbl?.text = "+966 \(self.mobNumTF.text!.removeStringValue())"
  
            UserModuleService.VerifyMobNumServiceForFogrotPwd(mobNum: mobNum) { (data) in
                self.countDown = 120
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCountDown), userInfo: nil, repeats: true)
                
                if data?.status == false {
                    DispatchQueue.main.async {
                        ANLoader.hide()
                    }
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    }
                }else{
                    DispatchQueue.main.async {
                        ANLoader.hide()
                    }
                    self.otpString = "\(String(describing: data!.successDic!.otp!))"
                    UIView.animate(withDuration: 0.6, animations: { () -> Void in
                        self.otpView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                        self.otpSubView.isHidden = false
                        self.view.addSubview(self.otpView)
                        //self.firstDigitTf.becomeFirstResponder()
                        
                    })
                }
            }
        }else{
            DispatchQueue.main.async {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
        }
    }
    @IBAction func countryCodeBtn_Tapped(_ sender: UIButton) {
           self.view.endEditing(true)

           var dropDownArray = [DropDownModel]()
               dropDownArray.append(DropDownModel(ID: "+966", Name: "Saudi Arabia"))
               dropDownArray.append(DropDownModel(ID: "+973", Name: "Bahrain"))

            let obj : DropDownVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropDownVC") as! DropDownVC)
            let height = dropDownArray.count * 50
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.modalPresentationStyle = .popover
           obj.preferredContentSize = CGSize(width: 180, height: height)
            
            let pVC = obj.popoverPresentationController
            pVC?.permittedArrowDirections = .up
            pVC?.sourceView = sender
            pVC?.delegate = self
           pVC?.sourceRect = CGRect(x: sender.frame.width - 15 , y: 0, width: 0, height: sender.frame.size.height)
            obj.whereObj = 1
            obj.dropDownArray = dropDownArray
            obj.dropDownVCDelegate = self
            self.present(obj, animated: false, completion: nil)
       }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    @IBAction func verifyOTPBtn_Tapped(_ sender:Any) {
        self.view.endEditing(true)
        let otpFromTF = "\(firstDigitTf.text!)\(secondDigitTf.text!)\(thirdDigitTf.text!)\(fourthDigitTf.text!)"
        if(firstDigitTf.hasText == false || secondDigitTf.hasText == false || thirdDigitTf.hasText == false || fourthDigitTf.hasText == false){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ActivationCode", value: "", table: nil))!)
            return
        }
        if(otpString != otpFromTF){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "IvalidActiveCode", value: "", table: nil))!)
            return
        }else{
            UIView.animate(withDuration: 0.7, animations: { () -> Void in
                self.newPasswordView.frame = CGRect(x:0, y: self.otpSubView.frame.origin.y+70, width: self.view.frame.size.width, height: 320)
                self.newPasswordView.layer.cornerRadius = 8.0
                self.newPasswordView.layer.masksToBounds = true
                self.otpSubView.isHidden = true
                self.view.addSubview(self.newPasswordView)
                self.firstDigitTf.text = ""; self.secondDigitTf.text = "";
                self.thirdDigitTf.text = "";  self.fourthDigitTf.text = "";
            })
        }
    }
    @IBAction func resendOTPBtn_Tapped(_ sender: UIButton) {
        firstDigitTf.text = ""; secondDigitTf.text = ""; thirdDigitTf.text = ""; fourthDigitTf.text = "";
        let mobNum = "\(countryCodeTf.text!.removePlusValue())\(self.mobNumTF.text!.removeStringValue())"
        if(Connectivity.isConnectedToInternet()){
            ANLoader.showLoading("Loading..", disableUI: true)
            UserModuleService.VerifyMobNumServiceForFogrotPwd(mobNum: mobNum) { (data) in
                if data?.status == false {
                    DispatchQueue.main.async {
                        ANLoader.hide()
                    }
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    }
                }else{
                    self.otpString = "\(String(describing: data!.successDic!.otp!))"
                    self.countDown = 120
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCountDown), userInfo: nil, repeats: true)
                    DispatchQueue.main.async {
                        ANLoader.hide()
                    }
                }
            }
        }else{
            DispatchQueue.main.async {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
        }
    }
    @IBAction func newPasswordSubmitBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        if(validPasswordInputParams() == false){
            return
        }
        let mobNum = "\(countryCodeTf.text!.removePlusValue())\(self.mobNumTF.text!.removeStringValue())"
        if(Connectivity.isConnectedToInternet()){
            ANLoader.showLoading("Loading..", disableUI: true)
            UserModuleService.setNewPassword(mobNum:mobNum , newPswd: self.confNewPasswordTF.text!) { (data) in
                DispatchQueue.main.async {
                    ANLoader.hide()
                }
                if data?.status == false{
                    
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    }
                }else{
                    UIView.animate(withDuration: 1.0, animations: { () -> Void in
                        UserDef.saveToUserDefault(value: data!.successDic!.userId!, key: "UserId")
                        let userDetailDic = ["Mobile":data!.successDic!.mobile!,"Email":data!.successDic!.email!,"Name":data!.successDic!.fullName!]
                        UserDef.storeUserProfileDic(dict: userDetailDic, withKey: "userDetails")
//                        self.newPasswordView.frame = CGRect(x: 10, y: self.view.frame.size.height+10, width: self.view.frame.size.width-20, height: 0)
//                        self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
//                        self.otpSubView.isHidden = false
//                        self.newPasswordView.removeFromSuperview()
//                        self.otpView.removeFromSuperview()
                    })
//                    print(data!.successDic!.email!)
//                    self.mobNumTF.text = ""
//                    self.confNewPasswordTF.text = ""
//                    self.newPasswordTF.text = ""
//                    self.firstDigitTf.text = ""; self.secondDigitTf.text = ""; self.thirdDigitTf.text = ""; self.fourthDigitTf.text = "";
                    if self.forgotFrom == 1{
                        self.tabBarController?.selectedIndex = 2
                        self.navigationController?.popToRootViewController(animated: false)
                    }else if self.forgotFrom == 2{
                        self.tabBarController?.selectedIndex = 3
                        self.navigationController?.popToRootViewController(animated: false)
                    }else{
                        self.navigationController?.popViewController(animated: false)
                    }
//                    Alert.showAlertWithOkAction(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: data!.message!, okAction: UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: .default, handler: { (UIAlertAction) in
//
//                    }))
                }
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
        }
    }
    @IBAction func editMobileNumberBtn_Tapped(_ sender: UIButton) {
        self.newPasswordView.frame = CGRect(x: self.view.frame.size.width/2, y: self.view.frame.size.height+10, width: 0, height: 0)
        self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        self.otpSubView.isHidden = false
        self.newPasswordView.removeFromSuperview()
        self.otpView.removeFromSuperview()
        firstDigitTf.text = ""; secondDigitTf.text = ""; thirdDigitTf.text = ""; fourthDigitTf.text = "";
        self.mobNumTF.becomeFirstResponder()
    }
    @IBAction func newPasswordCancelBtn_Tapped(_ sender: UIButton) {
        self.newPasswordView.frame = CGRect(x: self.view.frame.size.width/2, y: self.view.frame.size.height+10, width: 0, height: 0)
        self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        self.otpSubView.isHidden = false
        self.newPasswordView.removeFromSuperview()
        self.otpView.removeFromSuperview()
        self.mobNumTF.text = ""
        firstDigitTf.text = ""; secondDigitTf.text = ""; thirdDigitTf.text = ""; fourthDigitTf.text = "";
         self.makePrefix()
        self.mobNumTF.becomeFirstResponder()
    }
    func validPasswordInputParams() -> Bool {
        if newPasswordTF.text == nil || (newPasswordTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "New Password", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NewPassword", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isPasswordValid(password: newPasswordTF.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Invalid Password", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PasswordValidation", value: "", table: nil))!)
                return false
            }
        }
        if confNewPasswordTF.text == nil || (confNewPasswordTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Re-Type Password", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "RetypePassword", value: "", table: nil))!)
            return false
        }else{
            if !(newPasswordTF.text == confNewPasswordTF.text){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Wrong Re-Type Password", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "RetypePasswordValid", value: "", table: nil))!)
                return false
            }
        }
        return true
    }
    @objc func updateCountDown() {
        if(countDown > 0) {
            let minute = String(countDown/60)
            let seconds = String(countDown % 60)
            self.resendOTPBtn.backgroundColor = UIColor.init(red: 0.349, green: 0.635, blue: 0.812, alpha: 0.5)
            self.resendOTPBtn.isEnabled = false
            var str = "Resend OTP 0\(minute):\(seconds)"
            if AppDelegate.getDelegate().appLanguage == "English"{
                if(Int(seconds)! < 10){
                    str = "Resend OTP 0\(minute):0\(seconds)"
                }else{
                    str = "Resend OTP 0\(minute):\(seconds)"
                }
            }else{
                if(Int(seconds)! < 10){
                    str = "0إعادة ارسال كلمة مرور صالحه لمره واحده\(minute):0\(seconds)"
                }else{
                    str = "إعادة ارسال كلمة مرور صالحه لمره واحده0\(minute):\(seconds)"
                }
            }
            self.resendOTPBtn.setTitle(str, for: .normal)
            countDown = countDown - 1
        } else {
            self.resendOTPBtn.isEnabled = true
            self.resendOTPBtn.backgroundColor = UIColor.init(red: 0.349, green: 0.635, blue: 0.812, alpha: 1.0)
            var str = "Resend OTP"
            if AppDelegate.getDelegate().appLanguage == "English"{
                str = "Resend OTP"
            }else{
                str = "إعادة ارسال كلمة مرور صالحه لمره واحده"
            }
            self.resendOTPBtn.setTitle(str, for: .normal)
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    func validInputParams() -> Bool {
        if mobNumTF.text == nil || (mobNumTF.text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PhoneNo", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isMobileNoValid(mobile:mobNumTF.text!.removeStringValue(), country : countryCodeTf.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                return false
            }
            
        }
        return true
    }
    func makePrefix() {
        let attributedString = NSMutableAttributedString(string: "+966 ")
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0,5))
        mobNumTF.attributedText = attributedString
    }
    @objc func textFieldDidChange(textField: UITextField){
         if textField == self.mobNumTF {
               if countryCodeTf.text! == "+973"{
                    if self.mobNumTF.text!.count == 8 {
                      self.mobNumTF.resignFirstResponder()
                      self.sendBtn_Tapped(0)
                    }
                }else{
                  if self.mobNumTF.text!.count == 9 {
                    self.mobNumTF.resignFirstResponder()
                    self.sendBtn_Tapped(0)
9                  }
              }
            return
         }
        
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case firstDigitTf:
                secondDigitTf.becomeFirstResponder()
            case secondDigitTf:
                thirdDigitTf.becomeFirstResponder()
            case thirdDigitTf:
                fourthDigitTf.becomeFirstResponder()
            case fourthDigitTf:
                fourthDigitTf.resignFirstResponder()
            default:
                break
            }
        }
        if textField == self.firstDigitTf || textField == self.secondDigitTf || textField == self.thirdDigitTf || textField == self.fourthDigitTf {
            let otpFromTF = "\(firstDigitTf.text!)\(secondDigitTf.text!)\(thirdDigitTf.text!)\(fourthDigitTf.text!)"
            
            if otpFromTF.count == 4 {
                self.verifyOTPBtn_Tapped(0)
            }

        }
        
        

        
    }
    @objc func otpTextFieldChangeBackSpace(){
        if(backSpaceTF == firstDigitTf){
            if(fourthDigitTf.hasText == true){
                fourthDigitTf.becomeFirstResponder()
            }else{
                if(thirdDigitTf.hasText == true){
                    thirdDigitTf.becomeFirstResponder()
                }else{
                    if(secondDigitTf.hasText == true){
                        secondDigitTf.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }else if(backSpaceTF == secondDigitTf){
            if(firstDigitTf.hasText == true){
                firstDigitTf.becomeFirstResponder()
            }else{
                if(fourthDigitTf.hasText == true){
                    fourthDigitTf.becomeFirstResponder()
                }else{
                    if(thirdDigitTf.hasText == true){
                        thirdDigitTf.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }else if (backSpaceTF == thirdDigitTf){
            if(secondDigitTf.hasText == true){
                secondDigitTf.becomeFirstResponder()
            }else{
                if(firstDigitTf.hasText == true){
                    firstDigitTf.becomeFirstResponder()
                }else{
                    if(thirdDigitTf.hasText == true){
                        thirdDigitTf.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }else if (backSpaceTF == fourthDigitTf){
            
            if(thirdDigitTf.hasText == true){
                thirdDigitTf.becomeFirstResponder()
            }else{
                if(secondDigitTf.hasText == true){
                    secondDigitTf.becomeFirstResponder()
                }else{
                    if(firstDigitTf.hasText == true){
                        firstDigitTf.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.mobNumTF {
//            let  char = string.cString(using: String.Encoding.utf8)!
//            let isBackSpace = strcmp(char, "\\b")
//            if (isBackSpace == -92) {
//                if range.location == 0 {
//                    return false                        }
//            }
            
            if(self.mobNumTF.text?.count == 0){
                let num = "123456789"
                let checNum = CharacterSet.init(charactersIn: num)
                if (string.rangeOfCharacter(from: checNum) != nil){
                    return true
                }else{
                    return false
                }
            }
            
            
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
           if countryCodeTf.text! == "+973"{
                return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                updatedText.safelyLimitedTo(length: 8)
            }else{
                return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                    updatedText.safelyLimitedTo(length: 9)
            }
        }
        if textField == self.firstDigitTf || textField == self.secondDigitTf || textField == self.thirdDigitTf || textField == self.fourthDigitTf {
            if(range.length == 1){
                backSpaceTF = textField
                self.perform(#selector(otpTextFieldChangeBackSpace), with: nil, afterDelay: 0.2)
                return true
            }
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 1
        }
        return true
    }
    // Dismiss the keyboard when the user taps the "Return" key or its equivalent  while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if  textField == self.confNewPasswordTF {
            if(validPasswordInputParams() == true){
                self.newPasswordSubmitBtn_Tapped(0)
            }
        }
        return true;
    }
}
extension ForgotPasswordVC: DropDownVCDelegate{
    func didTapAction(id: String, name: String, whereType: Int) {
         countryCodeTf.text = id
        self.mobNumTF.text = ""
    }
}
