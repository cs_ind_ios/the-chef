//
//  RegisterVC.swift
//  Chef
//
//  Created by RAVI on 16/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController , UITextFieldDelegate, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var otpTiimeCountLbl: UILabel!
    @IBOutlet weak var mobileNoLbl: UILabel!
    @IBOutlet weak var firstDigitTf: UITextField!
    @IBOutlet weak var secondDigitTf: UITextField!
    @IBOutlet weak var thirdDigitTf: UITextField!
    @IBOutlet weak var fourthDigitTf: UITextField!
    @IBOutlet weak var resendOTPBtn: UIButton!
    @IBOutlet weak var tearmsConditionsBtn: UIButton!
    @IBOutlet weak var tearmsConditionsLbl: UILabel!
    @IBOutlet weak var tearmsConditionsView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var countryCodeTf: UITextField!

    var timer:Timer?
    var countDown = 120
    var mobNum = String()
    var otpString = String()
    var backSpaceTF = UITextField()
    var registerFrom = Int()
    var isAgree:Bool = false

    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16.0),
        NSAttributedString.Key.foregroundColor : UIColor.white,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        //self.resendOtpBtn.addTarget(self, action: #selector(resendOtpBtn_tap), for: .touchUpInside)
//        let attributeString = NSMutableAttributedString(string: (loginBtn.titleLabel?.text)!,attributes: attrs)
//        loginBtn.setAttributedTitle(attributeString, for: .normal)
        countryCodeTf.text = "+966"

        textFields[0].delegate = self
        textFields[1].delegate = self
        firstDigitTf.delegate=self
        secondDigitTf.delegate=self
        thirdDigitTf.delegate=self
        fourthDigitTf.delegate=self
        textFields[1].textAlignment = .left
        textFields[1].semanticContentAttribute = .forceLeftToRight

        if AppDelegate.getDelegate().appLanguage == "English"{
            textFields[0].textAlignment = .left
            textFields[0].semanticContentAttribute = .forceLeftToRight
            textFields[2].textAlignment = .left
            textFields[2].semanticContentAttribute = .forceLeftToRight
            textFields[3].textAlignment = .left
            //textFields[3].semanticContentAttribute = .forceLeftToRight
            textFields[4].textAlignment = .left
            //textFields[4].semanticContentAttribute = .forceLeftToRight
            
            tearmsConditionsView.semanticContentAttribute = .forceLeftToRight
            tearmsConditionsLbl.textAlignment = .left
        }else{
            textFields[0].textAlignment = .right
            textFields[0].semanticContentAttribute = .forceRightToLeft
            textFields[2].textAlignment = .right
            textFields[2].semanticContentAttribute = .forceRightToLeft
            textFields[3].textAlignment = .right
            textFields[3].semanticContentAttribute = .forceRightToLeft
            textFields[4].textAlignment = .right
            textFields[4].semanticContentAttribute = .forceRightToLeft
            tearmsConditionsView.semanticContentAttribute = .forceRightToLeft
            tearmsConditionsLbl.textAlignment = .right

        }
        
        // changing  OTP text fields auto matically one by one
        firstDigitTf.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        secondDigitTf.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        thirdDigitTf.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fourthDigitTf.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        textFields[1].addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        //self.makePrefix()
        
        
        // Label Word colors
        tearmsConditionsLbl.isUserInteractionEnabled = true
        tearmsConditionsLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "By signing up you agree to our Terms & Conditions", value: "", table: nil))!//"By signing up you agree to our Terms and Conditions"
        let text = (tearmsConditionsLbl.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range = (text as NSString).range(of: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms & Conditions", value: "", table: nil))!)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.BlueColor, range: range)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        tearmsConditionsLbl.attributedText = underlineAttriString
        
        // Label Action
        let gesture = UITapGestureRecognizer(target: self, action: #selector(tapLabel(gesture:)))
        tearmsConditionsLbl.addGestureRecognizer(gesture)
    }
    @IBAction func countryCodeBtn_Tapped(_ sender: UIButton) {
           self.view.endEditing(true)

           var dropDownArray = [DropDownModel]()
               dropDownArray.append(DropDownModel(ID: "+966", Name: "Saudi Arabia"))
               dropDownArray.append(DropDownModel(ID: "+973", Name: "Bahrain"))

            let obj : DropDownVC! = (self.storyboard?.instantiateViewController(withIdentifier: "DropDownVC") as! DropDownVC)
            let height = dropDownArray.count * 50
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.modalPresentationStyle = .popover
           obj.preferredContentSize = CGSize(width: 180, height: height)
            
            let pVC = obj.popoverPresentationController
            pVC?.permittedArrowDirections = .up
            pVC?.sourceView = sender
            pVC?.delegate = self
           pVC?.sourceRect = CGRect(x: sender.frame.width - 15 , y: 0, width: 0, height: sender.frame.size.height)
            obj.whereObj = 1
            obj.dropDownArray = dropDownArray
            obj.dropDownVCDelegate = self
            self.present(obj, animated: false, completion: nil)
       }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        let text = (tearmsConditionsLbl.text)!
        let termsRange = (text as NSString).range(of: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms & Conditions", value: "", table: nil))!)
        
        let termsRange1 = (text as NSString).range(of: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms", value: "", table: nil))!)
        
         let termsRange2 = (text as NSString).range(of: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Conditions", value: "", table: nil))!)
        
        let termsRange3 = (text as NSString).range(of: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "our", value: "", table: nil))!)
        
        if gesture.didTapAttributedTextInLabel(label: self.tearmsConditionsLbl, inRange: termsRange) || gesture.didTapAttributedTextInLabel(label: self.tearmsConditionsLbl, inRange: termsRange1) || gesture.didTapAttributedTextInLabel(label: self.tearmsConditionsLbl, inRange: termsRange2) || gesture.didTapAttributedTextInLabel(label: self.tearmsConditionsLbl, inRange: termsRange3) {
            
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let Obj = mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            Obj.titleStr = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Terms and Conditions", value: "", table: nil))!
            Obj.url = "http://app.thechefapp.net/TermsAndConditions"
            self.present(Obj, animated: true, completion: nil)
            
        }
        else {
            print("Tapped none")
        }
    }
    
    @IBAction func termsAndConditionsBtn_tapped(_ sender: Any) {
        if isAgree == false {
            isAgree = true
            tearmsConditionsBtn.setBackgroundImage(UIImage(named: "checked-checkbox"), for: .normal)
        }
        
//        else{
//            isAgree = false
//            tearmsConditionsBtn.setBackgroundImage(UIImage(named: "unchecked-checkbox"), for: .normal)
//
//        }
    }
    
    @IBAction func loginNowBtn_Tapped(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    @objc func editButton() {
        self.textFields[1].becomeFirstResponder()
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func signupBtn_Tapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if (self.validInputParams()==false){
            return
        }
        var str = ""
        if AppDelegate.getDelegate().appLanguage == "English"{
            str = "We will be verifying the mobile number:\n\(countryCodeTf.text!) \(String(describing: textFields[1].text!.removeStringValue()))\n Is this OK, or would you like to edit the number?"
        }else{
            str = "سنقوم بالتحقق من رقم الجوال:\n\(countryCodeTf.text!) \(String(describing: textFields[1].text!.removeStringValue()))\n هل أنت متأكد؟ أو هل ترغب بتعديل الرقم؟"
        }
        let alert = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Mobile Number", value: "", table: nil))!, message: str, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Edit", value: "", table: nil))!, style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            self.editButton()
        }))
        alert.addAction(UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Done", value: "", table: nil))!, style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            self.mobileNoLbl.text = "\(self.countryCodeTf.text!) \(self.textFields[1].text!.removeStringValue())"
            if(Connectivity.isConnectedToInternet()){
                self.verifyMobile()
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func validInputParams() -> Bool {
        if textFields[0].text == nil || (textFields[0].text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "YourName", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isCharCount(name: textFields[0].text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidCharacters", value: "", table: nil))!)
                return false
            }
        }
        if textFields[1].text == nil || (textFields[1].text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PhoneNo", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isMobileNoValid(mobile:textFields[1].text!.removeStringValue(), country : countryCodeTf.text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ValidPhoneNo", value: "", table: nil))!)
                return false
            }
            
        }
        if textFields[2].text == nil || (textFields[2].text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Email", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isValidEmail(email:textFields[2].text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EmailValidation", value: "", table: nil))!)
                return false
            }
        }
        if textFields[3].text == nil || (textFields[3].text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Password", value: "", table: nil))!)
            return false
        }else{
            if !Validations.isPasswordValid(password: textFields[3].text!){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PasswordValidation", value: "", table: nil))!)
                return false
            }
        }
        if textFields[4].text == nil || (textFields[4].text == ""){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ConfirmPassword", value: "", table: nil))!)
            return false
        }else{
            if textFields[3].text != textFields[4].text {
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "VerifyPassword", value: "", table: nil))!)
                return false
            }
        }
        
        if(isAgree == false){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Please accept terms and conditions", value: "", table: nil))!)

            return false
        }
        return true
    }
    
    @IBAction func verifyOtpBtn_Tapped(_ sender: Any) {
        self.view.endEditing(true)
        let otpFromTF = "\(firstDigitTf.text!)\(secondDigitTf.text!)\(thirdDigitTf.text!)\(fourthDigitTf.text!)"
        if(firstDigitTf.hasText == false || secondDigitTf.hasText == false || thirdDigitTf.hasText == false || fourthDigitTf.hasText == false){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ActivationCode", value: "", table: nil))!)
            return
        }
        if(otpString != otpFromTF){
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "IvalidActiveCode", value: "", table: nil))!)
            return
        }else{
            UIView.animate(withDuration: 0.6, animations: { () -> Void in
                self.otpView.removeFromSuperview()
                self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
            })
            if(Connectivity.isConnectedToInternet()){
                ANLoader.showLoading("Loading..", disableUI: true)
                firstDigitTf.text = ""; secondDigitTf.text = ""; thirdDigitTf.text = ""; fourthDigitTf.text = "";
                UserModuleService.signUpService(name: textFields[0].text!, mobNum: mobNum, email: textFields[2].text!, paswd: textFields[3].text!, lang: "En", otp: otpString,nickName: "", gender: "", userType: "1",userId : "0",deviceToken: "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", success: { (data) in
                        DispatchQueue.main.async {
                            ANLoader.hide()
                        }
                        if(data!.status == false){
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                        }else{
                            UserDef.saveToUserDefault(value: data!.successDic!.userId!, key: "UserId")
                            let userDetailDic = ["Mobile":data!.successDic!.mobile!,"Email":data!.successDic!.email!,"Name":data!.successDic!.fullName!]
                            UserDef.storeUserProfileDic(dict: userDetailDic, withKey: "userDetails")
                            self.doneAction()
                            
                        }
                    })
                
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
            }
        }
    }
    func doneAction(){
        self.textFields[1].text = ""; self.textFields[0].text = ""; self.textFields[2].text = ""; self.textFields[3].text = ""; self.textFields[4].text = "";
        if self.registerFrom == 1{
            self.tabBarController?.selectedIndex = 2
            self.navigationController?.popToRootViewController(animated: false)
        }else if self.registerFrom == 2{
            self.tabBarController?.selectedIndex = 3
            self.navigationController?.popToRootViewController(animated: false)
        }else{
            self.navigationController?.popViewController(animated: false)
        }
    }
    @IBAction func resendOtpBtn_Tapped(_ sender: UIButton) {
        mobNum = "\(countryCodeTf.text!.removePlusValue())\(self.textFields[1].text!.removeStringValue())"
        if(Connectivity.isConnectedToInternet()){
            ANLoader.showLoading("Loading..", disableUI: true)
            UserModuleService.VerifyMobNumService(mobNum: mobNum, email: textFields[2].text!) { (data) in
                DispatchQueue.main.async {
                    ANLoader.hide()
                }
                if data?.status == false{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    }
                }else{
                    self.otpString = "\(String(describing: data!.successDic!.otp!))"
                    self.countDown = 120
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCountDown), userInfo: nil, repeats: true)
                }
            }
        }else{
            DispatchQueue.main.async {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
        }
    }
    @IBAction func editMobileBtn_Tapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.6, animations: { () -> Void in
            self.otpView.removeFromSuperview()
            self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        })
        firstDigitTf.text = ""; secondDigitTf.text = ""; thirdDigitTf.text = ""; fourthDigitTf.text = "";
        self.textFields[1].becomeFirstResponder()
    }
    @IBAction func cancelOtpViewBtn_Tapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.6, animations: { () -> Void in
            self.otpView.removeFromSuperview()
            self.otpView.frame = CGRect(x: 0, y: self.view.frame.size.height+10, width: self.view.frame.size.width, height: self.view.frame.size.height-100)
        })
        firstDigitTf.text = ""; secondDigitTf.text = ""; thirdDigitTf.text = ""; fourthDigitTf.text = "";
    }
    
    // verify mobile Number
    func verifyMobile() {
        mobNum = "\(countryCodeTf.text!.removePlusValue())\(self.textFields[1].text!.removeStringValue())"
        if(Connectivity.isConnectedToInternet()){
            ANLoader.showLoading("Loading..", disableUI: true)
            UserModuleService.VerifyMobNumService(mobNum: mobNum, email:textFields[2].text!) { (data) in
                DispatchQueue.main.async {
                    ANLoader.hide()
                }
                if data?.status == false {
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    }
                }else{
                    self.otpString = "\(String(describing: data!.successDic!.otp!))"
                    UIView.animate(withDuration: 0.6, animations: { () -> Void in
                        self.otpView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                        self.resendOTPBtn.isEnabled = false
                        self.resendOTPBtn.backgroundColor = UIColor.init(red: 0.349, green: 0.635, blue: 0.812, alpha: 0.5)
                        self.countDown = 120
                        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCountDown), userInfo: nil, repeats: true)
                        self.view.addSubview(self.otpView)
                    })
                }
            }
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
        }
    }
    
    // count down timer
    @objc func updateCountDown() {
        if(countDown > 0) {
            let minute = String(countDown/60)
            let seconds = String(countDown % 60)
            self.resendOTPBtn.backgroundColor = UIColor.init(red: 0.349, green: 0.635, blue: 0.812, alpha: 0.5)
            otpTiimeCountLbl.isHidden = true
            self.resendOTPBtn.isEnabled = false
            var str = "Resend OTP 0\(minute):\(seconds)"
            if AppDelegate.getDelegate().appLanguage == "English"{
                if(Int(seconds)! < 10){
                    str = "Resend OTP 0\(minute):0\(seconds)"
                }else{
                    str = "Resend OTP 0\(minute):\(seconds)"
                }
            }else{
                if(Int(seconds)! < 10){
                    str = "0إعادة ارسال كلمة مرور صالحه لمره واحده\(minute):0\(seconds)"
                }else{
                    str = "إعادة ارسال كلمة مرور صالحه لمره واحده0\(minute):\(seconds)"
                }
            }
            self.resendOTPBtn.setTitle(str, for: .normal)
            countDown = countDown - 1
        } else {
            self.resendOTPBtn.isEnabled = true
            self.resendOTPBtn.backgroundColor = UIColor.init(red: 0.349, green: 0.635, blue: 0.812, alpha: 1.0)
            otpTiimeCountLbl?.text = ""
            var str = "Resend OTP"
            if AppDelegate.getDelegate().appLanguage == "English"{
                str = "Resend OTP"
            }else{
                str = "إعادة ارسال كلمة مرور صالحه لمره واحده"
            }
            self.resendOTPBtn.setTitle(str, for: .normal)
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    func makePrefix() {
        let attributedString = NSMutableAttributedString(string: "+966 ")
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0,5))
        textFields[1].attributedText = attributedString
    }
    @objc func textFieldDidChange(textField: UITextField){
        
        if textField == textFields[1] {
            if countryCodeTf.text! == "+973"{
                if self.textFields[1].text!.count == 8 {
                    self.textFields[2].becomeFirstResponder()
                }
            }else{
                if self.textFields[1].text!.count == 9 {
                    self.textFields[2].becomeFirstResponder()
                }
            }
            return
        }
        
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case firstDigitTf:
                secondDigitTf.becomeFirstResponder()
            case secondDigitTf:
                thirdDigitTf.becomeFirstResponder()
            case thirdDigitTf:
                fourthDigitTf.becomeFirstResponder()
            case fourthDigitTf:
                fourthDigitTf.resignFirstResponder()
            default:
                break
            }
        }
        
        if textField == self.firstDigitTf || textField == self.secondDigitTf || textField == self.thirdDigitTf || textField == self.fourthDigitTf {
            let otpFromTF = "\(firstDigitTf.text!)\(secondDigitTf.text!)\(thirdDigitTf.text!)\(fourthDigitTf.text!)"
            
            if otpFromTF.count == 4 {
                self.verifyOtpBtn_Tapped(0)
            }
            
        }
    }
    @objc func otpTextFieldChangeBackSpace(){
        
        if(backSpaceTF == firstDigitTf){
            if(fourthDigitTf.hasText == true){
                fourthDigitTf.becomeFirstResponder()
            }else{
                if(thirdDigitTf.hasText == true){
                    thirdDigitTf.becomeFirstResponder()
                }else{
                    if(secondDigitTf.hasText == true){
                        secondDigitTf.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }else if(backSpaceTF == secondDigitTf){
            
            if(firstDigitTf.hasText == true){
                firstDigitTf.becomeFirstResponder()
            }else{
                if(fourthDigitTf.hasText == true){
                    fourthDigitTf.becomeFirstResponder()
                }else{
                    if(thirdDigitTf.hasText == true){
                        thirdDigitTf.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }else if (backSpaceTF == thirdDigitTf){
            
            if(secondDigitTf.hasText == true){
                secondDigitTf.becomeFirstResponder()
            }else{
                if(firstDigitTf.hasText == true){
                    firstDigitTf.becomeFirstResponder()
                }else{
                    if(thirdDigitTf.hasText == true){
                        thirdDigitTf.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }else if (backSpaceTF == fourthDigitTf){
            
            if(thirdDigitTf.hasText == true){
                thirdDigitTf.becomeFirstResponder()
            }else{
                if(secondDigitTf.hasText == true){
                    secondDigitTf.becomeFirstResponder()
                }else{
                    if(firstDigitTf.hasText == true){
                        firstDigitTf.becomeFirstResponder()
                    }else{
                        self.view.endEditing(true)
                    }
                }
            }
        }
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.textFields[0]  {
            if(string == " " && self.textFields[0].text?.count == 0){
                return false;
            }
            let nameStr:NSString = (self.textFields[0].text! as NSString)
            if(nameStr.length >= 1){
                let code:NSString = nameStr.substring(from: nameStr.length-1) as NSString
                if(code.isEqual(to: " ")){
                    if(string == " "){
                        return false
                    }
                }
            }
            let letters = "!~`@#$%^&*-+();:={}[],.<>?\\/\"\'_•¥£€|"
            let cs:CharacterSet = CharacterSet.init(charactersIn: letters)
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return string == filtered
        }else if textField == self.textFields[1] {
//            let  char = string.cString(using: String.Encoding.utf8)!
//            let isBackSpace = strcmp(char, "\\b")
//            if (isBackSpace == -92) {
//                if range.location == 0 {
//                    return false                        }
//            }
            
            if(self.textFields[1].text?.count == 0){
                let num = "123456789"
                let checNum = CharacterSet.init(charactersIn: num)
                if (string.rangeOfCharacter(from: checNum) != nil){
                    return true
                }else{
                    return false
                }
            }
            
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            if countryCodeTf.text! == "+973"{
                return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                updatedText.safelyLimitedTo(length: 8)
            }else{
                return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                    updatedText.safelyLimitedTo(length: 9)
            }
        }else if textField == self.firstDigitTf || textField == self.secondDigitTf || textField == self.thirdDigitTf || textField == self.fourthDigitTf {
            if(range.length == 1){
                backSpaceTF = textField
                self.perform(#selector(otpTextFieldChangeBackSpace), with: nil, afterDelay: 0.2)
                return true
            }
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            return updatedText.containsOnlyCharactersIn(matchCharacters: "0123456789") &&
                updatedText.safelyLimitedTo(length: 1)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
}
extension RegisterVC: DropDownVCDelegate{
    func didTapAction(id: String, name: String, whereType: Int) {
         countryCodeTf.text = id
        self.textFields[1].text = ""
    }
    
    
}

