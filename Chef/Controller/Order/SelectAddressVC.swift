//
//  SelectAddressVC.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
protocol SelectLocationDelegate {
    func selectLocation(dic : Dictionary<String, Any>)
}
class SelectAddressVC: UIViewController, CLLocationManagerDelegate , GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate {
    
    var addresslistArray = [Address]()
    var selectedIndex:Int!
    
    var locationManagerMain: Location!

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var searchBtn_tapped: UIButton!
    @IBOutlet weak var addressTableView: UITableView!
    let cellReuseIdentifier = "cell"
    
    // VARIABLES
    var locationManager = CLLocationManager()
    var delegate : SelectLocationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addressTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SelectAddressTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SelectAddressTVCell", value: "", table: nil))!)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        self.addressTableView.estimatedRowHeight = 72
        self.addressTableView.rowHeight = UITableView.automaticDimension

        self.addressTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        self.tabBarController?.tabBar.isHidden = true
        
        // Do any additional setup after loading the view.
        if UserDef.isValueInUserDefaults(key: "UserId") == true {
            if  SaveAddressClass.getAddressArray.count > 0 {
                self.addresslistArray = SaveAddressClass.getAddressArray
                addressTableView.reloadData()
            }else{
                self .getAddressList()
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Select Address", value: "", table: nil))!
        self.tabBarController?.tabBar.isHidden = true
        if AppDelegate.getDelegate().homeScreen == true{
            searchBtn_tapped.isHidden = false
        }else{
            searchBtn_tapped.isHidden = true
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveData, object: nil)

    }
    @objc func onDidReceiveData(_ notification:Notification) {
        // Do something now
        NotificationCenter.default.removeObserver(self, name: .didReceiveData, object: nil)
        self .getAddressList()
    }

    func getAddressList() {
        ANLoader.showLoading("Loading...", disableUI: true)
        AccountApiRouter.getUserAdd(userId: UserDef.getUserId()) { (data) in
            if data?.status == false {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    Alert.showToastAlert(on: self, message:data!.message!)

                }else{
                    //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    Alert.showToastAlert(on: self, message:data!.message!)

                }
                
            }else{
                SaveAddressClass.getAddressArray.removeAll()
                for addressList in (data!.successDic!){
                    let addressDic = Address (AddressId: addressList["AddressId"] is NSNull ? 0 : addressList["AddressId"] as! Int,
                                              HouseNo: addressList["HouseNo"] is NSNull ? "" : addressList["HouseNo"] as! String,
                                              AddressName: addressList["AddressName"] is NSNull ? "" : addressList["AddressName"] as! String,
                                              LandMark: addressList["LandMark"] is NSNull ? "" : addressList["LandMark"] as! String,
                                              Address: addressList["Address"] is NSNull ? "" : addressList["Address"] as! String,
                                              AddressType: addressList["AddressType"] is NSNull ? "" : addressList["AddressType"] as! String,
                                              IsDeleted: addressList["IsDeleted"] is NSNull ? false : addressList["IsDeleted"] as! Bool ,
                                              Latitude: addressList["Latitude"] is NSNull ? 0.00 : addressList["Latitude"] as! Double,
                                              Longitude: addressList["Longitude"] is NSNull ? 0.00 : addressList["Longitude"] as! Double)
                    
                    SaveAddressClass.getAddressArray.append(addressDic)
                    //self.addresslistArray.append(addressDic)
                }
               
                self.addresslistArray = SaveAddressClass.getAddressArray
                self.addressTableView .reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
            }
        }
    }
    @IBAction func backBtn_tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated:true)
    }
    @IBAction func searchBtn_tapped(_ sender: Any){
        let vc = LocationAutocompleteVC(nibName: "LocationAutocompleteVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.locationAutocompleteVCDelegate = self
        self.present(vc, animated: true, completion: nil)
//        let autoCompleteController = GMSAutocompleteViewController()
//        autoCompleteController.delegate = self
//        self.present(autoCompleteController, animated: true, completion: nil)
    }
    
    // Search
    @IBAction func openSearchAddressBtn_tapped(_ sender: Any) {
        let autoCompleteController = GMSAutocompleteViewController()
        autoCompleteController.delegate = self
    //    self.locationManager.startUpdatingLocation()
        self.present(autoCompleteController, animated: true, completion: nil)
    }
    // MARK: GOOGLE AUTO COMPLETE DELEGATE
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let latitude: Double? = place.coordinate.latitude
        let longitude: Double? = place.coordinate.longitude
        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2DMake(latitude!, longitude!), completionHandler: { response, error in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            /*if let aResults = response?.results() {
                print("\(aResults)")
            }
            for addressObj: GMSAddress? in response?.results() ?? [GMSAddress?]() {
                if let anObj = addressObj {
                    print("lines=\((anObj.lines)!)")
                    print("lines=\(anObj.coordinate.longitude)")
                    
                }
                let addStr:[String] = (addressObj?.lines)!
                var str=NSString()
                for index in addStr {
                    str=str.appending(index) as NSString
                    
                }
                var subLocality: String = str as String
                if addressObj!.subLocality != nil {
                    subLocality = addressObj!.subLocality!
                }
               // SaveAddressClass.saveAddressDic = ["Latitude": addressObj!.coordinate.latitude as Any,"Longitude": addressObj!.coordinate.longitude, "Address": str, "AddressId" : 0, "subLocality": subLocality, "HouseNo": "", "LandMark": ""] as [String : Any]
                
                let dic = ["Adreess": str,
                           "FullAddress":"\(str), \(String(describing: subLocality)), \(String(describing: addressObj!.country!))",
                    "AddressId":0,
                    "Latitude":"\(addressObj!.coordinate.latitude)",
                    "Longitude":"\(addressObj!.coordinate.longitude)"] as [String : Any]
                UserDefaults.standard.set(dic, forKey:"LocationDetails")
                UserDefaults.standard.synchronize()
                
                
                self.navigationController?.popViewController(animated: false)
                return
            }*/
            
            if let addressObj = response?.firstResult() {
                //}
                
                //for addressObj: GMSAddress? in response?.firstResult() ?? [GMSAddress?]() {
                
                var str = ""
                if let sublocality = addressObj.subLocality {
                    str = sublocality
                }else if let locality = addressObj.locality{
                    str = locality
                }else{
                    str = addressObj.country!
                }
                
                var sLocality = ""
                if let lines = addressObj.lines {
                    let lines = lines as [String]
                    sLocality = lines.joined(separator: ",")
                }else if let sublocality = addressObj.subLocality {
                    sLocality = sublocality
                }else if let locality = addressObj.locality {
                    sLocality = locality
                }else{
                    sLocality = addressObj.country!
                }
                
                let dic = ["Adreess": str,
                           "FullAddress":"\(str), \(String(describing: sLocality)), \(String(describing: addressObj.country!))",
                    "AddressId":0,
                    "Latitude":"\(latitude!)",
                    "Longitude":"\(longitude!)"] as [String : Any]
                UserDefaults.standard.set(dic, forKey:"LocationDetails")
                UserDefaults.standard.synchronize()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                self.navigationController?.popViewController(animated: false)
            }
        })
        self.dismiss(animated: true, completion: nil) // dismiss after select place
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("ERROR AUTO COMPLETE \(error)")
        
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil) // when cancel search
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension SelectAddressVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2;
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return ""
        }else{
            return (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Save Address", value: "", table: nil))!
        }
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Gotham-Bold", size: 20.0)
        if AppDelegate.getDelegate().appLanguage == "English"{
            header.textLabel?.textAlignment = NSTextAlignment.left
        }else{
            header.textLabel?.textAlignment = NSTextAlignment.right
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return self.addresslistArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "SelectAddressTVCell", value: "", table: nil))!, for: indexPath) as! SelectAddressTVCell
            cell.textNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Current Location", value: "", table: nil))!
            cell.img.image = UIImage.init(named: "CurrentLocation")
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectAddressCell") as! SelectAddressCell
            cell.addressTitleLbl.text = addresslistArray[indexPath.row].AddressName!
            cell.addressDisLbl.text = "\(addresslistArray[indexPath.row].HouseNo!), \(addresslistArray[indexPath.row].LandMark!), \(addresslistArray[indexPath.row].Address!)"
            //let userDetails = UserDef.getUserProfileDic(key: "userDetails")
            //cell.mobileNumLbl.text = "+\((userDetails["Mobile"] as? String)!)"
//            if selectedIndex == indexPath.row{
//                cell.selectionBtn.setImage(UIImage (named: "paymentSelect"), for: .normal)
//            }else{
//                cell.selectionBtn.setImage(UIImage (named: "paymentNormal"), for: .normal)
//            }
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if AppDelegate.getDelegate().homeScreen == true{
                
                HomeVC.instance.isLocationChange = true
                //ANLoader.showLoading("Loading..", disableUI: true)
                // Location Manager
                locationManagerMain = Location()
                locationManagerMain.needToDisplayAlert = true
                locationManagerMain.delegate = self
    
                return
            }else{
                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                var Obj = SearchAddressVC()
                Obj = mainStoryBoard.instantiateViewController(withIdentifier: "SearchAddressVC") as! SearchAddressVC
                Obj.isNewUser = true
                Obj.isSearch = false
                Obj.whereObj = 1
                self.navigationController?.pushViewController(Obj, animated: true)
                
            }
            
        }else{
            if AppDelegate.getDelegate().homeScreen == true{
                HomeVC.instance.isLocationChange = true
                selectedIndex = indexPath.row
                addressTableView.reloadData()
                //SaveAddressClass.saveAddressDic = ["Latitude": addresslistArray[indexPath.row].Latitude! as Any,"Longitude": addresslistArray[indexPath.row].Longitude! as Any, "Address": addresslistArray[indexPath.row].Address! as Any, "AddressId" : addresslistArray[indexPath.row].AddressId! as Any, "subLocality": addresslistArray[indexPath.row].AddressName! as Any, "HouseNo": addresslistArray[indexPath.row].HouseNo! as Any, "LandMark": addresslistArray[indexPath.row].LandMark! as Any] as [String : Any]
                
                let dic = ["Adreess": addresslistArray[indexPath.row].AddressName!,
                           "FullAddress":"\(addresslistArray[indexPath.row].HouseNo!), \(addresslistArray[indexPath.row].LandMark!), \(addresslistArray[indexPath.row].Address!)",
                    "AddressId":addresslistArray[indexPath.row].AddressId! as Any,
                    "Latitude":"\(addresslistArray[indexPath.row].Latitude! as Any)",
                    "Longitude":"\(addresslistArray[indexPath.row].Longitude! as Any)"] as [String : Any]
                UserDefaults.standard.set(dic, forKey:"LocationDetails")
                UserDefaults.standard.synchronize()
                self.navigationController?.popViewController(animated: false)
                
            }else{
                let myLocation = CLLocation(latitude: addresslistArray[indexPath.row].Latitude!, longitude: addresslistArray[indexPath.row].Longitude!)
                let myBuddysLocation = CLLocation(latitude: SaveAddressClass.getBranchDetailsArray[0].Latitude!, longitude: SaveAddressClass.getBranchDetailsArray[0].Longitude!)
                let distance = myLocation.distance(from: myBuddysLocation) / 1000
                if distance > SaveAddressClass.getBranchDetailsArray[0].DeliveryDistance! {
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "The Disatnce too far from Restaurent Please Seelect Near Restaurent", value: "", table: nil))!)
                }else{
                    selectedIndex = indexPath.row
                    addressTableView.reloadData()
                   // SaveAddressClass.saveAddressDic = ["Latitude": addresslistArray[indexPath.row].Latitude! as Any,"Longitude": addresslistArray[indexPath.row].Longitude! as Any, "Address": addresslistArray[indexPath.row].Address! as Any, "AddressId" : addresslistArray[indexPath.row].AddressId! as Any, "subLocality": addresslistArray[indexPath.row].AddressName! as Any, "HouseNo": addresslistArray[indexPath.row].HouseNo! as Any, "LandMark": addresslistArray[indexPath.row].LandMark! as Any] as [String : Any]
                    
                    let dic = ["Adreess": addresslistArray[indexPath.row].AddressName!,
                               "FullAddress":"\(addresslistArray[indexPath.row].HouseNo!), \(addresslistArray[indexPath.row].LandMark!), \(addresslistArray[indexPath.row].Address!)",
                        "AddressId":addresslistArray[indexPath.row].AddressId! as Any,
                        "Latitude":"\(addresslistArray[indexPath.row].Latitude! as Double)",
                        "Longitude":"\(addresslistArray[indexPath.row].Longitude! as Double)"] as [String : Any]
                    UserDefaults.standard.set(dic, forKey:"LocationDetails")
                    UserDefaults.standard.synchronize()
                    self.navigationController?.popViewController(animated: false)
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 60
        }
        
        return UITableView.automaticDimension
        
    }
}

extension SelectAddressVC : LocationDelegate {
    func didFailedLocation(error: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        //self.checkLocationPermission()
        

        let alert = UIAlertController(title: "Allow Location Access", message: "The Chef needs access to your location. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)

                   // Button to Open Settings
                   alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                       guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                           return
                       }
                       if UIApplication.shared.canOpenURL(settingsUrl) {
                           UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                               print("Settings opened: \(success)")
                           })
                       }
                   }))
                   alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                   self.present(alert, animated: true, completion: nil)
    }
    func checkLocationPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                //open setting app when location services are disabled
                //Please enable location services for this app.
                openSettingApp(title:NSLocalizedString("Location Services Disabled", comment: ""), message:NSLocalizedString("Please go to Settings and turn on Location Service for this app.", comment: ""))
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
            openSettingApp(title:NSLocalizedString("Location services are not enabled", comment: ""), message:NSLocalizedString("Please go to Settings and turn on Location Service", comment: ""))
        }
    }
    func openSettingApp(title: String,message: String) {
        let alertController = UIAlertController (title: message, message:nil , preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)

        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }
    func didUpdateLocation(latitude: Double?, longitude: Double?){
        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2DMake((latitude!), (longitude!)), completionHandler: { response, error in
            if let addressObj = response?.firstResult() {
               // print("\(addressObj)")
                
                var str = ""
                if let sublocality = addressObj.subLocality {
                    str = sublocality
                }else if let locality = addressObj.locality{
                    str = locality
                }else{
                    str = addressObj.country!
                }
                
                var sLocality = ""
                if let lines = addressObj.lines {
                    let lines = lines as [String]
                    sLocality = lines.joined(separator: ",")
                }else if let sublocality = addressObj.subLocality {
                    sLocality = sublocality
                }else if let locality = addressObj.locality {
                    sLocality = locality
                }else{
                    sLocality = addressObj.country!
                }
                
                let dic = ["Adreess": str,
                           "FullAddress":"\(str), \(String(describing: sLocality)), \(String(describing: addressObj.country!))",
                    "AddressId":0,
                    "Latitude":"\(latitude!)",
                    "Longitude":"\(longitude!)"] as [String : Any]
                UserDefaults.standard.set(dic, forKey:"LocationDetails")
                UserDefaults.standard.synchronize()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                self.navigationController?.popViewController(animated: false)
            }
        });
        
    }
}
extension SelectAddressVC:LocationAutocompleteVCDelegate{
    func didTapAction(latitude: Double, longitude: Double, MainAddress: String, TotalAddress: String) {
        let dic = ["Adreess": MainAddress,
                   "FullAddress":"\(TotalAddress)",
            "AddressId":0,
            "Latitude":"\(latitude)",
            "Longitude":"\(longitude)"] as [String : Any]
        UserDefaults.standard.set(dic, forKey:"LocationDetails")
        UserDefaults.standard.synchronize()
        //print(MainAddress)
        DispatchQueue.main.async() {
            self.navigationController?.popViewController(animated: false)
        }
    }
}
