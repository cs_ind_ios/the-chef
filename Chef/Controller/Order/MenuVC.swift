//
//  MenuVC.swift
//  Chef
//
//  Created by RAVI on 16/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class MenuVC: UIViewController {

    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var listCollectionView: UICollectionView!
    @IBOutlet weak var titleNameLbl: UILabel!
    @IBOutlet weak var cartNumberLbl: UILabel!
    @IBOutlet weak var checkOutBtn: UIButton!
    
    
    let menuNames = ["Biryani"]
    var mainArr = [GetCategoryMenu]()
    var menuSearchItems = [GetItemsMenu]()
    var filterArr = [GetCategoryMenu]()
    var menuInfoArray = [[String:Any]]()
    var storeImageName = String()
    var storeName = String()
    var selectCatSection = "Biryani"
    var isLoaded = false
    var selectIndex:Int = 0
    static var instance: MenuVC!
    var onceOnly = false
    
    var isSearch : Bool = false
    var isCancel:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MenuVC.instance = self
        searchbar.delegate = self
        searchbar.setTextField(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
    checkOutBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "CheckOut", value: "", table: nil))!, for: .normal)

        if AppDelegate.getDelegate().appLanguage == "English"{
            categoryCollectionView.semanticContentAttribute = .forceLeftToRight
            listCollectionView.semanticContentAttribute = .forceLeftToRight
            searchbar.semanticContentAttribute = .forceLeftToRight
        }else{
            categoryCollectionView.semanticContentAttribute = .forceRightToLeft
            listCollectionView.semanticContentAttribute = .forceRightToLeft
            searchbar.semanticContentAttribute = .forceRightToLeft

        }
        
        searchbar.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Search menu", value: "", table: nil))!
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        
        listCollectionView.delegate = self
        listCollectionView.dataSource = self
        
        cartNumberLbl.layer.cornerRadius = cartNumberLbl.frame.height/2
        cartNumberLbl.layer.masksToBounds = true
        
//        let myIndexPath = IndexPath(item: 1, section: 0)
//        listCollectionView.scrollToItem(at: myIndexPath, at: .centeredHorizontally, animated: false)

        
        if AppDelegate.getDelegate().appLanguage == "English"{
            titleNameLbl.text = SaveAddressClass.selectStoreArray[0].BranchName_En?.capitalized
        }else{
            titleNameLbl.text = SaveAddressClass.selectStoreArray[0].BranchName_Ar?.capitalized
        }
        
        getMainMenuItemsService()
        self.tabBarController?.tabBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(selectItemDetails), name: NSNotification.Name(rawValue: "selectItemDetails"), object: nil)
    }
    
    @IBAction func backBtn_Tapped(_ sender: UIButton) {
        HomeVC.instance.getStoresServiceCall = false
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func checkOutBtn_Tapped(_ sender: Any) {
        self.tabBarController?.selectedIndex = 2
    }
    @IBAction func cartBtn_Tapped(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 2
    }
    @objc func selectItemDetails(){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = AdditionalsVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "AdditionalsVC") as! AdditionalsVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        //let count = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable where BranchId = '\(SaveAddressClass.selectStoreArray[0].BranchId!)'")
        let count = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable")
        if (count > 0){
            if let tabItems = tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[2]
                tabItem.badgeValue = "\(count)"
                cartNumberLbl.isHidden = false
                cartNumberLbl.text = "\(count)"
            }
        }else{
            if let tabItems = self.tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[2]
                tabItem.badgeValue = nil
                cartNumberLbl.isHidden = true
            }
        }
        super.viewWillAppear(animated)
        
        listCollectionView.setNeedsLayout()
        listCollectionView.layoutIfNeeded()

        self.listCollectionView.reloadData()
        self.categoryCollectionView.reloadData()
    }
    
    func getMainMenuItemsService(){
        if(Connectivity.isConnectedToInternet() == true){
            ANLoader.showLoading("Loading..", disableUI: true)
            let jsonDic = ["BranchId":"\(SaveAddressClass.selectStoreArray[0].BranchId!)"]
            OrderApiRouter.getMainMenuItems(dic: jsonDic, success: { (data) in
                if data?.status == false{
                    DispatchQueue.main.async {
                        ANLoader.hide()
                    }
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    }
                    self.isLoaded = false
                    
                }else{
                    SaveAddressClass.getMenuDetails.removeAll()
                    
                    for menuList in data!.successDic!.menuDetails! {
                        
                        let menuDic = GetCategoryMenu(
                            CategoryId : menuList["CategoryId"] is NSNull ? 0 : menuList["CategoryId"] as! Int,
                            CategoryName_En : menuList["CategoryName_En"] is NSNull ? "" : menuList["CategoryName_En"] as! String,
                            CategoryName_Ar : menuList["CategoryName_Ar"] is NSNull ? "" : menuList["CategoryName_Ar"] as! String,
                            CatImage : menuList["CatImage"] is NSNull ? "" : menuList["CatImage"] as! String,
                            IsOpen : false,
                            MenuItems: self.myArrayFunc(inputArray:  menuList["MenuItems"] as! Array<AnyObject> ) as! [GetItemsMenu]
                        )
                        // Save More Stores
                        SaveAddressClass.getMenuDetails.append(menuDic)
                    }
                    self.mainArr.removeAll()
                    self.mainArr = SaveAddressClass.getMenuDetails
                    self.categoryCollectionView.reloadData()
                    if AppDelegate.getDelegate().appLanguage != "English"{
                        self.categoryCollectionView.scrollToItem(at: IndexPath(item:0, section: 0), at: UICollectionView.ScrollPosition.right, animated: false)
                    }

                    self.isLoaded = true
                    self.listCollectionView.reloadData()
                   
                    DispatchQueue.main.async {
                        ANLoader.hide()
                    }
                }
            }) { (error) in
                DispatchQueue.main.async {
                    ANLoader.hide()
                }
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: error)
                self.isLoaded = false
            }
        }else{
            DispatchQueue.main.async {
                ANLoader.hide()
            }
            self.isLoaded = false
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
        }
    }
    func myArrayFunc(inputArray:Array<AnyObject>) -> Array<AnyObject> {
        var itemsArray = [GetItemsMenu]()
        for itemsList in inputArray as! [Dictionary<String, Any>] {
            
            let itemsDic = GetItemsMenu(
                ItemId : itemsList["ItemId"] is NSNull ? 0 : itemsList["ItemId"] as! Int,
                ItemName_En : itemsList["ItemName_En"] is NSNull ? "" : itemsList["ItemName_En"] as! String,
                ItemName_Ar : itemsList["ItemName_Ar"] is NSNull ? "" : itemsList["ItemName_Ar"] as! String,
                ItemDesc_En : itemsList["ItemDesc_En"] is NSNull ? "" : itemsList["ItemDesc_En"] as! String,
                ItemDesc_Ar : itemsList["ItemDesc_Ar"] is NSNull ? "" : itemsList["ItemDesc_Ar"] as! String,
                ItemImage : itemsList["ItemImage"] is NSNull ? "" : itemsList["ItemImage"] as! String,
                Price : itemsList["Price"] is NSNull ? 0 : itemsList["Price"] as! Double
            )
            itemsArray.append(itemsDic)
        }
        return itemsArray
    }
}
extension MenuVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == listCollectionView{
            return self.mainArr.count
        }
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCollectionView{
            if(isSearch) {
                return self.mainArr.count
            }else{
                if self.mainArr.count > 0 {
                    return self.mainArr.count + 1
                }else{
                    return 0
                }
            }
        }else{
            if self.mainArr.count > 0{
                 return mainArr[section].MenuItems.count
            }
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoryCollectionView{
            let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "MenuCategoryCVCell", for: indexPath) as! MenuCategoryCVCell
            if(isSearch) {
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.CategoryNameLbl.text = mainArr[indexPath.row].CategoryName_En?.capitalized
                }else{
                    cell.CategoryNameLbl.text = mainArr[indexPath.row].CategoryName_Ar?.capitalized
                }
                cell.cellView.layer.borderWidth = 1
                if selectIndex == indexPath.row {
                    cell.cellView.backgroundColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
                    cell.CategoryNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.cellView.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
                }else{
                    cell.cellView.backgroundColor = .clear
                    cell.CategoryNameLbl.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                    cell.cellView.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                }
            }else{
                if indexPath.row == 0{
                    cell.CategoryNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "All", value: "", table: nil))!
                }else{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        cell.CategoryNameLbl.text = mainArr[indexPath.row - 1].CategoryName_En?.capitalized
                    }else{
                        cell.CategoryNameLbl.text = mainArr[indexPath.row - 1].CategoryName_Ar?.capitalized
                    }
                }
                cell.cellView.layer.borderWidth = 1
                if selectIndex == indexPath.row {
                    cell.cellView.backgroundColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
                    cell.CategoryNameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    cell.cellView.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
                }else{
                    cell.cellView.backgroundColor = .clear
                    cell.CategoryNameLbl.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                    cell.cellView.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                }
            }
            
            
            return cell
        }else{
            let cell = listCollectionView.dequeueReusableCell(withReuseIdentifier: "MenuListCVCell", for: indexPath) as! MenuListCVCell
            //"\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!)
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.foodNameLbl.text = self.mainArr[indexPath.section].MenuItems[indexPath.row].ItemName_En?.capitalized
                cell.foodDescriptionLbl.text = self.mainArr[indexPath.section].MenuItems[indexPath.row].ItemDesc_En!.capitalized
                cell.foodCostLbl.text = "SAR \(self.mainArr[indexPath.section].MenuItems[indexPath.row].Price!.withCommas())"
            }else{
                cell.foodNameLbl.text = self.mainArr[indexPath.section].MenuItems[indexPath.row].ItemName_Ar?.capitalized
                cell.foodDescriptionLbl.text = self.mainArr[indexPath.section].MenuItems[indexPath.row].ItemDesc_Ar!.capitalized
                cell.foodCostLbl.text = "\(self.mainArr[indexPath.section].MenuItems[indexPath.row].Price!.withCommas()) SAR"
            }
            
            let imgStr:NSString = "\(url.ItemImg.imgPath())\(self.mainArr[indexPath.section].MenuItems[indexPath.row].ItemImage!)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let URLString = URL.init(string: urlStr as String)
            cell.foodImg.kf.indicatorType = .activity
            cell.foodImg.kf.setImage(with: URLString)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == listCollectionView{
            let screenSize: CGRect = self.listCollectionView.bounds
            let screenWidth = screenSize.width / 2
            let height = screenWidth * 0.55
            return CGSize(width: screenWidth - 6, height: height + 92)
        }else{
            let screenSize: CGRect = self.categoryCollectionView.bounds
            let screenWidth = screenSize.width
            return CGSize(width: screenWidth/3, height: 50)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == listCollectionView{
           // self.categoryCollectionView.reloadData()
            //self.listCollectionView.reloadData()
            SaveAddressClass.getSelectItemDetails.removeAll()
            let itemDic = ItemDetails(
                ItemId: self.mainArr[indexPath.section].MenuItems[indexPath.row].ItemId!,
                ItemName_En: self.mainArr[indexPath.section].MenuItems[indexPath.row].ItemName_En!,
                ItemName_Ar: self.mainArr[indexPath.section].MenuItems[indexPath.row].ItemName_Ar!,
                ItemDesc_En: self.mainArr[indexPath.section].MenuItems[indexPath.row].ItemDesc_En!,
                ItemDesc_Ar: self.mainArr[indexPath.section].MenuItems[indexPath.row].ItemDesc_Ar!,
                ItemImage: self.mainArr[indexPath.section].MenuItems[indexPath.row].ItemImage!,
                Price: self.mainArr[indexPath.section].MenuItems[indexPath.row].Price!,
                CategoryId: self.mainArr[indexPath.section].CategoryId!
            )
            SaveAddressClass.getSelectItemDetails.append(itemDic)
            
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "AdditionalsVC") as! AdditionalsVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            if self.mainArr.count > 0 {

            selectIndex = indexPath.row
            selectIndex = selectIndex > 0 ? selectIndex : 0
            
            collectionView.selectItem(at: NSIndexPath(item: selectIndex, section: 0) as IndexPath, animated: true, scrollPosition: .centeredHorizontally)
            if selectIndex > 0{
                listCollectionView.scrollToItem(
                    at: NSIndexPath(item: 0, section: selectIndex-1) as IndexPath,
                    at: .top,
                    animated: true)
            }else{
                listCollectionView.scrollToItem(
                    at: NSIndexPath(item: 0, section: 0) as IndexPath,
                    at: .top,
                    animated: true)
            }
            
            self.categoryCollectionView.reloadData()
           // self.listCollectionView.reloadData()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if collectionView == listCollectionView{
            return CGSize(width:collectionView.frame.size.width, height:44)
        }
        return CGSize.zero
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "MenuHeaderCVCell", for: indexPath) as! MenuHeaderCVCell
        if AppDelegate.getDelegate().appLanguage == "English"{
            headerView.headerNameLbl.text = self.mainArr[indexPath.section].CategoryName_En!.capitalized
        }else{
            headerView.headerNameLbl.text = self.mainArr[indexPath.section].CategoryName_Ar!.capitalized
        }
        return headerView
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.mainArr.count > 0 {
            if scrollView == self.listCollectionView {
                var indexes = self.listCollectionView.indexPathsForVisibleItems
                indexes.sort()
                var index = indexes.first!
                index[0] = index[0] > 0 ? index[0]:0
                selectIndex = index.section+1
                let visibleIndexPath: IndexPath = [0, selectIndex]
                DispatchQueue.main.async {
                self.categoryCollectionView.scrollToItem(at:visibleIndexPath, at: .centeredHorizontally, animated: true )
                    self.categoryCollectionView.reloadData()
                }
            }
        }
    }
}
//MARK: UISearchbar delegate
extension MenuVC: UISearchBarDelegate{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if isCancel == true{
            isSearch = false
            listCollectionView.isHidden = false
        }else{
            isSearch = true;
        }
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchBar.text == ""{
            isSearch = false;
        }else{
            isSearch = true;
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false;
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = true;
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text!.count > 0 {
            isSearch = true
            searchItems(searchText: searchText)
        }else{
            isCancel = true
            isSearch = false
            searchItems(searchText: "")
        }
        self.categoryCollectionView.reloadData()
        self.listCollectionView.reloadData()
    }
    func searchItems(searchText:String) {
        if searchText != "" {
            selectIndex = 0
            self.mainArr.removeAll()

            menuSearchItems.removeAll()
            mainArr = SaveAddressClass.getMenuDetails
            if mainArr.count == 0{
                getMainMenuItemsService()
                return
            }
          if AppDelegate.getDelegate().appLanguage == "English"{

            for i in 0...mainArr.count - 1{
                for j in 0...mainArr[i].MenuItems.count - 1{
                    let menuItems = mainArr[i].MenuItems[j].ItemName_En!.lowercased().contains(searchText.lowercased())
                   // let menuItems = mainArr[i].MenuItems[j].ItemName_En!.lowercased().contains(searchText.lowercased())  || mainArr[i].MenuItems[j].ItemName_Ar!.lowercased().contains(searchText.lowercased())
                    if menuItems == true{
                        menuSearchItems.append(mainArr[i].MenuItems[j])
                    }
                }
            }
            mainArr.removeAll()
            filterArr.removeAll()
            if menuSearchItems.count > 0{
                for i in 0...menuSearchItems.count - 1{
                    let categories = SaveAddressClass.getMenuDetails.filter { $0.MenuItems.contains(where: { ($0.ItemId! == menuSearchItems[i].ItemId!)})}
                    for j in 0...categories.count - 1{
                        self.filterArr = [categories[j]]
                        for k in 0...filterArr.count - 1{
                            if mainArr.count > 0{
                                if filterArr[k].CategoryName_En!.lowercased().contains(mainArr[j].CategoryName_En!.lowercased()) == false{
                                    let menuDetails = mainArr.filter({$0.CategoryName_En! == filterArr[k].CategoryName_En!})
                                    if menuDetails.count == 0{
                                        self.mainArr.append(categories[j])
                                    }
                                }
                            }else{
                                self.mainArr.append(categories[j])
                                mainArr[j].MenuItems = mainArr.flatMap({$0.MenuItems.filter({$0.ItemName_En!.lowercased().contains(searchText.lowercased())})})
                            }
                        }
                    }
                }
            }
            
           
          }else{
            for i in 0...mainArr.count - 1{
                for j in 0...mainArr[i].MenuItems.count - 1{
                    let menuItems = mainArr[i].MenuItems[j].ItemName_Ar!.lowercased().contains(searchText.lowercased())
                  
                    if menuItems == true{
                        menuSearchItems.append(mainArr[i].MenuItems[j])
                    }
                }
            }
            mainArr.removeAll()
            filterArr.removeAll()
            if menuSearchItems.count > 0{
                for i in 0...menuSearchItems.count - 1{
                    let categories = SaveAddressClass.getMenuDetails.filter { $0.MenuItems.contains(where: { ($0.ItemId! == menuSearchItems[i].ItemId!)})}
                    for j in 0...categories.count - 1{
                        self.filterArr = [categories[j]]
                        for k in 0...filterArr.count - 1{
                            if mainArr.count > 0{
                                if filterArr[k].CategoryName_En!.lowercased().contains(mainArr[j].CategoryName_En!.lowercased()) == false{
                                    let menuDetails = mainArr.filter({$0.CategoryName_En! == filterArr[k].CategoryName_En!})
                                    if menuDetails.count == 0{
                                        self.mainArr.append(categories[j])
                                    }
                                }
                            }else{
                                self.mainArr.append(categories[j])
                                mainArr[j].MenuItems = mainArr.flatMap({$0.MenuItems.filter({$0.ItemName_Ar!.lowercased().contains(searchText.lowercased())})})
                            }
                        }
                    }
                }
            }
          }
          
            self.categoryCollectionView.reloadData()
            self.listCollectionView.reloadData()
            
            
            
            //Swathi Total code for search category and item
//            self.mainArr = SaveAddressClass.getMenuDetails.filter({$0.CategoryName_En!.lowercased().contains(searchText.lowercased())})
//            if mainArr.count == 0{
//                menuSearchItems.removeAll()
//                mainArr = SaveAddressClass.getMenuDetails
//                for i in 0...mainArr.count - 1{
//                    for j in 0...mainArr[i].MenuItems.count - 1{
//                        let menuItems = mainArr[i].MenuItems[j].ItemName_En!.lowercased().contains(searchText.lowercased())
//                        if menuItems == true{
//                            menuSearchItems.append(mainArr[i].MenuItems[j])
//                        }
//                    }
//                }
//                mainArr.removeAll()
//                filterArr.removeAll()
//                if menuSearchItems.count > 0{
//                    for i in 0...menuSearchItems.count - 1{
//                        let categories = SaveAddressClass.getMenuDetails.filter { $0.MenuItems.contains(where: { ($0.ItemId! == menuSearchItems[i].ItemId!)})}
//                        for j in 0...categories.count - 1{
//                            self.filterArr = [categories[j]]
//                            for k in 0...filterArr.count - 1{
//                                if mainArr.count > 0{
//                                    if filterArr[k].CategoryName_En!.lowercased().contains(mainArr[j].CategoryName_En!.lowercased()) == false{
//                                        let menuDetails = mainArr.filter({$0.CategoryName_En! == filterArr[k].CategoryName_En!})
//                                        if menuDetails.count == 0{
//                                            self.mainArr.append(categories[j])
//                                        }
//                                    }
//                                }else{
//                                    self.mainArr.append(categories[j])
//                                    mainArr[j].MenuItems = mainArr.flatMap({$0.MenuItems.filter({$0.ItemName_En!.lowercased().contains(searchText.lowercased())})})
//                                }
//                            }
//                        }
//                    }
//                }
//            }else{
//                var menuDetailsArray = [GetCategoryMenu]()
//                menuDetailsArray = SaveAddressClass.getMenuDetails
//                menuSearchItems.removeAll()
//                for i in 0...menuDetailsArray.count - 1{
//                    for j in 0...menuDetailsArray[i].MenuItems.count - 1{
//                        let menuItems = menuDetailsArray[i].MenuItems[j].ItemName_En!.lowercased().contains(searchText.lowercased())
//                        if menuItems == true{
//                            menuSearchItems.append(menuDetailsArray[i].MenuItems[j])
//                        }
//                    }
//                    print(menuSearchItems)
//                }
//                if menuSearchItems.count > 0{
//                    for i in 0...menuSearchItems.count - 1{
//                        let categories = SaveAddressClass.getMenuDetails.filter { $0.MenuItems.contains(where: { ($0.ItemId! == menuSearchItems[i].ItemId!)})}
//                        filterArr.removeAll()
//                        for j in 0...categories.count - 1{
//                            self.filterArr = [categories[j]]
//                            for k in 0...filterArr.count - 1{
//                                if filterArr[k].CategoryName_En!.lowercased().contains(mainArr[j].CategoryName_En!.lowercased()) == false{
//                                    let menuDetails = mainArr.filter({$0.CategoryName_En! == filterArr[k].CategoryName_En!})
//                                    if menuDetails.count == 0{
//                                        self.mainArr.append(categories[j])
//                                        mainArr[j].MenuItems = mainArr.flatMap({$0.MenuItems.filter({$0.ItemName_En!.lowercased().contains(searchText.lowercased())})})
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            self.categoryCollectionView.reloadData()
//            self.listCollectionView.reloadData()
        }else{
            getMainMenuItemsService()
        }
    }
}
