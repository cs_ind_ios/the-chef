//
//  PromotionsVC.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
//import Presentr

//MARK: - Custom Delegate
protocol promoCodeDelegate: AnyObject
{
    func getPromoCode(_ promoCode: String, amount: Double, count: Int,minSpend: Double, maxAmount:Double,percentage: Double)
}

class PromotionsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {
    
    weak var promoDelegate: promoCodeDelegate?
    @IBOutlet weak var VCTitle_Lbl: UILabel!
    @IBOutlet weak var promotionsTableView: UITableView!
    // For PopUp Objects
    @IBOutlet var popUp_view: UIView!
    @IBOutlet weak var popUP_iconImgView: UIImageView!
    @IBOutlet weak var popUp_PromoCodeLbl: UILabel!
    @IBOutlet weak var popUp_PromoTitleLbl: UILabel!
    @IBOutlet weak var popUp_TxtView: UITextView!
    
    @IBOutlet weak var popUp_ValidityLbl: UILabel!
    @IBOutlet var blackView: UIView!
    
    var tap = UITapGestureRecognizer() // for gesture
    var obj_Offer : Int = 0
    
    // MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Specifying Promotions TableView Delegates
        self.promotionsTableView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PromotionsTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PromotionsTVCell", value: "", table: nil))!)
        self.promotionsTableView.separatorStyle = .none
        self.promotionsTableView.delegate = self
        self.promotionsTableView.dataSource = self
        
        self.tabBarController?.tabBar.isHidden = true // Hidding TabBar
        
        self.blackView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(self.blackView)
        self.blackView.isHidden = true
        
        // Setting default height for PopUP
        self.popUp_view.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: 200)
        self.view.addSubview(self.popUp_view)
        
        self.tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        self.tap.delegate = self
        self.blackView.addGestureRecognizer(self.tap)
        
        // Dynamic Height for PopUp Description TextView
        self.popUp_TxtView.translatesAutoresizingMaskIntoConstraints = true
        //self.popUp_TxtView.sizeToFit()
        self.popUp_TxtView.isScrollEnabled = false
        
        if obj_Offer == 1 {
            getPromotionsHistoryService()
        }
        
    }
    
    func getPromotionsHistoryService()  {
        var UserId = "0"
        if isUserLogIn() == true {
            UserId = "\(UserDef.getUserId())"
        }
        let dic:[String:Any] = ["BranchId":0,"DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))","UserId":UserId,"BrandId":0]
        OrderApiRouter.getPromotionsService(dic: dic,isLoader:true,isUIDisable:true, success: { (data) in
            if(data.Status == false){
                DispatchQueue.main.async {
                    ANLoader.hide()
                }
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.Message)
                SaveAddressClass.getPromotionsArray.removeAll()
                self.promotionsTableView.reloadData()
            }else{
                SaveAddressClass.getPromotionsArray.removeAll()
                for Data in data.Data!{
                    SaveAddressClass.getPromotionsArray.append(Data)
                    self.promotionsTableView.reloadData()
                }
                DispatchQueue.main.async {
                    ANLoader.hide()
                }
            }
        }){ (error) in
            DispatchQueue.main.async {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            SaveAddressClass.getPromotionsArray.removeAll()
            self.promotionsTableView.reloadData()
        }
    }

    // MARK: - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return SaveAddressClass.getPromotionsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PromotionsTVCell", value: "", table: nil))!, for: indexPath) as! PromotionsTVCell
        cell.backGroundView.layer.cornerRadius = 8
        cell.backGroundView.layer.masksToBounds = true
                //cell.backGroundView.backgroundColor = UIColor.blue
        if obj_Offer == 1 {
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
        }
        if SaveAddressClass.getPromotionsArray[indexPath.row].DiscountType == 1{
            cell.titleLbl.text = "\(SaveAddressClass.getPromotionsArray[indexPath.row].DiscountAmt.withCommas()) SAR \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Off", value: "", table: nil))!)"
        }else{
            cell.titleLbl.text = "\(Int(SaveAddressClass.getPromotionsArray[indexPath.row].DiscountAmt))% \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Discount", value: "", table: nil))!)"
        }
        cell.minimumSpendLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum Spend", value: "", table: nil))!) \(SaveAddressClass.getPromotionsArray[indexPath.row].MinSpend.withCommas()) SAR"
        cell.uptoDiscountLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Upto maximum discount", value: "", table: nil))!) \(SaveAddressClass.getPromotionsArray[indexPath.row].MaxAmount.withCommas()) SAR"
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.promoCodeLbl.text = SaveAddressClass.getPromotionsArray[indexPath.row].PromoTitle_En
            cell.descriptionTxtView.text = SaveAddressClass.getPromotionsArray[indexPath.row].Msg_En
        }else{
            cell.promoCodeLbl.text = SaveAddressClass.getPromotionsArray[indexPath.row].PromoTitle_Ar
            cell.descriptionTxtView.text = SaveAddressClass.getPromotionsArray[indexPath.row].Msg_Ar
        }
        
        for i in 0...6{
            cell.weeksDayBtn[i].backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        }
        
        let promoDic = SaveAddressClass.getPromotionsArray[indexPath.row].PromotionDays
        for i in 0...promoDic.count-1{
            if promoDic[i].DayName == "Monday"{
                cell.weeksDayBtn[1].backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
            }else if promoDic[i].DayName == "Tuesday"{
                cell.weeksDayBtn[2].backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
            }else if promoDic[i].DayName == "Wednesday"{
                cell.weeksDayBtn[3].backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
            }else if promoDic[i].DayName == "Thursday"{
                cell.weeksDayBtn[4].backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
            }else if promoDic[i].DayName == "Friday"{
                cell.weeksDayBtn[5].backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
            }else if promoDic[i].DayName == "Saturday"{
                cell.weeksDayBtn[6].backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
            }else if promoDic[i].DayName == "Sunday"{
                cell.weeksDayBtn[0].backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
            }else{
                cell.weeksDayBtn[i].backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 235
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if obj_Offer == 0 {
            if SaveAddressClass.getPromotionsArray[indexPath.row].TodayAvailable == true {
                
                let selectPromoDic = SaveAddressClass.getPromotionsArray[indexPath.row]
                
                let totalAmount = DBObject.geTotalOrderAmount(Sql:"SELECT sum(TotalAmount) FROM OrderTable")
                let vat = totalAmount * 0.05
                
                if (totalAmount + vat) > selectPromoDic.MinSpend {
                    var amount = 0
                    var percent = 0.0
                    if SaveAddressClass.getPromotionsArray[indexPath.row].DiscountType == 1{
                        amount = Int(SaveAddressClass.getPromotionsArray[indexPath.row].DiscountAmt)
                    }else{
                        percent = Double(SaveAddressClass.getPromotionsArray[indexPath.row].DiscountAmt)
                    }
                    let promoTitile = selectPromoDic.PromotionCode
                    //                    if AppDelegate.getDelegate().appLanguage == "English"{
                    //                        promoTitile = selectPromoDic.PromoTitle_En
                    //                    }else{
                    //                        promoTitile = selectPromoDic.PromoTitle_Ar
                    //                    }
                    
                    promoDelegate?.getPromoCode(promoTitile,amount: Double(amount), count: 1, minSpend: Double(selectPromoDic.MinSpend), maxAmount: Double(selectPromoDic.MaxAmount), percentage: percent)
                    self.navigationController?.popViewController(animated: true)
                    
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\(String(format:"\(String(describing: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Minimum cart amount", value: "", table: nil))!)) %@ SAR", selectPromoDic.MinSpend.withCommas()))")
                }
            }else{
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sorry there is no promotion available for today", value: "", table: nil))!)
            }
        }
    }
    //MARK: - Button Actions
    @IBAction func backBtn_Tapped(_ sender: Any)
    {
        if(obj_Offer == 1){
            self.navigationController?.popViewController(animated: true)
        }
        else{
            dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }
    }
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.blackView.isHidden = true
        self.popUp_view.isHidden = true
        self.perform(#selector(hideBlackView), with: nil, afterDelay: 0.4)
    }
    @objc func hideBlackView(){
        self.blackView.isHidden = true
        self.popUp_view.isHidden = true
    }
}
