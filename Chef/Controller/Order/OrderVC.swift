//
//  OrderVC.swift
//  Chef
//
//  Created by RAVI on 26/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import MapKit

protocol OrderVCDelegate {
    func didTapAction(orderId:Int, ActionType:Int, IsFavourite:Bool)
}

class OrderVC: UIViewController, FloatRatingViewDelegate {
    var objType:Int?
    
    var orderVCDelegate: OrderVCDelegate!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var orderNameCollectionView: UICollectionView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var ratingMainView: UIView!
    @IBOutlet weak var ratingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var ratingSkipBtn: UIButton!
    @IBOutlet weak var cancelBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var cancelBtn: UIButton!

    //Order Status outlets
    @IBOutlet weak var orderStatusView: UIView!
    @IBOutlet weak var orderSummeryMainView: UIView!
    @IBOutlet weak var orderStatusMainView: UIView!
    @IBOutlet weak var orderStatusScrollView: UIScrollView!
    @IBOutlet weak var orderSummeryScrollView: UIScrollView!
    
    @IBOutlet weak var submittedTimeLbl: UILabel!
    @IBOutlet weak var submittedDateLbl: UILabel!
    @IBOutlet weak var submittedLbl: UILabel!
    @IBOutlet weak var submittedDicLbl: UILabel!
    @IBOutlet weak var submittedImg: UIImageView!
    
    @IBOutlet weak var acceptedDateLbl: UILabel!
    @IBOutlet weak var acceptedTimeLbl: UILabel!
    @IBOutlet weak var acceptedLbl: UILabel!
    @IBOutlet weak var acceptedDicLbl: UILabel!
    @IBOutlet weak var acceptedImg: UIImageView!
    @IBOutlet weak var mapViewOne: MKMapView!
    
    @IBOutlet weak var readyTimeLbl: UILabel!
    @IBOutlet weak var readyDateLbl: UILabel!
    @IBOutlet weak var readyLbl: UILabel!
    @IBOutlet weak var readyDicLbl: UILabel!
    @IBOutlet weak var readyImg: UIImageView!
    @IBOutlet weak var orderStatusMapBtn: UIButton!
    @IBOutlet weak var orderSummeryMapBtn: UIButton!
    @IBOutlet weak var deliveryTimeLbl: UILabel!
    @IBOutlet weak var deliveryDateLbl: UILabel!
    @IBOutlet weak var deliveryToLbl: UILabel!
    @IBOutlet weak var deliveryAddressLbl: UILabel!
    @IBOutlet weak var deliveryToPackageImg: UIImageView!
    @IBOutlet weak var sideLblOne: UILabel!
    @IBOutlet weak var sideLblTwo: UILabel!
    @IBOutlet weak var orderStatusViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var onTheWayTimeLbl: UILabel!
    @IBOutlet weak var onTheWayDateLbl: UILabel!
    @IBOutlet weak var onTheWayDescLbl: UILabel!
    @IBOutlet weak var onTheWayMapBtn: UIButton!
    @IBOutlet weak var onTheWayViewHeight: NSLayoutConstraint!
    
    //Order Summary OutLets
    @IBOutlet weak var orderSummeryView: UIView!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var billNoLbl: UILabel!
    @IBOutlet weak var OrderDeliveryDateLbl: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    @IBOutlet weak var couponDiscountLbl: UILabel!
    @IBOutlet weak var vatLbl: UILabel!
    @IBOutlet weak var itemsTotalLbl: UILabel!
    @IBOutlet weak var vatPercentageLbl: UILabel!
    @IBOutlet weak var deliveryChargeLbl: UILabel!
    @IBOutlet weak var subTotalLbl: UILabel!
    
    @IBOutlet weak var billNoNameLbl: UILabel!
    @IBOutlet weak var pickupDateNameLbl: UILabel!
    @IBOutlet weak var itemTotalNameLbl: UILabel!
    @IBOutlet weak var subTotalNameLbl: UILabel!
    @IBOutlet weak var couponDiscountNameLBl: UILabel!
    @IBOutlet weak var totalNameLbl: UILabel!
    
    @IBOutlet weak var orderSummeryDeliveryAddressLbl: UILabel!
    @IBOutlet weak var serviceChargeLbl: UILabel!
    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var itemsTableViewHeight: NSLayoutConstraint!

    @IBOutlet weak var storeNameLbl: UILabel!
//    @IBOutlet weak var cancelBtn: CustomButton!
//    @IBOutlet weak var favouriteBtn: CustomButton!
    var OrderType:Int = 0
    var orderId = Int()
    var orderFrom = Int()
    var statusInt:Int = 0
    var apiLatitude:Double = 0.0
    var apiLongtitude:Double = 0.0
    var locationManager: Location!
    var branchName = "EAT EGYPT"
    let regionRadius: CLLocationDistance = 3000
    var initialLocation:CLLocation!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        if AppDelegate.getDelegate().appLanguage == "English"{
            orderNameCollectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            orderNameCollectionView.semanticContentAttribute = .forceRightToLeft
        }
        
        orderNameCollectionView.delegate = self
        orderNameCollectionView.dataSource = self
        itemsTableView.delegate = self
        itemsTableView.dataSource = self
        
        mapView.layer.cornerRadius = 5
        mapViewOne.layer.cornerRadius = 5
        orderSummeryMapBtn.layer.cornerRadius = 5
        orderStatusMapBtn.layer.cornerRadius = 5
        cancelBtnHeight.constant = 0
        ratingViewHeight.constant = 0
        ratingView.delegate = self
        
        titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ORDER", value: "", table: nil))!
        billNoNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Bill No", value: "", table: nil))!
        pickupDateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pickup Date", value: "", table: nil))!
        itemTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Item Total", value: "", table: nil))!
        subTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sub Total", value: "", table: nil))!
        couponDiscountNameLBl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupon Discount", value: "", table: nil))!
        totalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total", value: "", table: nil))!
        cancelBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancel", value: "", table: nil))!, for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(getOrderTrackingDetrails), name: NSNotification.Name(rawValue: "getOrderTrackingDetails"), object: nil)

    }
    @objc func getOrderTrackingDetrails(){
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "getOrderTrackingDetails"), object: nil)
        DispatchQueue.main.async {
           self.getOrderTrackingService()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.getOrderTrackingService()
    }
    override func viewWillDisappear(_ animated: Bool) {
        ANLoader.hide()
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "getOrderTrackingDetails"), object: nil)
    }
    override func viewWillLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
        mapViewOne.setRegion(coordinateRegion, animated: true)
    }
    @IBAction func storeCallBtn_Tapped(_ sender: Any) {
        if let url = URL(string: "tel://\(SaveAddressClass.getOrderTrackingMainArray[0].BranchMobileNo!)") {
            UIApplication.shared.open(url)
        }
    }
    @IBAction func ratingSkipBtn_Tapped(_ sender: Any) {
        self.viewSlideInFromTopToBottom(view: ratingMainView)
        ratingMainView.isHidden = true
        self.ratingViewHeight.constant = 0
    }
    @IBAction func onTheWayMapBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj : LiveTrackingVC! = (mainStoryBoard.instantiateViewController(withIdentifier: "LiveTrackingVC") as! LiveTrackingVC)
        obj.orderID = SaveAddressClass.getOrderTrackingMainArray[0].OrderId!
        obj.expectTime = DateConverts.convertStringToStringDates(inputDateStr: SaveAddressClass.getOrderTrackingMainArray[0].ExpectedTime!, inputDateformatType: .dateNTimeSS, outputDateformatType: .timeA)
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func getOrderTrackingService(){
        ANLoader.showLoading("Loading..", disableUI: true)
        let jsonDic = ["UserId":"\(UserDef.getUserId())", "OrderId":"\(orderId)"]
        OrderApiRouter.getOrderTracking(dic: jsonDic, success: { (data) in
            if data?.status == false{
                DispatchQueue.main.async {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                }
                self.navigationController?.popViewController(animated: true)
                return
            }else{
                SaveAddressClass.getOrderTrackingMainArray.removeAll()
                
                for Dic in data!.successDic! {
                    let dic = Dic.OrderMain!
                    let OrderDic = OrderMainModel(
                        OrderId: dic["OrderId"] is NSNull ? 0 : dic["OrderId"] as! Int,
                        BranchId:  dic["BranchId"] is NSNull ? 0 : dic["BranchId"] as! Int,
                        BranchName_En:  dic["BranchName_En"] is NSNull ? "" : dic["BranchName_En"] as! String,
                        BranchName_Ar: dic["BranchName_Ar"] is NSNull ? "" : dic["BranchName_Ar"] as! String,
                        BrandId: dic["BrandId"] is NSNull ? 0 : dic["BrandId"] as! Int,
                        BrandName_En: dic["BrandName_En"] is NSNull ? "" : dic["BrandName_En"] as! String,
                        BrandName_Ar: dic["BrandName_Ar"] is NSNull ? "" :dic["BrandName_Ar"] as! String,
                        SubTotal:dic["SubTotal"] is  NSNull ? 0 :dic["SubTotal"] as! Double,
                        VatCharges:dic["VatCharges"] is  NSNull ? 0 :dic["VatCharges"] as! Double,
                        VatPercentage:dic["VatPercentage"] is  NSNull ? 0 :dic["VatPercentage"] as! Double,
                        TotalPrice:dic["TotalPrice"] is  NSNull ? 0 :dic["TotalPrice"] as! Double,
                        PaymentMode:dic["PaymentMode"] is NSNull ? "" :dic["PaymentMode"] as! String,
                        PaymentType:dic["PaymentType"] is NSNull ? 0 :dic["PaymentType"] as! Int,
                        InvoiceNo: dic["InvoiceNo"] is NSNull ? "" :dic["InvoiceNo"] as! String,
                        OrderMode_En:dic["OrderMode_En"] is NSNull ? "" :dic["OrderMode_En"] as! String,
                        OrderMode_Ar:dic["OrderMode_Ar"] is NSNull ? "" :dic["OrderMode_Ar"] as! String,
                        OrderType:dic["OrderType"] is NSNull ? 0 :dic["OrderType"] as! Int,
                        OrderStatus:dic["OrderStatus"] is NSNull ? "" :dic["OrderStatus"] as! String,
                        ExpectedTime:dic["ExpectedTime"] is NSNull ? "" :dic["ExpectedTime"] as! String,
                        DeliveryCharges: dic["DeliveryCharges"] is NSNull ? 0 :dic["DeliveryCharges"] as! Double,
                        BrandLogo: dic["BrandLogo"] is NSNull ? "" :dic["BrandLogo"] as! String,
                        BranchLatitude: dic["BranchLatitude"] is NSNull ? 0 :dic["BranchLatitude"] as! Double,
                        BranchLongitude: dic["BranchLongitude"] is NSNull ? 0 :dic["BranchLongitude"] as! Double,
                        BranchAddress: dic["BranchAddress"] is NSNull ? "" :dic["BranchAddress"] as! String,
                        BranchMobileNo:  dic["BranchMobileNo"] is NSNull ? "0" :dic["BranchMobileNo"] as! String,
                        IsFavourite: dic["IsFavourite"] is NSNull ? false :dic["IsFavourite"] as! Bool,
                        Rating: dic["Rating"] is NSNull ? 0.0 :dic["Rating"] as! Float,
                        BrandImage: dic["BrandImage"] is NSNull ? "" :dic["BrandImage"] as! String,
                        TotalItemsQty: dic["TotalItemsQty"] is NSNull ? 0 : dic["TotalItemsQty"] as! Int,
                        CouponAmount:dic["CouponAmount"] is  NSNull ? 0 :dic["CouponAmount"] as! Double,
                        UserAddress: dic["UserAddress"] is NSNull ? "" :dic["UserAddress"] as! String,
                        UserLatitude: dic["UserLatitude"] is NSNull ? 0 :dic["UserLatitude"] as! Double,
                        UserLongitude: dic["UserLongitude"] is NSNull ? 0 :dic["UserLongitude"] as! Double
                        
                    )
                    SaveAddressClass.getOrderTrackingMainArray.append(OrderDic)
                    let OrderItems = Dic.OrderItems!
                    SaveAddressClass.getOrderTrackingItemsArray.removeAll()
                    for itemsDic in OrderItems {
                        let dic = OrderItemsModel(
                            ItemId: itemsDic["ItemId"] is NSNull ? 0 : itemsDic["ItemId"] as! Int,
                            ItemName_En:itemsDic["ItemName_En"] is NSNull ? "" : itemsDic["ItemName_En"] as! String,
                            ItemName_Ar: itemsDic["ItemName_Ar"] is NSNull ? "" : itemsDic["ItemName_Ar"] as! String,
                            ItemPrice: itemsDic["ItemPrice"] is NSNull ? 0 : itemsDic["ItemPrice"] as! Double,
                            Quantity: itemsDic["Quantity"] is NSNull ? 0 : itemsDic["Quantity"] as! Int,
                            ItemSizeName_En: itemsDic["ItemSizeName_En"] is NSNull ? "" : itemsDic["ItemSizeName_En"] as! String,
                            ItemSizeName_Ar: itemsDic["ItemSizeName_Ar"] is NSNull ? "" : itemsDic["ItemSizeName_Ar"] as! String,
                            SizeId: itemsDic["SizeId"] is NSNull ? 0 : itemsDic["SizeId"] as! Int,
                            ItemImage: itemsDic["ItemImage"] is NSNull ? "" : itemsDic["ItemImage"] as! String,
                            itemcomment: itemsDic["ItemComment"] is NSNull ? "" : itemsDic["ItemComment"] as! String,
                            AdditionalItems: self.myAdditionalArrayFunc(inputArray: itemsDic["AdditionalItems"] as! Array<AnyObject> ) as! [AdditionalItemsModel]
                        )
                        SaveAddressClass.getOrderTrackingItemsArray.append(dic)
                    }
                    
                    self.itemsTableView.reloadData()
                    self.OrderDetailsMethod()
                    
                    // Tracking Order
                    self.acceptedDateLbl.text = ""
                    self.readyDateLbl.text = ""
                    self.deliveryDateLbl.text = ""
                    self.submittedDateLbl.text = ""
                    self.acceptedTimeLbl.text = ""
                    self.readyTimeLbl.text = ""
                    self.deliveryTimeLbl.text = ""
                    self.submittedTimeLbl.text = ""
                    self.onTheWayDateLbl.text = ""
                    self.onTheWayTimeLbl.text = ""

                    //let datetime = self.dateConveration_MMDDhhmma(str: dic["OrderDate"] as! String)
                    
                    if SaveAddressClass.getOrderTrackingMainArray[0].OrderMode_En == "Take Away/CarryOut"{
                        self.orderStatusViewHeight.constant = 500
                        self.onTheWayViewHeight.constant = 0
                    }else if SaveAddressClass.getOrderTrackingMainArray[0].OrderMode_En == "Delivery"{
                        self.orderStatusViewHeight.constant = 620
                        self.onTheWayViewHeight.constant = 120
                    }
                
                     if  SaveAddressClass.getOrderTrackingMainArray[0].OrderStatus == "New" || SaveAddressClass.getOrderTrackingMainArray[0].OrderStatus == "Accepted" {
                        self.cancelBtnHeight.constant = 50
                    }
                    let dateArray = (dic["OrderDate"] as! String).split(separator: ".")
                    if dateArray.count > 0{
                        self.submittedTimeLbl.text = "\(self.dateConveration(str:String(dateArray[0])))"
                    }
                    //self.submittedTimeLbl.text = "\(datetime)"
                    let submittedDate = self.dateConveration2(str: dic["OrderDate"] as! String)
                    self.submittedDateLbl.text = "\(submittedDate)"
                    for OrderTrackingDic in Dic.TrackingDetails! {
                        if (OrderTrackingDic["Received"] != nil)  {
                            let array = OrderTrackingDic["Received"] as! [Any]
                            let dic = array[0] as! [String:Any]
                            let timeArray = (dic["TrackingTime"] as! String).split(separator: ".")
                            if timeArray.count > 0{
                                self.submittedTimeLbl.text = "\(self.timeConveration(str:String(timeArray[0])))"
                            }
                            self.statusInt = self.statusInt > 0 ? self.statusInt : 1
                            if  self.statusInt == 1 {
                                let datetime = self.dateConveration2(str: dic["TrackingTime"] as! String)
                                self.submittedDateLbl.text = "\(datetime)"
                            }
                        }else if (OrderTrackingDic["Accepted"] != nil) {
                            self.acceptedLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Accepted", value: "", table: nil))!  //Waiting for ‏Acceptance
                            self.acceptedDicLbl.isHidden = false
                            let array = OrderTrackingDic["Accepted"] as! [Any]
                            let dic = array[0] as! [String:Any]
                            
                            let timeArray = (dic["TrackingTime"] as! String).split(separator: ".")
                            if timeArray.count > 0{
                                self.acceptedTimeLbl.text = "\(self.timeConveration(str:String(timeArray[0])))"
                            }
                            if SaveAddressClass.getOrderTrackingMainArray[0].OrderStatus! == "Cancel"{
                                self.statusInt = self.statusInt > 5 ? self.statusInt : 5
                                if self.statusInt == 5 {
                                    let datetime = self.dateConveration2(str: dic["TrackingTime"] as! String)
                                    self.acceptedDateLbl.text = "\(datetime)"
                                }
                            }else{
                                self.statusInt = self.statusInt > 2 ? self.statusInt : 2
                                if self.statusInt == 2 {
                                    let datetime = self.dateConveration2(str: dic["TrackingTime"] as! String)
                                    self.acceptedDateLbl.text = "\(datetime)"
                                }
                            }
                            self.acceptedImg.image = UIImage(named: "accept")
                        }else if (OrderTrackingDic["Ready"] != nil) {
                            self.readyLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ready", value: "", table: nil))! // Preparing
                            self.readyDicLbl.isHidden = false

                            if  SaveAddressClass.getOrderTrackingMainArray[0].OrderStatus != "Cancel"  {
                                let array = OrderTrackingDic["Ready"] as! [Any]
                                let dic = array[0] as! [String:Any]
                                let timeArray = (dic["TrackingTime"] as! String).split(separator: ".")
                                if timeArray.count > 0{
                                    self.readyTimeLbl.text = "\(self.timeConveration(str:String(timeArray[0])))"
                                }
                                self.statusInt = self.statusInt > 3 ? self.statusInt : 3
                                if self.statusInt == 3 {
                                    let datetime = self.dateConveration2(str: dic["TrackingTime"] as! String)
                                    self.readyDateLbl.text = "\(datetime)"
                                }
                            }else{
                                self.readyTimeLbl.isHidden = true
                                self.readyDateLbl.isHidden = true
                            }
                        }else if (OrderTrackingDic["Picked"] != nil) {
                            self.onTheWayDescLbl.isHidden = false
                            self.onTheWayMapBtn.isHidden = false
                            if  SaveAddressClass.getOrderTrackingMainArray[0].OrderStatus != "Cancel"  {
                                let array = OrderTrackingDic["Picked"] as! [Any]
                                let dic = array[0] as! [String:Any]
                                let timeArray = (dic["TrackingTime"] as! String).split(separator: ".")
                                if timeArray.count > 0{
                                    self.onTheWayTimeLbl.text = "\(self.timeConveration(str:String(timeArray[0])))"
                                }
                                self.statusInt = self.statusInt > 3 ? self.statusInt : 3
                                if  self.statusInt == 3 {
                                    let datetime = self.dateConveration2(str: dic["TrackingTime"] as! String)
                                    self.onTheWayDateLbl.text = "\(datetime)"
                                }
                            }else{
                                self.onTheWayTimeLbl.isHidden = true
                                self.onTheWayDateLbl.isHidden = true
                            }
                        }else if (OrderTrackingDic["Close"] != nil) {
                            self.onTheWayMapBtn.isHidden = true
                            if  SaveAddressClass.getOrderTrackingMainArray[0].OrderStatus != "Cancel"  {
                                let array = OrderTrackingDic["Close"] as! [Any]
                                let dic = array[0] as! [String:Any]
                                let timeArray = (dic["TrackingTime"] as! String).split(separator: ".")
                                if timeArray.count > 0{
                                    self.deliveryTimeLbl.text = "\(self.timeConveration(str:String(timeArray[0])))"
                                }
                                self.statusInt = self.statusInt > 4 ? self.statusInt : 4
                                if  self.statusInt == 4 {
                                    let datetime = self.dateConveration2(str: dic["TrackingTime"] as! String)
                                    self.deliveryDateLbl.text = "\(datetime)"
                                }
                                
                                if SaveAddressClass.getOrderTrackingMainArray[0].Rating! > 0{
                                    self.ratingMainView.isHidden = true
                                    self.ratingViewHeight.constant = 0
                                }else{
                                    self.ratingView.rating = Double(SaveAddressClass.getOrderTrackingMainArray[0].Rating!)
                                    self.ratingMainView.isHidden = false
                                    self.ratingViewHeight.constant = 90
                                }
                                
                            }else{
                                self.deliveryTimeLbl.isHidden = true
                                self.deliveryDateLbl.isHidden = true
                            }
                            if SaveAddressClass.getOrderTrackingMainArray[0].OrderMode_En == "Take Away/CarryOut"{
                                self.orderSummeryDeliveryAddressLbl.text! = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Served", value: "", table: nil))!
                                self.addressLbl.text = SaveAddressClass.getOrderTrackingMainArray[0].BranchAddress!.capitalized
                            }else if SaveAddressClass.getOrderTrackingMainArray[0].OrderMode_En == "Delivery"{
                                self.orderSummeryDeliveryAddressLbl.text! = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivered", value: "", table: nil))!
                                self.addressLbl.text = SaveAddressClass.getOrderTrackingMainArray[0].UserAddress!.capitalized
                            }
                        }else if (OrderTrackingDic["Delivered"] != nil) {
                            self.onTheWayMapBtn.isHidden = true
                            if  SaveAddressClass.getOrderTrackingMainArray[0].OrderStatus != "Cancel"  {
                                let array = OrderTrackingDic["Delivered"] as! [Any]
                                let dic = array[0] as! [String:Any]
                                let timeArray = (dic["TrackingTime"] as! String).split(separator: ".")
                                if timeArray.count > 0{
                                    self.deliveryTimeLbl.text = "\(self.timeConveration(str:String(timeArray[0])))"
                                }
                                self.statusInt = self.statusInt > 4 ? self.statusInt : 4
                                if  self.statusInt == 4 {
                                    let datetime = self.dateConveration2(str: dic["TrackingTime"] as! String)
                                    self.deliveryDateLbl.text = "\(datetime)"
                                }
                                
                                if SaveAddressClass.getOrderTrackingMainArray[0].Rating! > 0{
                                    self.ratingMainView.isHidden = true
                                    self.ratingViewHeight.constant = 0
                                }else{
                                    self.ratingView.rating = Double(SaveAddressClass.getOrderTrackingMainArray[0].Rating!)
                                    self.ratingMainView.isHidden = false
                                    self.ratingViewHeight.constant = 90
                                }
                                
                            }else{
                                self.deliveryTimeLbl.isHidden = true
                                self.deliveryDateLbl.isHidden = true
                            }
                            if SaveAddressClass.getOrderTrackingMainArray[0].OrderMode_En == "Take Away/CarryOut"{
                                self.orderSummeryDeliveryAddressLbl.text! = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Served", value: "", table: nil))!
                                self.addressLbl.text = SaveAddressClass.getOrderTrackingMainArray[0].BranchAddress!.capitalized
                            }else if SaveAddressClass.getOrderTrackingMainArray[0].OrderMode_En == "Delivery"{
                                self.orderSummeryDeliveryAddressLbl.text! = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivered", value: "", table: nil))!
                                self.addressLbl.text = SaveAddressClass.getOrderTrackingMainArray[0].UserAddress!.capitalized
                            }
                        }else if (OrderTrackingDic["Cancel"] != nil) {
                            let array = OrderTrackingDic["Cancel"] as! [Any]
                            let dic = array[0] as! [String:Any]
                            let timeArray = (dic["TrackingTime"] as! String).split(separator: ".")
                            let datetime = self.dateConveration2(str: dic["TrackingTime"] as! String)

                            if Dic.TrackingDetails!.count == 3 {
                                //if timeArray.count > 0{
                                self.readyTimeLbl.text = "\(self.timeConveration(str:String(timeArray[0])))"
                                self.readyDateLbl.text = "\(datetime)"
                                self.readyDicLbl.text = "\(dic["CancelReason"] as! String)"
                                self.readyLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancel", value: "", table: nil))!
                                self.readyImg.image = UIImage(named: "not_done")
                                self.orderStatusViewHeight.constant = 375
                                //}
                            }else{
                                self.acceptedTimeLbl.text = "\(self.timeConveration(str:String(timeArray[0])))"
                                self.acceptedDateLbl.text = "\(datetime)"
                                self.acceptedDicLbl.text = "\(dic["CancelReason"] as! String)"
                                self.acceptedLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancel", value: "", table: nil))!
                                self.acceptedImg.image = UIImage(named: "not_done")
                                self.orderStatusViewHeight.constant = 250
                                
                                // Hide
                                self.readyTimeLbl.isHidden = true
                                self.readyDateLbl.isHidden = true
                                self.readyImg.isHidden = true
                                self.sideLblOne.isHidden = true
                                self.readyDicLbl.isHidden = true
                                self.readyLbl.isHidden = true
                            }
                            // Hide
                            self.onTheWayViewHeight.constant = 0
                            self.deliveryTimeLbl.isHidden = true
                            self.deliveryToPackageImg.isHidden = true
                            self.deliveryDateLbl.isHidden = true
                            self.mapViewOne.isHidden = true
                            self.orderStatusMapBtn.isHidden = true
                            self.sideLblTwo.isHidden = true
                            self.deliveryToLbl.isHidden = true
                            self.deliveryAddressLbl.isHidden = true
                            

                            self.statusInt = self.statusInt > 5 ? self.statusInt : 5
                            if  self.statusInt == 5 {
//                                let datetime = self.dateConveration_MMDDhhmma(str: dic["TrackingTime"] as! String)
//                                self.orderStatusLbl.text = "Order Cancel on \(datetime) by \(dic["AcceptedBy"] as! String)"
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    ANLoader.hide()
                }
            }
        }) { (error) in
            DispatchQueue.main.async {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            self.navigationController?.popViewController(animated: true)
        }
    }
    // Items MVC
    func myAdditionalArrayFunc(inputArray:Array<AnyObject>) -> Array<AnyObject> {
        var itemsArray = [AdditionalItemsModel]()
        for Dic in inputArray as! [Dictionary<String, Any>] {
            let additionalsDic = AdditionalItemsModel(
                AddItemId:Dic["AddItemId"] is NSNull ? 0 : Dic["AddItemId"] as! Int,
                AddItem_En:Dic["AddItem_En"] is NSNull ? "" : Dic["AddItem_En"] as! String,
                AddItem_Ar:Dic["AddItem_Ar"] is NSNull ? "" : Dic["AddItem_Ar"] as! String,
                AddItemPrice:Dic["AddItemPrice"] is NSNull ? 0 : Dic["AddItemPrice"] as! Double,
                AddQuantity:Dic["AddQuantity"] is NSNull ? 0 : Dic["AddQuantity"] as! Int
            )
            itemsArray.append(additionalsDic)
        }
        return itemsArray
    }
    @objc func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "RattingViewVC") as! RattingViewVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.OrderId = SaveAddressClass.getOrderTrackingMainArray[0].OrderId!
        obj.orderType = SaveAddressClass.getOrderTrackingMainArray[0].OrderType!
        if AppDelegate.getDelegate().appLanguage == "English"{
            obj.StoreName = SaveAddressClass.getOrderTrackingMainArray[0].BrandName_En!.capitalized
        }else{
            obj.StoreName = SaveAddressClass.getOrderTrackingMainArray[0].BrandName_Ar!.capitalized
        }
        obj.modalPresentationStyle = .overFullScreen
        present(obj, animated: true, completion: nil)
        
        // Hide Rarting
        self.viewSlideInFromTopToBottom(view: ratingMainView)
        ratingMainView.isHidden = true
        self.ratingViewHeight.constant = 0
    }
    func OrderDetailsMethod()  {
//        submittedDicLbl.isUserInteractionEnabled = true
//        submittedDicLbl.text = "Your order has been send to \(branchName)"
//        let text = (submittedDicLbl.text)!
//        let underlineAttriString = NSMutableAttributedString(string: text)
//        let range = (text as NSString).range(of: "\(branchName)")
//        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Helvetica-Bold", size: 14.0)!, range: range)
//        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: range)
//        submittedDicLbl.attributedText = underlineAttriString
        
        submittedLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Submitted", value: "", table: nil))!
        acceptedLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Waiting for ‏Acceptance", value: "", table: nil))!  //Accepted
        acceptedDicLbl.isHidden = true
        readyDicLbl.isHidden = true
        onTheWayDescLbl.isHidden = true
        onTheWayMapBtn.isHidden = true

        readyLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Preparing", value: "", table: nil))! // Ready
        acceptedDicLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order is being processed", value: "", table: nil))!
        readyDicLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order is ready for delivery", value: "", table: nil))!
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            branchName = SaveAddressClass.getOrderTrackingMainArray[0].BrandName_En!.capitalized
            self.storeNameLbl.text = SaveAddressClass.getOrderTrackingMainArray[0].BrandName_En!.capitalized
            let dateArray = SaveAddressClass.getOrderTrackingMainArray[0].ExpectedTime!.split(separator: ".")
            if dateArray.count > 0{
                self.OrderDeliveryDateLbl.text = "\(self.estimatedateConveration(str:String(dateArray[0])))"
            }
        }else{
            branchName = SaveAddressClass.getOrderTrackingMainArray[0].BrandName_Ar!.capitalized
            self.storeNameLbl.text = SaveAddressClass.getOrderTrackingMainArray[0].BrandName_Ar!.capitalized
            let dateArray = SaveAddressClass.getOrderTrackingMainArray[0].ExpectedTime!.split(separator: ".")
            if dateArray.count > 0{
                self.OrderDeliveryDateLbl.text = "\(self.estimatedateConveration(str:String(dateArray[0])))"
            }
        }
        self.vatPercentageLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "VAT", value: "", table: nil))!) (\(Int(SaveAddressClass.getOrderTrackingMainArray[0].VatPercentage!))%)"
        self.deliveryChargeLbl.text = "+ \(SaveAddressClass.getOrderTrackingMainArray[0].DeliveryCharges!.withCommas())"
        self.itemsTotalLbl.text = "\(SaveAddressClass.getOrderTrackingMainArray[0].SubTotal!.withCommas())"
        self.vatLbl.text = "+ \(SaveAddressClass.getOrderTrackingMainArray[0].VatCharges!.withCommas())"
        self.couponDiscountLbl.text = "- \(SaveAddressClass.getOrderTrackingMainArray[0].CouponAmount!.withCommas())"
        
        let att = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14),NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.1298420429, green: 0.1298461258, blue: 0.1298439503, alpha: 0.9028788527)] as [NSAttributedString.Key : Any]
        let boldBranchName = NSMutableAttributedString(string:branchName, attributes:att)
        let text = NSMutableAttributedString(string: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your order has been send to", value: "", table: nil))!)
        let combination = NSMutableAttributedString()
        combination.append(text)
        combination.append(boldBranchName)
        self.submittedDicLbl.attributedText = combination
        
        if SaveAddressClass.getOrderTrackingMainArray[0].OrderMode_En == "Take Away/CarryOut"{
            orderSummeryDeliveryAddressLbl.text! = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PickUp From", value: "", table: nil))!
            serviceChargeLbl.text! = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Service Charge", value: "", table: nil))!
            deliveryToLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Served to", value: "", table: nil))!
            pickupDateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pickup Date", value: "", table: nil))!
            self.deliveryAddressLbl.text = SaveAddressClass.getOrderTrackingMainArray[0].BranchAddress!.capitalized
            self.addressLbl.text = SaveAddressClass.getOrderTrackingMainArray[0].BranchAddress!.capitalized
            initialLocation = CLLocation(latitude: SaveAddressClass.getOrderTrackingMainArray[0].BranchLatitude!, longitude: SaveAddressClass.getOrderTrackingMainArray[0].BranchLongitude!)
            //centerMapOnLocation(location: initialLocation)

        }else if SaveAddressClass.getOrderTrackingMainArray[0].OrderMode_En == "Delivery" {
            orderSummeryDeliveryAddressLbl.text! = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivered to", value: "", table: nil))!
            serviceChargeLbl.text! = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Charge", value: "", table: nil))!
            deliveryToLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivered to", value: "", table: nil))!
            pickupDateNameLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Date", value: "", table: nil))!) :"
            self.deliveryAddressLbl.text = SaveAddressClass.getOrderTrackingMainArray[0].UserAddress!.capitalized
            self.addressLbl.text = SaveAddressClass.getOrderTrackingMainArray[0].UserAddress!.capitalized
            initialLocation = CLLocation(latitude: SaveAddressClass.getOrderTrackingMainArray[0].UserLatitude!, longitude: SaveAddressClass.getOrderTrackingMainArray[0].UserLongitude!)
            
            //centerMapOnLocation(location: initialLocation)

        }
        
        self.billNoLbl.text = "#\(SaveAddressClass.getOrderTrackingMainArray[0].InvoiceNo!)"
        let subTotal = SaveAddressClass.getOrderTrackingMainArray[0].SubTotal! + SaveAddressClass.getOrderTrackingMainArray[0].VatCharges! + SaveAddressClass.getOrderTrackingMainArray[0].DeliveryCharges!
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            self.totalPriceLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(SaveAddressClass.getOrderTrackingMainArray[0].TotalPrice!.withCommas())"
        }else{
            self.totalPriceLbl.text = "\(SaveAddressClass.getOrderTrackingMainArray[0].TotalPrice!.withCommas()) SAR"
        }
        
        self.subTotalLbl.text = "\(subTotal.withCommas())"
        if let _ = initialLocation {
            centerMapOnLocation(location: initialLocation)
        }
        
        
//        if SaveAddressClass.getOrderTrackingMainArray[0].IsFavourite == true  {
//            self.favouriteBtn .setTitle("REMOVE AS FAVOURITE", for: .normal)
//        }else{
//            self.favouriteBtn .setTitle("MARK AS FAVOURITE", for: .normal)
//        }
//        self.cancelBtn.isHidden = false
//        if  SaveAddressClass.getOrderTrackingMainArray[0].OrderStatus == "Cancel" ||  SaveAddressClass.getOrderTrackingMainArray[0].OrderStatus == "Close" {
//            self.cancelBtn .setTitle("REORDER", for: .normal)
//        }else{
//            if  SaveAddressClass.getOrderTrackingMainArray[0].OrderStatus == "New"  || SaveAddressClass.getOrderTrackingMainArray[0].OrderStatus == "Accepted" {
//                self.cancelBtn .setTitle("CANCEL", for: .normal)
//            }else{
//                self.cancelBtn.isHidden = true
//                self.cancelBtn .setTitle("REORDER", for: .normal)
//            }
//        }
        self.itemsTableViewHeight.constant = self.tableViewHeight - 10
    }
    var tableViewHeight: CGFloat {
        itemsTableView.layoutIfNeeded()
        return itemsTableView.contentSize.height
    }
    func dateConveration(str:String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //2018-10-09T12:42:00
        dateFormatterGet.locale = Locale(identifier: "EN")

        let date =  dateFormatterGet.date(from: str)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy" //12:42 PM
        dateFormatter.locale = Locale(identifier: "EN")

        return "\(dateFormatter.string(from: date!))"
    }
    
    func estimatedateConveration(str:String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //2018-10-09T12:42:00
        dateFormatterGet.locale = Locale(identifier: "EN")
        let date =  dateFormatterGet.date(from: str)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a" //12:42 PM
        dateFormatter.locale = Locale(identifier: "EN")
        
//        let dateFormatter1 = DateFormatter()
//        dateFormatter1.dateFormat = "hh:mm a" //12:42 PM
//        dateFormatter1.locale = Locale(identifier: "EN")
        
        return "\(dateFormatter.string(from: date!))"
    }
    func timeConveration(str:String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //2018-10-09T12:42:00
        dateFormatterGet.locale = Locale(identifier: "EN")

        let date =  dateFormatterGet.date(from: str)
        
        let dateFormatterGet2 = DateFormatter()
        dateFormatterGet2.dateFormat = "hh:mm a" //12:42 PM
        dateFormatterGet2.locale = Locale(identifier: "EN")

        return "\(dateFormatterGet2.string(from: date!))"
    }
    func dateConveration_MMDDhhmma(str:String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatterGet.locale = Locale(identifier: "EN")
//2018-10-08T10:35:57.713
        let date =  dateFormatterGet.date(from: str)
        
        let dateFormatterGet2 = DateFormatter()
        dateFormatterGet2.dateFormat = "MMM dd hh:mm a" //12:42 PM
        dateFormatterGet2.locale = Locale(identifier: "EN")
        return "\(dateFormatterGet2.string(from: date!))"
    }
    func dateConveration2(str:String) -> String {
        let date = str.components(separatedBy: ".").dropLast().joined(separator: ".")
    
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //2018-10-08T10:35:57
        dateFormatterGet.locale = Locale(identifier: "EN")
        var dateGet =  dateFormatterGet.date(from: date)
        if dateGet == nil{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //2018-10-08T10:35:57
            dateFormatterGet.locale = Locale(identifier: "EN")

            dateGet =  dateFormatterGet.date(from: str)
        }
        
        let dateFormatterGet2 = DateFormatter()
        dateFormatterGet2.dateFormat = "dd-MM-yyy" //12:42 PM
        dateFormatterGet2.locale = Locale(identifier: "EN")

        return "\(dateFormatterGet2.string(from: dateGet!))"
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        if orderFrom == 1{
          //  HomeVC.instance.getStoresServiceCall = true
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: false)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func CancelOrderBtn_Tapped(_ sender: Any) {
        DispatchQueue.main.async {
            ANLoader.showLoading("Loading..", disableUI: true)
            let jsonDic = ["OrderId":"\(self.orderId)"]
            OrderApiRouter.cancelOrder(dic: jsonDic, success: { (data) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if data?.status == false{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                        
                    }
                }else{
                    self.cancelBtnHeight.constant = 0
                    self.getOrderTrackingService()
                }
            }) { (error) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
        }
    }
    @IBAction func orderStatusMapBtn_Tapped(_ sender: Any) {
        // Location Manager
        locationManager = Location()
        locationManager.needToDisplayAlert = true
        locationManager.delegate = self
    }
    @IBAction func orderSummaryMapBtn_Tapped(_ sender: Any) {
        // Location Manager
        locationManager = Location()
        locationManager.needToDisplayAlert = true
        locationManager.delegate = self
    }
//    @IBAction func cancelBtn_Tapped(_ sender: Any) {
//        if cancelBtn.titleLabel?.text == "REORDER"{
//            let count = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable")
//            if (count > 0){
//                let appearance = SCLAlertView.SCLAppearance(
//                    showCloseButton: false
//                )
//                let alertView = SCLAlertView(appearance: appearance)
//                alertView.addButton("Cancel") {
//                    return
//                }
//                alertView.addButton("Replace") {
//                    _ = DBObject.InsertOrderString(insertSql: "DELETE From OrderTable")
//                    //_ = DBObject.InsertOrderString(insertSql: "DELETE From StoreTable")
//                    _ = DBObject.InsertOrderString(insertSql: "DELETE From AdditionalTable")
//                    self.insertOrderMethod()
//                }
//                alertView.showInfo("Replace Cart?", subTitle: "There are items in your cart. Do you wish to replace them?")
//            }else{
//                self.insertOrderMethod()
//            }
//        }else {
//            let appearance = SCLAlertView.SCLAppearance(
//                showCloseButton: false
//            )
//            let alertView = SCLAlertView(appearance: appearance)
//            alertView.addButton("No") {
//                return
//            }
//            alertView.addButton("Yes") {
//                self.cancelOrderMethod()
//            }
//            alertView.showInfo("Cancel Order?", subTitle: "Are you sure cancel order")
//        }
//    }
    func insertOrderMethod() {
        for itemsDic in SaveAddressClass.getOrderTrackingItemsArray {
            var TotalAmount : Double = itemsDic.ItemPrice!
            for Dic  in itemsDic.AdditionalItems {
                let AddItemPrice = Dic.AddItemPrice! * Double(Dic.AddQuantity!)
                TotalAmount = TotalAmount + AddItemPrice
            }
            TotalAmount = TotalAmount * Double(itemsDic.Quantity!)
            let Sql = "INSERT INTO OrderTable (BranchId, CategoryId, ItemId, ItemImg, ItemName_En, ItemName_Ar, SizeId, Size_En, Size_Ar, SizePrice, Quantity, TotalAmount, Comments) VALUES (\(SaveAddressClass.getOrderTrackingMainArray[0].BranchId!),'0',\(itemsDic.ItemId!),'\(itemsDic.ItemImage!)','\(itemsDic.ItemName_En!)','\(itemsDic.ItemName_Ar!)',\(itemsDic.SizeId!),'\(itemsDic.ItemSizeName_En!)','\(itemsDic.ItemSizeName_Ar!)',\(itemsDic.ItemPrice!),\(itemsDic.Quantity!),\(TotalAmount),'');"
            _ = DBObject.InsertOrderString(insertSql: Sql)
            let orderId = DBObject.getMaxOrderId(Sql:"SELECT MAX(OrderId) from OrderTable")
            if itemsDic.AdditionalItems.count > 0 {
                for additionalDic  in itemsDic.AdditionalItems {
                    let AddTotalPrice = additionalDic.AddItemPrice! * Double(additionalDic.AddQuantity!)
                    let Sql = "INSERT INTO AdditionalTable (OrderId, AdditionalId, AddtionalName_En, AddtionalName_Ar, AddPrice, Quantity, AddTotalPrice) VALUES (\(orderId), \(additionalDic.AddItemId!), '\(additionalDic.AddItem_En!)', '\(additionalDic.AddItem_Ar!)', \(additionalDic.AddItemPrice!), \(additionalDic.AddQuantity!), \(AddTotalPrice));"
                    _ = DBObject.InsertOrderString(insertSql: Sql)
                }
            }
        }
        self.tabBarController?.selectedIndex = 3
    }
    func cancelOrderMethod() {
        
    }
    func viewSlideInFromTopToBottom(view: UIView) -> Void {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        view.layer.add(transition, forKey: kCATransition)
    }
//    @IBAction func favouriteBtn_Tapped(_ sender: Any) {
//        ANLoader.showLoading("Loading..", disableUI: true)
//
//        if SaveAddressClass.getOrderTrackingMainArray[0].IsFavourite == true  {
//
//            let jsonDic = ["UserId":"\(UserDef.getUserId())", "OrderId":"\(String(describing: SaveAddressClass.getOrderTrackingMainArray[0].OrderId!))",  "CommandType":"2", "IsFavourite":"False"] as [String : Any]
//
//            OrderApiRouter.favouriteOrder(dic: jsonDic, success: { (data) in
//                ANLoader.hide()
//                if data?.status == false{
//                    Alert.showAlert(on: self, title: Title, message: "\(data!.message!)")
//                    return
//                }else{
//                    SaveAddressClass.getOrderTrackingMainArray[0].IsFavourite = false
//                    self.favouriteBtn .setTitle("MARK AS FAVOURITE", for: .normal)
//                    //self.orderVCDelegate.didTapAction(orderId: self.orderId, ActionType: 2, IsFavourite:  SaveAddressClass.getOrderTrackingMainArray[0].IsFavourite!)
//                }
//            }){ (error) in
//                ANLoader.hide()
//                Alert.showAlert(on: self, title: Title, message: "\(error)")
//            }
//        }else{
//            let jsonDic = ["UserId":"\(UserDef.getUserId())", "OrderId":"\(String(describing: SaveAddressClass.getOrderTrackingMainArray[0].OrderId!))",  "CommandType":"2", "IsFavourite":"True"] as [String : Any]
//
//            OrderApiRouter.favouriteOrder(dic: jsonDic, success: { (data) in
//                ANLoader.hide()
//
//                if data?.status == false{
//                    Alert.showAlert(on: self, title: Title, message: "\(data!.message!)")
//                    return
//                }else{
//                    SaveAddressClass.getOrderTrackingMainArray[0].IsFavourite = true
//                    self.favouriteBtn .setTitle("REMOVE AS FAVOURITE", for: .normal)
//                    //self.orderVCDelegate.didTapAction(orderId: self.orderId, ActionType: 2, IsFavourite:  SaveAddressClass.getOrderTrackingMainArray[0].IsFavourite!)
//                }
//            }){ (error) in
//                ANLoader.hide()
//                Alert.showAlert(on: self, title: Title, message: "\(error)")
//            }
//        }
//    }
}
extension OrderVC : UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderCVCell", for: indexPath) as! OrderCVCell
        if AppDelegate.getDelegate().appLanguage == "English"{
            if indexPath.row == 0 {
                cell.nameLbl.text = "Order Status"
            }else if indexPath.row == 1 {
                cell.nameLbl.text = "Order Summary"
            }
        }else{
            if indexPath.row == 0 {
                cell.nameLbl.text = "حالة الطلب"
            }else if indexPath.row == 1 {
                cell.nameLbl.text = "ملخص الطلب"
            }
        }
        if OrderType ==  indexPath.row {
            cell.lineLbl.isHidden = false
        }else{
            cell.lineLbl.isHidden = true
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = self.orderNameCollectionView.bounds
        let screenWidth = screenSize.width
        return CGSize(width: screenWidth/2, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        OrderType = indexPath.row
        self.orderNameCollectionView.reloadData()
        if indexPath.row == 0 {
            orderStatusMainView.isHidden = false
            orderSummeryMainView.isHidden = true
        }else {
            itemsTableView.reloadData()
            orderStatusMainView.isHidden = true
            orderSummeryMainView.isHidden = false
        }
        self.itemsTableView.reloadData()
    }
}
extension OrderVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SaveAddressClass.getOrderTrackingItemsArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderSummaryTVCell", for: indexPath) as! OrderSummaryTVCell
        cell.lineLbl.isHidden = true
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.nameLbl.text = SaveAddressClass.getOrderTrackingItemsArray[indexPath.row].ItemName_En!.capitalized
        }else{
            cell.nameLbl.text = SaveAddressClass.getOrderTrackingItemsArray[indexPath.row].ItemName_Ar!.capitalized
        }
         cell.noteLbl.text = ""
        if let comment = SaveAddressClass.getOrderTrackingItemsArray[indexPath.row].itemcomment {
            if comment.count > 0 {
               cell.noteLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Note", value: "", table: nil))!): \(comment)"
            }
        }
       
        let Quantity = SaveAddressClass.getOrderTrackingItemsArray[indexPath.row].Quantity!
        cell.qtyLbl.text = "x\(Quantity)"
        let totalPrice:Double = Double(Quantity) * SaveAddressClass.getOrderTrackingItemsArray[indexPath.row].ItemPrice!
        cell.priceLbl.text = "SAR \(totalPrice.withCommas())"
        let imgStr:NSString = "\(url.ItemImg.imgPath())\(SaveAddressClass.getOrderTrackingItemsArray[indexPath.row].ItemImage! )" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let URLString = URL.init(string: urlStr as String)
        cell.itemImg.kf.indicatorType = .activity
        cell.itemImg.kf.setImage(with: URLString)
        cell.itemImg.layer.cornerRadius = 10
        self.itemsTableView.layoutIfNeeded()
        self.itemsTableViewHeight.constant = tableView.contentSize.height
        if SaveAddressClass.getOrderTrackingItemsArray.count > 1{
            cell.lineLbl.isHidden = false
        }
        return cell
    }
}
extension OrderVC : LocationDelegate {
    func didFailedLocation(error: String) {
        //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        let alert = UIAlertController(title: "Allow Location Access", message: "The Chef needs access to your location. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)

                   // Button to Open Settings
                   alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                       guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                           return
                       }
                       if UIApplication.shared.canOpenURL(settingsUrl) {
                           UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                               print("Settings opened: \(success)")
                           })
                       }
                   }))
                   alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                   self.present(alert, animated: true, completion: nil)
    }
    func didUpdateLocation(latitude: Double?, longitude: Double?) {
        apiLatitude = Double(latitude!)
        apiLongtitude = Double(longitude!)
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(apiLatitude),\(apiLongtitude)&zoom=14&views=traffic&q=\(SaveAddressClass.getOrderTrackingMainArray[0].BranchLatitude!),\(SaveAddressClass.getOrderTrackingMainArray[0].BranchLongitude!)")!, options: [:], completionHandler: nil)
        }else {
            print("Can't use comgooglemaps://")
            UIApplication.shared.open(URL(string:"http://maps.google.com/maps?q=\(apiLatitude),\(apiLongtitude)&zoom=14&views=traffic&q=\(SaveAddressClass.getOrderTrackingMainArray[0].BranchLatitude!),\(SaveAddressClass.getOrderTrackingMainArray[0].BranchLongitude!)")!, options: [:], completionHandler: nil)
        }
    }
}
