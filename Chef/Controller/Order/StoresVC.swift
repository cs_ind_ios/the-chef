//
//  StoresVC.swift
//  Chef
//
//  Created by RAVI on 22/07/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class StoresVC: UIViewController {

    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var storesTableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        storesTableView.delegate = self
        storesTableView.dataSource = self
        
        self.storesTableView.estimatedRowHeight = 109
        self.storesTableView.rowHeight = UITableView.automaticDimension
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            navigationBarTitleLbl.text = SaveAddressClass.selectBannerArray[0].BannerName_En!
        }else{
            navigationBarTitleLbl.text = SaveAddressClass.selectBannerArray[0].BannerName_Ar!
        }
    }
    
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension StoresVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SaveAddressClass.selectBannerArray[0].StoreDetails.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = storesTableView.dequeueReusableCell(withIdentifier: "StoresTVCell", for: indexPath) as! StoresTVCell
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.storeNameLbl.text = SaveAddressClass.selectBannerArray[0].StoreDetails[indexPath.row].BranchName_En?.capitalized
            cell.subStoreNameLbl.text = SaveAddressClass.selectBannerArray[0].StoreDetails[indexPath.row].StoreType!.capitalized
            cell.descriptionLbl.text = SaveAddressClass.selectBannerArray[0].StoreDetails[indexPath.row].BranchDescription_En?.capitalized
        }else{
            cell.storeNameLbl.text = SaveAddressClass.selectBannerArray[0].StoreDetails[indexPath.row].BranchName_Ar?.capitalized
            cell.subStoreNameLbl.text = SaveAddressClass.selectBannerArray[0].StoreDetails[indexPath.row].StoreType_Ar?.capitalized
            cell.descriptionLbl.text = SaveAddressClass.selectBannerArray[0].StoreDetails[indexPath.row].BranchDescription_Ar?.capitalized
        }
        cell.visitedNumbersLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "can serve 1 to", value: "", table: nil))!) \(SaveAddressClass.selectBannerArray[0].StoreDetails[indexPath.row].Serving!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "people", value: "", table: nil))!)"
        cell.distanceLbl.text = "\(String(format: "%.2f", SaveAddressClass.selectBannerArray[0].StoreDetails[indexPath.row].Distance!))KM"
        cell.ratingLbl.text = "\(SaveAddressClass.selectBannerArray[0].StoreDetails[indexPath.row].Rating!)"
        let imgStr:NSString = "\(url.storeImg.imgPath())\(SaveAddressClass.selectBannerArray[0].StoreDetails[indexPath.row].StoreLogo_En!)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let URLString = URL.init(string: urlStr as String)
        cell.img.kf.indicatorType = .activity
        cell.img.kf.setImage(with: URLString!)
        cell.img.layer.cornerRadius = 4
        cell.img.clipsToBounds = true
        tableViewHeight.constant = tableView.contentSize.height + 10
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SaveAddressClass.selectStoreArray.removeAll()
        SaveAddressClass.selectStoreArray = [SaveAddressClass.selectBannerArray[0].StoreDetails[indexPath.row]]
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
