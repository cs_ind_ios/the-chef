//
//  UpdateAppVC.swift
//  Bakery&Company
//
//  Created by Creative Solutions on 5/5/20.
//  Copyright © 2020 Creative Solutions. All rights reserved.
//

import UIKit

class UpdateAppVC: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func updateNowBtn_Tapped(_ sender: Any) {
        let url = URL(string: "itms-apps://itunes.apple.com/app/1464131797")
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        self.dismiss(animated: false, completion: nil)
    }
}
