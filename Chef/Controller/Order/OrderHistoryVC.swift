//
//  OrderHistoryVC.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class OrderHistoryVC: UIViewController {

    var branchId = Int()
    var mainArray = [OrderHistory]()
    var filterArray = [OrderHistory]()
    @IBOutlet weak var OrderTableView: UITableView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    var isSearch : Bool = false
    @IBOutlet weak var noOrdersView: UIView!
    @IBOutlet weak var searchBarHeight: NSLayoutConstraint!
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    var pageNumber:Int = 1
    var totalRows:Int = 0
    var isCancel:Bool = false
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        self.OrderTableView.rowHeight = UITableView.automaticDimension
        self.OrderTableView.estimatedRowHeight = 80
        self.OrderTableView.delegate = self
        self.OrderTableView.dataSource = self
        self.searchBar.delegate = self
        
        searchBarHeight.constant = 0
        searchBar.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Search", value: "", table: nil))!
        navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order History", value: "", table: nil))!
        
//        if AppDelegate.getDelegate().appLanguage == "English"{
//            searchBar.semanticContentAttribute = .forceLeftToRight
//        }else{
//            searchBar.semanticContentAttribute = .forceRightToLeft
//        }
        
        DispatchQueue.main.async {
            SaveAddressClass.getOrderHistoryArray.removeAll()
            self.getOrderHistoryService()
        }
        
        if #available(iOS 10.0, *) {
            OrderTableView.refreshControl = refreshControl
        } else {
            OrderTableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        
    }
    @objc private func refreshWeatherData(_ sender: Any) {
        pageNumber = 1
        self.getOrderHistoryService()
    }
    @objc func indexChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex{
        case 0:
            //print("All");
            self.mainArray.removeAll()
            self.mainArray = SaveAddressClass.getOrderHistoryArray
        case 1:
            //print("Active")
            self.mainArray.removeAll()
            self.mainArray = SaveAddressClass.getOrderHistoryArray.filter{($0.OrderStatus == "New") || ($0.OrderStatus == "Received") || ($0.OrderStatus == "Accepted") || ($0.OrderStatus == "Ready")}
        case 2:
            //print("Favourite")
            self.mainArray.removeAll()
            self.mainArray = SaveAddressClass.getOrderHistoryArray.filter({$0.IsFavourite == true })
        case 3:
            //print("Cancel")
            self.mainArray.removeAll()
            self.mainArray = SaveAddressClass.getOrderHistoryArray.filter({$0.OrderStatus == "Cancel" })
        default:
            break
        }
        if self.mainArray.count == 0 {
            self.OrderTableView.reloadData()
        }else{
            self.OrderTableView.reloadData()
            let indexPath = IndexPath(row: 0, section: 0)
            self.OrderTableView.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        if let index = self.OrderTableView.indexPathForSelectedRow{
            self.OrderTableView.deselectRow(at: index, animated: true)
        }
    }
    
    func getOrderHistoryService(){
        if pageNumber == 1 {
           ANLoader.showLoading("Loading..", disableUI: false)
        }
        let jsonDic = ["UserId":"\(UserDef.getUserId())", "BranchId":"\(branchId)","PageSize":"20", "PageNumber":pageNumber] as [String : Any]
        OrderApiRouter.getOrderhistory(dic: jsonDic, success: { (data) in
            if data?.status == false{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                }
                self.navigationController?.popViewController(animated: true)
                return
            }else{
                if self.pageNumber == 1 {
                    SaveAddressClass.getOrderHistoryArray.removeAll()
                }
                
                for Dic in data!.successDic! {
                    var itemsArray = [String]()
                    var itemsQty:Int = 0
                    var Qty:Int = 0
                    let dic = Dic.OrderMain!
                    self.totalRows = Dic.Rows!
                    
                    for itemdDic in Dic.OrderItems! {
                        itemsArray.append("\(String(describing: itemdDic["ItemName_En"]!)) x\(String(describing: itemdDic["Quantity"]!))")
                        itemsQty  =  itemdDic["Quantity"] as! Int
                        Qty = Qty + itemsQty
                    }
                    var itemsStr :String = ""
                    if itemsArray.count != 0 {
                        itemsStr = itemsArray.joined(separator: ", ")
                    }
                    let ItemnamesDic = OrderHistory(
                        OrderId: dic["OrderId"] is NSNull ? 0 : dic["OrderId"] as! Int,
                        BranchId:  dic["BranchId"] is NSNull ? 0 : dic["BranchId"] as! Int,
                        BranchName_En:  dic["BranchName_En"] is NSNull ? "" : dic["BranchName_En"] as! String,
                        BranchName_Ar: dic["BranchName_Ar"] is NSNull ? "" : dic["BranchName_Ar"] as! String,
                        BrandId: dic["BrandId"] is NSNull ? 0 : dic["BrandId"] as! Int,
                        BrandName_En: dic["BrandName_En"] is NSNull ? "" : dic["BrandName_En"] as! String,
                        BrandName_Ar: dic["BrandName_Ar"] is NSNull ? "" :dic["BrandName_Ar"] as! String,
                        SubTotal: dic["SubTotal"] is  NSNull ? 0 :dic["SubTotal"] as! Double,
                        VatCharges:dic["VatCharges"] is  NSNull ? 0 :dic["VatCharges"] as! Double,
                        VatPercentage:dic["VatPercentage"] is  NSNull ? 0 :dic["VatPercentage"] as! Double,
                        TotalPrice:dic["TotalPrice"] is  NSNull ? 0 :dic["TotalPrice"] as! Double,
                        PaymentMode:dic["PaymentMode"] is NSNull ? "" :dic["PaymentMode"] as! String,
                        PaymentType:dic["PaymentType"] is NSNull ? 0 :dic["PaymentType"] as! Int,
                        InvoiceNo: dic["InvoiceNo"] is NSNull ? "" :dic["InvoiceNo"] as! String, OrderDate: dic["OrderDate"] is NSNull ? "" :dic["OrderDate"] as! String,
                        OrderMode_En:dic["OrderMode_En"] is NSNull ? "" :dic["OrderMode_En"] as! String,
                        OrderMode_Ar:dic["OrderMode_Ar"] is NSNull ? "" :dic["OrderMode_Ar"] as! String,
                        OrderType:dic["OrderType"] is NSNull ? 0 :dic["OrderType"] as! Int,
                        OrderStatus:dic["OrderStatus"] is NSNull ? "" :dic["OrderStatus"] as! String,
                        ExpectedTime:dic["ExpectedTime"] is NSNull ? "" :dic["ExpectedTime"] as! String,
                        ItemsNames: itemsStr,
                        Qunatity:Qty,
                        BranchAddress: dic["BranchAddress"] is NSNull ? "" :dic["BranchAddress"] as! String,
                        IsFavourite:  dic["IsFavourite"] is NSNull ? false :dic["IsFavourite"] as! Bool,
                        Rating: dic["Rating"] is NSNull ? 0.0 :dic["Rating"] as! Float,
                        BrandLogo: dic["BrandLogo"] is NSNull ? "" :dic["BrandLogo"] as! String
                    )
                    SaveAddressClass.getOrderHistoryArray.append(ItemnamesDic)
                }
                
                self.mainArray.removeAll()
                self.mainArray = SaveAddressClass.getOrderHistoryArray
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                    self.OrderTableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            }
            
        }) { (error) in
            //print("error:\(error)")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            if error != (AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))! {
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func backBtn_tapped(_ sender: Any) {
        APIHandler.APIHandlerRequestCancel()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func searchBtnTapped(_ sender: Any) {
        if searchBarHeight.constant == 0{
            searchBarHeight.constant = 56
            searchBar.becomeFirstResponder()
        }else if searchBarHeight.constant == 56{
            searchBarHeight.constant = 0
            searchBar.resignFirstResponder()
        }
    }
    func dateConveration(str:String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //2018-10-09T12:42:00
        dateFormatterGet.locale = Locale(identifier: "EN")

        let date =  dateFormatterGet.date(from: str)
        let dateFormatterGet1 = DateFormatter()
        dateFormatterGet1.dateFormat = "dd MMM" //09 Oct'18 12:42 PM
        dateFormatterGet1.locale = Locale(identifier: "EN")

        let dateFormatterGet2 = DateFormatter()
        dateFormatterGet2.dateFormat = "hh:mm a" //09 Oct'18 12:42 PM
        dateFormatterGet2.locale = Locale(identifier: "EN")

        let dateFormatterGet3 = DateFormatter()
        dateFormatterGet3.dateFormat = "dd-MM-yyyy" //09 Oct'18 12:42 PM
        dateFormatterGet3.locale = Locale(identifier: "EN")

        return "\(dateFormatterGet2.string(from: date!)) \(dateFormatterGet3.string(from: date!))"
    }
    func dateConveration2(str:String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //2018-10-09T12:42:00
        dateFormatterGet.locale = Locale(identifier: "EN")

        let date =  dateFormatterGet.date(from: str)
        let dateFormatterGet1 = DateFormatter()
        dateFormatterGet1.dateFormat = "dd MMM" //09 Oct'18 12:42 PM
        dateFormatterGet1.locale = Locale(identifier: "EN")

        let dateFormatterGet2 = DateFormatter()
        dateFormatterGet2.dateFormat = "hh:mm a" //09 Oct'18 12:42 PM
        dateFormatterGet2.locale = Locale(identifier: "EN")

        let dateFormatterGet3 = DateFormatter()
        dateFormatterGet3.dateFormat = "dd-MM-yyyy" //09 Oct'18 12:42 PM
        dateFormatterGet3.locale = Locale(identifier: "EN")
        return "\(dateFormatterGet2.string(from: date!)) \(dateFormatterGet3.string(from: date!))"
    }
    @IBAction func orderTrackingBtn_tapped(sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = OrderVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
        obj.orderId = self.mainArray[sender.tag].OrderId!
        obj.orderFrom = 0
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func favouriteBtn_tapped(sender: UIButton) {
        self.mainArray[sender.tag].IsFavourite = self.mainArray[sender.tag].IsFavourite! == true ? false : true
        let  IsFavourite = self.mainArray[sender.tag].IsFavourite! == true ? "True":"False"
        let jsonDic = ["UserId":"\(UserDef.getUserId())", "OrderId":"\(String(describing: self.mainArray[sender.tag].OrderId!))",  "CommandType":"2", "IsFavourite":IsFavourite] as [String : Any]
        
        OrderApiRouter.favouriteOrder(dic: jsonDic, success: { (data) in
            if data?.status == false{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                }
                self.mainArray[sender.tag].IsFavourite = self.mainArray[sender.tag].IsFavourite! == true ? false : true
                let indexPathRow:Int = sender.tag
                let indexPosition = IndexPath(row: indexPathRow, section: 0)
                self.OrderTableView.reloadRows(at: [indexPosition], with: .none)
                return
            }
        }){ (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
        let indexPathRow:Int = sender.tag
        let indexPosition = IndexPath(row: indexPathRow, section: 0)
        self.OrderTableView.reloadRows(at: [indexPosition], with: .none)
    }
    @IBAction func ReOrderBtn_tapped(sender: UIButton) {
        let count = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable")
        if (count > 0){
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Cancel") {
                return
            }
            alertView.addButton("Replace") {
                _ = DBObject.InsertOrderString(insertSql: "DELETE From OrderTable")
                _ = DBObject.InsertOrderString(insertSql: "DELETE From StoreTable")
                _ = DBObject.InsertOrderString(insertSql: "DELETE From AdditionalTable")
                self.insertOrderMethod()
            }
            alertView.showInfo("Replace Cart?", subTitle: "There are items in your cart. Do you wish to replace them?")
        }else{
            self.insertOrderMethod()
        }
    }
    func insertOrderMethod() {
        for itemsDic in SaveAddressClass.getOrderTrackingItemsArray {
            var TotalAmount : Double = itemsDic.ItemPrice!
            for Dic  in itemsDic.AdditionalItems {
                let AddItemPrice = Dic.AddItemPrice! * Double(Dic.AddQuantity!)
                TotalAmount = TotalAmount + AddItemPrice
            }
            TotalAmount = TotalAmount * Double(itemsDic.Quantity!)
            let Sql = "INSERT INTO OrderTable (BranchId, CategoryId, ItemId, ItemImg, ItemName_En, ItemName_Ar, SizeId, Size_En, Size_Ar, SizePrice, Quantity, TotalAmount, Comments) VALUES (\(SaveAddressClass.getOrderTrackingMainArray[0].BranchId!),'0',\(itemsDic.ItemId!),'\(itemsDic.ItemImage!)','\(itemsDic.ItemName_En!)','\(itemsDic.ItemName_Ar!)',\(itemsDic.SizeId!),'\(itemsDic.ItemSizeName_En!)','\(itemsDic.ItemSizeName_Ar!)',\(itemsDic.ItemPrice!),\(itemsDic.Quantity!),\(TotalAmount),'');"
            _ = DBObject.InsertOrderString(insertSql: Sql)
            let orderId = DBObject.getMaxOrderId(Sql:"SELECT MAX(OrderId) from OrderTable")
            if itemsDic.AdditionalItems.count > 0 {
                for additionalDic  in itemsDic.AdditionalItems {
                    let AddTotalPrice = additionalDic.AddItemPrice! * Double(additionalDic.AddQuantity!)
                    let Sql = "INSERT INTO AdditionalTable (OrderId, AdditionalId, AddtionalName_En, AddtionalName_Ar, AddPrice, Quantity, AddTotalPrice) VALUES (\(orderId), \(additionalDic.AddItemId!), '\(additionalDic.AddItem_En!)', '\(additionalDic.AddItem_Ar!)', \(additionalDic.AddItemPrice!), \(additionalDic.AddQuantity!), \(AddTotalPrice));"
                    _ = DBObject.InsertOrderString(insertSql: Sql)
                }
            }
        }
        self.tabBarController?.selectedIndex = 3
    }
}

extension OrderHistoryVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isSearch) {
            return filterArray.count
        }
        if  totalRows >  self.mainArray.count {
            return self.mainArray.count + 1
        }else{
            return self.mainArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.mainArray.count == indexPath.row {
           return 50
        }
        return 120
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.mainArray.count == indexPath.row && totalRows > self.mainArray.count {
               self.OrderTableView.register(UINib(nibName:"LoaderCell", bundle: nil), forCellReuseIdentifier:"LoaderCell")
                let cell = tableView.dequeueReusableCell(withIdentifier: "LoaderCell", for: indexPath) as! LoaderCell
              cell.activityIndicatorView.startAnimating()
               cell.selectionStyle = .none
               return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryCell", for: indexPath) as! OrderHistoryCell
        cell.rateBtn.tag = indexPath.row
        if(isSearch){
            if self.filterArray.count > 0{
                cell.expDateTimeLbl.text = self.dateConveration(str: self.filterArray[indexPath.row].OrderDate!)

                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.brandLbl.text = self.filterArray[indexPath.row].BrandName_En!.capitalized
                    cell.deliveryStatusLbl.text = self.filterArray[indexPath.row].OrderStatus!
                }else{
                    cell.brandLbl.text = self.filterArray[indexPath.row].BrandName_Ar!.capitalized
                    //cell.expDateTimeLbl.text = self.dateConveration2(str: self.filterArray[indexPath.row].ExpectedTime!)
                    cell.deliveryStatusLbl.text = self.filterArray[indexPath.row].OrderStatus!
                }
                
                let imgStr:NSString = "\(url.storeImg.imgPath())\(self.filterArray[indexPath.row].BrandLogo! )" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let URLString = URL.init(string: urlStr as String)
                cell.brandImg.kf.indicatorType = .activity
                cell.brandImg.kf.setImage(with: URLString)
                cell.brandImg.layer.cornerRadius = 15
                
                //cell.ratingWidth.constant = 0
                cell.ratingView.isHidden = true
                cell.rateBtn.isHidden = true
                
                if self.filterArray[indexPath.row].Rating! > 0 {
                    //cell.ratingWidth.constant = 0
                    cell.ratingView.isHidden = false
                    cell.ratingView.rating = Double(filterArray[indexPath.row].Rating!)
                }else{
                    //cell.ratingWidth.constant = 60
                    cell.ratingView.isHidden = true
                    cell.rateBtn.isHidden = false
                    cell.rateBtn.setTitle("\(filterArray[indexPath.row].Rating!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate", value: "", table: nil))!)", for: .normal)
                    cell.rateBtn.addTarget(self, action: #selector(rateBtn_Tapped), for: .touchUpInside)
                }
            }
        } else {

            let dic = self.mainArray[indexPath.row]
            cell.expDateTimeLbl.text = self.dateConveration(str: dic.OrderDate!)
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.brandLbl.text = dic.BrandName_En!.capitalized
            }else{
                cell.brandLbl.text = dic.BrandName_Ar!.capitalized
                //cell.expDateTimeLbl.text = self.dateConveration2(str: self.mainArray[indexPath.row].ExpectedTime!)
            }
            cell.deliveryStatusLbl.text = dic.OrderStatus!
            if dic.OrderStatus! == "New"{
                cell.deliveryStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "New", value: "", table: nil))!
            }else if dic.OrderStatus! == "Close"{
                if dic.OrderType! == 2{
                    cell.deliveryStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Served", value: "", table: nil))!
                }else{
                    cell.deliveryStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivered", value: "", table: nil))!
                }
            }else if dic.OrderStatus! == "Cancel"{
                cell.deliveryStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancel", value: "", table: nil))!
            }else if dic.OrderStatus! == "Accepted"{
                cell.deliveryStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Accepted", value: "", table: nil))!
            }else if dic.OrderStatus! == "Ready"{
                cell.deliveryStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ready", value: "", table: nil))!
            }else if dic.OrderStatus! == "OnTheWay"{
                cell.deliveryStatusLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "On The Way", value: "", table: nil))!
            }
            
            let imgStr:NSString = "\(url.storeImg.imgPath())\(dic.BrandLogo! )" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let URLString = URL.init(string: urlStr as String)
            cell.brandImg.kf.indicatorType = .activity
            cell.brandImg.kf.setImage(with: URLString)
            cell.brandImg.layer.cornerRadius = 15
            //cell.ratingWidth.constant = 0
            cell.ratingView.isHidden = true
            cell.rateBtn.isHidden = true
            
            if dic.Rating! > 0 {
                //cell.ratingWidth.constant = 0
                cell.ratingView.isHidden = false
                cell.ratingView.rating = Double(dic.Rating!)
            }else{
                if dic.OrderStatus! == "Close"{
                    //cell.ratingWidth.constant = 60
                    cell.ratingView.isHidden = true
                    cell.rateBtn.isHidden = false
                    cell.rateBtn.setTitle("\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate", value: "", table: nil))!)", for: .normal)
                    cell.rateBtn.addTarget(self, action: #selector(rateBtn_Tapped), for: .touchUpInside)
                }else{
                    cell.ratingView.isHidden = true
                    cell.rateBtn.isHidden = true
                }
            }
//            if self.mainArray.count == indexPath.row+1 {
//                if totalRows >  self.mainArray.count {
//                    pageNumber = pageNumber + 1
//                    self.getOrderHistoryService()
//                }
//            }
        }
        cell.selectionStyle = .none
        return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                if self.mainArray.count == indexPath.row+1 {
                    if totalRows >  self.mainArray.count {
                        pageNumber = pageNumber + 1
                        self.getOrderHistoryService()
                    }
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if self.mainArray.count == indexPath.row && totalRows > self.mainArray.count {
        }else{
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = OrderVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
        obj.orderId = self.mainArray[indexPath.row].OrderId!
        obj.orderFrom = 0
        obj.orderVCDelegate = (self as OrderVCDelegate)
        obj.objType = 0
        self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    
    @objc func rateBtn_Tapped(sender:UIButton){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "RattingViewVC") as! RattingViewVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.OrderId = self.mainArray[sender.tag].OrderId!
        obj.orderType = self.mainArray[sender.tag].OrderType!
        if AppDelegate.getDelegate().appLanguage == "English"{
            obj.StoreName = self.mainArray[sender.tag].BrandName_En!.capitalized
        }else{
            obj.StoreName = self.mainArray[sender.tag].BrandName_Ar!.capitalized
        }
        obj.modalPresentationStyle = .overFullScreen
        present(obj, animated: true, completion: nil)
    }
}
extension OrderHistoryVC: OrderVCDelegate {
    func didTapAction(orderId: Int, ActionType: Int, IsFavourite: Bool) {
        if ActionType == 1 {
            self.mainArray.filter({$0.OrderId == orderId}).first?.OrderStatus = "Cancel"
        }else{
            self.mainArray.filter({$0.OrderId == orderId}).first?.IsFavourite = IsFavourite
        }
        self.OrderTableView.reloadData()
    }
}
extension OrderHistoryVC: UISearchBarDelegate{
    //MARK: UISearchbar delegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if isCancel == true{
            isSearch = false
            OrderTableView.isHidden = false
            noOrdersView.isHidden = true
        }else{
            isSearch = true;
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = true;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = true;
        filterArray = mainArray.filter({$0.BrandName_En!.lowercased().contains(searchBar.text!.lowercased())})
        if searchBar.text!.count > 0 {
            if (filterArray.count == 0){
                OrderTableView.isHidden = true
                noOrdersView.isHidden = false
                isSearch = false
            }else{
                OrderTableView.isHidden = false
                noOrdersView.isHidden = true
                isSearch = true
            }
        }else{
            isCancel = true
            isSearch = false
            OrderTableView.isHidden = false
            noOrdersView.isHidden = true
        }
        self.OrderTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterArray = mainArray.filter({$0.BrandName_En!.lowercased().contains(searchText.lowercased())})
        if searchBar.text!.count > 0 {
            if (filterArray.count == 0){
                OrderTableView.isHidden = true
                noOrdersView.isHidden = false
                isSearch = false
            }else{
                OrderTableView.isHidden = false
                noOrdersView.isHidden = true
                isSearch = true
            }
        }else{
            isCancel = true
            isSearch = false
            OrderTableView.isHidden = false
            noOrdersView.isHidden = true
        }
        self.OrderTableView.reloadData()
    }
}
