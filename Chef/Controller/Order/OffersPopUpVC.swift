//
//  OffersPopUpVC.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class OffersPopUpVC: UIViewController {

    @IBOutlet weak var promoCodeTitle_Lbl: UILabel!
    @IBOutlet weak var promoCode_Lbl: UILabel!
    @IBOutlet weak var promoCodeDescription_TxtView: UITextView!
    @IBOutlet weak var validDate_Lbl: UILabel!
    @IBOutlet weak var top_HeightConstraint: NSLayoutConstraint!
    var offer_Dict = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.top_HeightConstraint.constant = 30 //self.view.frame.height/2
        
        self.promoCode_Lbl.text = offer_Dict["promocode"] as? String
        self.promoCodeTitle_Lbl.text = offer_Dict["title"] as? String
        self.promoCodeDescription_TxtView.text = offer_Dict["description"] as? String
        self.validDate_Lbl.text = offer_Dict["validity"] as? String
    }
}
