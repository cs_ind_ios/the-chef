//
//  LocationSelectionVC.swift
//  Chef
//
//  Created by RAVI on 20/08/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

protocol LocationSelectionVCDelegrate {
    func didTapAction()
}
class LocationSelectionVC: UIViewController, CLLocationManagerDelegate , GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate {
    var locationSelectionVCDelegrate : LocationSelectionVCDelegrate!

    @IBOutlet weak var AutoBtn: UIButton!
    @IBOutlet weak var manualBtn: UIButton!
    @IBOutlet weak var orLbl: UILabel!
    var noNetWork = NoNetworkView()

    
    var locationManagerMain: Location!
    var locationManager = CLLocationManager()
    var delegate : SelectLocationDelegate?
    var isFirst:Bool = true
    var storesDic = StoreInfoDic.self
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //ANLoader.hide()
        
        AutoBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Allow", value: "", table: nil))!, for: .normal)
        manualBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Manual", value: "", table: nil))!, for: .normal)
        orLbl.text = "(\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "OR", value: "", table: nil))!))"
    }
    override func viewWillAppear(_ animated: Bool) {
         ANLoader.hide()
    }
    func closeWindow(){
        //ANLoader.showLoading("Loading...", disableUI: true)
        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
     //   HomeVC.instance.getStoresServiceCall = false
        self.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
        self.locationSelectionVCDelegrate.didTapAction()

        //}
    }
    @IBAction func autoLocationBtn_Tapped(_ sender: Any) {
        //ANLoader.showLoading("Loading..", disableUI: true)
        // Location Manager
        locationManagerMain = Location()
        locationManagerMain.needToDisplayAlert = true
        locationManagerMain.delegate = self
        return
    }
    @IBAction func manualLocationBtn_Tapped(_ sender: Any) {
        let vc = LocationAutocompleteVC(nibName: "LocationAutocompleteVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.locationAutocompleteVCDelegate = self
        self.present(vc, animated: true, completion: nil)
//        let autoCompleteController = GMSAutocompleteViewController()
//       // autoCompleteController.autocompleteFilter?.country = "KSA"
//        autoCompleteController.delegate = self
//        autoCompleteController.modalPresentationStyle = .overFullScreen
//        self.present(autoCompleteController, animated: true, completion: nil)
    }
    
    // MARK: GOOGLE AUTO COMPLETE DELEGATE
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let latitude: Double? = place.coordinate.latitude
        let longitude: Double? = place.coordinate.longitude
        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2DMake(latitude!, longitude!), completionHandler: { response, error in
            ANLoader.hide()
            
            if let addressObj = response?.firstResult() {
                //print("\(addressObj)")

                var str = ""
                if let sublocality = addressObj.subLocality {
                    str = sublocality
                }else if let locality = addressObj.locality{
                    str = locality
                }else{
                    str = addressObj.country!
                }
                
                var sLocality = ""
                if let lines = addressObj.lines {
                    let lines = lines as [String]
                    sLocality = lines.joined(separator: ",")
                }else if let sublocality = addressObj.subLocality {
                    sLocality = sublocality
                }else if let locality = addressObj.locality {
                    sLocality = locality
                }else{
                    sLocality = addressObj.country!
                }
                
                let dic = ["Adreess": str,
                           "FullAddress":"\(str), \(String(describing: sLocality)), \(String(describing: addressObj.country!))",
                    "AddressId":0,
                    "Latitude":"\(latitude!)",
                    "Longitude":"\(longitude!)"] as [String : Any]
                UserDefaults.standard.set(dic, forKey:"LocationDetails")
                UserDefaults.standard.synchronize()
                ANLoader.hide()
                DispatchQueue.main.async() {
                    //self.closeWindow()
                    self.getStoreInformation()
                }
                //self.navigationController?.popViewController(animated: false)
            }
        })
        self.dismiss(animated: true, completion: nil) // dismiss after select place
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("ERROR AUTO COMPLETE \(error)")
        
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil) // when cancel search
    }
    func saveToFile(input:[Dictionary<String, Any>],fileName:String) {
        // Get the url of Persons.json in document directory
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent(fileName)
        
        // Transform array into data and save it into file
        do {
            let data = try JSONSerialization.data(withJSONObject: input, options: [])
            try data.write(to: fileUrl, options: [])
        } catch {
            print(error)
        }
    }
    func getStoreInformation()  {
       let dic = UserDefaults.standard.object(forKey: "LocationDetails") as! [String:Any]
       let latitude = Double(dic["Latitude"] as! String) as! Double
       let longitude = Double(dic["Longitude"] as! String) as! Double
        
        let jsonDic :[String:Any]  = ["Latitude" : "\(latitude)", "Longitude" : "\(longitude)","pageNumber":"1","pageSize":"40"]
        if(Connectivity.isConnectedToInternet()){
            ANLoader.showLoading("Loading..", disableUI: true)
            OrderApiRouter.getStoreInformation(dic: jsonDic, success: { (data) in
                ANLoader.hide()
                if data?.status == false{
                    //print(data!)
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    }
                    DispatchQueue.main.async() {
                        self.closeWindow()
                    }
                }else{
                    // Stores
                    if data!.successDic!.StoresDetails!.count > 0 {
                        // Save documenation
                        //self.saveToFile(input: data!.successDic!.StoresDetails!, fileName:"StoreInformation")
                        // Store Detailes
                        SaveAddressClass.getStoresArray.removeAll()
                        for storeList in data!.successDic!.StoresDetails!{
                            let storesDic = GetStores(AvgPreparationTime: storeList["AvgPreparationTime"] is NSNull ? 0 : storeList["AvgPreparationTime"] as! Int,
                                                      BranchId: storeList["BranchId"] is NSNull ? 0 : storeList["BranchId"] as! Int,
                                                      BranchName_En: storeList["BranchName_En"] is NSNull ? "" : storeList["BranchName_En"] as! String,
                                                      BranchName_Ar: storeList["BranchName_Ar"] is NSNull ? "" : storeList["BranchName_Ar"] as! String,
                                                      DeliveryCharges: storeList["DeliveryCharges"] is NSNull ? 0 : storeList["DeliveryCharges"] as! Double,
                                                      Address: storeList["Address"] is NSNull ? "" : storeList["Address"] as! String,
                                                      MinOrderCharges: storeList["MinOrderCharges"] is NSNull ? 0 : storeList["MinOrderCharges"] as! Double,
                                                      EmailId: storeList["EmailId"] is NSNull ? "" : storeList["EmailId"] as! String,
                                                      MobileNo: storeList["MobileNo"] is NSNull ? "" : storeList["MobileNo"] as! String,
                                                      OrderTypes: storeList["OrderTypes"] is NSNull ? "" : storeList["OrderTypes"] as! String,
                                                      Rating: storeList["Rating"] is NSNull ? "" : storeList["Rating"] as! String,
                                                      RatingCount: storeList["RatingCount"] is NSNull ? 0 : storeList["RatingCount"] as! Int,
                                                      StoreLogo_En: storeList["StoreLogo_En"] is NSNull ? "" : storeList["StoreLogo_En"] as! String,
                                                      StoreImage_En: storeList["StoreImage_En"] is NSNull ? "" : storeList["StoreImage_En"] as! String,
                                                      StoreStatus: storeList["StoreStatus"] is NSNull ? "" : storeList["StoreStatus"] as! String,
                                                      StoreType: storeList["StoreType"] is NSNull ? "" : storeList["StoreType"] as! String,
                                                      TakeAwayDistance: storeList["TakeAwayDistance"] is NSNull ? 0 : storeList["TakeAwayDistance"] as! Int,
                                                      DeliveryDistance: storeList["DeliveryDistance"] is NSNull ? 0.00 : storeList["DeliveryDistance"] as! Double,
                                                      StarDateTime: storeList["StarDateTime"] is NSNull ? "" : storeList["StarDateTime"] as! String,
                                                      EndDateTime: storeList["EndDateTime"] is NSNull ? "" : storeList["EndDateTime"] as! String,
                                                      CurrentDateTime: storeList["CurrentDateTime"] is NSNull ? "" : storeList["CurrentDateTime"] as! String,
                                                      IsPromoted: storeList["IsPromoted"] is NSNull ? false : storeList["IsPromoted"] as! Bool,
                                                      Popular: storeList["Popular"] is NSNull ? false : storeList["Popular"] as! Bool,
                                                      DiscountAmt: storeList["DiscountAmt"] is NSNull ? 0 : storeList["DiscountAmt"] as! Int,
                                                      IsNew: storeList["IsNew"] is NSNull ? false : storeList["IsNew"] as! Bool,
                                                      Latitude: storeList["Latitude"] is NSNull ? 0.00 : storeList["Latitude"] as! Double,
                                                      Longitude: storeList["Longitude"] is NSNull ? 0.00 : storeList["Longitude"] as! Double,
                                                      Distance: storeList["Distance"] is NSNull ? 0.00 : storeList["Distance"] as! Double,
                                                      BrandId: storeList["BrandId"] is NSNull ? 0 : storeList["BrandId"] as! Int,
                                                      BindingCount: 1,
                                                      BrandName_En: storeList["BrandName_En"] is NSNull ? "" : storeList["BrandName_En"] as! String,
                                                      BrandName_Ar: storeList["BrandName_Ar"] is NSNull ? "" : storeList["BrandName_Ar"] as! String,
                                                      PaymentType:  storeList["PaymentType"] is NSNull ? "" : storeList["PaymentType"] as! String,
                                                      BranchDescription_En: storeList["BranchDescription_En"] is NSNull ? "" : storeList["BranchDescription_En"] as! String,                                                 Serving: storeList["Serving"] is NSNull ? 0 : storeList["Serving"] as! Int, FutureOrderDay: storeList["FutureOrderDay"] is NSNull ? 0 : storeList["FutureOrderDay"] as! Int, StoreType_Ar:storeList["StoreType_Ar"] is NSNull ? "" : storeList["StoreType_Ar"] as! String, BranchDescription_Ar:storeList["BranchDescription_Ar"] is NSNull ? "" : storeList["BranchDescription_Ar"] as! String, UpdatesAvailable: storeList["UpdatesAvailable"] is NSNull ? 0 : storeList["UpdatesAvailable"] as! Int, UpdateSeverity: storeList["UpdateSeverity"] is NSNull ? 0 : storeList["UpdateSeverity"] as! Int, VatPercentage: storeList["VatPercentage"] is NSNull ? 0 : storeList["VatPercentage"] as! Double
                            )
                            
                            // Save More Stores
                            SaveAddressClass.getStoresArray.append(storesDic)
                        }
                        
                        // Filters
                        SaveAddressClass.getStoresArray = SortingFilters.sortingFilters(array: SaveAddressClass.getStoresArray)
                    }
                    // Banners
                    if data!.successDic!.BannerDetails!.count > 0 {
                        // Save documenation
                        self.saveToFile(input: data!.successDic!.BannerDetails!, fileName:"BannerDetails")
                        SaveAddressClass.getStoreBannerArray.removeAll()
                        for bannerList in data!.successDic!.BannerDetails!{
                            let bannerDic = GetBanners(BannerName_En: bannerList["BannerName_En"] is NSNull ? "" : bannerList["BannerName_En"] as! String,
                                                       BannerName_Ar: bannerList["BannerName_Ar"] is NSNull ? "" : bannerList["BannerName_Ar"] as! String,
                                                       BannerImage: bannerList["BannerImage"] is NSNull ? "" : bannerList["BannerImage"] as! String,
                                                       StoreDetails: self.getStores(inputArray:  bannerList["StoreDetails"] as! Array<AnyObject> ) as! [GetStores])
                            
                            SaveAddressClass.getStoreBannerArray.append(bannerDic)
                        }
                    }
                    // Filters
                    SaveAddressClass.getFilters.removeAll()
                    for storeList in data!.successDic!.FilterCategories!{
                        let filterDic = GetFilterCategory(FilterTypeId: storeList["FilterTypeId"] is NSNull ? 0 : storeList["FilterTypeId"] as! Int, FilterTypeName_En: storeList["FilterTypeName_En"] is NSNull ? "" : storeList["FilterTypeName_En"] as! String, FilterTypeName_Ar: storeList["FilterTypeName_Ar"] is NSNull ? "" : storeList["FilterTypeName_Ar"] as! String, Filters: self.filtersArrayFunc(inputArray: storeList["Filters"] as! Array<AnyObject> ) as! [GetAdditionalFilters])
                        
                        // Save filters
                        SaveAddressClass.getFilters.append(filterDic)
                    }
                    HomeVC.instance.totalRows = data!.successDic!.TotalRows!
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async() {
                        self.closeWindow()
                    }
                }
            }){ (error) in
                ANLoader.hide()
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: error)
                DispatchQueue.main.async() {
                    self.closeWindow()
                }
            }
        }else{
            ANLoader.hide()
            self.noNetWork = (Bundle.main.loadNibNamed("NoNetworkView", owner: self, options: nil)?.first as? NoNetworkView)!
            self.noNetWork.delegate = self
            self.noNetWork.frame = CGRect(x:0,y:0,width:self.view.frame.width,height:self.view.frame.height-49)
            self.view.addSubview(self.noNetWork)

        }
    }
    func filtersArrayFunc(inputArray:Array<AnyObject>) -> Array<AnyObject> {
        var itemsArray = [GetAdditionalFilters]()
        for Dic in inputArray as! [Dictionary<String, Any>] {
            let additionalsDic = GetAdditionalFilters(FilterId: Dic["FilterId"] is NSNull ? 0 : Dic["FilterId"] as! Int, FilterName_En: Dic["FilterName_En"] is NSNull ? "" : Dic["FilterName_En"] as! String, FilterName_Ar: Dic["FilterName_Ar"] is NSNull ? "" : Dic["FilterName_Ar"] as! String, Status: false)
            itemsArray.append(additionalsDic)
        }
        return itemsArray
    }
    // Get Stores
    func getStores(inputArray:Array<AnyObject>) -> Array<AnyObject> {
        var getStoresDetails = [GetStores]()
        
        for storeList in inputArray as! [Dictionary<String, Any>] {
            
            let storesDic = GetStores(AvgPreparationTime: storeList["AvgPreparationTime"] is NSNull ? 0 : storeList["AvgPreparationTime"] as! Int,
                                      BranchId: storeList["BranchId"] is NSNull ? 0 : storeList["BranchId"] as! Int,
                                      BranchName_En: storeList["BranchName_En"] is NSNull ? "" : storeList["BranchName_En"] as! String,
                                      BranchName_Ar: storeList["BranchName_Ar"] is NSNull ? "" : storeList["BranchName_Ar"] as! String,
                                      DeliveryCharges: storeList["DeliveryCharges"] is NSNull ? 0 : storeList["DeliveryCharges"] as! Double,
                                      Address: storeList["Address"] is NSNull ? "" : storeList["Address"] as! String,
                                      MinOrderCharges: storeList["MinOrderCharges"] is NSNull ? 0 : storeList["MinOrderCharges"] as! Double,
                                      EmailId: storeList["EmailId"] is NSNull ? "" : storeList["EmailId"] as! String,
                                      MobileNo: storeList["MobileNo"] is NSNull ? "" : storeList["MobileNo"] as! String,
                                      OrderTypes: storeList["OrderTypes"] is NSNull ? "" : storeList["OrderTypes"] as! String,
                                      Rating: storeList["Rating"] is NSNull ? "" : storeList["Rating"] as! String,
                                      RatingCount: storeList["RatingCount"] is NSNull ? 0 : storeList["RatingCount"] as! Int,
                                      StoreLogo_En: storeList["StoreLogo_En"] is NSNull ? "" : storeList["StoreLogo_En"] as! String,
                                      StoreImage_En: storeList["StoreImage_En"] is NSNull ? "" : storeList["StoreImage_En"] as! String,
                                      StoreStatus: storeList["StoreStatus"] is NSNull ? "" : storeList["StoreStatus"] as! String,
                                      StoreType: storeList["StoreType"] is NSNull ? "" : storeList["StoreType"] as! String,
                                      TakeAwayDistance: storeList["TakeAwayDistance"] is NSNull ? 0 : storeList["TakeAwayDistance"] as! Int,
                                      DeliveryDistance: storeList["DeliveryDistance"] is NSNull ? 0.00 : storeList["DeliveryDistance"] as! Double,
                                      StarDateTime: storeList["StarDateTime"] is NSNull ? "" : storeList["StarDateTime"] as! String,
                                      EndDateTime: storeList["EndDateTime"] is NSNull ? "" : storeList["EndDateTime"] as! String,
                                      CurrentDateTime: storeList["CurrentDateTime"] is NSNull ? "" : storeList["CurrentDateTime"] as! String,
                                      IsPromoted: storeList["IsPromoted"] is NSNull ? false : storeList["IsPromoted"] as! Bool,
                                      Popular: storeList["Popular"] is NSNull ? false : storeList["Popular"] as! Bool,
                                      DiscountAmt: storeList["DiscountAmt"] is NSNull ? 0 : storeList["DiscountAmt"] as! Int,
                                      IsNew: storeList["IsNew"] is NSNull ? false : storeList["IsNew"] as! Bool,
                                      Latitude: storeList["Latitude"] is NSNull ? 0.00 : storeList["Latitude"] as! Double,
                                      Longitude: storeList["Longitude"] is NSNull ? 0.00 : storeList["Longitude"] as! Double,
                                      Distance: storeList["Distance"] is NSNull ? 0.00 : storeList["Distance"] as! Double,
                                      BrandId: storeList["BrandId"] is NSNull ? 0 : storeList["BrandId"] as! Int,
                                      BindingCount: 1,
                                      BrandName_En: storeList["BrandName_En"] is NSNull ? "" : storeList["BrandName_En"] as! String,
                                      BrandName_Ar: storeList["BrandName_Ar"] is NSNull ? "" : storeList["BrandName_Ar"] as! String,
                                      PaymentType:  storeList["PaymentType"] is NSNull ? "" : storeList["PaymentType"] as! String,
                                      BranchDescription_En: storeList["BranchDescription_En"] is NSNull ? "" : storeList["BranchDescription_En"] as! String,
                                      Serving: storeList["Serving"] is NSNull ? 0 : storeList["Serving"] as! Int, FutureOrderDay: storeList["FutureOrderDay"] is NSNull ? 0 : storeList["FutureOrderDay"] as! Int, StoreType_Ar:storeList["StoreType_Ar"] is NSNull ? "" : storeList["StoreType_Ar"] as! String, BranchDescription_Ar:storeList["BranchDescription_Ar"] is NSNull ? "" : storeList["BranchDescription_Ar"] as! String, UpdatesAvailable: storeList["UpdatesAvailable"] is NSNull ? 0 : storeList["UpdatesAvailable"] as! Int, UpdateSeverity: storeList["UpdateSeverity"] is NSNull ? 0 : storeList["UpdateSeverity"] as! Int, VatPercentage: storeList["VatPercentage"] is NSNull ? 0 : storeList["VatPercentage"] as! Double
            )
            getStoresDetails.append(storesDic)
        }
        return getStoresDetails
    }
}
extension LocationSelectionVC : LocationDelegate {
    func didFailedLocation(error: String) {
//        if isFirst == true {
//          isFirst = false
//            return
//        }
        ANLoader.hide()
        //self.checkLocationPermission()
        
        let alert = UIAlertController(title: "Allow Location Access", message: "The Chef needs access to your location. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)

                   // Button to Open Settings
                   alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                       guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                           return
                       }
                       if UIApplication.shared.canOpenURL(settingsUrl) {
                           UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                               print("Settings opened: \(success)")
                           })
                       }
                   }))
                   alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                   self.present(alert, animated: true, completion: nil)

    }
    
    func checkLocationPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                //open setting app when location services are disabled
                //Please enable location services for this app.
                openSettingApp(title:NSLocalizedString("Location Services Disabled", comment: ""), message:NSLocalizedString("Please go to Settings and turn on Location Service for this app.", comment: ""))
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
            openSettingApp(title:NSLocalizedString("Location services are not enabled", comment: ""), message:NSLocalizedString("Please go to Settings and turn on Location Service", comment: ""))
        }
    }
    func openSettingApp(title: String,message: String) {
        let alertController = UIAlertController (title: message, message:nil , preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)

        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    func didUpdateLocation(latitude: Double?, longitude: Double?){
        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2DMake((latitude!), (longitude!)), completionHandler: { response, error in
            if let addressObj = response?.firstResult() {
                //print("\(addressObj)")
               
                var str = ""
                if let sublocality = addressObj.subLocality {
                    str = sublocality
                }else if let locality = addressObj.locality{
                    str = locality
                }else{
                    str = addressObj.country!
                }
                
                var sLocality = ""
                if let lines = addressObj.lines {
                    let lines = lines as [String]
                    sLocality = lines.joined(separator: ",")
                }else if let sublocality = addressObj.subLocality {
                    sLocality = sublocality
                }else if let locality = addressObj.locality {
                    sLocality = locality
                }else{
                    sLocality = addressObj.country!
                }
                
                let dic = ["Adreess": str,
                           "FullAddress":"\(str), \(String(describing: sLocality)), \(String(describing: addressObj.country!))",
                    "AddressId":0,
                    "Latitude":"\(latitude!)",
                    "Longitude":"\(longitude!)"] as [String : Any]
                UserDefaults.standard.set(dic, forKey:"LocationDetails")
                UserDefaults.standard.synchronize()
               // ANLoader.hide()
                //self.navigationController?.popViewController(animated: false)
                
                DispatchQueue.main.async() {
                    //self.closeWindow()
                    self.getStoreInformation()
                }
            }
        });
    }
}
extension LocationSelectionVC : NoNetworkViewDelegate {
    func TryAgain() {
        DispatchQueue.main.async() {
            self.noNetWork.isHidden = true
            //self.closeWindow()
            self.getStoreInformation()
        }
    }
}
extension LocationSelectionVC:LocationAutocompleteVCDelegate{
    func didTapAction(latitude: Double, longitude: Double, MainAddress: String, TotalAddress: String) {
        let dic = ["Adreess": MainAddress,
                   "FullAddress":"\(TotalAddress)",
            "AddressId":0,
            "Latitude":"\(latitude)",
            "Longitude":"\(longitude)"] as [String : Any]
        UserDefaults.standard.set(dic, forKey:"LocationDetails")
        UserDefaults.standard.synchronize()
        print(MainAddress)
        DispatchQueue.main.async() {
            self.getStoreInformation()
        }
    }
}
