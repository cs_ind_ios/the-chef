//
//  SearchStoresVC.swift
//  Chef
//
//  Created by RAVI on 22/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class SearchStoresVC: UIViewController, NoNetworkViewDelegate,UIGestureRecognizerDelegate {
    func TryAgain() {
        DispatchQueue.main.async {
            self.noNetWork.isHidden = true
            self.getSearchStoreInformation(searchText: "")
        }
    }
    
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive : Bool = false
    
    @IBOutlet weak var storesTableView: UITableView!
    // VARIABLES
    var noNetWork = NoNetworkView()
    var isLoaded = false
    var mainArr = [GetStores]()
    var bindingArray = [GetStores]()
    
    // Binding Outlet
    @IBOutlet var outletsView: UIView!
    @IBOutlet weak var outLetImageVIew: UIImageView!
    @IBOutlet weak var outletNameLbl: UILabel!
    @IBOutlet weak var outletsTableView: UITableView!
    @IBOutlet weak var numOfOutletsLbl: UILabel!
    @IBOutlet weak var outletsTVHeightConst: NSLayoutConstraint!
    @IBOutlet var blackView: UIView!
    var tap = UITapGestureRecognizer()
    var pageNumber:Int = 1
    var totalRows:Int = 10
    var searchingText:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if AppDelegate.getDelegate().appLanguage == "English"{
            searchBar.semanticContentAttribute = .forceLeftToRight
        }else{
            searchBar.semanticContentAttribute = .forceRightToLeft
        }
        
        SaveAddressClass.getSearchStoresArray.removeAll()
        
        /* Setup delegates */
        self.searchBar.delegate = self
        self.searchBar.showsCancelButton = false
        
        self.storesTableView.isHidden = true
        
        self.storesTableView.delegate = self
        self.storesTableView.dataSource = self
        self.storesTableView.reloadData()

//        let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField
        //textFieldInsideSearchBar?.textColor = UIColor.white

        //searchBar.placeholder = "Search for restaurants and food"
        searchBar.placeholder = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Search for Shops", value: "", table: nil))!
        searchBar.set(textColor: .white)
        searchBar.setTextField(color: #colorLiteral(red: 0.2705882353, green: 0.6117647059, blue: 0.8823529412, alpha: 1))
        searchBar.setPlaceholder(textColor: .white)
        searchBar.setSearchImage(color: .white)
        searchBar.setImage(UIImage(named: "cancel White"), for: .clear, state: .normal)
        
        storesTableView.register(UINib(nibName: "SearchTabTVCell", bundle: nil), forCellReuseIdentifier: "SearchTabTVCell")
        
        // Outlets view frame
        self.blackView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(self.blackView)
        self.tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.tap.delegate = self
        self.blackView.addGestureRecognizer(self.tap)
        self.outletsView.frame = CGRect(x: 0, y: self.view.frame.size.height+60, width: self.view.frame.size.width, height: 100)
        self.view.addSubview(self.outletsView)
        self.outletsTableView.delegate = self
        self.outletsTableView.dataSource = self
        self.blackView.isHidden = true
        self.outletsView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13, *){
            let statusBar = UIView(frame: (UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame)!)
            statusBar.backgroundColor = #colorLiteral(red: 0.2705882353, green: 0.6117647059, blue: 0.8823529412, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        }else{
            let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
            let statusBarColor = #colorLiteral(red: 0.2705882353, green: 0.6117647059, blue: 0.8823529412, alpha: 1)
            statusBarView.backgroundColor = statusBarColor
            view.addSubview(statusBarView)
        }
        
        HomeVC.instance.getStoresServiceCall = true
        self.tabBarController?.tabBar.isHidden = false
        if !SaveAddressClass.SelectSearchCatDic.isEmpty {
            self.isLoaded = false
            self.storesTableView.isHidden = false
            self.storesTableView.reloadData()
            self.searchBar.text = SaveAddressClass.SelectSearchCatDic["CatetgoryName_En"]! as? String
            self.searchBar.resignFirstResponder()
            getSearchStoreInformation(searchText: "")
            searchActive = false
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        SaveAddressClass.SelectSearchCatDic.removeAll()
        

        //APIHandler.APIHandlerRequestCancel()
        if #available(iOS 13, *)
        {
            let statusBar = UIView(frame: (UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame)!)
            statusBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        }else{
            let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
            let statusBarColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            statusBarView.backgroundColor = statusBarColor
            view.addSubview(statusBarView)
        }
    }
    func getSearchStoreInformation(searchText:String){
        let dic = UserDefaults.standard.object(forKey: "LocationDetails") as! [String:Any]
        let jsonDic = ["Latitude" : "\(Double(dic["Latitude"] as! String)!)", "Longitude" : "\(Double(dic["Longitude"] as! String)!)","pageNumber":"\(pageNumber)","pageSize":"20","SearchText":"\(String(describing: searchText))"]
            
        if(Connectivity.isConnectedToInternet()){
            if self.isLoaded == false {
               // ANLoader.showLoading("Loading..", disableUI: true)
            }
        OrderApiRouter.getStoreInformation(dic: jsonDic, success: { (data) in
            ANLoader.hide()
            if data?.status == false{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    Alert.showToastAlert(on: self, message: data!.message!)
                }else{
                    Alert.showToastAlert(on: self, message: data!.messageAr!)

                    //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                }
                        
                self.isLoaded = false
                //self.storesTableView.isHidden = true
                //self.storesTableView.reloadData()
                }else{
                        
                // Store Detailes
                if self.pageNumber == 1 {
                    SaveAddressClass.getSearchStoresArray.removeAll()
                }
                for storeList in data!.successDic!.StoresDetails!{
                    let storesDic = GetStores(AvgPreparationTime: storeList["AvgPreparationTime"] is NSNull ? 0 : storeList["AvgPreparationTime"] as! Int,
                        BranchId: storeList["BranchId"] is NSNull ? 0 : storeList["BranchId"] as! Int,
                        BranchName_En: storeList["BranchName_En"] is NSNull ? "" : storeList["BranchName_En"] as! String,
                        BranchName_Ar: storeList["BranchName_Ar"] is NSNull ? "" : storeList["BranchName_Ar"] as! String,
                        DeliveryCharges: storeList["DeliveryCharges"] is NSNull ? 0 : storeList["DeliveryCharges"] as! Double,
                        Address: storeList["Address"] is NSNull ? "" : storeList["Address"] as! String,
                        MinOrderCharges: storeList["MinOrderCharges"] is NSNull ? 0 : storeList["MinOrderCharges"] as! Double,
                        EmailId: storeList["EmailId"] is NSNull ? "" : storeList["EmailId"] as! String,
                        MobileNo: storeList["MobileNo"] is NSNull ? "" : storeList["MobileNo"] as! String,
                        OrderTypes: storeList["OrderTypes"] is NSNull ? "" : storeList["OrderTypes"] as! String,
                        Rating: storeList["Rating"] is NSNull ? "" : storeList["Rating"] as! String,
                        RatingCount: storeList["RatingCount"] is NSNull ? 0 : storeList["RatingCount"] as! Int,
                        StoreLogo_En: storeList["StoreLogo_En"] is NSNull ? "" : storeList["StoreLogo_En"] as! String,
                        StoreImage_En: storeList["StoreImage_En"] is NSNull ? "" : storeList["StoreImage_En"] as! String,
                        StoreStatus: storeList["StoreStatus"] is NSNull ? "" : storeList["StoreStatus"] as! String,
                        StoreType: storeList["StoreType"] is NSNull ? "" : storeList["StoreType"] as! String,
                        TakeAwayDistance: storeList["TakeAwayDistance"] is NSNull ? 0 : storeList["TakeAwayDistance"] as! Int,
                        DeliveryDistance: storeList["DeliveryDistance"] is NSNull ? 0.00 : storeList["DeliveryDistance"] as! Double,
                        StarDateTime: storeList["StarDateTime"] is NSNull ? "" : storeList["StarDateTime"] as! String,
                        EndDateTime: storeList["EndDateTime"] is NSNull ? "" : storeList["EndDateTime"] as! String,
                        CurrentDateTime: storeList["CurrentDateTime"] is NSNull ? "" : storeList["CurrentDateTime"] as! String,
                        IsPromoted: storeList["IsPromoted"] is NSNull ? false : storeList["IsPromoted"] as! Bool,
                        Popular: storeList["Popular"] is NSNull ? false : storeList["Popular"] as! Bool,
                        DiscountAmt: storeList["DiscountAmt"] is NSNull ? 0 : storeList["DiscountAmt"] as! Int,
                        IsNew: storeList["IsNew"] is NSNull ? false : storeList["IsNew"] as! Bool,
                        Latitude: storeList["Latitude"] is NSNull ? 0.00 : storeList["Latitude"] as! Double,
                        Longitude: storeList["Longitude"] is NSNull ? 0.00 : storeList["Longitude"] as! Double,
                        Distance: storeList["Distance"] is NSNull ? 0.00 : storeList["Distance"] as! Double,
                        BrandId: storeList["BrandId"] is NSNull ? 0 : storeList["BrandId"] as! Int,
                        BindingCount: 1,
                        BrandName_En: storeList["BrandName_En"] is NSNull ? "" : storeList["BrandName_En"] as! String,
                        BrandName_Ar: storeList["BrandName_Ar"] is NSNull ? "" : storeList["BrandName_Ar"] as! String,
                        PaymentType:  storeList["PaymentType"] is NSNull ? "" : storeList["PaymentType"] as! String,
                        BranchDescription_En: storeList["BranchDescription_En"] is NSNull ? "" : storeList["BranchDescription_En"] as! String,                                                 Serving: storeList["Serving"] is NSNull ? 0 : storeList["Serving"] as! Int, FutureOrderDay: storeList["FutureOrderDay"] is NSNull ? 0 : storeList["FutureOrderDay"] as! Int, StoreType_Ar:storeList["StoreType_Ar"] is NSNull ? "" : storeList["StoreType_Ar"] as! String, BranchDescription_Ar:storeList["BranchDescription_Ar"] is NSNull ? "" : storeList["BranchDescription_Ar"] as! String, UpdatesAvailable: storeList["UpdatesAvailable"] is NSNull ? 0 : storeList["UpdatesAvailable"] as! Int, UpdateSeverity: storeList["UpdateSeverity"] is NSNull ? 0 : storeList["UpdateSeverity"] as! Int, VatPercentage: storeList["VatPercentage"] is NSNull ? 0 : storeList["VatPercentage"] as! Double
                        )
                        // Save More Stores
                    SaveAddressClass.getSearchStoresArray.append(storesDic)
                    }
                    // Filters
                    SaveAddressClass.getSearchStoresArray = SortingFilters.sortingFilters(array: SaveAddressClass.getSearchStoresArray)
                    self.totalRows = data!.successDic!.TotalRows!
                    // Mapping brands
                    self.mainArr.removeAll()
                    self.mainArr = MappingBrands.SearchBrands()
                        
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        self.isLoaded = true
                        self.storesTableView.isHidden = false
                        self.storesTableView.reloadData()
                    }
                }
            }){ (error) in
                ANLoader.hide()
               // Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: error)
                Alert.showToastAlert(on: self, message: error)
                self.isLoaded = false
                self.storesTableView.isHidden = true
                //self.storesTableView.reloadData()
            }
        }else{
            ANLoader.hide()
            self.isLoaded = false
            //self.storesTableView.reloadData()
            self.storesTableView.isHidden = true
            self.noNetWork = (Bundle.main.loadNibNamed("NoNetworkView", owner: self, options: nil)?.first as? NoNetworkView)!
            self.noNetWork.delegate = self
            self.noNetWork.frame = CGRect(x:0,y:0,width:self.view.frame.width,height:self.view.frame.height-49)
            self.view.addSubview(self.noNetWork)
        }
    }
    
    // For Hidding
    @IBAction func handleTap(_ sender: Any) {
        self.perform(#selector(hideBlackView), with: nil, afterDelay: 0.4)
        UIView.animate(withDuration: 0.6) {
            self.outletsView.frame = CGRect(x: 0, y: self.view.frame.size.height+60, width: self.view.frame.size.width, height: self.outletsTableView.frame.size.height)
            self.view.layoutIfNeeded()
        }
    }
    @objc func hideBlackView(){
        self.blackView.isHidden = true
        self.outletsView.isHidden = true
        self.tap.isEnabled = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension SearchStoresVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if SaveAddressClass.getSearchStoresArray.count > 0{
            return SaveAddressClass.getSearchStoresArray.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 109
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = storesTableView.dequeueReusableCell(withIdentifier: "SearchTabTVCell", for: indexPath) as! SearchTabTVCell
        if SaveAddressClass.getSearchStoresArray.count > 0{
            let dic = SaveAddressClass.getSearchStoresArray[indexPath.row]
             if AppDelegate.getDelegate().appLanguage == "English"{
                 cell.storeNameLbl.text = dic.BranchName_En?.capitalized
                 //cell.subStoreNameLbl.text = dic.StoreType!.capitalized
                 cell.descriptionLbl.text = dic.BranchDescription_En?.capitalized
                if dic.AvgPreparationTime! > 60 {
                    let hour = dic.AvgPreparationTime!/60
                        cell.subStoreNameLbl.text = "Order Before - \(hour) Hours"
                }else{
                        cell.subStoreNameLbl.text = "Order Before - \(dic.AvgPreparationTime!) Minutes"
                }
             }else{
                 cell.storeNameLbl.text = dic.BranchName_Ar?.capitalized
                 //cell.subStoreNameLbl.text = dic.StoreType_Ar?.capitalized
                 cell.descriptionLbl.text = dic.BranchDescription_Ar?.capitalized
                if dic.AvgPreparationTime! > 60 {
                    let hour = dic.AvgPreparationTime!/60
                    cell.subStoreNameLbl.text = "الطلب قبل - \(hour) ساعة"
                }else{
                    cell.subStoreNameLbl.text = "الطلب قبل - \(dic.AvgPreparationTime!) الدقائق"
                }
             }
             cell.visitedNumbersLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "can serve 1 to", value: "", table: nil))!) \(dic.Serving!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "people", value: "", table: nil))!)"
             cell.distanceLbl.text = "\(String(format: "%.2f", dic.Distance!))KM"
             cell.ratingLbl.text = "\(dic.Rating!)"
             let imgStr:NSString = "\(url.storeImg.imgPath())\(dic.StoreLogo_En!)" as NSString
             let charSet = CharacterSet.urlFragmentAllowed
             let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
             let URLString = URL.init(string: urlStr as String)
             cell.img.kf.indicatorType = .activity
             cell.img.kf.setImage(with: URLString!)
             cell.img.layer.cornerRadius = 4
             cell.img.clipsToBounds = true
            // print("Array count: \(SaveAddressClass.getSearchStoresArray.count)")
             //print("IndexPath Row: \(indexPath.row)")
            // print("Total Rows : \(totalRows)")
             if SaveAddressClass.getSearchStoresArray.count == indexPath.row + 1 {
                 if totalRows > SaveAddressClass.getSearchStoresArray.count {
                     pageNumber = pageNumber + 1
                     ANLoader.showLoading("Loading..", disableUI: true)
                     self.getSearchStoreInformation(searchText: searchingText)
                 }
             }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        SaveAddressClass.selectStoreArray.removeAll()
        SaveAddressClass.selectStoreArray = [SaveAddressClass.getSearchStoresArray[indexPath.row]]
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
extension SearchStoresVC: UISearchBarDelegate{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchActive == false {
            self.searchBar.resignFirstResponder()
        }
        searchActive = true
    }
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //print(searchBar.text!)
        if(text == " " && self.searchBar.text?.count == 0){
            return false;
        }
        let nameStr:NSString = (self.searchBar.text! as NSString)
        if(nameStr.length >= 1){
            let code:NSString = nameStr.substring(from: nameStr.length-1) as NSString
            if(code.isEqual(to: " ")){
                if(text == " "){
                    return false
                }
            }
        }
        let letters = "!~`@#$%^&*-+();:={}[],.<>?\\/\"\'_•¥£€|1234567890"
        let cs:CharacterSet = CharacterSet.init(charactersIn: letters)
        let filtered = text.components(separatedBy: cs).joined(separator: "")
        
        return text == filtered
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = true
        SaveAddressClass.SelectSearchCatDic.removeAll()
        self.storesTableView.isHidden = true
        self.searchBar.resignFirstResponder()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //print(searchBar.text as Any)
        self.searchBar.resignFirstResponder()
        searchingText = searchBar.text!
        if searchBar.text!.count > 2 {
            getSearchStoreInformation(searchText: searchBar.text!)
        }else{
            Alert.showAlert(on: self, title: "", message: "Please enter at least 3 Characters")
        }
        searchActive = true
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchingText = searchBar.text!
        if searchText == "" {
            searchActive = false
            pageNumber = 1
            SaveAddressClass.SelectSearchCatDic.removeAll()
            self.searchBar.resignFirstResponder()
           // print("UISearchBar.text cleared!")
            self.storesTableView.isHidden = true
        }
        if searchBar.text!.count > 2 {
            getSearchStoreInformation(searchText: searchBar.text!)
        }else{
            self.storesTableView.isHidden = true
        }
    }
}
extension UISearchBar {
    func getTextField() -> UITextField? { return value(forKey: "searchField") as? UITextField }
    func set(textColor: UIColor) { if let textField = getTextField() { textField.textColor = textColor } }
    func setPlaceholder(textColor: UIColor) { getTextField()?.setPlaceholder(textColor: textColor) }
    func setClearButton(color: UIColor) { getTextField()?.setClearButton(color: color) }

    func setTextField(color: UIColor) {
        guard let textField = getTextField() else { return }
        switch searchBarStyle {
        case .minimal:
            textField.layer.backgroundColor = color.cgColor
            textField.layer.cornerRadius = 6
        case .prominent, .default: textField.backgroundColor = color
        @unknown default: break
        }
    }
    func setSearchImage(color: UIColor) {
        guard let imageView = getTextField()?.leftView as? UIImageView else { return }
        imageView.tintColor = color
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
    }
}

private extension UITextField {
    private class Label: UILabel {
        private var _textColor = UIColor.lightGray
        override var textColor: UIColor! {
            set { super.textColor = _textColor }
            get { return _textColor }
        }
        init(label: UILabel, textColor: UIColor = .lightGray) {
            _textColor = textColor
            super.init(frame: label.frame)
            self.text = label.text
            self.font = label.font
        }
        required init?(coder: NSCoder) { super.init(coder: coder) }
    }
    private class ClearButtonImage {
        static private var _image: UIImage?
        static private var semaphore = DispatchSemaphore(value: 1)
        static func getImage(closure: @escaping (UIImage?)->()) {
            DispatchQueue.global(qos: .userInteractive).async {
                semaphore.wait()
                DispatchQueue.main.async {
                    if let image = _image { closure(image); semaphore.signal(); return }
                    guard let window = UIApplication.shared.windows.first else { semaphore.signal(); return }
                    let searchBar = UISearchBar(frame: CGRect(x: 0, y: -200, width: UIScreen.main.bounds.width, height: 44))
                    window.rootViewController?.view.addSubview(searchBar)
                    searchBar.text = "txt"
                    searchBar.layoutIfNeeded()
                    _image = searchBar.getTextField()?.getClearButton()?.image(for: .normal)
                    closure(_image)
                    searchBar.removeFromSuperview()
                    semaphore.signal()
                }
            }
        }
    }
    func setClearButton(color: UIColor) {
        ClearButtonImage.getImage { [weak self] image in
            guard   let image = image,
                let button = self?.getClearButton() else { return }
            button.imageView?.tintColor = color
            button.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }
    var placeholderLabel: UILabel? { return value(forKey: "placeholderLabel") as? UILabel }
    func setPlaceholder(textColor: UIColor) {
        guard let placeholderLabel = placeholderLabel else { return }
        let label = Label(label: placeholderLabel, textColor: textColor)
        setValue(label, forKey: "placeholderLabel")
    }
    func getClearButton() -> UIButton? { return value(forKey: "clearButton") as? UIButton }
}
