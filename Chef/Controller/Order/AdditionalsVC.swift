//
//  AdditionalsVC.swift
//  Chef
//
//  Created by RAVI on 22/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class AdditionalsVC: UIViewController, UIScrollViewDelegate, AdditionalsCellDelegate,UITableViewDelegate,UITableViewDataSource {
    func SelectAdditionalBtn_tapped(cell: UITableViewCell) {
        let indexPath = additionalsTable.indexPath(for: cell)
        if(indexPath!.section == 0){
            for i in 0..<SaveAddressClass.getItemSize.count{
                SaveAddressClass.getItemSize[i].isSelect = false
            }
            SaveAddressClass.getItemSize[indexPath!.row].isSelect = true
            // Select Size Additionals
            qty = 1
            self.totalArr.removeAll()
            let dic = ["AddGrpName_En":"Choice of size", "MinSelection":1, "MaxSelection":1,"AddGrpName_Ar":"اختيار الحجم"] as [String : Any]
            self.totalArr.append(dic)
            self.additionalsArray = SaveAddressClass.getAdditionals.filter { ($0.PriceId! == SaveAddressClass.getItemSize[indexPath!.row].priceId) }
           // print(self.additionalsArray)
            for Dic  in self.additionalsArray {
                let dic = ["AddGrpName_En":Dic.AddGrpName_En!,"MinSelection":Dic.MinSelection!,"MaxSelection":Dic.MaxSelection!,"AddGrpName_Ar":Dic.AddGrpName_Ar!] as [String : Any]
                self.totalArr.append(dic)
            }
            self.priceCalucaltion()
        }else{
            if self.additionalsArray[indexPath!.section-1].getAdditionls[indexPath!.row].isSelect! == false {
                let maxSelect = additionalsArray[indexPath!.section-1].MaxSelection!
                var maxSelectd:Int = 0
                for i in 0..<additionalsArray[indexPath!.section-1].getAdditionls.count{
                    if additionalsArray[indexPath!.section-1].getAdditionls[i].isSelect == true{
                        maxSelectd = maxSelectd + 1
                    }
                }
                if maxSelectd >= maxSelect {
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection", value: "", table: nil))!) \(maxSelect) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "only", value: "", table: nil))!)")
                    return
                }
                self.additionalsArray[indexPath!.section-1].getAdditionls[indexPath!.row].isSelect = true
                self.additionalsArray[indexPath!.section-1].getAdditionls[indexPath!.row].Quantity = 1
                self.additionalsArray[indexPath!.section-1].getAdditionls[indexPath!.row].AddTotalPrice = self.additionalsArray[indexPath!.section-1].getAdditionls[indexPath!.row].Addprice!
            }else{
                self.additionalsArray[indexPath!.section-1].getAdditionls[indexPath!.row].isSelect = false
                self.additionalsArray[indexPath!.section-1].getAdditionls[indexPath!.row].Quantity = 0
                self.additionalsArray[indexPath!.section-1].getAdditionls[indexPath!.row].AddTotalPrice = 0.00
            }
            self.priceCalucaltion()
        }
    }
    
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productDicLbl: UILabel!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var additionalsTable: UITableView!
    //@IBOutlet weak var navigationBarImg: UIImageView!
    //@IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var myScrollView: UIScrollView!
    var addItemSizes = [AddItemsSizes]()
    var addItemSizeName = [[String:Any]]()
    var totalArr = [Any]()
    var additionalsArray = [GetAdditionalGroup]()
    var qty : Int?
    var section : Int?
    var comments = ""
    
    @IBOutlet weak var commentTextLbl: UILabel!
    @IBOutlet weak var quantityTextLbl: UILabel!
    @IBOutlet weak var priceTextLbl: UILabel!
    @IBOutlet weak var amountTextLbl: UILabel!
    @IBOutlet weak var qtyLbl: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var addCartBtn: UIButton!
    @IBOutlet weak var tableHight: NSLayoutConstraint!
    
    @IBOutlet weak var priceLbl: UILabel!
    var isLoaded = false
    @IBOutlet weak var commentsTV: UITextView!
    
    @IBOutlet weak var itemPriceLbl: UILabel!
    @IBOutlet weak var cartNumberLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationBarImg.isHidden = true
        cartNumberLbl.layer.cornerRadius = cartNumberLbl.frame.height/2
        cartNumberLbl.layer.masksToBounds = true
        
        commentTextLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Comment", value: "", table: nil))!
        quantityTextLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Qunatity", value: "", table: nil))!
        amountTextLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ammount", value: "", table: nil))!
        priceTextLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Price", value: "", table: nil))!

        self.tabBarController?.tabBar.isHidden = true
        myScrollView.delegate = self
        // Do any additional setup after loading the view.
        if AppDelegate.getDelegate().appLanguage == "English"{
            self.productNameLbl.text = SaveAddressClass.getSelectItemDetails[0].ItemName_En!.capitalized
            self.productDicLbl.text = "\(SaveAddressClass.getSelectItemDetails[0].ItemDesc_En!.capitalized)"
        }else{
            self.productNameLbl.text = SaveAddressClass.getSelectItemDetails[0].ItemName_Ar!.capitalized
            self.productDicLbl.text = "\(SaveAddressClass.getSelectItemDetails[0].ItemDesc_Ar!.capitalized)"
        }
        
        addCartBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add", value: "", table: nil))!, for: .normal)
        
        if SaveAddressClass.selectStoreArray[0].DiscountAmt! > 0 {
            let discountPercentage = (Double(SaveAddressClass.selectStoreArray[0].DiscountAmt!) / 100)
            let price:Double = SaveAddressClass.getSelectItemDetails[0].Price! * discountPercentage
            self.priceLbl.text = "\(String(describing: SaveAddressClass.getSelectItemDetails[0].Price! - price))"
            self.itemPriceLbl.text = "\((String(format: "%.2f", SaveAddressClass.getSelectItemDetails[0].Price! - price)))"
        }else{
            self.priceLbl.text = "\(SaveAddressClass.getSelectItemDetails[0].Price!)"
            self.itemPriceLbl.text = "\((String(format: "%.2f", SaveAddressClass.getSelectItemDetails[0].Price!)))"
        }
        qty = 1
        self.qtyLbl.text = "\(qty!)"
        self.minusBtn.isEnabled = false
        
        // selected store image
        let imgStr:NSString = "\(url.ItemImg.imgPath())\(SaveAddressClass.getSelectItemDetails[0].ItemImage! )" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let URLString = URL.init(string: urlStr as String)
        self.productImg.kf.indicatorType = .activity
        self.productImg.kf.setImage(with: URLString!)
//        self.navigationBarImg.kf.setImage(with: URLString)
        
        commentsTV.delegate = self
        additionalsTable.delegate = self
        additionalsTable.dataSource = self
        additionalsTable.rowHeight = UITableView.automaticDimension
        additionalsTable.estimatedRowHeight = 44
        getAdditionalsService()
        additionalsTable.isScrollEnabled = false
        //  additionalsTable .reloadData()
        let borderGray = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        self.commentsTV.layer.borderColor = borderGray.cgColor
        self.commentsTV.layer.borderWidth = 1.0
        self.commentsTV.layer.cornerRadius = 5.0
        //self.commentsTV.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "I want to have extra cheese", value: "", table: nil))!
        self.commentsTV.textColor = UIColor.lightGray
        if AppDelegate.getDelegate().appLanguage == "English"{
            titleLbl.text = SaveAddressClass.selectStoreArray[0].BranchName_En?.capitalized
            commentsTV.textAlignment = .left
            commentsTV.semanticContentAttribute = .forceLeftToRight
        }else{
            titleLbl.text = SaveAddressClass.selectStoreArray[0].BranchName_Ar?.capitalized
            commentsTV.textAlignment = .right
            commentsTV.semanticContentAttribute = .forceRightToLeft
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        //let count = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable where BranchId = '\(SaveAddressClass.selectStoreArray[0].BranchId!)'")
        let count = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable")
        if (count > 0){
            if let tabItems = tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[2]
                tabItem.badgeValue = "\(count)"
                cartNumberLbl.isHidden = false
                cartNumberLbl.text = "\(count)"
            }
        }else{
            if let tabItems = self.tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[2]
                tabItem.badgeValue = nil
                cartNumberLbl.isHidden = true
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
    }
    func getAdditionalsService(){
        if(Connectivity.isConnectedToInternet() == true){
            ANLoader.showLoading("Loading..", disableUI: true)
            let jsonDic = ["ItemId":"\(SaveAddressClass.getSelectItemDetails[0].ItemId!)", "BranchId":"\(SaveAddressClass.selectStoreArray[0].BranchId!)"]
            OrderApiRouter.getAdditional(dic: jsonDic, success: { (data) in
                if data?.status == false{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        ANLoader.hide()
                    }
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    }
                    self.isLoaded = false
                    self.navigationController?.popViewController(animated: true)
                    return
                }else{
                    SaveAddressClass.getItemSize.removeAll()
                    SaveAddressClass.getAdditionals.removeAll()
                    var isSelect = true
                    for Dic in data!.successDic!.PricesAndAdditionals![0].ItemSizesAndPrices! {
                        let ItemSizesDic = GetItemSizesAndAdditionals(
                            priceId: Dic["priceId"] is NSNull ? 0 : Dic["priceId"] as! Int,
                            ItemSizeName_En : Dic["ItemSizeName_En"] is NSNull ? "" : Dic["ItemSizeName_En"] as! String,
                            ItemSizeName_Ar : Dic["ItemSizeName_Ar"] is NSNull ? "" : Dic["ItemSizeName_Ar"] as! String,
                            Itemprice : Dic["Itemprice"] is NSNull ? 0 : Dic["Itemprice"] as! Double,
                            isSelect : isSelect
                        )
                        isSelect = false
                        SaveAddressClass.getItemSize.append(ItemSizesDic)
                        self.myArrayFunc(inputArray: Dic["addgrp"] as! Array<AnyObject>, priceId: Dic["priceId"] is NSNull ? 0 : Dic["priceId"] as! Int)
                    }
                    self.totalArr.removeAll()
                    let dic = ["AddGrpName_En":"Choice of size", "MinSelection":1, "MaxSelection":1,"AddGrpName_Ar":"اختيار الحجم"] as [String : Any]
                    self.totalArr.append(dic)
                    self.additionalsArray = SaveAddressClass.getAdditionals.filter { ($0.PriceId == SaveAddressClass.getItemSize[0].priceId) }
                    //print(self.additionalsArray)
                    for Dic  in self.additionalsArray {
                        let dic = ["AddGrpName_En":Dic.AddGrpName_En!,"MinSelection":Dic.MinSelection!,"MaxSelection":Dic.MaxSelection!,"AddGrpName_Ar":Dic.AddGrpName_Ar! ] as [String : Any]
                        self.totalArr.append(dic)
                    }
                    self.priceCalucaltion()
                    self.additionalsTable.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        ANLoader.hide()
                    }
                }
            }) { (error) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: error)
                self.isLoaded = false
                self.navigationController?.popViewController(animated: true)
                return
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            self.isLoaded = false
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "internetFail", value: "", table: nil))!)
            self.navigationController?.popViewController(animated: true)
            return
        }
    }
    
    // Items MVC
    func myArrayFunc(inputArray:Array<AnyObject>, priceId: Int){
        //func myArrayFunc(inputArray:Array<AnyObject>, priceId: Int) -> Array<AnyObject> {
        //var itemsArray = [GetAdditionalGroup]()
        for Dic in inputArray as! [Dictionary<String, Any>] {
            if let addGrpId = Dic["AddGrpId"] {
                
                let additionalGroupDic = GetAdditionalGroup(
                    PriceId:priceId,
                    AddGrpId: addGrpId as! Int,
                    AddGrpName_En: Dic["AddGrpName_En"] is NSNull ? "" : Dic["AddGrpName_En"] as! String,
                    AddGrpName_Ar: Dic["AddGrpName_Ar"] is NSNull ? "" : Dic["AddGrpName_Ar"] as! String,
                    MinSelection: Dic["MinSelection"] is NSNull ? 0 : Dic["MinSelection"] as! Int,
                    MaxSelection: Dic["MaxSelection"] is NSNull ? 0 : Dic["MaxSelection"] as! Int,
                    getAdditionls: self.myAdditionalArrayFunc(inputArray: Dic["additems"] as! Array<AnyObject> ) as! [getAdditionls]
                )
                // itemsArray.append(additionalGroupDic)
                SaveAddressClass.getAdditionals.append(additionalGroupDic)
            }
        }
        //return  itemsArray
    }
    
    // Items MVC
    func myAdditionalArrayFunc(inputArray:Array<AnyObject>) -> Array<AnyObject> {
        var itemsArray = [getAdditionls]()
        for Dic in inputArray as! [Dictionary<String, Any>] {
            let priceArray = Dic["addprice"] as! [Any]
            let  priceDic = priceArray[0] as! [String: Any]
            let price = priceDic["Addprice"] as! Double
            let additionalsDic = getAdditionls(
                AdditionalId: Dic["AdditionalId"] is NSNull ? 0 : Dic["AdditionalId"] as! Int,
                AddtionalName_en: Dic["AddtionalName_en"] is NSNull ? "" : Dic["AddtionalName_en"] as! String,
                AddtionalName_Ar: Dic["AddtionalName_Ar"] is NSNull ? "" : Dic["AddtionalName_Ar"] as! String,
                Addprice: Double(price),
                isSelect : false,
                MinSelection: Dic["MinSelection"] is NSNull ? 0 : Dic["MinSelection"] as! Int,
                MaxSelection: Dic["MaxSelection"] is NSNull ? 0 : Dic["MaxSelection"] as! Int,
                Quantity: 0,
                AddTotalPrice: 0.00
            )
            itemsArray.append(additionalsDic)
        }
        return itemsArray
    }
    @IBAction func cartBtn_Tapped(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func backBtn_tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    var tableViewHeight: CGFloat {
        additionalsTable.layoutIfNeeded()
        return additionalsTable.contentSize.height - 10
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        //print(self.totalArr.count)
        return self.totalArr.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            if SaveAddressClass.getItemSize.count == 1 {
                return 1
            }
        }else if section == 1 {
            return 75
        }
        return 30
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 0))
        let titleLbl = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.bounds.width-20, height: 30))
        let viewAllLabl = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.bounds.width-20, height: 30))

        if section == 1 {
            let additionalLbl = UILabel(frame: CGRect(x: 0, y: 1, width: tableView.bounds.width, height: 44))
            additionalLbl.font = UIFont(name: FontMedium, size: 16)
            additionalLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "EXTRAS", value: "", table: nil))!
            additionalLbl.textColor = UIColor.darkGray
            additionalLbl.backgroundColor = UIColor.white
            if AppDelegate.getDelegate().appLanguage == "English"{
                additionalLbl.textAlignment = .left
            }else{
                additionalLbl.textAlignment = .right
            }
            // Category
            titleLbl.frame = CGRect(x: 10, y: 44, width: tableView.bounds.width-20, height: 30)
            viewAllLabl.frame = CGRect(x: 10, y: 44, width: tableView.bounds.width-20, height: 30)
            view.addSubview(additionalLbl)

        }
        
        //Category
        titleLbl.font = UIFont(name: FontBook, size: 14)
        let dic = self.totalArr[section] as! [String:Any]
        titleLbl.text = (dic[(AppDelegate.getDelegate().filePath?.localizedString(forKey: "AddGrpName_En", value: "", table: nil))!] as! String).capitalized
        
//        if AppDelegate.getDelegate().appLanguage == "English"{
//
//        }else{
//            titleLbl.text = (dic["AddGrpName_Ar"] as! String)
//        }
        titleLbl.textColor = UIColor.black
        
        
        //Quantity
        viewAllLabl.font = UIFont(name: FontBook, size: 14)
        viewAllLabl.textColor = UIColor.black
        
        if dic["MinSelection"] as! Int  == 1 &&  dic["MaxSelection"] as! Int == 1 {
            viewAllLabl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Required", value: "", table: nil))!
        }else if dic["MaxSelection"] as! Int >= 1 {
            viewAllLabl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum", value: "", table: nil))!) \(String(describing: dic["MaxSelection"]!))"
        }
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            titleLbl.textAlignment = .left
            viewAllLabl.textAlignment = .right
        }else{
            titleLbl.textAlignment = .right
            viewAllLabl.textAlignment = .left
        }
        
        view.addSubview(titleLbl)
        view.addSubview(viewAllLabl)
        view.clipsToBounds = true
        return view
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let dic = self.totalArr[section] as! [String:Any]
        if AppDelegate.getDelegate().appLanguage == "English"{
            if let secTitle = (dic["AddGrpName_En"]){
                return (secTitle as AnyObject).capitalized
            }
        }else{
            if let secTitle = (dic["AddGrpName_Ar"]){
                return (secTitle as AnyObject).capitalized
            }
        }
        
        return ""
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return SaveAddressClass.getItemSize.count
        }else{
            return self.additionalsArray[section-1].getAdditionls.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdditionalsCell", for: indexPath) as! AdditionalsCell
        cell.delegate = self
        cell.maxiQtyLbl.text = nil
        if(indexPath.section == 0){
            cell.quanitityHeightConstraint.constant = 0
            let dic = SaveAddressClass.getItemSize[indexPath.row]
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.additionalNameLbl.text! = dic.ItemSizeName_En!.capitalized
            }else{
                cell.additionalNameLbl.text! = dic.ItemSizeName_Ar!.capitalized
            }
            
            if SaveAddressClass.selectStoreArray[0].DiscountAmt! > 0 {
                // Line Draw horizentaly discount price
                let attributeString = NSMutableAttributedString(string: "\(String(describing: dic.Itemprice!.withCommas()))")
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                cell.originalPriceLbl?.attributedText = attributeString
                let discountPercentage = (Double(SaveAddressClass.selectStoreArray[0].DiscountAmt!) / 100)
                let price:Double = dic.Itemprice! * discountPercentage
                let finalprice:Double = dic.Itemprice! - price
                cell.priceLbl?.text = finalprice.withCommas()
                cell.priceConstraintY.constant = 5
            }else{
                cell.originalPriceLbl?.text = nil
                cell.priceLbl.text! = dic.Itemprice!.withCommas()
                cell.priceConstraintY.constant = -5
            }
            if dic.isSelect == true {
                cell.selectImgBtn .setImage(UIImage (named: "checked-checkbox"), for: .normal)
            }else{
                cell.selectImgBtn .setImage(UIImage (named: "unchecked-checkbox"), for: .normal)
            }
        }else{
            let dic = self.additionalsArray[indexPath.section-1].getAdditionls[indexPath.row]
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.additionalNameLbl.text! = dic.AddtionalName_en!.capitalized
            }else{
                cell.additionalNameLbl.text! = dic.AddtionalName_Ar!.capitalized
            }
            
            if SaveAddressClass.selectStoreArray[0].DiscountAmt! > 0 {
                // Line Draw horizentaly discount price
                let attributeString = NSMutableAttributedString(string: "\(String(describing: dic.Addprice!))")
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                cell.originalPriceLbl?.attributedText = attributeString
                let discountPercentage = (Double(SaveAddressClass.selectStoreArray[0].DiscountAmt!) / 100)
                let price:Double = dic.Addprice! * discountPercentage
                let finalPrice:Double = dic.Addprice! - price
                cell.priceLbl?.text = finalPrice.withCommas()
                cell.priceConstraintY.constant = 5
            }else{
                cell.originalPriceLbl?.text = nil
                cell.priceLbl.text! =  dic.Addprice!.withCommas()
                cell.priceConstraintY.constant = -5
            }
            section = indexPath.section-1
           // print(section!)
            if dic.isSelect == true {
                cell.selectImgBtn .setImage(UIImage (named: "checked-checkbox"), for: .normal)
                if dic.MaxSelection! > 1 {
                    cell.quanitityHeightConstraint.constant = dic.MaxSelection! > 1 ? 30 : 0
                    cell.maxiQtyLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum", value: "", table: nil))!) \( dic.MaxSelection!)"
                    if dic.Quantity! >= dic.MaxSelection! {
                        cell.plusBtn.isEnabled = false
                        cell.plusBtn.layer.opacity = 0.5
                    }else{
                        cell.plusBtn.isEnabled = true
                        cell.plusBtn.layer.opacity = 1.0
                    }
                }else{
                    cell.quanitityHeightConstraint.constant = 0
                    cell.maxiQtyLbl.text = nil
                }
                cell.quanitityLbl.text = "\(String(describing: dic.Quantity!))"
                cell.plusBtn.tag = indexPath.row
                cell.plusBtn.addTarget(self, action: #selector(AddPlusBtn_tapped(sender:)), for: .touchUpInside)
                cell.minusBtn.tag = indexPath.row
                cell.minusBtn.addTarget(self, action: #selector(AddMinusBtn_tapped(sender:)), for: .touchUpInside)
                
            }else{
                cell.quanitityHeightConstraint.constant = 0
                cell.selectImgBtn .setImage(UIImage (named: "unchecked-checkbox"), for: .normal)
            }
            //cell.selectImgBtn.tag = indexPath.row
            //cell.selectImgBtn.addTarget(self, action: #selector(SelectImgBtn_tapped(sender:)), for: .touchUpInside)
        }
        self.tableHight.constant = tableView.contentSize.height - 10
        cell.selectionStyle = .none
        return cell
    }
    @IBAction func SelectImgSizeBtn_tapped(sender:UIButton) {
        for i in 0..<SaveAddressClass.getItemSize.count{
            SaveAddressClass.getItemSize[i].isSelect = false
        }
        SaveAddressClass.getItemSize[sender.tag].isSelect = true
        // Select Size Additionals
        self.totalArr.removeAll()
        let dic = ["AddGrpName_En":"Choice of size", "MinSelection":1, "MaxSelection":1,"AddGrpName_Ar":"اختيار الحجم"] as [String : Any]
        self.totalArr.append(dic)
        self.additionalsArray = SaveAddressClass.getAdditionals.filter { ($0.PriceId == SaveAddressClass.getItemSize[sender.tag].priceId) }
        //print(self.additionalsArray)
        for Dic  in self.additionalsArray {
            let dic = ["AddGrpName_En":Dic.AddGrpName_En!,"MinSelection":Dic.MinSelection!,"MaxSelection":Dic.MaxSelection!,"AddGrpName_Ar":Dic.AddGrpName_Ar!] as [String : Any]
            self.totalArr.append(dic)
        }
        self.priceCalucaltion()
    }
    @IBAction func SelectImgBtn_tapped(sender:UIButton) {
        if self.additionalsArray[section!].getAdditionls[sender.tag].isSelect == false {
            let maxSelect = additionalsArray[section!].MaxSelection!
            var maxSelectd:Int = 0
            for i in 0..<additionalsArray[section!].getAdditionls.count{
                if additionalsArray[section!].getAdditionls[i].isSelect == true{
                    maxSelectd = maxSelectd + 1
                }
            }
            if maxSelectd > maxSelect {
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum selection", value: "", table: nil))!) \(maxSelect) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "only", value: "", table: nil))!)")
                return
            }
            self.additionalsArray[section!].getAdditionls[sender.tag].isSelect = true
            self.additionalsArray[section!].getAdditionls[sender.tag].Quantity = 1
            self.additionalsArray[section!].getAdditionls[sender.tag].AddTotalPrice = self.additionalsArray[section!].getAdditionls[sender.tag].Addprice!
        }else{
            self.additionalsArray[section!].getAdditionls[sender.tag].isSelect = false
            self.additionalsArray[section!].getAdditionls[sender.tag].Quantity = 0
            self.additionalsArray[section!].getAdditionls[sender.tag].AddTotalPrice = 0.00
        }
        self.priceCalucaltion()
    }
    @IBAction func AddPlusBtn_tapped(sender:UIButton) {
        let maxSelect = additionalsArray[section!].getAdditionls[sender.tag].MaxSelection!
        var addQty  =  additionalsArray[section!].getAdditionls[sender.tag].Quantity!
        if addQty >= maxSelect {
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Maximum quantity", value: "", table: nil))!) \(maxSelect) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "only", value: "", table: nil))!)")
            return
        }
        addQty = addQty + 1
        self.additionalsArray[section!].getAdditionls[sender.tag].Quantity = addQty
        self.additionalsArray[section!].getAdditionls[sender.tag].AddTotalPrice = self.additionalsArray[section!].getAdditionls[sender.tag].Addprice! * Double(addQty)
        self.priceCalucaltion()
    }
    @IBAction func AddMinusBtn_tapped(sender:UIButton) {
        var addQty  =  additionalsArray[section!].getAdditionls[sender.tag].Quantity!
        if addQty <= 1 { //
            self.additionalsArray[section!].getAdditionls[sender.tag].isSelect = false
            self.additionalsArray[section!].getAdditionls[sender.tag].Quantity = 0
            self.additionalsArray[section!].getAdditionls[sender.tag].AddTotalPrice = 0.00
            self.priceCalucaltion()
            return
        }
        addQty = addQty - 1
        self.additionalsArray[section!].getAdditionls[sender.tag].Quantity = addQty
        self.additionalsArray[section!].getAdditionls[sender.tag].AddTotalPrice = self.additionalsArray[section!].getAdditionls[sender.tag].Addprice! * Double(addQty)
        self.priceCalucaltion()
    }
    func priceCalucaltion() {
        var price : Double?
        for i in 0..<SaveAddressClass.getItemSize.count{
            if  SaveAddressClass.getItemSize[i].isSelect == true {
                price = SaveAddressClass.getItemSize[i].Itemprice
            }
        }
        for Dic  in self.additionalsArray {
            for i in 0..<Dic.getAdditionls.count{
                if  Dic.getAdditionls[i].isSelect == true {
                    price = price! + (Dic.getAdditionls[i].AddTotalPrice)! as Double
                }
            }
        }
        self.itemPriceLbl.text = "\(price!.withCommas())"
        price = price! * Double(qty!)
        if SaveAddressClass.selectStoreArray[0].DiscountAmt! > 0 {
            let discountPercentage = (Double(SaveAddressClass.selectStoreArray[0].DiscountAmt!) / 100)
            let priceDouble:Double = price! * discountPercentage
            price = price! - priceDouble
        }
        self.qtyLbl.text = "\(qty!)"
        self.priceLbl.text = "\(price!.withCommas())"
        if qty! > 1{
            self.minusBtn.isEnabled = true
        }else{
            self.minusBtn.isEnabled = false
        }
        self.additionalsTable.reloadData()
        self.tableHight.constant = self.tableViewHeight - 9
    }
    @IBAction func plusBtn_tapped(_ sender: Any) {
        qty = qty! + 1
        self.priceCalucaltion()
    }
//    @IBAction func shareItemBtn_tapped(_ sender: Any) {
//        ANLoader.showLoading("Loading..", disableUI: true)
//        
//        // image to share
//        var image:UIImage? = nil
//        let imgStr:NSString = "\(url.ItemImg.imgPath())\(SaveAddressClass.getSelectItemDetails[0].ItemImage! )" as NSString
//        let charSet = CharacterSet.urlFragmentAllowed
//        let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
//        let URLString = URL(string:urlStr as String)
//        if let data = try? Data(contentsOf: URLString!)
//        {
//            image = UIImage(data: data)
//        }
//        let text = SaveAddressClass.getSelectItemDetails[0].ItemName_En!.capitalized
//        let desc = SaveAddressClass.getSelectItemDetails[0].ItemDesc_En!.capitalized
//        
//        // set up activity view controller
//        let imageToShare = [text, desc, image as Any ] as [Any]
//        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
//        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
//        
//        // present the view controller
//        self.present(activityViewController, animated: true, completion: {
//            ANLoader.hide()
//        })
//    }
    @IBAction func minusBtn_tapped(_ sender: Any) {
        if qty! > 1{
            qty = qty! - 1
            self.priceCalucaltion()
        }
    }
    @IBAction func addCartBtn_tapped(_ sender: Any) {
        let count = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable")
        if (count > 0){
            let count1 = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable where BranchId ='\(SaveAddressClass.selectStoreArray[0].BranchId!)'")
            if (count1 > 0){
                
                self.insertItemDetails()

//                let alertController = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Successfully Added to Cart", value: "", table: nil))!, preferredStyle: .alert)
//                let okAction = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: UIAlertAction.Style.default) {
//                    UIAlertAction in
//                    self.insertItemDetails()
//                }
//                alertController.addAction(okAction)
//                self.present(alertController, animated: false, completion: nil)
            }else{
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancel", value: "", table: nil))!) {
                    return
                }
                alertView.addButton((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Replace", value: "", table: nil))!) {
                    _ = DBObject.InsertOrderString(insertSql: "DELETE From OrderTable")
                    _ = DBObject.InsertOrderString(insertSql: "DELETE From AdditionalTable")
                    AppDelegate.getDelegate().dictionary_FinalAmount.removeAll()
                    AppDelegate.getDelegate().dictionary_FinalDiscount.removeAll()
                    self.insertItemDetails()
//                    let alertController = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Successfully Added to Cart", value: "", table: nil))!, preferredStyle: .alert)
//                    let okAction = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: UIAlertAction.Style.default) {
//                        UIAlertAction in
//                        self.insertItemDetails()
//                    }
//                    alertController.addAction(okAction)
//                    self.present(alertController, animated: false, completion: nil)
                }
                alertView.showInfo((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Replace Cart?", value: "", table: nil))!, subTitle: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "There are items in your cart. Do you wish to replace them?", value: "", table: nil))!)
            }
        }else{
             self.insertItemDetails()
//            let alertController = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Order Successfully Placed", value: "", table: nil))!, preferredStyle: .alert)
//            let okAction = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: UIAlertAction.Style.default) {
//                UIAlertAction in
//                self.insertItemDetails()
//            }
//            alertController.addAction(okAction)
//            self.present(alertController, animated: false, completion: nil)
        }
    }
    func insertItemDetails() {
        var discountPercentage:Double = 0
        if SaveAddressClass.selectStoreArray[0].DiscountAmt! > 0 {
            discountPercentage = (Double(SaveAddressClass.selectStoreArray[0].DiscountAmt!) / 100)
        }
        var price : Double?
        var SizeId : Int?
        var Size_En : String?
        var Size_Ar : String?
        for i in 0..<SaveAddressClass.getItemSize.count{
            if  SaveAddressClass.getItemSize[i].isSelect == true {
                price = SaveAddressClass.getItemSize[i].Itemprice!
                SizeId = SaveAddressClass.getItemSize[i].priceId!
                Size_En = SaveAddressClass.getItemSize[i].ItemSizeName_En!
                Size_Ar = SaveAddressClass.getItemSize[i].ItemSizeName_Ar!
            }
        }
        var TotalAmount : Double = price!
        let originalItmePrice : Double = price!
        for Dic  in self.additionalsArray {
            for i in 0..<Dic.getAdditionls.count{
                if  Dic.getAdditionls[i].isSelect == true {
                    TotalAmount = TotalAmount + (Dic.getAdditionls[i].AddTotalPrice)! as Double
                }
            }
        }
        TotalAmount = TotalAmount * Double(qty!)
        let originalTotalAmount : Double = TotalAmount
        if discountPercentage > 0 {
            let priceDouble:Double = TotalAmount * discountPercentage
            TotalAmount = TotalAmount - priceDouble
            // Total Item Price with discount
            let itemPriceDouble:Double = price! * discountPercentage
            price = price! - itemPriceDouble
        }
//        if commentsTV.text == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "I want to have extra cheese", value: "", table: nil))!{
//            comments = ""
//        }else{
            comments = commentsTV.text!
       // }
        let Sql = "INSERT INTO OrderTable (BranchId, CategoryId, ItemId, ItemImg, ItemName_En, ItemName_Ar, SizeId, Size_En, Size_Ar, SizePrice, Quantity, TotalAmount, Comments, OriginalSizePrice, OriginalTotalAmount, DiscountPercentage) VALUES (\(SaveAddressClass.selectStoreArray[0].BranchId!),\(SaveAddressClass.getSelectItemDetails[0].CategoryId!),\(SaveAddressClass.getSelectItemDetails[0].ItemId!),'\(SaveAddressClass.getSelectItemDetails[0].ItemImage!)','\(SaveAddressClass.getSelectItemDetails[0].ItemName_En!)','\(SaveAddressClass.getSelectItemDetails[0].ItemName_Ar!)',\(SizeId!),'\(Size_En!)','\(Size_Ar!)',\(price!),\(qty!),\(TotalAmount),'\(comments)','\(originalItmePrice)','\(originalTotalAmount)', '\(discountPercentage)');"
        
        let inserOrdertBool = DBObject.InsertOrderString(insertSql: Sql)
        //print(inserOrdertBool)
       // print(Sql)
        
        let orderId = DBObject.getMaxOrderId(Sql:"SELECT MAX(OrderId) from OrderTable")
        //print(orderId)
        for Dic  in self.additionalsArray {
            for i in 0..<Dic.getAdditionls.count{
                if  Dic.getAdditionls[i].isSelect == true {
                    
                    var additionalPric = Dic.getAdditionls[i].Addprice!
                    var totalAdditionalPric = Dic.getAdditionls[i].AddTotalPrice!
                    
                    if discountPercentage > 0 {
                        let addPriceDouble:Double = additionalPric * discountPercentage
                        additionalPric = additionalPric - addPriceDouble
                        
                        // Total Additiuonal Price with discount
                        let addTotalPriceDouble:Double = totalAdditionalPric * discountPercentage
                        totalAdditionalPric = totalAdditionalPric - addTotalPriceDouble
                    }
                    let Sql = "INSERT INTO AdditionalTable (OrderId, AdditionalId, AddtionalName_En, AddtionalName_Ar, AddPrice, Quantity, AddTotalPrice, OriginalAddPrice, OriginalAddTotalPrice) VALUES (\(orderId),\(Dic.getAdditionls[i].AdditionalId!),'\(Dic.getAdditionls[i].AddtionalName_en!)','\(Dic.getAdditionls[i].AddtionalName_Ar!)',\(additionalPric),\(Dic.getAdditionls[i].Quantity!),\(totalAdditionalPric), '\(Dic.getAdditionls[i].Addprice!)' , '\(Dic.getAdditionls[i].AddTotalPrice!)');"
                    let inserOrdertBool = DBObject.InsertOrderString(insertSql: Sql)
                   // print(inserOrdertBool)
                }
            }
        }
        let getOrderBool = DBObject.GetOrder(getSql: "SELECT * FROM OrderTable")
        //print(getOrderBool)
        self.navigationController?.popViewController(animated: false)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView == self.myScrollView){
           // print(scrollView.contentOffset.y)
            if(scrollView.contentOffset.y > 90){
                UIView.animate(withDuration: 0.7, animations: {
                })
            }else{
                UIView.animate(withDuration: 0.7, animations: {
                })
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension AdditionalsVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
//        if textView.textColor == UIColor.lightGray {
//            textView.text = nil
//            textView.textColor = UIColor.black
//        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
//        if textView.text.isEmpty {
//            textView.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "I want to have extra cheese", value: "", table: nil))!
//            textView.textColor = UIColor.lightGray
//        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let maxLength = 250
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
}
