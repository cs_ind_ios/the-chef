//
//  HomeVC.swift
//  Chef
//
//  Created by RAVI on 16/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Crashlytics

class HomeVC: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, NoNetworkViewDelegate, UIGestureRecognizerDelegate, AddFilterVCDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    //@IBOutlet weak var scrollView: UIScrollView!
    //@IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var storesTableView: UITableView!
   // @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var popViewBtn: UIButton!
    @IBOutlet weak var locationNameLbl: UILabel!
    @IBOutlet weak var addressBtn: UIButton!
    @IBOutlet weak var locationViewActionHeight: NSLayoutConstraint!
    @IBOutlet weak var languageBtn: UIButton!

    //pop up outlets
    @IBOutlet var firstTimePopUp: UIView!
    @IBOutlet weak var popUpLocTxtLbl: UILabel!
    @IBOutlet var searchView: UIView!

    //Location Popup
    @IBOutlet weak var alertMsgLbl: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var changeLocationBtn: UIButton!
    @IBOutlet var locationView: UIView!
    
    @IBOutlet weak var resultNumOfRecordsLbl: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBtn: CustomButton!
    @IBOutlet weak var searchBtnWidthConstraint: NSLayoutConstraint!
    //@IBOutlet weak var filterBtn: UIButton!
    //@IBOutlet weak var listViewBtn: UIButton!
    //@IBOutlet weak var mapViewBtn: UIButton!
//    // Binding Outlet
//    @IBOutlet var outletsView: UIView!
//    @IBOutlet weak var outLetImageVIew: UIImageView!
//    @IBOutlet weak var outletNameLbl: UILabel!
//    @IBOutlet weak var outletsTableView: UITableView!
//    @IBOutlet weak var numOfOutletsLbl: UILabel!
//    @IBOutlet weak var outletsTVHeightConst: NSLayoutConstraint!
//    @IBOutlet var blackView: UIView!
    
    var tap = UITapGestureRecognizer()
    var locationManager = CLLocationManager()
    var locationManagerMain: Location!
    var mainArr = [GetStores]()
    var noNetWork = NoNetworkView()
    var isLoaded = false
    var bindingArray = [GetStores]()
    var headesArray = ["More CAFE"]
    var selectFilters: [String] = []
    var isFirst:Bool = true
    var isClass:Bool = true

    // Latitude and Longitude
    var latitude:Double = 24.7135517
    var longitude:Double = 46.6752957
    var locationBool:Bool = false
    static var instance: HomeVC!
    let customMarkerWidth: Int = 300
    let customMarkerHeight: Int = 145
    var markers = [GMSMarker]()
    var getSelectFiltersArray = [Int]()
    var getSelectFilters = ""
    var SwiftTimer = Timer()
    var popUpTimer = Timer()
    var filterDistance:Int = 40
    var getStoresServiceCall:Bool = true

    var popover = Popover()
    var isLocationChange:Bool = false
    private var doubleTapGesture: UITapGestureRecognizer!
    private var SingleTapGesture: UITapGestureRecognizer!
    
    private let refreshControl = UIRefreshControl()
    var isSearch : Bool = false
    var isCancel:Bool = false
    var searchingText:String = ""
    var pageNumber:Int = 1
    var totalRows:Int = 10
    
    @IBAction func crashButtonTapped(_ sender: AnyObject) {
        Crashlytics.sharedInstance().crash()
    }
    
    @objc func applicationDidEnterBackground(application:UIApplication)  {
        //print("Background")
        SwiftTimer.invalidate()
    }
    @objc func applicationEnteredForeground(application:UIApplication)  {
        //print("Foreground")
        startTimer()
        pageNumber = 1
        self.getStoresData(isLoader: true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        SwiftTimer.invalidate()
        isClass = false
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.selectedIndex = 0
       // searchView.isHidden = true
        
//        let amount = 10.126
//        let formatter = NumberFormatter()
//        formatter.numberStyle = .decimal
//        formatter.maximumFractionDigits = 2
//        let formattedAmount = formatter.string(from: amount as NSNumber)!
//        print(formattedAmount) // 10
//
//        let vat = 10.126
//        print("2.vat:\(String(format: "%.2f", round(vat*100)/100))")
        
        HomeVC.instance = self
        self.isLoaded = false
        mapView.isHidden=true
        //self.storesTableView.isScrollEnabled = false
        self.storesTableView.estimatedRowHeight = 109
        self.storesTableView.rowHeight = UITableView.automaticDimension
        storesTableView.delegate = self
        storesTableView.dataSource = self
        //self.searchBar.delegate = self
        self.getStoresData(isLoader: true)
        Alert.showAlert(on: self, title: Title, message: "جالسين نصلح اشياء مره رهيبه😍 .. لا تروح بعيد .. خلك قريب")
        if AppDelegate.getDelegate().appLanguage == "English"{
            self.tabBarController?.tabBar.semanticContentAttribute = .forceLeftToRight
           // bannerCollectionView.semanticContentAttribute = .forceLeftToRight
            languageBtn.setTitle("العربية", for: .normal)
        }else{
            self.tabBarController?.tabBar.semanticContentAttribute = .forceRightToLeft
           // bannerCollectionView.semanticContentAttribute = .forceRightToLeft
            languageBtn.setTitle("English", for: .normal)
        }
        //bannerCollectionView.delegate = self
       // bannerCollectionView.dataSource = self

        
//        if  SaveAddressClass.getStoresArray.count > 0 {
//            searchView.isHidden = false
//            self.mainArr.removeAll()
//            self.mainArr = MappingBrands.Brands()
//            self.headesArray.removeAll()
//            self.headesArray = HeaderNames.headerNames()
//            self.isLoaded = true
//            self.storesTableView.reloadData()
//        }else{
//            self.getStoresData(isLoader: true)
//
//        }
        self.firstTimePopUp.frame = CGRect(x: 15, y:self.locationNameLbl.frame.origin.y+self.locationNameLbl.frame.size.height+40 , width: 0, height: 50)
        NotificationCenter.default.addObserver(self, selector: #selector(tabBarCallIndex), name: NSNotification.Name(rawValue: "tabBarCallIndex"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selectStoreSetails), name: NSNotification.Name(rawValue: "selectStoreDetails"), object: nil)
        self.setUpDoubleTap()
        self.mapView.delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            // Crashlytics
            Crashlytics.sharedInstance().setObjectValue("\(UserDefaults.standard.object(forKey: "DeviceToken")!)", forKey: "Device Token")
            Crashlytics.sharedInstance().setObjectValue(AppDelegate.getDelegate().appLanguage!, forKey: "App Language")
            Crashlytics.sharedInstance().setObjectValue(getDeviceLanguage(), forKey: "Device Language")
            if isUserLogIn() == true {
                let userDic = UserDef.getUserProfileDic(key: "userDetails")
                Crashlytics.sharedInstance().setUserIdentifier("\(UserDef.getUserId())")
                Crashlytics.sharedInstance().setUserName("\(userDic["Name"]!)")
                Crashlytics.sharedInstance().setUserEmail("\(userDic["Mobile"]!)")
            }
            
        }
        
//        changeLocationBtn.addTarget(self, action: #selector(ChangeLocationBtn_Tapped(sender:)), for: .touchUpInside)
//        closeBtn.addTarget(self, action: #selector(CloseBtn_Tapped(sender:)), for: .touchUpInside)
        
        //navigationBarTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Stores", value: "", table: nil))!

        if #available(iOS 10.0, *) {
            storesTableView.refreshControl = refreshControl
        } else {
            storesTableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
    }
    @objc private func refreshWeatherData(_ sender: Any) {
        pageNumber = 1
        self.getStoresData(isLoader: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if getStoresServiceCall == true{
            pageNumber = 1
            self.getStoresData(isLoader: true)
        }else{
            getStoresServiceCall = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        isClass = true
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationEnteredForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        let count = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable")
        if (count > 0){
            if let tabItems = tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[2]
                tabItem.badgeValue = "\(count)"
            }
        }else{
            if let tabItems = self.tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[2]
                tabItem.badgeValue = nil
            }
        }

//        if SaveAddressClass.getStoresArray.count == 0{
//            isLoaded = true
//            self.getStoresData(isLoader: true)
//        }
//        if getStoresServiceCall == true{
//            pageNumber = 1
//            self.isLoaded = false
//            self.getStoresData(isLoader: true)
//        }else{
//            pageNumber = 2
//            getStoresServiceCall = true
//        }
    }
    
    func setUpDoubleTap() {
        doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(didDoubleTapCollectionView))
        doubleTapGesture.numberOfTapsRequired = 2
        addressBtn.addGestureRecognizer(doubleTapGesture)
        
        SingleTapGesture = UITapGestureRecognizer(target: self, action: #selector(didSingleTapCollectionView))
        SingleTapGesture.numberOfTapsRequired = 1
        addressBtn.addGestureRecognizer(SingleTapGesture)
        
        SingleTapGesture.require(toFail: doubleTapGesture)
    }
    @objc func popUpClose(){
        popUpTimer.invalidate()
        popover.dismiss()
    }
    //MARK: Search Functionality
    @IBAction func searchBtn_Tapped(_ sender: UIButton) {
        pageNumber = 1
        self.isLoaded = false
        self.getStoreInformation()
    }
    @objc func didSingleTapCollectionView() {
        let dic = UserDefaults.standard.object(forKey: "LocationDetails") as! [String:Any]
        self.locationViewActionHeight.constant = 24
            self.alertMsgLbl.text = "\(dic["FullAddress"]!)"
            self.changeLocationBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Location", value: "", table: nil))!, for: .normal)
            let height = self.view.safeAreaInsets.top
            let startPoint = CGPoint(x: self.locationNameLbl.frame.midX, y: self.locationNameLbl.frame.maxY + height - 10)
            self.locationView.frame =  CGRect(x: 0, y: 0, width: 300, height: 75)
            self.popover.show(self.locationView, point: startPoint)
        popUpTimer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(HomeVC.popUpClose), userInfo: nil, repeats: false);
    }
    
    @objc func didDoubleTapCollectionView() {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        AppDelegate.getDelegate().homeScreen = true
        var obj = SelectAddressVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "SelectAddressVC") as! SelectAddressVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func popChangeLocationBtn_Tapped(_ sender: Any) {
        popover.dismiss()
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        AppDelegate.getDelegate().homeScreen = true
        var obj = SelectAddressVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "SelectAddressVC") as! SelectAddressVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func getStoresData(isLoader:Bool)  {
        if isLocationArray() ==  true {
            let dic = UserDefaults.standard.object(forKey: "LocationDetails") as! [String:Any]
            let str = "\(dic["Adreess"]!)"
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "down arrow 2")
            let myString:NSMutableAttributedString = NSMutableAttributedString(string: str)
            if AppDelegate.getDelegate().appLanguage == "English"{
                attachment.bounds = CGRect(x: 03, y: -3, width: 15, height: 15)
                let attachmentStr = NSAttributedString(attachment: attachment)
                myString.append(attachmentStr)
                self.locationNameLbl.attributedText = myString
            }else{
                attachment.bounds = CGRect(x: -03, y: -3, width: 15, height: 15)
                let attachmentStr = NSAttributedString(attachment: attachment)
                let name:NSMutableAttributedString = attachmentStr as! NSMutableAttributedString
                name.append(myString)
                self.locationNameLbl.attributedText = name
            }
            self.popUpLocTxtLbl.text = "\(dic["FullAddress"]!)"
            self.latitude = Double(dic["Latitude"] as! String)!
            self.longitude = Double(dic["Longitude"] as! String)!
            
            // Retrive Data
            //self.retrieveFromStoreInformationFile()
            //self.retrieveBannerDetailsFile()
            
            self.SwiftTimer.invalidate()
            self.startTimer()
            
            //ANLoader.showLoading("Loading..", disableUI: true)
            //isClass = true
            //DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
              //self.getStoreInformation()
            //}
            self.alertFirstTime()
            self.isLoaded = isLoader
            self.getStoreInformation()

            
//            if getStoresServiceCall == true{
//                pageNumber = 1
//                self.getStoreInformation()
//            }else{
//                getStoresServiceCall = true
//            }
            
            // Closed Ravi
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//               if self.isLocationChange == true ||  SaveAddressClass.getStoresArray.count == 0 || isLoader == true{
//                    self.isLoaded = false
//                    self.getStoreInformation()
//                }else{
//                    self.isLoaded = true
//                    self.getStoreInformation()
//                }
//            }
            
            
            
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "LocationSelectionVC") as! LocationSelectionVC
            obj.modalPresentationStyle = .overFullScreen
            obj.locationSelectionVCDelegrate = self
            present(obj, animated: false, completion: nil)
            
        }
    }
    func alertFirstTime(){
        if AppDelegate.getDelegate().popUpAlert == false {
            AppDelegate.getDelegate().popUpAlert = true
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: "جالسين نصلح اشياء مره رهيبه😍 .. لا تروح بعيد .. خلك قريب")
        }
    }
    func didTapAction(dic: [Int],distance:Int) {
        pageNumber = 1
        getSelectFiltersArray = dic
        filterDistance = distance
        let stringArray = getSelectFiltersArray.map  { String($0) }
        getSelectFilters = stringArray.joined(separator: ",")
        getStoreInformation()
    }
    @IBAction func languageBtn_Tapped(_ sender: Any) {
        let language = AppDelegate.getDelegate().appLanguage == "English" ? "Ar":"En"
        self.laguageChangeFunc()
        if Connectivity.isConnectedToInternet() == true && isUserLogIn() == true {
            DispatchQueue.global(qos: .background).async {
                let dic:[String:Any] = ["Flag":"1","deviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))","UserId":"\(UserDef.getUserId())","Language":language, "DeviceType":"iOS"]
                UserModuleService.UpdateLanguageService(dic: dic,isLoader:false,isUIDisable:true, success: { (data) in
                }){ (error) in
                     print (error)
                }
            }
        }
    }
    
    func laguageChangeFunc() {
        if(AppDelegate.getDelegate().myLanguage == "English"){
            var path = String()
            path = Bundle.main.path(forResource: "ar", ofType: "lproj")!
            AppDelegate.getDelegate().filePath = Bundle.init(path: path)
            AppDelegate.getDelegate().myLanguage = "Arabic"
            UserDefaults.standard.set("Arabic", forKey: "Language")
            AppDelegate.getDelegate().appLanguage = "Arabic"
            AppDelegate.getDelegate().tabBarController()
        }else{
            var path = String()
            path = Bundle.main.path(forResource: "en", ofType: "lproj")!
            AppDelegate.getDelegate().filePath = Bundle.init(path: path)
            AppDelegate.getDelegate().myLanguage = "English"
            UserDefaults.standard.set("English", forKey: "Language")
            AppDelegate.getDelegate().appLanguage = "English"
            AppDelegate.getDelegate().tabBarController()
        }
    }
    
//    @objc func scrollToNextCell(){
//        //get cell size
//        let cellSize = CGSize(width: bannerCollectionView.frame.width, height: bannerCollectionView.frame.height)
//
//        //get current content Offset of the Collection view
//        let contentOffset = bannerCollectionView.contentOffset;
//
//        if bannerCollectionView.contentSize.width <= bannerCollectionView.contentOffset.x + cellSize.width
//        {
//            bannerCollectionView.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y  , width: cellSize.width, height: cellSize.height), animated: true)
//        } else {
//            bannerCollectionView.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width , y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
//        }
//    }
    
    func startTimer() {
        //SwiftTimer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(HomeVC.scrollToNextCell), userInfo: nil, repeats: true);
    }
    
    @IBAction func addressBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        AppDelegate.getDelegate().homeScreen = true
        var obj = SelectAddressVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "SelectAddressVC") as! SelectAddressVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func popViewBtn_Tapped(_ sender: UIButton) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        obj.addFilterVCDelegate = self
        obj.modalPresentationStyle = .overCurrentContext
        present(obj, animated: false, completion: nil)
    }
    

    @objc func tabBarCallIndex()  {
        self.tabBarController?.selectedIndex = 1
    }
    
    @objc func bannersViewAll(){
//        var obj = ViewAllVC()
//        obj = self.storyboard?.instantiateViewController(withIdentifier: "ViewAllVC") as! ViewAllVC
//        obj.objStr = "Banners"
//        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func selectStoreSetails(){
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        var obj = MenuVC()
        obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func CloseBtn_Tapped(sender:UIButton){
        popover.dismiss()
    }
    
//    @objc func ChangeLocationBtn_Tapped(sender:UIButton){
//        if changeLocationBtn.titleLabel?.text! == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Location", value: "", table: nil))!{
//            let dic = UserDefaults.standard.object(forKey: "ChangeLocationDetails") as! [String:Any]
//            let str = "\(dic["Adreess"]!)"
//            self.locationNameLbl.text! = str
//            popover.dismiss()
//            let dic2 = ["Adreess": dic["Adreess"]!,
//                       "FullAddress":dic["FullAddress"]!,
//                "AddressId":0,
//                "Latitude":dic["Latitude"]!,
//                "Longitude":dic["Longitude"]!] as [String : Any]
//            UserDefaults.standard.set(dic2, forKey:"LocationDetails")
//            UserDefaults.standard.synchronize()
//            self.isLoaded = false
//            self.getStoreInformation()
//            return
//        }else{
//            checkLocationPermission()
//            popover.dismiss()
//        }
//    }
    
    func LocationChecking(){
        // Location Manager
        locationManagerMain = Location()
        locationManagerMain.needToDisplayAlert = true
        locationManagerMain.delegate = self
    }
    
    func initGoogleMaps() {
        let camera = GMSCameraPosition.camera(withLatitude:24.7135517, longitude: 46.6752957, zoom: 12.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = false
//        self.googleMapsView.camera = camera
//        self.googleMapsView.delegate = self
//        self.googleMapsView.isMyLocationEnabled = true
//        self.googleMapsView.settings.myLocationButton = false
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 24.7135517, longitude: 46.6752957)
        marker.title = ""
        marker.snippet = ""
        marker.map = mapView
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        //let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        //self.googleMapsView.animate(to: camera)
        self.locationManager.stopUpdatingLocation()
        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2DMake((location?.coordinate.latitude)!, (location?.coordinate.longitude)!), completionHandler: { response, error in
            /*if let aResults = response?.results() {
                print("\(aResults)")
            }
            for addressObj: GMSAddress? in response?.results() ?? [GMSAddress?]() {
                
                var str = ""
                if let sublocality = addressObj!.subLocality {
                    str = sublocality
                }else if let locality = addressObj!.locality{
                    str = locality
                }else{
                    str = addressObj!.country!
                }
                
                var sLocality = ""
                if let locality = addressObj!.locality {
                    sLocality = locality
                }else{
                    sLocality = addressObj!.country!
                }
                
                //let str = "\(String(describing: addressObj!.subLocality!))"
                let attachment = NSTextAttachment()
                attachment.image = UIImage(named: "dropArrow_Blue")
                
                let myString:NSMutableAttributedString = NSMutableAttributedString(string: str)
                if AppDelegate.getDelegate().appLanguage == "English"{
                    attachment.bounds = CGRect(x: 03, y: -3, width: 15, height: 15)
                    let attachmentStr = NSAttributedString(attachment: attachment)
                    myString.append(attachmentStr)
                    self.locationNameLbl.attributedText = myString
                }else{
                    attachment.bounds = CGRect(x: -03, y: -3, width: 15, height: 15)
                    let attachmentStr = NSAttributedString(attachment: attachment)
                    let name:NSMutableAttributedString = attachmentStr as! NSMutableAttributedString
                    name.append(myString)
                    self.locationNameLbl.attributedText = name
                }
                
                
             
                
                self.popUpLocTxtLbl.text = "\(str), \(String(describing: sLocality)), \(String(describing: addressObj!.country!))"
                
                self.latitude = addressObj!.coordinate.latitude
                self.longitude = addressObj!.coordinate.longitude
                
                let dic = ["Adreess": str,
                           "FullAddress":"\(str), \(String(describing: sLocality)), \(String(describing: addressObj!.country!))",
                           "AddressId":0,
                           "Latitude":"\(self.latitude)",
                    "Longitude":"\(self.longitude)"] as [String : Any]
                UserDefaults.standard.set(dic, forKey:"LocationDetails")
                UserDefaults.standard.synchronize()
                */
            
            
            
            if let addressObj = response?.firstResult() {
                
                //for addressObj: GMSAddress? in response?.firstResult() ?? [GMSAddress?]() {
                
                var str = ""
                if let sublocality = addressObj.subLocality {
                    str = sublocality
                }else if let locality = addressObj.locality{
                    str = locality
                }else{
                    str = addressObj.country!
                }
                
                var sLocality = ""
                if let lines = addressObj.lines {
                    let lines = lines as [String]
                    sLocality = lines.joined(separator: ",")
                }else if let sublocality = addressObj.subLocality {
                    sLocality = sublocality
                }else if let locality = addressObj.locality {
                    sLocality = locality
                }else{
                    sLocality = addressObj.country!
                }
                
                self.latitude = addressObj.coordinate.latitude
                self.longitude = addressObj.coordinate.longitude
                
                let dic = ["Adreess": str,
                           "FullAddress":"\(str), \(String(describing: sLocality)), \(String(describing: addressObj.country!))",
                    "AddressId":0,
                    "Latitude":"\(self.latitude)",
                    "Longitude":"\(self.longitude)"] as [String : Any]
                UserDefaults.standard.set(dic, forKey:"CurrentLocationDetails")
                UserDefaults.standard.synchronize()
                ANLoader.hide()
                self.navigationController?.popViewController(animated: false)
                
            }
            
            
                if  self.locationBool == false {
                    self.locationBool = true
                    // Move to a background thread to do some long running work
                   // DispatchQueue.global(qos: .userInitiated).async {
                        self.getStoreInformation()
                   // }
                }
                return
            //}
        })
    }
    // MARK: - Notifications
    @objc func addPopUp(){
        self.firstTimePopUp.isHidden = false
        self.firstTimePopUp.layer.borderColor = UIColor.gray.cgColor
        self.firstTimePopUp.layer.borderWidth = 1
        self.firstTimePopUp.layer.cornerRadius = 8
        self.firstTimePopUp.layer.masksToBounds = true
        self.firstTimePopUp.frame = CGRect(x: 15, y:self.locationNameLbl.frame.origin.y+self.locationNameLbl.frame.size.height+40 , width: self.view.frame.size.width-30, height: 50)
        self.view.addSubview(firstTimePopUp)
        UIView.animate(withDuration: 1) {
            self.view.layoutIfNeeded()
        }
    }
    @objc func closePopUp(){
        UIView.animate(withDuration: 1) {
            self.firstTimePopUp.frame = CGRect(x: 15, y:self.locationNameLbl.frame.origin.y+self.locationNameLbl.frame.size.height+40 , width: 0, height: 50)
            self.view.layoutIfNeeded()
        }
        //self.firstTimePopUp.isHidden = true
    }
    func TryAgain() {
        DispatchQueue.main.async {
            self.noNetWork.isHidden = true
            self.getStoreInformation()
        }
    }
    
    func retrieveFromStoreInformationFile() {
        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("StoreInformation")
        
        // Read data from .json file and transform data into an array
        do {
            let data = try Data(contentsOf: fileUrl, options: [])
            guard let personArray = try JSONSerialization.jsonObject(with: data, options: []) as? [Dictionary<String, Any>] else { return }
            // Store Detailes
            SaveAddressClass.getStoresArray.removeAll()
            for storeList in personArray {
                
                let storesDic = GetStores(AvgPreparationTime: storeList["AvgPreparationTime"] is NSNull ? 0 : storeList["AvgPreparationTime"] as! Int,
                                          BranchId: storeList["BranchId"] is NSNull ? 0 : storeList["BranchId"] as! Int,
                                          BranchName_En: storeList["BranchName_En"] is NSNull ? "" : storeList["BranchName_En"] as! String,
                                          BranchName_Ar: storeList["BranchName_Ar"] is NSNull ? "" : storeList["BranchName_Ar"] as! String,
                                          DeliveryCharges: storeList["DeliveryCharges"] is NSNull ? 0 : storeList["DeliveryCharges"] as! Double,
                                          Address: storeList["Address"] is NSNull ? "" : storeList["Address"] as! String,
                                          MinOrderCharges: storeList["MinOrderCharges"] is NSNull ? 0 : storeList["MinOrderCharges"] as! Double,
                                          EmailId: storeList["EmailId"] is NSNull ? "" : storeList["EmailId"] as! String,
                                          MobileNo: storeList["MobileNo"] is NSNull ? "" : storeList["MobileNo"] as! String,
                                          OrderTypes: storeList["OrderTypes"] is NSNull ? "" : storeList["OrderTypes"] as! String,
                                          Rating: storeList["Rating"] is NSNull ? "" : storeList["Rating"] as! String,
                                          RatingCount: storeList["RatingCount"] is NSNull ? 0 : storeList["RatingCount"] as! Int,
                                          StoreLogo_En: storeList["StoreLogo_En"] is NSNull ? "" : storeList["StoreLogo_En"] as! String,
                                          StoreImage_En: storeList["StoreImage_En"] is NSNull ? "" : storeList["StoreImage_En"] as! String,
                                          StoreStatus: storeList["StoreStatus"] is NSNull ? "" : storeList["StoreStatus"] as! String,
                                          StoreType: storeList["StoreType"] is NSNull ? "" : storeList["StoreType"] as! String,
                                          TakeAwayDistance: storeList["TakeAwayDistance"] is NSNull ? 0 : storeList["TakeAwayDistance"] as! Int,
                                          DeliveryDistance: storeList["DeliveryDistance"] is NSNull ? 0.00 : storeList["DeliveryDistance"] as! Double,
                                          StarDateTime: storeList["StarDateTime"] is NSNull ? "" : storeList["StarDateTime"] as! String,
                                          EndDateTime: storeList["EndDateTime"] is NSNull ? "" : storeList["EndDateTime"] as! String,
                                          CurrentDateTime: storeList["CurrentDateTime"] is NSNull ? "" : storeList["CurrentDateTime"] as! String,
                                          IsPromoted: storeList["IsPromoted"] is NSNull ? false : storeList["IsPromoted"] as! Bool,
                                          Popular: storeList["Popular"] is NSNull ? false : storeList["Popular"] as! Bool,
                                          DiscountAmt: storeList["DiscountAmt"] is NSNull ? 0 : storeList["DiscountAmt"] as! Int,
                                          IsNew: storeList["IsNew"] is NSNull ? false : storeList["IsNew"] as! Bool,
                                          Latitude: storeList["Latitude"] is NSNull ? 0.00 : storeList["Latitude"] as! Double,
                                          Longitude: storeList["Longitude"] is NSNull ? 0.00 : storeList["Longitude"] as! Double,
                                          Distance: storeList["Distance"] is NSNull ? 0.00 : storeList["Distance"] as! Double,
                                          BrandId: storeList["BrandId"] is NSNull ? 0 : storeList["BrandId"] as! Int,
                                          BindingCount: 1,
                                          BrandName_En: storeList["BrandName_En"] is NSNull ? "" : storeList["BrandName_En"] as! String,
                                          BrandName_Ar: storeList["BrandName_Ar"] is NSNull ? "" : storeList["BrandName_Ar"] as! String,
                                          PaymentType:  storeList["PaymentType"] is NSNull ? "" : storeList["PaymentType"] as! String,
                                          BranchDescription_En: storeList["BranchDescription_En"] is NSNull ? "" : storeList["BranchDescription_En"] as! String,                                                 Serving: storeList["Serving"] is NSNull ? 0 : storeList["Serving"] as! Int, FutureOrderDay: storeList["FutureOrderDay"] is NSNull ? 0 : storeList["FutureOrderDay"] as! Int, StoreType_Ar:storeList["StoreType_Ar"] is NSNull ? "" : storeList["StoreType_Ar"] as! String, BranchDescription_Ar:storeList["BranchDescription_Ar"] is NSNull ? "" : storeList["BranchDescription_Ar"] as! String, UpdatesAvailable: storeList["UpdatesAvailable"] is NSNull ? 0 : storeList["UpdatesAvailable"] as! Int, UpdateSeverity: storeList["UpdateSeverity"] is NSNull ? 0 : storeList["UpdateSeverity"] as! Int, VatPercentage: storeList["VatPercentage"] is NSNull ? 0 : storeList["VatPercentage"] as! Double
                )
                
                // Save More Stores
                SaveAddressClass.getStoresArray.append(storesDic)
            }
            
            // Filters
            SaveAddressClass.getStoresArray = SortingFilters.sortingFilters(array: SaveAddressClass.getStoresArray)

            DispatchQueue.main.async {
                self.isLoaded = true
                //self.searchView.isHidden = false
                //self.bannerCollectionView.reloadData()
                self.storesTableView.reloadData()
            }
            
        } catch {
            print(error)
        }
    }
    
    
    func retrieveBannerDetailsFile() {
        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentsDirectoryUrl.appendingPathComponent("BannerDetails")
        
        // Read data from .json file and transform data into an array
        do {
            let data = try Data(contentsOf: fileUrl, options: [])
            guard let personArray = try JSONSerialization.jsonObject(with: data, options: []) as? [Dictionary<String, Any>] else { return }
            
            SaveAddressClass.getStoreBannerArray.removeAll()
            for bannerList in personArray{
                let bannerDic = GetBanners(BannerName_En: bannerList["BannerName_En"] is NSNull ? "" : bannerList["BannerName_En"] as! String,
                                           BannerName_Ar: bannerList["BannerName_Ar"] is NSNull ? "" : bannerList["BannerName_Ar"] as! String,
                                           BannerImage: bannerList["BannerImage"] is NSNull ? "" : bannerList["BannerImage"] as! String,
                                           StoreDetails: self.getStores(inputArray:  bannerList["StoreDetails"] as! Array<AnyObject> ) as! [GetStores])
                
                SaveAddressClass.getStoreBannerArray.append(bannerDic)
            }
            
            
            // Filters
            SaveAddressClass.getStoresArray = SortingFilters.sortingFilters(array: SaveAddressClass.getStoresArray)
            
            DispatchQueue.main.async {
                self.isLoaded = true
               // self.bannerCollectionView.reloadData()
                self.storesTableView.reloadData()
            }
            
        } catch {
            print(error)
        }
    }
    
    func saveToFile(input:[Dictionary<String, Any>],fileName:String) {
        // Get the url of Persons.json in document directory
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent(fileName)
        
        // Transform array into data and save it into file
        do {
            let data = try JSONSerialization.data(withJSONObject: input, options: [])
            try data.write(to: fileUrl, options: [])
        } catch {
            print(error)
        }
    }
    
    func getStoreInformation()  {
        if self.latitude == 0 || self.longitude == 0 {
            ANLoader.hide()
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Location Services Disabled! Please enable Location Based Services for better results! We promise to keep your location private", value: "", table: nil))!)
            return
        }
        var jsonDic = [String:Any]()
        let getSelectVendor = AppDelegate.getDelegate().selectVendor.count
        if getSelectVendor == 0{
            AppDelegate.getDelegate().selectVendor.append(0)
        }
        if getSelectFilters == "" && filterDistance == 40{
            jsonDic = ["Latitude" : "\(self.latitude)", "Longitude" : "\(self.longitude)","pageNumber":"\(pageNumber)","pageSize":"40","SearchText":"\(String(describing: searchingText))"]
        }else if getSelectFilters == ""{
            jsonDic = ["Latitude" : "\(self.latitude)", "Longitude" : "\(self.longitude)", "KM":filterDistance,"SearchText":"\(String(describing: searchingText))","pageNumber":"\(pageNumber)","pageSize":"40","VendorType":AppDelegate.getDelegate().selectVendor[0]]
        }else{
            jsonDic = ["Latitude" : "\(self.latitude)", "Longitude" : "\(self.longitude)", "FilterIds" :"\(getSelectFilters)","KM":filterDistance,"SearchText":"\(String(describing: searchingText))","pageNumber":"\(pageNumber)","pageSize":"40","VendorType":AppDelegate.getDelegate().selectVendor[0]]
        }
        
        if(Connectivity.isConnectedToInternet()){
            if self.isLoaded == true  {
                isClass = true
            }
            if pageNumber == 1 {
                ANLoader.showLoading("Loading..", disableUI: true)
            }
            OrderApiRouter.getStoreInformation(dic: jsonDic, success: { (data) in
                ANLoader.hide()
                if data?.status == false{
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    }
                    
                    self.isLoaded = false
                    self.refreshControl.endRefreshing()
                    self.storesTableView.reloadData()
                }else{
                    self.refreshControl.endRefreshing()
                    // Store Detailes
                    if self.pageNumber == 1 {
                        SaveAddressClass.getStoresArray.removeAll()
                    }
                    for storeList in data!.successDic!.StoresDetails!{
                        let storesDic = GetStores(AvgPreparationTime: storeList["AvgPreparationTime"] is NSNull ? 0 : storeList["AvgPreparationTime"] as! Int,
                                                  BranchId: storeList["BranchId"] is NSNull ? 0 : storeList["BranchId"] as! Int,
                                                  BranchName_En: storeList["BranchName_En"] is NSNull ? "" : storeList["BranchName_En"] as! String,
                                                  BranchName_Ar: storeList["BranchName_Ar"] is NSNull ? "" : storeList["BranchName_Ar"] as! String,
                                                  DeliveryCharges: storeList["DeliveryCharges"] is NSNull ? 0 : storeList["DeliveryCharges"] as! Double,
                                                  Address: storeList["Address"] is NSNull ? "" : storeList["Address"] as! String,
                                                  MinOrderCharges: storeList["MinOrderCharges"] is NSNull ? 0 : storeList["MinOrderCharges"] as! Double,
                                                  EmailId: storeList["EmailId"] is NSNull ? "" : storeList["EmailId"] as! String,
                                                  MobileNo: storeList["MobileNo"] is NSNull ? "" : storeList["MobileNo"] as! String,
                                                  OrderTypes: storeList["OrderTypes"] is NSNull ? "" : storeList["OrderTypes"] as! String,
                                                  Rating: storeList["Rating"] is NSNull ? "" : storeList["Rating"] as! String,
                                                  RatingCount: storeList["RatingCount"] is NSNull ? 0 : storeList["RatingCount"] as! Int,
                                                  StoreLogo_En: storeList["StoreLogo_En"] is NSNull ? "" : storeList["StoreLogo_En"] as! String,
                                                  StoreImage_En: storeList["StoreImage_En"] is NSNull ? "" : storeList["StoreImage_En"] as! String,
                                                  StoreStatus: storeList["StoreStatus"] is NSNull ? "" : storeList["StoreStatus"] as! String,
                                                  StoreType: storeList["StoreType"] is NSNull ? "" : storeList["StoreType"] as! String,
                                                  TakeAwayDistance: storeList["TakeAwayDistance"] is NSNull ? 0 : storeList["TakeAwayDistance"] as! Int,
                                                  DeliveryDistance: storeList["DeliveryDistance"] is NSNull ? 0.00 : storeList["DeliveryDistance"] as! Double,
                                                  StarDateTime: storeList["StarDateTime"] is NSNull ? "" : storeList["StarDateTime"] as! String,
                                                  EndDateTime: storeList["EndDateTime"] is NSNull ? "" : storeList["EndDateTime"] as! String,
                                                  CurrentDateTime: storeList["CurrentDateTime"] is NSNull ? "" : storeList["CurrentDateTime"] as! String,
                                                  IsPromoted: storeList["IsPromoted"] is NSNull ? false : storeList["IsPromoted"] as! Bool,
                                                  Popular: storeList["Popular"] is NSNull ? false : storeList["Popular"] as! Bool,
                                                  DiscountAmt: storeList["DiscountAmt"] is NSNull ? 0 : storeList["DiscountAmt"] as! Int,
                                                  IsNew: storeList["IsNew"] is NSNull ? false : storeList["IsNew"] as! Bool,
                                                  Latitude: storeList["Latitude"] is NSNull ? 0.00 : storeList["Latitude"] as! Double,
                                                  Longitude: storeList["Longitude"] is NSNull ? 0.00 : storeList["Longitude"] as! Double,
                                                  Distance: storeList["Distance"] is NSNull ? 0.00 : storeList["Distance"] as! Double,
                                                  BrandId: storeList["BrandId"] is NSNull ? 0 : storeList["BrandId"] as! Int,
                                                  BindingCount: 1,
                                                  BrandName_En: storeList["BrandName_En"] is NSNull ? "" : storeList["BrandName_En"] as! String,
                                                  BrandName_Ar: storeList["BrandName_Ar"] is NSNull ? "" : storeList["BrandName_Ar"] as! String,
                                                  PaymentType:  storeList["PaymentType"] is NSNull ? "" : storeList["PaymentType"] as! String,
                                                  BranchDescription_En: storeList["BranchDescription_En"] is NSNull ? "" : storeList["BranchDescription_En"] as! String,                                                 Serving: storeList["Serving"] is NSNull ? 0 : storeList["Serving"] as! Int, FutureOrderDay: storeList["FutureOrderDay"] is NSNull ? 0 : storeList["FutureOrderDay"] as! Int, StoreType_Ar:storeList["StoreType_Ar"] is NSNull ? "" : storeList["StoreType_Ar"] as! String, BranchDescription_Ar:storeList["BranchDescription_Ar"] is NSNull ? "" : storeList["BranchDescription_Ar"] as! String,UpdatesAvailable: storeList["UpdatesAvailable"] is NSNull ? 0 : storeList["UpdatesAvailable"] as! Int, UpdateSeverity: storeList["UpdateSeverity"] is NSNull ? 0 : storeList["UpdateSeverity"] as! Int, VatPercentage: storeList["VatPercentage"] is NSNull ? 0 : storeList["VatPercentage"] as! Double
                        )
                        
                        // Save More Stores
                        SaveAddressClass.getStoresArray.append(storesDic)
                    }
                    
                    // Filters
                    SaveAddressClass.getStoresArray = SortingFilters.sortingFilters(array: SaveAddressClass.getStoresArray)
                    SaveAddressClass.getStoreBannerArray.removeAll()
                    for bannerList in data!.successDic!.BannerDetails!{
                        let bannerDic = GetBanners(BannerName_En: bannerList["BannerName_En"] is NSNull ? "" : bannerList["BannerName_En"] as! String,
                                                   BannerName_Ar: bannerList["BannerName_Ar"] is NSNull ? "" : bannerList["BannerName_Ar"] as! String,
                                                   BannerImage: bannerList["BannerImage"] is NSNull ? "" : bannerList["BannerImage"] as! String,
                                                   StoreDetails: self.getStores(inputArray:  bannerList["StoreDetails"] as! Array<AnyObject> ) as! [GetStores])
                        
                        SaveAddressClass.getStoreBannerArray.append(bannerDic)
                    }
                    
                    // Brands
                    self.mainArr.removeAll()
                    self.mainArr = MappingBrands.Brands()
                    
                    // Header Names
                    self.headesArray.removeAll()
                    self.headesArray = HeaderNames.headerNames()
                    // Filters
                    SaveAddressClass.getFilters.removeAll()
                    for storeList in data!.successDic!.FilterCategories!{
                        let filterDic = GetFilterCategory(FilterTypeId: storeList["FilterTypeId"] is NSNull ? 0 : storeList["FilterTypeId"] as! Int, FilterTypeName_En: storeList["FilterTypeName_En"] is NSNull ? "" : storeList["FilterTypeName_En"] as! String, FilterTypeName_Ar: storeList["FilterTypeName_Ar"] is NSNull ? "" : storeList["FilterTypeName_Ar"] as! String, Filters: self.filtersArrayFunc(inputArray: storeList["Filters"] as! Array<AnyObject> ) as! [GetAdditionalFilters])
                        
                        // Save filters
                        SaveAddressClass.getFilters.append(filterDic)
                    }
                    
                    //VendorTypes
                    SaveAddressClass.getVendorTypes.removeAll()
                    for storeList in data!.successDic!.VendorTypes!{
                        let vendorDic = GetVendorTypes(Id: storeList["Id"] is NSNull ? 0 : storeList["Id"] as! Int, Name_En: storeList["Name_En"] is NSNull ? "" : storeList["Name_En"] as! String,
                            Name_Ar: storeList["Name_Ar"] is NSNull ? "" : storeList["Name_Ar"] as! String,
                            Status: false)
                        
                        // Save Vendor Types
                        SaveAddressClass.getVendorTypes.append(vendorDic)
                    }
                    self.totalRows = data!.successDic!.TotalRows!
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        self.isLoaded = true
                        //self.searchView.isHidden = false

                        //self.bannerCollectionView.reloadData()
                        self.storesTableView.reloadData()

                         if AppDelegate.getDelegate().isFirst == true{
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                if(isUserLogIn() == true && appDelegate.activeOrder == true)    {
                                       // Move to a background thread to do some long running work
                                        DispatchQueue.global(qos: .userInitiated).async {
                                          self.getActiveOrder()
                                      }
                                 }
                          
                         
                              AppDelegate.getDelegate().isFirst = false
                              DispatchQueue.main.async() {
                               self.LocationChecking()
                              }
                          }
                    }

                }
            }){ (error) in
                ANLoader.hide()
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: error)
                //self.navigationController?.popViewController(animated: true)
                self.isLoaded = false
                self.storesTableView.reloadData()
            }
        }else{
            ANLoader.hide()
            self.isLoaded = false
            self.storesTableView.reloadData()
            self.noNetWork = (Bundle.main.loadNibNamed("NoNetworkView", owner: self, options: nil)?.first as? NoNetworkView)!
            self.noNetWork.delegate = self
            self.noNetWork.frame = CGRect(x:0,y:0,width:self.view.frame.width,height:self.view.frame.height-49)
            self.view.addSubview(self.noNetWork)
            // _ = SCLAlertView().showInfo(appTitle, subTitle: internetFail,closeButtonTitle: "OK")
        }
    }
    func filtersArrayFunc(inputArray:Array<AnyObject>) -> Array<AnyObject> {
        var itemsArray = [GetAdditionalFilters]()
        for Dic in inputArray as! [Dictionary<String, Any>] {
            let additionalsDic = GetAdditionalFilters(FilterId: Dic["FilterId"] is NSNull ? 0 : Dic["FilterId"] as! Int, FilterName_En: Dic["FilterName_En"] is NSNull ? "" : Dic["FilterName_En"] as! String, FilterName_Ar: Dic["FilterName_Ar"] is NSNull ? "" : Dic["FilterName_Ar"] as! String, Status: false)
            itemsArray.append(additionalsDic)
        }
        return itemsArray
    }
    // Get Stores
    func getStores(inputArray:Array<AnyObject>) -> Array<AnyObject> {
        var getStoresDetails = [GetStores]()
        
        for storeList in inputArray as! [Dictionary<String, Any>] {
            
            let storesDic = GetStores(AvgPreparationTime: storeList["AvgPreparationTime"] is NSNull ? 0 : storeList["AvgPreparationTime"] as! Int,
                                      BranchId: storeList["BranchId"] is NSNull ? 0 : storeList["BranchId"] as! Int,
                                      BranchName_En: storeList["BranchName_En"] is NSNull ? "" : storeList["BranchName_En"] as! String,
                                      BranchName_Ar: storeList["BranchName_Ar"] is NSNull ? "" : storeList["BranchName_Ar"] as! String,
                                      DeliveryCharges: storeList["DeliveryCharges"] is NSNull ? 0 : storeList["DeliveryCharges"] as! Double,
                                      Address: storeList["Address"] is NSNull ? "" : storeList["Address"] as! String,
                                      MinOrderCharges: storeList["MinOrderCharges"] is NSNull ? 0 : storeList["MinOrderCharges"] as! Double,
                                      EmailId: storeList["EmailId"] is NSNull ? "" : storeList["EmailId"] as! String,
                                      MobileNo: storeList["MobileNo"] is NSNull ? "" : storeList["MobileNo"] as! String,
                                      OrderTypes: storeList["OrderTypes"] is NSNull ? "" : storeList["OrderTypes"] as! String,
                                      Rating: storeList["Rating"] is NSNull ? "" : storeList["Rating"] as! String,
                                      RatingCount: storeList["RatingCount"] is NSNull ? 0 : storeList["RatingCount"] as! Int,
                                      StoreLogo_En: storeList["StoreLogo_En"] is NSNull ? "" : storeList["StoreLogo_En"] as! String,
                                      StoreImage_En: storeList["StoreImage_En"] is NSNull ? "" : storeList["StoreImage_En"] as! String,
                                      StoreStatus: storeList["StoreStatus"] is NSNull ? "" : storeList["StoreStatus"] as! String,
                                      StoreType: storeList["StoreType"] is NSNull ? "" : storeList["StoreType"] as! String,
                                      TakeAwayDistance: storeList["TakeAwayDistance"] is NSNull ? 0 : storeList["TakeAwayDistance"] as! Int,
                                      DeliveryDistance: storeList["DeliveryDistance"] is NSNull ? 0.00 : storeList["DeliveryDistance"] as! Double,
                                      StarDateTime: storeList["StarDateTime"] is NSNull ? "" : storeList["StarDateTime"] as! String,
                                      EndDateTime: storeList["EndDateTime"] is NSNull ? "" : storeList["EndDateTime"] as! String,
                                      CurrentDateTime: storeList["CurrentDateTime"] is NSNull ? "" : storeList["CurrentDateTime"] as! String,
                                      IsPromoted: storeList["IsPromoted"] is NSNull ? false : storeList["IsPromoted"] as! Bool,
                                      Popular: storeList["Popular"] is NSNull ? false : storeList["Popular"] as! Bool,
                                      DiscountAmt: storeList["DiscountAmt"] is NSNull ? 0 : storeList["DiscountAmt"] as! Int,
                                      IsNew: storeList["IsNew"] is NSNull ? false : storeList["IsNew"] as! Bool,
                                      Latitude: storeList["Latitude"] is NSNull ? 0.00 : storeList["Latitude"] as! Double,
                                      Longitude: storeList["Longitude"] is NSNull ? 0.00 : storeList["Longitude"] as! Double,
                                      Distance: storeList["Distance"] is NSNull ? 0.00 : storeList["Distance"] as! Double,
                                      BrandId: storeList["BrandId"] is NSNull ? 0 : storeList["BrandId"] as! Int,
                                      BindingCount: 1,
                                      BrandName_En: storeList["BrandName_En"] is NSNull ? "" : storeList["BrandName_En"] as! String,
                                      BrandName_Ar: storeList["BrandName_Ar"] is NSNull ? "" : storeList["BrandName_Ar"] as! String,
                                      PaymentType:  storeList["PaymentType"] is NSNull ? "" : storeList["PaymentType"] as! String,
                                      BranchDescription_En: storeList["BranchDescription_En"] is NSNull ? "" : storeList["BranchDescription_En"] as! String,
                                      Serving: storeList["Serving"] is NSNull ? 0 : storeList["Serving"] as! Int, FutureOrderDay: storeList["FutureOrderDay"] is NSNull ? 0 : storeList["FutureOrderDay"] as! Int, StoreType_Ar:storeList["StoreType_Ar"] is NSNull ? "" : storeList["StoreType_Ar"] as! String, BranchDescription_Ar:storeList["BranchDescription_Ar"] is NSNull ? "" : storeList["BranchDescription_Ar"] as! String, UpdatesAvailable: storeList["UpdatesAvailable"] is NSNull ? 0 : storeList["UpdatesAvailable"] as! Int, UpdateSeverity: storeList["UpdateSeverity"] is NSNull ? 0 : storeList["UpdateSeverity"] as! Int, VatPercentage: storeList["VatPercentage"] is NSNull ? 0 : storeList["VatPercentage"] as! Double
            )
            getStoresDetails.append(storesDic)
        }
        return getStoresDetails
    }
    func getActiveOrder()  {
        if(Connectivity.isConnectedToInternet()){
            let jsonDic : [String:Any] = ["UserId" : "\(UserDef.getUserId())"]
            OrderApiRouter.activeOrder(dic: jsonDic, success: { (data) in
                if data?.status == false{
                    //self.activeOrderViewHeightConstaint.constant = 0
                }else{
                    SaveAddressClass.getActiveOrderArray.removeAll()
                    for dic in data!.successDic!{
                        let activeOrderDic = ActiveOrderModelArray(
                            OrderId: dic["OrderId"] is NSNull ? 0 : dic["OrderId"] as! Int,
                            OrderStatus: dic["OrderStatus"] is NSNull ? "" : dic["OrderStatus"] as! String,
                            BrandName: "Dajen",
                            BrandImage: "olaya_1.jpg"
                        )
                        SaveAddressClass.getActiveOrderArray.append(activeOrderDic)
                    }
                    //self.activeOrderMethod()
                    
                }
            }){ (error) in
                //self.activeOrderViewHeightConstaint.constant = 0
            }
        }
    }
}
extension HomeVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if SaveAddressClass.getStoreBannerArray.count > 0 {
            if totalRows > SaveAddressClass.getStoresArray.count {
                return SaveAddressClass.getStoresArray.count + 2
            }else{
               return SaveAddressClass.getStoresArray.count + 1
            }
        }else if SaveAddressClass.getStoresArray.count > 0{
            if totalRows > SaveAddressClass.getStoresArray.count {
               return SaveAddressClass.getStoresArray.count + 1
            }else{
                return SaveAddressClass.getStoresArray.count

            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if SaveAddressClass.getStoreBannerArray.count > 0 {
            if indexPath.row == 0 {
                let screenSize: CGRect = self.storesTableView.bounds
                return screenSize.width * 0.55
            }
//            else if indexPath.row == 1 {
//                return 54
//            }
         }else if SaveAddressClass.getStoresArray.count > 0{
            if indexPath.row == 0 {
               return 54
             }
         }
        
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if SaveAddressClass.getStoreBannerArray.count > 0 {
            if indexPath.row == 0 {
                storesTableView.register(UINib(nibName: "BannerCell", bundle: nil), forCellReuseIdentifier: "BannerCell")
                let cell = storesTableView.dequeueReusableCell(withIdentifier: "BannerCell", for: indexPath) as! BannerCell
                let flow = UICollectionViewFlowLayout()
                flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                flow.minimumInteritemSpacing = 0
                flow.minimumLineSpacing = 0
                let widht : CGFloat = storesTableView.bounds.width
                flow.itemSize = CGSize(width: widht, height: widht * 0.55)
                flow.scrollDirection = .horizontal
                cell.bannerCollectionView.collectionViewLayout = flow
                cell.bannerCollectionView.reloadData()
                cell.selectionStyle = .none
                return cell
            }else{
                if SaveAddressClass.getStoresArray.count == indexPath.row-1 && totalRows > SaveAddressClass.getStoresArray.count {
                    self.storesTableView.register(UINib(nibName:"LoaderCell", bundle: nil), forCellReuseIdentifier:"LoaderCell")
                      let cell = tableView.dequeueReusableCell(withIdentifier: "LoaderCell", for: indexPath) as! LoaderCell
                    cell.activityIndicatorView.startAnimating()
                     cell.selectionStyle = .none
                     return cell
                }else{
                let cell = storesTableView.dequeueReusableCell(withIdentifier: "HomeTVCell", for: indexPath) as! HomeTVCell

                let dic = SaveAddressClass.getStoresArray[indexPath.row - 1]
                if AppDelegate.getDelegate().appLanguage == "English"{
                    cell.storeNameLbl.text = dic.BranchName_En?.capitalized
                    //cell.subStoreNameLbl.text = dic.StoreType!.capitalized
                    cell.descriptionLbl.text = dic.BranchDescription_En?.capitalized
                    if dic.AvgPreparationTime! > 60 {
                        let hour = dic.AvgPreparationTime!/60
                        cell.subStoreNameLbl.text = "Order Before - \(hour) Hours"
                    }else{
                        cell.subStoreNameLbl.text = "Order Before - \(dic.AvgPreparationTime!) Minutes"
                    }
                }else{
                    cell.storeNameLbl.text = dic.BranchName_Ar?.capitalized
                    //cell.subStoreNameLbl.text = dic.StoreType_Ar?.capitalized
                    cell.descriptionLbl.text = dic.BranchDescription_Ar?.capitalized
                    if dic.AvgPreparationTime! > 60 {
                        let hour = dic.AvgPreparationTime!/60
                        cell.subStoreNameLbl.text = "الطلب قبل - \(hour) ساعة"
                    }else{
                        cell.subStoreNameLbl.text = "الطلب قبل - \(dic.AvgPreparationTime!) الدقائق"
                    }
                }
                
                
                cell.subStoreNameLbl.textColor = .red
                cell.visitedNumbersLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "can serve 1 to", value: "", table: nil))!) \(dic.Serving!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "people", value: "", table: nil))!)"
                cell.distanceLbl.text = "\(String(format: "%.2f", dic.Distance!))KM"
                cell.ratingLbl.text = "\(dic.Rating!)"
                let imgStr:NSString = "\(url.storeImg.imgPath())\(dic.StoreLogo_En!)" as NSString
                let charSet = CharacterSet.urlFragmentAllowed
                let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
                let URLString = URL.init(string: urlStr as String)
                cell.img.kf.indicatorType = .activity
                cell.img.kf.setImage(with: URLString!)
                cell.img.layer.cornerRadius = 4
                cell.img.clipsToBounds = true
               //tableHeight.constant = tableView.contentSize.height + 10
//                print("Array count: \(SaveAddressClass.getStoresArray.count)")
//                print("IndexPath Row: \(indexPath.row)")
//                if SaveAddressClass.getStoreBannerArray.count > 0 {
//                    if SaveAddressClass.getStoresArray.count == indexPath.row+1 {
//                        if totalRows > SaveAddressClass.getStoresArray.count {
//                            pageNumber = pageNumber + 1
//                            ANLoader.showLoading("Loading..", disableUI: true)
//                            self.getStoreInformation()
//                        }
//                    }
//                }else{
//                    if SaveAddressClass.getStoresArray.count == indexPath.row+2 {
//                        if totalRows > SaveAddressClass.getStoresArray.count {
//                            pageNumber = pageNumber + 1
//                            ANLoader.showLoading("Loading..", disableUI: true)
//                            self.getStoreInformation()
//                        }
//                    }
//                }
                return cell
            }
            }
        }else{

            if SaveAddressClass.getStoresArray.count == indexPath.row && totalRows > SaveAddressClass.getStoresArray.count {
                self.storesTableView.register(UINib(nibName:"LoaderCell", bundle: nil), forCellReuseIdentifier:"LoaderCell")
                  let cell = tableView.dequeueReusableCell(withIdentifier: "LoaderCell", for: indexPath) as! LoaderCell
                cell.activityIndicatorView.startAnimating()
                 cell.selectionStyle = .none
                 return cell
            }else{
            let cell = storesTableView.dequeueReusableCell(withIdentifier: "HomeTVCell", for: indexPath) as! HomeTVCell
            let dic = SaveAddressClass.getStoresArray[indexPath.row]
            if AppDelegate.getDelegate().appLanguage == "English"{
                cell.storeNameLbl.text = dic.BranchName_En?.capitalized
                cell.subStoreNameLbl.text = dic.StoreType!.capitalized
                cell.descriptionLbl.text = dic.BranchDescription_En?.capitalized
            }else{
                cell.storeNameLbl.text = dic.BranchName_Ar?.capitalized
                cell.subStoreNameLbl.text = dic.StoreType_Ar?.capitalized
                cell.descriptionLbl.text = dic.BranchDescription_Ar?.capitalized
            }
            cell.visitedNumbersLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "can serve 1 to", value: "", table: nil))!) \(dic.Serving!) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "people", value: "", table: nil))!)"
            cell.distanceLbl.text = "\(String(format: "%.2f", dic.Distance!))KM"
            cell.ratingLbl.text = "\(dic.Rating!)"
            let imgStr:NSString = "\(url.storeImg.imgPath())\(dic.StoreLogo_En!)" as NSString
            let charSet = CharacterSet.urlFragmentAllowed
            let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
            let URLString = URL.init(string: urlStr as String)
            cell.img.kf.indicatorType = .activity
            cell.img.kf.setImage(with: URLString!)
            cell.img.layer.cornerRadius = 4
            cell.img.clipsToBounds = true
           // tableHeight.constant = tableView.contentSize.height + 10
//            print("Array count: \(SaveAddressClass.getStoresArray.count)")
//            print("IndexPath Row: \(indexPath.row)")
//            if SaveAddressClass.getStoreBannerArray.count > 0 {
//                if SaveAddressClass.getStoresArray.count == indexPath.row {
//                    if totalRows > SaveAddressClass.getStoresArray.count {
//                        pageNumber = pageNumber + 1
//                        ANLoader.showLoading("Loading..", disableUI: true)
//                        self.getStoreInformation()
//                    }
//                }
//            }else{
//                if SaveAddressClass.getStoresArray.count == indexPath.row+1 {
//                    if totalRows > SaveAddressClass.getStoresArray.count {
//                        pageNumber = pageNumber + 1
//                        ANLoader.showLoading("Loading..", disableUI: true)
//                        self.getStoreInformation()
//                    }
//                }
//            }
            return cell
            }
        //}
        }
    }
   func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
           if indexPath == lastVisibleIndexPath {
               if SaveAddressClass.getStoreBannerArray.count > 0 {
                   if SaveAddressClass.getStoresArray.count == indexPath.row {
                       if totalRows > SaveAddressClass.getStoresArray.count {
                           pageNumber = pageNumber + 1
                            //print("pageNumber:\(pageNumber)")
                           //ANLoader.showLoading("Loading..", disableUI: true)
                            //self.isLoaded = false
                           self.getStoreInformation()
                       }
                   }
               }else{
                   if SaveAddressClass.getStoresArray.count == indexPath.row+1 {
                       if totalRows > SaveAddressClass.getStoresArray.count {
                           pageNumber = pageNumber + 1
                           //print("pageNumber:\(pageNumber)")
                            //self.isLoaded = false
                           //ANLoader.showLoading("Loading..", disableUI: true)
                           self.getStoreInformation()
                       }
                   }
               }
           }
       }
   }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SaveAddressClass.selectStoreArray.removeAll()
        if SaveAddressClass.getStoreBannerArray.count > 0 {
            if SaveAddressClass.getStoresArray.count == indexPath.row-1 && totalRows > SaveAddressClass.getStoresArray.count {
            }else{
               SaveAddressClass.selectStoreArray = [SaveAddressClass.getStoresArray[indexPath.row - 1]]
            }
        }else{
            if SaveAddressClass.getStoresArray.count == indexPath.row && totalRows > SaveAddressClass.getStoresArray.count {
            }else{
               SaveAddressClass.selectStoreArray = [SaveAddressClass.getStoresArray[indexPath.row]]
            }
        }
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//    }
}
extension HomeVC:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SaveAddressClass.getStoreBannerArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCVCell", for: indexPath) as! HomeCVCell
        let imgStr:NSString = "\(url.storeImg.imgPath())\(SaveAddressClass.getStoreBannerArray[indexPath.row].BannerImage!)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let URLString = URL.init(string: urlStr as String)
        cell.bannerImg.kf.indicatorType = .activity
        cell.bannerImg.kf.setImage(with: URLString!)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = self.view.bounds
        let screenWidth = screenSize.width
        return CGSize(width: screenWidth, height: screenWidth * 0.55)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if SaveAddressClass.getStoreBannerArray[indexPath.row].StoreDetails.count == 1{
            SaveAddressClass.selectStoreArray.removeAll()
            SaveAddressClass.selectStoreArray = SaveAddressClass.getStoreBannerArray[indexPath.row].StoreDetails
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
            self.navigationController?.pushViewController(obj, animated: true)
            
        }else if SaveAddressClass.getStoreBannerArray[indexPath.row].StoreDetails.count > 1{
            SaveAddressClass.selectBannerArray.removeAll()
            SaveAddressClass.selectBannerArray = [SaveAddressClass.getStoreBannerArray[indexPath.row]]
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "StoresVC") as! StoresVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>){
//
//        if scrollView == storesTableView{
//         targetContentOffset.pointee = scrollView.contentOffset
//         var indexes = self.storesTableView.indexPathsForVisibleRows
//         indexes!.sort()
//         var index = indexes!.first!
//         print(index)
//        }else{
//        if SaveAddressClass.getStoreBannerArray.count > 0 {
//        if scrollView == bannerCollectionView{
//
//            targetContentOffset.pointee = scrollView.contentOffset
//            var indexes = self.bannerCollectionView.indexPathsForVisibleItems
//            indexes.sort()
//            var index = indexes.first!
//            if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
//                print("left")
//                index.row = index.row - 1
//
//            } else {
//                print("right")
//                index.row = index.row + 1
//
//            }
//
//            index.row = index.row > 0 ? index.row:0
//            if SaveAddressClass.getStoreBannerArray.count > index.row {
//                self.bannerCollectionView.scrollToItem(at: index, at: .right, animated: true )
//            }else{
//                DispatchQueue.main.async {
//                    let indexPath = IndexPath(item: 0, section: 0)
//                    self.bannerCollectionView.scrollToItem(at: indexPath, at: [], animated: false )
//                }
//            }
//        }
//      }
//        }
 //   }
}
extension HomeVC : LocationDelegate {
    func didFailedLocation(error: String) {
//        if isFirst == true {
//            isFirst = false
//            return
//        }
        ANLoader.hide()
        let alert = UIAlertController(title: "Allow Location Access", message: "The Chef needs access to your location. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)

                   // Button to Open Settings
                   alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                       guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                           return
                       }
                       if UIApplication.shared.canOpenURL(settingsUrl) {
                           UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                               print("Settings opened: \(success)")
                           })
                       }
                   }))
                   alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                   self.present(alert, animated: true, completion: nil)
    }
    func checkLocationPermission() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                //open setting app when location services are disabled
                //Please enable location services for this app.
                openSettingApp(title:NSLocalizedString("Location Services Disabled", comment: ""), message:NSLocalizedString("Please go to Settings and turn on Location Service for this app.", comment: ""))
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
            openSettingApp(title:NSLocalizedString("Location services are not enabled", comment: ""), message:NSLocalizedString("Please go to Settings and turn on Location Service", comment: ""))
        }
    }
    func openSettingApp(title: String,message: String) {
        let alertController = UIAlertController (title: message, message:nil , preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    func didUpdateLocation(latitude: Double?, longitude: Double?){
        if isClass == false{
            return
        }

        GMSGeocoder().reverseGeocodeCoordinate(CLLocationCoordinate2DMake((latitude!), (longitude!)), completionHandler: { response, error in
            if let addressObj = response?.firstResult() {
                
                var str = ""
                if let sublocality = addressObj.subLocality {
                    str = sublocality
                }else if let locality = addressObj.locality{
                    str = locality
                }else{
                    str = addressObj.country!
                }
                
                var sLocality = ""
                if let lines = addressObj.lines {
                    let lines = lines as [String]
                    sLocality = lines.joined(separator: ",")
                }else if let sublocality = addressObj.subLocality {
                    sLocality = sublocality
                }else if let locality = addressObj.locality {
                    sLocality = locality
                }else{
                    sLocality = addressObj.country!
                }
                
                let dic = ["Adreess": str,
                           "FullAddress":"\(str), \(String(describing: sLocality)), \(String(describing: addressObj.country!))",
                    "AddressId":0,
                    "Latitude":"\(latitude!)",
                    "Longitude":"\(longitude!)"] as [String : Any]
                UserDefaults.standard.set(dic, forKey:"ChangeLocationDetails")
                UserDefaults.standard.synchronize()
                ANLoader.hide()
                self.locationViewActionHeight.constant = 24
                let myLocation = CLLocation(latitude: self.latitude, longitude: self.longitude)
                 let mycurrentLocation = CLLocation(latitude: latitude!, longitude: longitude!)
                let distance = myLocation.distance(from: mycurrentLocation) / 1000
                if distance > 0.3{
                    self.alertMsgLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Location Msg", value: "", table: nil))!
                    self.changeLocationBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Location", value: "", table: nil))!, for: .normal)

                    let height = self.view.safeAreaInsets.top
                    let startPoint = CGPoint(x: self.locationNameLbl.frame.midX, y: self.locationNameLbl.frame.maxY + height - 10)
                     if AppDelegate.getDelegate().appLanguage == "English"{
                    self.locationView.frame =  CGRect(x: 0, y: 0, width: 300, height: 75)
                     }else{
                        self.locationView.frame =  CGRect(x: 0, y: 0, width: 300, height:60)

                    }
                    self.popover.show(self.locationView, point: startPoint)
                }
                self.navigationController?.popViewController(animated: false)
            }
        });
    }
}
extension HomeVC: LocationSelectionVCDelegrate{
    func didTapAction() {
        //ANLoader.showLoading("Loading..", disableUI: true)
        //isClass = true
         NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground),
                                                      name: UIApplication.didEnterBackgroundNotification,
                                                      object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationEnteredForeground),
                                                      name: UIApplication.willEnterForegroundNotification,
                                                      object: nil)
        
        // Brands
        self.mainArr.removeAll()
        self.mainArr = MappingBrands.Brands()
                           
        // Header Names
        self.headesArray.removeAll()
        self.headesArray = HeaderNames.headerNames()
        //searchView.isHidden = false

        //resultNumOfRecordsLbl.text = "Total Restaurants : \(totalRows)"

        //bannerCollectionView.reloadData()
        storesTableView.reloadData()
        self.getStoresData(isLoader: true)
   }
}
//MARK: UISearchbar delegate
extension HomeVC: UISearchBarDelegate{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if isCancel == true{
            isSearch = false
            storesTableView.isHidden = false
        }else{
            isSearch = true;
        }
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //searchBar.resignFirstResponder()
        isSearch = true;
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false;
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchBar.text!.count > 0 {
        isSearch = true;
        let button = UIButton.init()
        button.tag = searchBar.tag
        self.searchBtn_Tapped(button)
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                searchBar.resignFirstResponder()
            }
            searchingText = ""
            isCancel = true
            isSearch = false
            let button = UIButton.init()
            button.tag = searchBar.tag
            self.searchBtn_Tapped(button)
                 
        }else{
           isSearch = true
            searchingText = searchText
        }
        
//        if searchBar.text!.count > 0 {
//            //searchBtnWidthConstraint.constant = 80
//            isSearch = true
//            searchingText = searchText
////            if searchingText.count == 1 {
////              let indexPath = IndexPath(row: searchBar.tag, section: 0)
////               self.storesTableView.reloadRows(at: [indexPath], with: .automatic)
////            }
//        }else{
//            //searchBar.resignFirstResponder()
//            searchingText = ""
//            //let indexPath = IndexPath(row: searchBar.tag, section: 0)
//            //self.storesTableView.reloadRows(at: [indexPath], with: .automatic)
//            //searchBtnWidthConstraint.constant = 0
//            isCancel = true
//            isSearch = false
//            self.getStoresData(isLoader: true)
//        }
        //searchView.isHidden = false
        //self.storesTableView.reloadData()
    }
}
