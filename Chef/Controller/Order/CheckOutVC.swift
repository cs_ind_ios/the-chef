//
//  CheckOutVC.swift
//  Chef
//
//  Created by RAVI on 24/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import CoreLocation
import SafariServices

class CheckOutVC: UIViewController, NoNetworkViewDelegate, UIScrollViewDelegate, SFSafariViewControllerDelegate {
    var checkoutProvider: OPPCheckoutProvider?
    var transaction: OPPTransaction?
    var objType:Int = 0
    var cardDic:GetCardModel?
    var token:String?
    var cardBrand:String?

    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var bottomAmountView: UIView!
    @IBOutlet weak var navigationBarImg: UIImageView!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateNameLbl: UILabel!
    @IBOutlet weak var QuantityNameLbl: UILabel!
    @IBOutlet weak var yourOrderNameLbl: UILabel!
    @IBOutlet weak var paymentInfoNameLbl: UILabel!
    @IBOutlet weak var contactInfoNameLbl: UILabel!
    @IBOutlet weak var nameTitleLbl: UILabel!
    @IBOutlet weak var phoneNumberTitleLbl: UILabel!
    @IBOutlet weak var addressTitleLbl: UILabel!
    @IBOutlet weak var deliveryDataTimeTitleLbl: UILabel!
    
    @IBOutlet weak var contactIndoView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var dateAndTimeLbl: UILabel!
    
    @IBOutlet weak var paymentInfoView: UIView!
    @IBOutlet weak var cardBtn: UIButton!
    @IBOutlet weak var cashBtn: UIButton!
    
    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var invoiceView: UIView!
    @IBOutlet weak var invoiceViewHeight: NSLayoutConstraint!
    @IBOutlet weak var vatLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var totalPriceLbl: UILabel!
    @IBOutlet weak var totalFinalPriceLbl: UILabel!
    @IBOutlet weak var subTotalAmountLbl: UILabel!
    @IBOutlet weak var deliveryChargeLbl: UILabel!
    @IBOutlet weak var orderBillNameLbl: UILabel!
    @IBOutlet weak var itemTotalNameLbl: UILabel!
    @IBOutlet weak var subTotalNameLbl: UILabel!
    @IBOutlet weak var couponDiscountNameLBl: UILabel!
    @IBOutlet weak var totalNameLbl: UILabel!
    @IBOutlet weak var FinalTotalNameLbl: UILabel!
    
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var oneLbl: UILabel!
    @IBOutlet weak var twoLbl: UILabel!
    @IBOutlet weak var threeLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var serviceChargeLbl: UILabel!
    @IBOutlet weak var confirmBtn: CustomButton!
    @IBOutlet weak var addAddressView: UIView!
    @IBOutlet weak var addAddressBtn: UIButton!
    @IBOutlet weak var addAddressMsgLbl: UILabel!
    @IBOutlet weak var vatPercentageLbl: UILabel!
    @IBOutlet weak var changeAddressBtn: UIButton!
    
    var couponDiscoun = Double()
    var amount = Double()
    var vat = Double()
    var deliveryCharge:Double = 0
    var promoAmount = Double()
    var promoParcenatge = Double()
    var highestDiscount = Double()
    var OrderTypeId:Int = CartVC.instance.OrderTypeId
    var PaymentMode:Int = 1
    var noNetWork = NoNetworkView()
    var addressId:Int = 0
    
    var SwiftTimer = Timer()
    
    //ApplePay
    var checkoutID:String = ""
    let provider = OPPPaymentProvider(mode: OPPProviderMode.live)
    var applePay:Bool = false
    @IBOutlet weak var applePayBtn: UIButton!
    @IBOutlet weak var stcPayBtn: UIButton!
    var stcPay:Bool = false
    var stcPayNumber:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBarImg.isHidden = true
        myScrollView.delegate = self
        self.tabBarController?.tabBar.isHidden = true
        //allData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        SwiftTimer.invalidate()
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        //NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    @objc func applicationDidEnterBackground(application:UIApplication)  {
       // print("Background")
        SwiftTimer.invalidate()
        
    }
    @objc func applicationEnteredForeground(application:UIApplication)  {
        self.navigationController?.popViewController(animated: false)

    }
    @objc func getCurrentDateAndTime(){
         CartVC.instance.currentDate =  Calendar.current.date(byAdding: .minute, value: 1, to: CartVC.instance.currentDate)!
        let startDate = DateFormate.dateConverstion(dateStr: SaveAddressClass.getBranchDetailsArray[0].StarDateTime!)
        if SaveAddressClass.getBranchDetailsArray[0].StoreStatus! == "Close" {
            self.navigationController?.popViewController(animated: false)
            return
        }
        
        if startDate > CartVC.instance.currentDate {
            self.navigationController?.popViewController(animated: false)
            return
        }
        
        if CartVC.instance.isChangeTime == false {
            self.ExpetectedTimeCalculatMethod()
        }else{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd MMM yyyy hh:mm a"
            dateFormatterGet.locale = Locale(identifier: "EN")
            let calendar = Calendar.current
            let date = calendar.date(byAdding: .minute, value: 0, to: CartVC.instance.currentDate)
            let expectedDate = dateFormatterGet.date(from: CartVC.instance.expectedTimeLbl.text!)
            if expectedDate!.compare(date!) == .orderedSame || expectedDate!.compare(date!) == .orderedAscending {
                CartVC.instance.isChangeTime = false
                self.ExpetectedTimeCalculatMethod()
            }
        }
    }
    func ExpetectedTimeCalculatMethod() {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd MMM yyyy hh:mm a"
            dateFormatterGet.locale = Locale(identifier: "EN")
            let calendar = Calendar.current
            let date = calendar.date(byAdding: .minute, value: 0, to: CartVC.instance.currentDate)
             CartVC.instance.expectedTimeLbl.text = dateFormatterGet.string(from:date!)
             allData()
    }
    override func viewWillAppear(_ animated: Bool) {
        if objType == 1 {
            self.navigationController?.popViewController(animated: false)
        }else{
            self.SwiftTimer = Timer.scheduledTimer(timeInterval:60.0, target: self, selector: #selector(self.getCurrentDateAndTime), userInfo: nil, repeats: true);

            NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground),
                                                   name: UIApplication.didEnterBackgroundNotification,
                                                   object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(applicationEnteredForeground),
                                                   name: UIApplication.willEnterForegroundNotification,
                                                   object: nil)
           allData()
        }
    }
    func allData(){
        //cardBtn.isEnabled = false
        
        oneLbl.layer.cornerRadius = oneLbl.frame.height/2
        oneLbl.layer.masksToBounds = true
        twoLbl.layer.cornerRadius = twoLbl.frame.height/2
        twoLbl.layer.masksToBounds = true
        threeLbl.layer.cornerRadius = threeLbl.frame.height/2
        threeLbl.layer.masksToBounds = true
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd MMM yyyy hh:mm a"
        dateFormatterGet.locale = Locale(identifier: "EN")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        dateFormatter.locale = Locale(identifier: "EN")

        let expTimeDate = dateFormatterGet.date(from: CartVC.instance.expectedTimeLbl.text!)
        let dateFormatterExp = DateFormatter()
        dateFormatterExp.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatterExp.locale = Locale(identifier: "EN")
//"ExpectedTime":"2018-05-24T10:30:00",
        
        let timeFormat = DateFormatter()
        timeFormat.dateFormat = "HH:mm a"
        timeFormat.locale = Locale(identifier: "EN")

        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy"
        dateFormat.locale = Locale(identifier: "EN")

        priceLbl.text = CartVC.instance.totalAmountLbl.text
        quantityLbl.text = "\(CartVC.instance.TotalQtyStr!)"
        dateLbl.text = "\(dateFormatter.string(from:expTimeDate!)) \(timeFormat.string(from:expTimeDate!))"
        dateAndTimeLbl.text = "\(dateFormat.string(from:expTimeDate!)) / \(timeFormat.string(from:expTimeDate!))"

        subTotalAmountLbl.text = "\(CartVC.instance.subTotalAmount.text!)"
        deliveryChargeLbl.text = "\(CartVC.instance.deliveryChargeLbl.text!)"
        if CartVC.instance.orderTypeLbl.text == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))! {
            let userDetails = UserDef.getUserProfileDic(key: "userDetails")
            phoneNumberLbl.text = "+\((userDetails["Mobile"] as? String)!)"
            nameLbl.text = "\((userDetails["Name"] as? String)!)"
            addressTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DeliveryAddress", value: "", table: nil))!
            deliveryDataTimeTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "DELIVERY DATE / TIME Title", value: "", table: nil))!
             contactInfoNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Contact Info", value: "", table: nil))!

        }else{
            phoneNumberLbl.text = "+\(SaveAddressClass.getBranchDetailsArray[0].MobileNo!)"
            nameLbl.text = CartVC.instance.branchNameLbl.text
            addressTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PickUpAddress", value: "", table: nil))!
            deliveryDataTimeTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PICKUP DATE / TIME Title", value: "", table: nil))!
             contactInfoNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Resturant Info", value: "", table: nil))!

        }
        
    
        if isLocationArray() ==  true {
        let dic = UserDefaults.standard.object(forKey: "LocationDetails") as! [String:Any]
            if dic["AddressId"]! as! Int == 0 {
                if CartVC.instance.orderTypeLbl.text == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!{
                    bottomAmountView.isHidden = true
                    addAddressView.isHidden = false
                    addressLbl.text = ""
                }else{
                    changeAddressBtn.isHidden = true
                    bottomAmountView.isHidden = false
                    addAddressView.isHidden = true
                    addressLbl.text = CartVC.instance.branchAddressLbl.text!
                }
            }else{
                if CartVC.instance.orderTypeLbl.text == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!{
                   // print(dic)
                    let Latitude:Double = Double(dic["Latitude"] as! String)!
                    let Longitude:Double = Double(dic["Longitude"] as! String)!
                    
                    let myLocation = CLLocation(latitude: Latitude, longitude: Longitude)
                    let myBuddysLocation = CLLocation(latitude: SaveAddressClass.getBranchDetailsArray[0].Latitude!, longitude: SaveAddressClass.getBranchDetailsArray[0].Longitude!)
                    let distance = myLocation.distance(from: myBuddysLocation) / 1000
                    if distance > SaveAddressClass.getBranchDetailsArray[0].DeliveryDistance! {
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "The Disatnce too far from Restaurent Please Seelect Near Restaurent", value: "", table: nil))!)
                        bottomAmountView.isHidden = true
                        addAddressView.isHidden = false
                        changeAddressBtn.isHidden = false

                        addressLbl.text = "\(dic["FullAddress"]!)"
                        return
                    }
                    
                    
                    changeAddressBtn.isHidden = false
                    bottomAmountView.isHidden = false
                    addAddressView.isHidden = true
                    
                    addressLbl.text = "\(dic["FullAddress"]!)"
                    addressId = dic["AddressId"]! as! Int
                }else{
                    changeAddressBtn.isHidden = true
                    bottomAmountView.isHidden = false
                    addAddressView.isHidden = true
                    addressLbl.text = CartVC.instance.branchAddressLbl.text!
                }
            }
        }else{
            if CartVC.instance.orderTypeLbl.text == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!{
                bottomAmountView.isHidden = true
                addAddressView.isHidden = false
                addressLbl.text = ""
            }else{
                changeAddressBtn.isHidden = true
                bottomAmountView.isHidden = false
                addAddressView.isHidden = true
                addressLbl.text = CartVC.instance.branchAddressLbl.text!
            }
        }
        
        
//        if SaveAddressClass.saveAddressDic.count > 0{
//            bottomAmountView.isHidden = false
//            addAddressView.isHidden = true
//            addressLbl.text = "\(SaveAddressClass.saveAddressDic["HouseNo"]!),\(SaveAddressClass.saveAddressDic["LandMark"]!), \(SaveAddressClass.saveAddressDic["Address"]!)"
//            addressId = SaveAddressClass.saveAddressDic["AddressId"] as! UInt8
//        }else{
//            if CartVC.instance.orderTypeLbl.text == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!{
//                bottomAmountView.isHidden = true
//                addAddressView.isHidden = false
//                addressLbl.text = ""
//            }else{
//                bottomAmountView.isHidden = false
//                addAddressView.isHidden = true
//                addressLbl.text = CartVC.instance.branchAddressLbl.text!
//            }
//        }
        
        
        serviceChargeLbl.text = CartVC.instance.serviceChargeLbl.text
        vatLbl.text = CartVC.instance.vatAmountLbl.text
        discountLbl.text = CartVC.instance.couponDiscountLbl.text
        totalPriceLbl.text = CartVC.instance.itemTotalLbl.text
        totalFinalPriceLbl.text = CartVC.instance.totalAmountLbl.text
        totalAmountLbl.text = CartVC.instance.totalAmountLbl.text
        vatPercentageLbl.text = CartVC.instance.vatPercentageLbl.text!
        
        addAddressBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SELECT ADDRESS", value: "", table: nil))!, for: UIControl.State.normal)
        confirmBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "CONFIRM", value: "", table: nil))!, for: UIControl.State.normal)
        addAddressMsgLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add your Address", value: "", table: nil))!
        productNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CheckOut", value: "", table: nil))!
        titleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "CheckOut", value: "", table: nil))!
        dateNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Date", value: "", table: nil))!
        QuantityNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Quantity", value: "", table: nil))!
        yourOrderNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your Order", value: "", table: nil))!
        paymentInfoNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Payment Info", value: "", table: nil))!
       
        nameTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NAME", value: "", table: nil))!
        phoneNumberTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "PHONE NUMBER Title", value: "", table: nil))!
        
        
        
        orderBillNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Bill", value: "", table: nil))!
        itemTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Item Total", value: "", table: nil))!
        subTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sub Total", value: "", table: nil))!
        couponDiscountNameLBl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupon Discount", value: "", table: nil))!
        totalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Amount", value: "", table: nil))!
        FinalTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total", value: "", table: nil))!
        changeAddressBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Change Address", value: "", table: nil))!, for: .normal)
    }
    
    @IBAction func changeAddressBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        if(isUserLogIn() == true) {
            AppDelegate.getDelegate().homeScreen = false
            var obj = SelectAddressVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "SelectAddressVC") as! SelectAddressVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let loginVCObj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC")
            self.navigationController?.pushViewController(loginVCObj, animated: true)
        }
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func paymentBtn_Tapped(_ sender: UIButton) {
        
        self.cardBtn.setImage(UIImage(named: "Card_new"), for: .normal)
        self.cashBtn.setImage(UIImage(named: "Cash_new"), for: .normal)
        self.applePayBtn.setImage(UIImage(named: "applePay_new"), for: .normal)
        self.stcPayBtn.setImage(UIImage(named: "STCPay"), for: .normal)
        stcPay = false
        applePay = false
        if sender.tag == 1{ //Card_new_select //applePay_new_select // Cash_new_select
            PaymentMode = 2
            self.applePayBtn.setImage(UIImage(named: "applePay_new"), for: .normal)
            self.cardBtn.setImage(UIImage(named: "Card_new_select"), for: .normal)
            self.cashBtn.setImage(UIImage(named: "Cash_new"), for: .normal)
            self.stcPayBtn.setImage(UIImage(named: "STCPay"), for: .normal)
        } else if sender.tag == 2{
            PaymentMode = 1
            self.applePayBtn.setImage(UIImage(named: "applePay_new"), for: .normal)
            self.cardBtn.setImage(UIImage(named: "Card_new"), for: .normal)
            self.cashBtn.setImage(UIImage(named: "Cash_new_select"), for: .normal)
            self.stcPayBtn.setImage(UIImage(named: "STCPay"), for: .normal)
        }else if sender.tag == 3{
            PaymentMode = 2
            applePay = true
            stcPay = false
            self.applePayBtn.setImage(UIImage(named: "applePay_new_select"), for: .normal)
            self.cardBtn.setImage(UIImage(named: "Card_new"), for: .normal)
            self.cashBtn.setImage(UIImage(named: "Cash_new"), for: .normal)
            self.stcPayBtn.setImage(UIImage(named: "STCPay"), for: .normal)
        }else if sender.tag == 4{
            PaymentMode = 2
            applePay = false
            stcPay = true
            self.applePayBtn.setImage(UIImage(named: "applePay_new"), for: .normal)
            self.cardBtn.setImage(UIImage(named: "Card_new"), for: .normal)
            self.cashBtn.setImage(UIImage(named: "Cash_new"), for: .normal)
            self.stcPayBtn.setImage(UIImage(named: "STCPaySelect"), for: .normal)
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let obj = mainStoryBoard.instantiateViewController(withIdentifier: "STCPayVC") as! STCPayVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.stcPayVCDelegate =  self
            self.present(obj, animated: false, completion: nil)
        }
    }
    func TryAgain() {
        DispatchQueue.main.async {
        self.noNetWork.isHidden = true
        self.confirmBtnTapped(self.confirmBtn)
        }
    }
    @IBAction func confirmBtnTapped(_ sender: UIButton) {
        if(isUserLogIn() == true) {
            if(Connectivity.isConnectedToInternet()){
            if PaymentMode == 1 {

                ANLoader.showLoading("Loading..", disableUI: true)
                _ = DBObject.GetOrder(getSql: "SELECT * FROM OrderTable")
               // print(getOrderBool)
                let subAmount:Double = DBObject.geTotalOrderAmount(Sql:"SELECT sum(TotalAmount) FROM OrderTable")
                let subTotal = subAmount + CartVC.instance.deliveryCharge
                let vat: Double = subTotal * (SaveAddressClass.getBranchDetailsArray[0].VatPercentage!/100.00)
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "dd MMM yyyy hh:mm a"
                dateFormatterGet.locale = Locale(identifier: "EN")

                let expTimeDate = dateFormatterGet.date(from: CartVC.instance.expectedTimeLbl.text!)
                let dateFormatterExp = DateFormatter()
                dateFormatterExp.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                dateFormatterExp.locale = Locale(identifier: "EN")
//"ExpectedTime":"2018-05-24T10:30:00",
                let timeFormat = DateFormatter()
                timeFormat.dateFormat = "HH:mm:ss"
                timeFormat.locale = Locale(identifier: "EN")

                let dateFormat = DateFormatter()
                dateFormat.dateFormat = "yyyy-MM-dd"
                dateFormat.locale = Locale(identifier: "EN")

                let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
                let version = nsObject as! String
                // Main order Details
                let OrderDetailsDic:[String:Any] = ["UserId":"\(UserDef.getUserId())", "BranchId":"\(SaveAddressClass.getBranchDetailsArray[0].BranchId!)", "BrandId":"\(SaveAddressClass.getBranchDetailsArray[0].BrandId!)", "AddressID":"\(addressId)", "TotalPrice":"\(CartVC.instance.amount)", "SubTotal":"\(subAmount)", "VatCharges":String(format: "%.2f", round(vat*100)/100), "OrderType":"\(OrderTypeId)", "Version":"iOS V\(version)","VatPercentage":"\(SaveAddressClass.getBranchDetailsArray[0].VatPercentage!)", "Devicetoken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "ExpectedTime":"\(dateFormat.string(from:expTimeDate!))T\(timeFormat.string(from:expTimeDate!))", "PaymentMode":"\(PaymentMode)", "DeliveryCharges":CartVC.instance.deliveryCharge, "CouponDiscount":couponDiscoun, "CouponCode":CartVC.instance.promocode, "CouponAmount":CartVC.instance.couponDiscoun]
                //\(dateFormatterExp.string(from:expTimeDate!))
                // Items Details
                var itemsArray = [[String:Any]]()
                for orderDic in SaveAddressClass.getOrderArray{
                    DBObject.GetAdditionals(getSql:"SELECT * FROM AdditionalTable where OrderId = \(orderDic.OrderId!)")
                    var getAddDetailsAarry = [[String:Any]]()
                    for additionalsList in SaveAddressClass.getAdditionalsArray {
                        let additionalDic:[String:Any] = ["AddItemId":"\(additionalsList.AdditionalId!)","AddItemPrice":"\(additionalsList.AddPrice!)", "Quantity":"\(additionalsList.Quantity!)"]
                        getAddDetailsAarry.append(additionalDic)
                    }
                    let itemDic:[String:Any] = ["ItemId":"\(orderDic.ItemId!)", "Quantity":"\(orderDic.Quantity!)", "SizeId":"\(orderDic.SizeId!)", "ItemPrice":"\(orderDic.SizePrice!)", "Comments":"\(orderDic.Comments!)", "AdditionalItems": getAddDetailsAarry]
                    itemsArray.append(itemDic) // Items Append
                }
                let mainDic:[String:Any] = ["OrderItems":itemsArray, "OrderDetails":OrderDetailsDic]
               // print(mainDic)
                
                let orderItemsJsonData = try? JSONSerialization.data(withJSONObject: mainDic, options: [])
                let orderItemsJsonString = String(data: orderItemsJsonData!, encoding: .utf8)
                let mainDic2:[String:Any] = ["OrderDetails":orderItemsJsonString!]
                //Conver String Formate
                
                //print(mainDic2)
                
                OrderApiRouter.PlaceOrder(dic: mainDic2, success: { (data) in
                    ANLoader.hide()
                    if data?.status == false{
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                        }else{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                        }
                    }else{
                       // print(data!.successDic!)
                        self.objType = 1
                        _ = DBObject.InsertOrderString(insertSql: "DELETE From OrderTable")
                        // _ = DBObject.InsertOrderString(insertSql: "DELETE From StoreTable")
                        _ = DBObject.InsertOrderString(insertSql: "DELETE From AdditionalTable")
                        DispatchQueue.main.async {
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[2]
                                    tabItem.badgeValue = nil
                                }
                                StoreReviewHelper.checkAndAskForReview()

                                let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                                var obj = OrderVC()
                                obj = mainStoryBoard.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
                                obj.orderFrom = 1
                                obj.orderId = data!.successDic!["OrderId"] as! Int
                                self.navigationController?.pushViewController(obj, animated: true)
                        }
                    }
                }) { (error) in
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
                }
            }else{
                // self.processingView.startAnimating()
                ANLoader.showLoading("Loading..", disableUI: true)
                let userDetails = UserDef.getUserProfileDic(key: "userDetails")
                let merchantTransactionId = "\(UserDef.getUserId())iOS\(self.TimeStamp())"
                var customerPhone = "\((userDetails["Mobile"] as? String)!)"
                if self.stcPay == true{
                    customerPhone = stcPayNumber
                }
                let dic:[String:Any] = [
                    "amount":String(format: "%.2f", CartVC.instance.amount),
                    "shopperResultUrl": "\(Config.urlScheme)://result" ,
                    "isCardRegistration": "false",
                    "merchantTransactionId": merchantTransactionId,
                    "customerEmail":"\((userDetails["Email"] as? String)!)",
                    "userId":"\(UserDef.getUserId())",
                    "isApplePay":applePay,
                    "isSTCPay":stcPay,
                    "customerPhone":customerPhone,
                    "customerName":"\((userDetails["Name"] as? String)!)"
                    //"notificationUrl":"http://csadms.com/ChefAppAPITest/api/Notify/NewInfo"
                ]
                print(dic)
                PaymentModuleServices.getPaymentCheckOutService(dic: dic, success: { (data) in
                    if(data.Status == false){
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.Message)
                        }else{
                            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                        }
                    }else{
                        DispatchQueue.main.async {
                        //print(data.Data![0].id)
                        self.checkoutID = data.Data![0].id

                        if self.applePay == true{
                            let request = OPPPaymentProvider.paymentRequest(withMerchantIdentifier: "merchant.creative-sols.TheChefLive", countryCode: "SA")
                                request.currencyCode = "SAR"
                                let amount = NSDecimalNumber(value: CartVC.instance!.amount)
                                request.paymentSummaryItems = [PKPaymentSummaryItem(label: "The Chef", amount: amount)]
                                  if OPPPaymentProvider.canSubmitPaymentRequest(request) {
                                      if let vc = PKPaymentAuthorizationViewController(paymentRequest: request) as PKPaymentAuthorizationViewController? {
                                          vc.delegate = self
                                        self.present(vc, animated: true, completion: nil)
                                      } else {
                                        Alert.showAlert(on: self, title: "Failure", message: "Unable to present Apple Pay authorization.")
                                          NSLog("Apple Pay not supported.");
                                      }
                                  }else{
                                        Alert.showAlert(on: self, title: "Failure", message: "Unable to present Apple Pay authorization.")
                                        NSLog("Apple Pay not supported.");
                                    }
                            }else{
                            self.checkoutProvider = self.configureCheckoutProvider(checkoutID: self.checkoutID)
                            self.checkoutProvider?.delegate = self
                            self.checkoutProvider?.presentCheckout(forSubmittingTransactionCompletionHandler: { (transaction, error) in
                                DispatchQueue.main.async {
                                    self.handleTransactionSubmission(transaction: transaction, error: error)
                                }
                            }, cancelHandler: nil)
                            }
                        }
                    }
                    
                }){ (error) in
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
                }
               }
            }else{
                ANLoader.hide()
                self.noNetWork = (Bundle.main.loadNibNamed("NoNetworkView", owner: self, options: nil)?.first as? NoNetworkView)!
                self.noNetWork.delegate = self
                self.noNetWork.frame = CGRect(x:0,y:10,width:self.view.frame.width,height:self.view.frame.height-20)
                self.view.addSubview(self.noNetWork)
            }
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let loginVCObj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC")
            self.navigationController?.pushViewController(loginVCObj, animated: true)
        }
    }
    func TimeStamp() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyyMMMddHHmmssSSS"
        dateFormatter.locale = Locale(identifier: "EN")
        //Specify your format that you want
        return dateFormatter.string(from: date)
    }
    @IBAction func addAddressBtn_Tapped(_ sender: Any) {
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        if(isUserLogIn() == true) {
            AppDelegate.getDelegate().homeScreen = false
            var obj = SelectAddressVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "SelectAddressVC") as! SelectAddressVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let loginVCObj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC")
            self.navigationController?.pushViewController(loginVCObj, animated: true)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView == self.myScrollView){
           // print(scrollView.contentOffset.y)
            if(scrollView.contentOffset.y > 60){
                UIView.animate(withDuration: 0.7, animations: {
                    //self.view.layoutIfNeeded()
                    //self.navBarTitleLbl.text = self.storeName
                    //self.productNameLbl.textColor = UIColor.white
                    //self.navigationBarView.backgroundColor = UIColor.black
                    self.navigationBarView.isHidden = false
                    self.navigationBarView.isOpaque = true
                    self.navigationBarImg.isHidden = false
                })
            }else{
                //self.navigationBarView.backgroundColor = UIColor.clear
                self.navigationBarView.isOpaque = false
                
                UIView.animate(withDuration: 0.7, animations: {
                    // self.view.layoutIfNeeded()
                    self.navigationBarView.backgroundColor = UIColor.clear
                    self.navigationBarImg.isHidden = true
                    self.navigationBarView.isHidden = true
                })
            }
        }
    }
    
    func placeOrderMethod() {
        _ = DBObject.GetOrder(getSql: "SELECT * FROM OrderTable")
       // print(getOrderBool)
        let subAmount:Double = DBObject.geTotalOrderAmount(Sql:"SELECT sum(TotalAmount) FROM OrderTable")
        let vat: Double = subAmount * 0.05
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd MMM yyyy hh:mm a"
        dateFormatterGet.locale = Locale(identifier: "EN")
        
        let expTimeDate = dateFormatterGet.date(from: CartVC.instance.expectedTimeLbl.text!)
        let dateFormatterExp = DateFormatter()
        dateFormatterExp.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatterExp.locale = Locale(identifier: "EN")
        //"ExpectedTime":"2018-05-24T10:30:00",
        let timeFormat = DateFormatter()
        timeFormat.dateFormat = "HH:mm:ss"
        timeFormat.locale = Locale(identifier: "EN")
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        dateFormat.locale = Locale(identifier: "EN")
        
        // Main order Details
        let OrderDetailsDic:[String:Any] = ["UserId":"\(UserDef.getUserId())", "BranchId":"\(SaveAddressClass.getBranchDetailsArray[0].BranchId!)", "BrandId":"\(SaveAddressClass.getBranchDetailsArray[0].BrandId!)", "AddressID":"\(addressId)", "TotalPrice":"\(CartVC.instance.amount)", "SubTotal":"\(subAmount)", "VatCharges":"\(vat)", "OrderType":"\(OrderTypeId)", "Version":"iOS V1.0","VatPercentage":"\(SaveAddressClass.getBranchDetailsArray[0].VatPercentage!)", "Devicetoken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))", "ExpectedTime":"\(dateFormat.string(from:expTimeDate!))T\(timeFormat.string(from:expTimeDate!))", "PaymentMode":"\(PaymentMode)", "DeliveryCharges":CartVC.instance.deliveryCharge, "CouponDiscount":couponDiscoun]
        //\(dateFormatterExp.string(from:expTimeDate!))
        // Items Details
        var itemsArray = [[String:Any]]()
        for orderDic in SaveAddressClass.getOrderArray{
            DBObject.GetAdditionals(getSql:"SELECT * FROM AdditionalTable where OrderId = \(orderDic.OrderId!)")
            var getAddDetailsAarry = [[String:Any]]()
            for additionalsList in SaveAddressClass.getAdditionalsArray {
                let additionalDic:[String:Any] = ["AddItemId":"\(additionalsList.AdditionalId!)","AddItemPrice":"\(additionalsList.AddPrice!)", "Quantity":"\(additionalsList.Quantity!)"]
                getAddDetailsAarry.append(additionalDic)
            }
            let itemDic:[String:Any] = ["ItemId":"\(orderDic.ItemId!)", "Quantity":"\(orderDic.Quantity!)", "SizeId":"\(orderDic.SizeId!)", "ItemPrice":"\(orderDic.SizePrice!)", "Comments":"\(orderDic.Comments!)", "AdditionalItems": getAddDetailsAarry]
            itemsArray.append(itemDic) // Items Append
        }
        let mainDic:[String:Any] = ["OrderItems":itemsArray, "OrderDetails":OrderDetailsDic]
       // print(mainDic)
        
        let orderItemsJsonData = try? JSONSerialization.data(withJSONObject: mainDic, options: [])
        let orderItemsJsonString = String(data: orderItemsJsonData!, encoding: .utf8)
        let mainDic2:[String:Any] = ["OrderDetails":orderItemsJsonString!]
        //Conver String Formate
        
        OrderApiRouter.PlaceOrder(dic: mainDic2, success: { (data) in
            ANLoader.hide()
            if data?.status == false{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                }
            }else{
               // print(data!.successDic!)
                self.objType = 1
                _ = DBObject.InsertOrderString(insertSql: "DELETE From OrderTable")
                // _ = DBObject.InsertOrderString(insertSql: "DELETE From StoreTable")
                _ = DBObject.InsertOrderString(insertSql: "DELETE From AdditionalTable")
                DispatchQueue.main.async {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        // In this case we want to modify the badge number of the third tab:
                        let tabItem = tabItems[2]
                        tabItem.badgeValue = nil
                    }
                    
                    StoreReviewHelper.checkAndAskForReview()
                    
                    let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                    var obj = OrderVC()
                    obj = mainStoryBoard.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
                    obj.orderFrom = 1
                    obj.orderId = data!.successDic!["OrderId"] as! Int
                    self.navigationController?.pushViewController(obj, animated: true)
                    
                    
                    
                }
            }
        }) { (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    
    @objc func didReceiveSucussPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "paymentSuccess"), object: nil)

        DispatchQueue.main.async {
            self.placeOrderMethod()
        }
    }
    @objc func didReceiveFailedPaymentCallback() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
        }
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "paymentFailed"), object: nil)

        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:"Your payment was not successful")

    }
}
extension CheckOutVC:PKPaymentAuthorizationViewControllerDelegate{
   func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
       Alert.showAlert(on: self, title: "Failure", message: "Your payment was not successful")
        controller.dismiss(animated: true, completion: nil)
    }
    
    
 func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
    //print(self.checkoutID)
    //print(payment.token.paymentData)

    if let params = try? OPPApplePayPaymentParams(checkoutID: self.checkoutID, tokenData: payment.token.paymentData) as OPPApplePayPaymentParams? {
            self.transaction  = OPPTransaction(paymentParams: params!)
            self.provider.submitTransaction(OPPTransaction(paymentParams: params!), completionHandler: { (transaction, error) in
                if (error != nil) {
                    controller.dismiss(animated: true, completion: nil)
                  //  print(error?.localizedDescription as Any)
                    DispatchQueue.main.async {
                        Alert.showAlert(on: self, title: "Failure", message: error!.localizedDescription)

                    }
                    
                } else {
                    // Send request to your server to obtain transaction status.
                    controller.dismiss(animated: true, completion: nil)

                    self.provider.requestCheckoutInfo(withCheckoutID: self.checkoutID) { (checkoutInfo, error) in
                        DispatchQueue.main.async {
                            guard let resourcePath = checkoutInfo?.resourcePath else {
                                Alert.showAlert(on: self, title: "Failure", message: "Your payment was not successful..")
                            return
                            }
                            
                            ANLoader.showLoading("Loading..", disableUI: true)
                                    self.transaction = nil
                                    let dic:[String:Any] = ["resourcePath": resourcePath,"userId":"\(UserDef.getUserId())"]
                                    //print(dic)
                                    PaymentModuleServices.getPaymentStatusServiceWithOptions(dic: dic, isLoader: false, isUIDisable: true,success: { (data) in
                                        if(data.Status == false){
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                                ANLoader.hide()
                                            }
                                            
                                            if AppDelegate.getDelegate().appLanguage == "English"{
                                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.Message)
                                            }else{
                                                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                                            }
                                        }else{
                                            if data.Data![0].customPayment == "OK" {
                                               // DispatchQueue.main.async {
                                                    self.placeOrderMethod()
                                                //}
                                            }else{
                                                Alert.showAlert(on: self, title: "Failure", message: "Your payment was not successful...")

                                            }
                                        }
                                        
                                    }){ (error) in
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                            ANLoader.hide()
                                            
                                        }
                                        Alert.showAlert(on: self, title: "Failure", message: error)
                                    }
                        }
                    }
                }
            })
        }else{
            Alert.showAlert(on: self, title: "Failure", message: "Your payment was not successful....")
    }
    }
    
    
    
}
extension CheckOutVC: OPPCheckoutProviderDelegate{
    // MARK: - OPPCheckoutProviderDelegate methods
    
    // This method is called right before submitting a transaction to the Server.
    func checkoutProvider(_ checkoutProvider: OPPCheckoutProvider, continueSubmitting transaction: OPPTransaction, completion: @escaping (String?, Bool) -> Void) {
        // To continue submitting you should call completion block which expects 2 parameters:
        // checkoutID - you can create new checkoutID here or pass current one
        // abort - you can abort transaction here by passing 'true'
        completion(transaction.paymentParams.checkoutID, false)
    }
    
    // MARK: - Payment helpers
    func handleTransactionSubmission(transaction: OPPTransaction?, error: Error?) {
        guard let transaction = transaction else {
            Utils.showResult(presenter: self, success: false, message: error?.localizedDescription)
            return
        }
        
        self.transaction = transaction
        if transaction.type == .synchronous {
            // If a transaction is synchronous, just request the payment status
            self.requestPaymentStatus()
        } else if transaction.type == .asynchronous {
            // If a transaction is asynchronous, SDK opens transaction.redirectUrl in a browser
            // Subscribe to notifications to request the payment status when a shopper comes back to the app
            NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveAsynchronousPaymentCallback), name: Notification.Name(rawValue: Config.asyncPaymentCompletedNotificationKey), object: nil)
            
        } else {
            Utils.showResult(presenter: self, success: false, message: "Invalid transaction")
        }
    }
    
    func configureCheckoutProvider(checkoutID: String) -> OPPCheckoutProvider? {
        let provider = OPPPaymentProvider.init(mode: .live)
        let checkoutSettings = OPPCheckoutSettings.init()
        if self.stcPay == true{
            checkoutSettings.paymentBrands = Config.STCPAYPaymentBrands
        }else{
            checkoutSettings.paymentBrands = Config.checkoutPaymentBrands
        }
        checkoutSettings.shopperResultURL = Config.urlScheme + "://payment"
        checkoutSettings.theme.navigationBarBackgroundColor = Config.mainColor
        checkoutSettings.theme.confirmationButtonColor = Config.mainColor
        checkoutSettings.theme.accentColor = Config.mainColor
        checkoutSettings.theme.cellHighlightedBackgroundColor = Config.mainColor
        checkoutSettings.theme.sectionBackgroundColor = Config.mainColor.withAlphaComponent(0.05)
        checkoutSettings.storePaymentDetails = .prompt
        return OPPCheckoutProvider.init(paymentProvider: provider, checkoutID: checkoutID, settings: checkoutSettings)
    }
    
    func requestPaymentStatus() {
        guard let resourcePath = self.transaction?.resourcePath else {
            Utils.showResult(presenter: self, success: false, message: "Resource path is invalid")
            return
        }
        ANLoader.showLoading("Loading..", disableUI: true)
        self.transaction = nil
        // self.processingView.startAnimating()
        let dic:[String:Any] = ["resourcePath": resourcePath,"userId":"\(UserDef.getUserId())"]
        PaymentModuleServices.getPaymentStatusServiceWithOptions(dic: dic, isLoader: false, isUIDisable: true,success: { (data) in
            if(data.Status == false){
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.Message)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
            }else{
                if data.Data![0].customPayment == "OK" {
                   // DispatchQueue.main.async {
                        self.placeOrderMethod()
                    //}
                }else{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        ANLoader.hide()
                    }
                    Utils.showResult(presenter: self, success: false, message: "Your payment was not successful")
                }
            }
            
        }){ (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.showToastAlert(on: self, message:error)
        }
    }
    
    // MARK: - Async payment callback
    
    @objc func didReceiveAsynchronousPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: Config.asyncPaymentCompletedNotificationKey), object: nil)
        self.checkoutProvider?.dismissCheckout(animated: true) {
            DispatchQueue.main.async {
                self.requestPaymentStatus()
            }
        }
    }
    

}
extension CheckOutVC: STCPayVCDelegate{
    func didTapAction(mobileNumber: String) {
        stcPayNumber = mobileNumber
     }
    
    
    
}
