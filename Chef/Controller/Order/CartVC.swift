//
//  CartVC.swift
//  Chef
//
//  Created by RAVI on 16/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import MapKit

class CartVC: UIViewController, NoNetworkViewDelegate, promoCodeDelegate {
    // location
    var apiLatitude:Double = 0.0
    var apiLongtitude:Double = 0.0
    var locationManager: Location!
    var currentDate : Date = Date()
    var SwiftTimer = Timer()
    var isChangeTime:Bool = false

    //MARK: - Prmocode Delegate Func
    func getPromoCode(_ promoCode: String, amount: Double, count: Int,minSpend: Double, maxAmount:Double,percentage: Double) {
        self.promotionCodeLbl_HeightConstraint.constant = self.promotionMainView.frame.size.height-20
        self.promotionCodeLbl.text = promoCode.capitalized
        //self.promotionSubLbl.isHidden = true
        promoAmount = amount
        promoParcenatge = percentage
        self.promotionCancelBtn.setTitle("x", for: .normal)
        self.count = count
        self.minSpend = minSpend
        self.promocode = promoCode
        self.maxAmount = maxAmount
        priceCalucaltion()
    }
    static var instance: CartVC!
    var count : Int = 0
    @IBOutlet weak var navigationBarTitleLbl: UILabel!
    @IBOutlet weak var proceedToPayView: UIStackView!
    @IBOutlet weak var mainView: UIView!
    //@IBOutlet weak var brandImg: UIImageView!
    @IBOutlet weak var branchNameLbl: UILabel!
    @IBOutlet weak var branchAddressLbl: UILabel!
    
    @IBOutlet weak var dateCancelBtn: UIButton!
    @IBOutlet weak var dateDoneBtn: UIButton!
    @IBOutlet weak var deliveryTimeNameLbl: UILabel!
    @IBOutlet weak var deliveryTypeNameLbl: UILabel!
    @IBOutlet weak var pickupBtn: UIButton!
    @IBOutlet weak var deliveryBtn: UIButton!
    
    @IBOutlet weak var orderTypeLbl: UILabel!
    @IBOutlet weak var expectedTimeLbl: UILabel!
    @IBOutlet weak var itmesTableView: UITableView!
    @IBOutlet weak var promotionCodeLbl: UILabel!
    @IBOutlet weak var promotionCodeLbl_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var promotionSubLbl: UILabel!
    @IBOutlet weak var promotionCancelBtn: CustomButton!
    @IBOutlet weak var couponDiscountLbl: UILabel!
    @IBOutlet weak var vatAmountLbl: UILabel!
    @IBOutlet weak var itemTotalLbl: UILabel!
    @IBOutlet weak var vatPercentageLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var toPayAmountLbl: UILabel!
    @IBOutlet weak var proceedToPayBtn: UIButton!
    @IBOutlet weak var tableViewHieghtConstrains: NSLayoutConstraint!
    @IBOutlet weak var ItemViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var orderTypeBtn: UIButton!
    @IBOutlet weak var orderTypeView: UIView!
    
    @IBOutlet weak var offerDiscountTextHeightConstraint: NSLayoutConstraint!
    // @IBOutlet weak var offerDiscountPriceHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var offerDiscountLbl: UILabel!
    @IBOutlet weak var deliveryChargeLbl: UILabel!
    @IBOutlet weak var serviceChargeLbl: UILabel!
    @IBOutlet weak var miniOrderLbl: UILabel!
    @IBOutlet weak var subTotalAmount: UILabel!
    @IBOutlet weak var miniOrderHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardPaymentBtn: UIButton!
    @IBOutlet weak var cashPaymentBtn: UIButton!
    @IBOutlet weak var emptyCartView: UIView!
    @IBOutlet weak var TotalQtyLbl: UILabel!
    var TotalQtyStr:String!

    @IBOutlet weak var orderBillNameLbl: UILabel!
    @IBOutlet weak var itemTotalNameLbl: UILabel!
    @IBOutlet weak var subTotalNameLbl: UILabel!
    @IBOutlet weak var couponDiscountNameLBl: UILabel!
    @IBOutlet weak var totalNameLbl: UILabel!
    
    @IBOutlet weak var emptyCartLbl: UILabel!

    // Change time
    @IBOutlet var ChangeTimeView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var changeDateTimeBtn: UIButton!
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var ExpectedTimeMainView: UIView!
    @IBOutlet weak var orderTypeMainView: UIView!
    @IBOutlet weak var promotionMainView: UIView!
    @IBOutlet weak var invoiceMainView: UIView!
    @IBOutlet weak var paymentModeMainView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    
    var popover = Popover()
    var couponDiscoun = Double()
    var amount = Double()
    var vat = Double()
    var deliveryCharge:Double = 0
    var minSpend:Double = 0
    var promocode:String = ""
    var maxAmount:Double = 0
    var minOrderCharge = Double()
    
    var promoAmount = Double()
    var promoParcenatge = Double()
    var highestDiscount = Double()
    var whereObj:Int = 1
    
    var noNetWork = NoNetworkView()
    var OrderTypeId:Int = 3 // 1:DineIn, 2:PickUp, 3:Delivery
    var PaymentMode:Int = 1
    let regionRadius: CLLocationDistance = 3000
    var firstTime:Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        itmesTableView.isScrollEnabled = false
        CartVC.instance = self
        self.itmesTableView.delegate = self
        self.itmesTableView.dataSource = self
        self.emptyCartView.isHidden = true
        self.itmesTableView.estimatedRowHeight = 100
        self.itmesTableView.rowHeight = UITableView.automaticDimension
        self.miniOrderHeightConstraint.constant = 0
        orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!
        //orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up", value: "", table: nil))!
        emptyCartLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Your cart is empty. Add something from the menu.", value: "", table: nil))!
    }
    override func viewWillLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    @objc func applicationDidEnterBackground(application:UIApplication)  {
        print("Background")
        SwiftTimer.invalidate()
    }
    
    @objc func applicationEnteredForeground(application:UIApplication)  {
        print("Foreground")
        //New
        let count = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable")
        if (count > 0){
                 whereObj = 2
                self.getBranchDeatils()
        }else{
            if let tabItems = self.tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[2]
                tabItem.badgeValue = nil
            }
            self.emptyCartView.isHidden = false
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SwiftTimer.invalidate()
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        //NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationEnteredForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        /*NotificationCenter.default.addObserver(self, selector: #selector(applicationEnteredForeground),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)*/
        
        //New
        let count = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable")
        if (count > 0){
              if whereObj != 2 {
                DispatchQueue.main.async {
                    self.getBranchDeatils()
                }
              }else {
                whereObj = 0
              }
        }else{
            if let tabItems = self.tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[2]
                tabItem.badgeValue = nil
            }
            self.emptyCartView.isHidden = false
        }
        self.priceCalucaltion()
        proceedToPayBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Proceed to Pay", value: "", table: nil))!, for: .normal)
        dateCancelBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cancel", value: "", table: nil))!, for: .normal)
        dateDoneBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Done", value: "", table: nil))!, for: .normal)
        pickupBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up", value: "", table: nil))!, for: .normal)
        deliveryBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!, for: .normal)
        deliveryTimeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Time", value: "", table: nil))!
        deliveryTypeNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Type", value: "", table: nil))!
        if promoParcenatge < 0{
            promotionCodeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Apply Promo Code", value: "", table: nil))!
        }
        orderBillNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Order Bill", value: "", table: nil))!
        itemTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Item Total", value: "", table: nil))!
        subTotalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Sub Total", value: "", table: nil))!
        couponDiscountNameLBl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Coupon Discount", value: "", table: nil))!
        totalNameLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Total Amount", value: "", table: nil))!
        
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func getBranchDeatils(){
        _ = DBObject.GetOrder(getSql: "SELECT * FROM OrderTable")
        if(Connectivity.isConnectedToInternet() == true){
            ANLoader.showLoading("Loading..", disableUI: true)
            let jsonDic = ["BranchId":"\(SaveAddressClass.getOrderArray[0].BranchId!)","AppType":"iOS",
            "AppVersion":"\(AppUtility.getApplicationVersion())"]
           // print(jsonDic)
            OrderApiRouter.getStoreInformation(dic: jsonDic, success: { (data) in
                //ANLoader.hide()
                self.firstTime = false
                if data?.status == false{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        ANLoader.hide()
                    }
                    SaveAddressClass.getBranchDetailsArray.removeAll()
                    self.proceedToPayBtn.isEnabled = false
                    self.proceedToPayBtn.layer.opacity = 0.5
                    self.miniOrderLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Online orders not accepted", value: "", table: nil))!
                    self.miniOrderHeightConstraint.constant = 25
                    return
                }else{
                    //print(data!.successDic!.StoresDetails!)
                    SaveAddressClass.getBranchDetailsArray.removeAll()
                    for storeList in data!.successDic!.StoresDetails!{
                       // print(storeList)
                        let storesDic = GetStores(AvgPreparationTime: storeList["AvgPreparationTime"] is NSNull ? 0 : storeList["AvgPreparationTime"] as! Int,
                                                  BranchId: storeList["BranchId"] is NSNull ? 0 : storeList["BranchId"] as! Int,
                                                  BranchName_En: storeList["BranchName_En"] is NSNull ? "" : storeList["BranchName_En"] as! String,
                                                  BranchName_Ar:storeList["BranchName_Ar"] is NSNull ? "" : storeList["BranchName_Ar"] as! String,
                                                  DeliveryCharges: storeList["DeliveryCharges"] is NSNull ? 0 : storeList["DeliveryCharges"] as! Double,
                                                  Address: storeList["Address"] is NSNull ? "" : storeList["Address"] as! String,
                                                  MinOrderCharges: storeList["MinOrderCharges"] is NSNull ? 0 : storeList["MinOrderCharges"] as! Double,
                                                  EmailId: storeList["EmailId"] is NSNull ? "" : storeList["EmailId"] as! String,
                                                  MobileNo: storeList["MobileNo"] is NSNull ? "" : storeList["MobileNo"] as! String,
                                                  OrderTypes: storeList["OrderTypes"] is NSNull ? "" : storeList["OrderTypes"] as! String,
                                                  Rating: storeList["Rating"] is NSNull ? "" : storeList["Rating"] as! String,
                                                  RatingCount: storeList["RatingCount"] is NSNull ? 0 : storeList["RatingCount"] as! Int,
                                                  StoreLogo_En: storeList["StoreLogo_En"] is NSNull ? "" : storeList["StoreLogo_En"] as! String,
                                                  StoreImage_En: storeList["StoreImage_En"] is NSNull ? "" : storeList["StoreImage_En"] as! String,
                                                  StoreStatus: storeList["StoreStatus"] is NSNull ? "" : storeList["StoreStatus"] as! String,
                                                  StoreType: storeList["StoreType"] is NSNull ? "" : storeList["StoreType"] as! String,
                                                  TakeAwayDistance: storeList["TakeAwayDistance"] is NSNull ? 0 : storeList["TakeAwayDistance"] as! Int,
                                                  DeliveryDistance: storeList["DeliveryDistance"] is NSNull ? 0.00 : storeList["DeliveryDistance"] as! Double,
                                                  StarDateTime: storeList["StarDateTime"] is NSNull ? "" : storeList["StarDateTime"] as! String,
                                                  EndDateTime: storeList["EndDateTime"] is NSNull ? "" : storeList["EndDateTime"] as! String,
                                                  CurrentDateTime: storeList["CurrentDateTime"] is NSNull ? "" : storeList["CurrentDateTime"] as! String,
                                                  IsPromoted: storeList["IsPromoted"] is NSNull ? false : storeList["IsPromoted"] as! Bool,
                                                  Popular: storeList["Popular"] is NSNull ? false : storeList["Popular"] as! Bool,
                                                  DiscountAmt: storeList["DiscountAmt"] is NSNull ? 0 : storeList["DiscountAmt"] as! Int,
                                                  IsNew: storeList["IsNew"] is NSNull ? false : storeList["IsNew"] as! Bool,
                                                  Latitude: storeList["Latitude"] is NSNull ? 0.00 : storeList["Latitude"] as! Double,
                                                  Longitude: storeList["Longitude"] is NSNull ? 0.00 : storeList["Longitude"] as! Double,
                                                  Distance: storeList["Distance"] is NSNull ? 0.00 : storeList["Distance"] as! Double,
                                                  BrandId: storeList["BrandId"] is NSNull ? 0 : storeList["BrandId"] as! Int,
                                                  BindingCount: 1,
                                                  BrandName_En: storeList["BrandName_En"] is NSNull ? "" : storeList["BrandName_En"] as! String,
                                                  BrandName_Ar: storeList["BrandName_Ar"] is NSNull ? "" : storeList["BrandName_Ar"] as! String,
                                                  PaymentType:  storeList["PaymentType"] is NSNull ? "" : storeList["PaymentType"] as! String,
                                                  BranchDescription_En: storeList["BranchDescription_En"] is NSNull ? "" : storeList["BranchDescription_En"] as! String,
                                                  Serving: storeList["Serving"] is NSNull ? 0 : storeList["Serving"] as! Int,
                                                  FutureOrderDay: storeList["FutureOrderDay"] is NSNull ? 0 : storeList["FutureOrderDay"] as! Int,
                                                  StoreType_Ar:storeList["StoreType_Ar"] is NSNull ? "" : storeList["StoreType_Ar"] as! String, BranchDescription_Ar:storeList["BranchDescription_Ar"] is NSNull ? "" : storeList["BranchDescription_Ar"] as! String, UpdatesAvailable: storeList["UpdatesAvailable"] is NSNull ? 0 : storeList["UpdatesAvailable"] as! Int, UpdateSeverity: storeList["UpdateSeverity"] is NSNull ? 0 : storeList["UpdateSeverity"] as! Int, VatPercentage: storeList["VatPercentage"] is NSNull ? 0 : storeList["VatPercentage"] as! Double
                        )
                        // Save More Stores
                        SaveAddressClass.getBranchDetailsArray.append(storesDic)
                    }
                    if SaveAddressClass.getBranchDetailsArray.count > 0 {
                        self.branchNameLbl.backgroundColor = UIColor.clear
                        self.branchAddressLbl.backgroundColor = UIColor.clear
                        //self.brandImg.backg
                        if AppDelegate.getDelegate().appLanguage == "English"{
                            self.navigationBarTitleLbl.text = SaveAddressClass.getBranchDetailsArray[0].BranchName_En!.capitalized
                            self.branchNameLbl.text = SaveAddressClass.getBranchDetailsArray[0].BranchName_En!.capitalized
                        }else{
                            self.navigationBarTitleLbl.text = SaveAddressClass.getBranchDetailsArray[0].BranchName_Ar!.capitalized
                            self.branchNameLbl.text = SaveAddressClass.getBranchDetailsArray[0].BranchName_Ar!.capitalized
                        }
                        self.vatPercentageLbl.text = "\( (AppDelegate.getDelegate().filePath?.localizedString(forKey: "VAT", value: "", table: nil))!) (\(SaveAddressClass.getBranchDetailsArray[0].VatPercentage!)%)"
                        self.currentDate = DateFormate.dateConverstion(dateStr: SaveAddressClass.getBranchDetailsArray[0].CurrentDateTime!)
                        self.currentDate =  Calendar.current.date(byAdding: .minute, value: SaveAddressClass.getBranchDetailsArray[0].AvgPreparationTime!, to: self.currentDate)!

                        if self.isChangeTime == false {
                          self.ExpetectedTimeCalculatMethod()
                        }
                        self.SwiftTimer = Timer.scheduledTimer(timeInterval:60.0, target: self, selector: #selector(self.getCurrentDateAndTime), userInfo: nil, repeats: true);
                        
                        self.firstTime = false
                        self.priceCalucaltion()
                        self.branchAddressLbl.text = SaveAddressClass.getBranchDetailsArray[0].Address!
                        let initialLocation = CLLocation(latitude: SaveAddressClass.getBranchDetailsArray[0].Latitude!, longitude: SaveAddressClass.getBranchDetailsArray[0].Longitude!)
                        self.centerMapOnLocation(location: initialLocation)
                        
                        
                        if SaveAddressClass.getBranchDetailsArray[0].StoreStatus! == "Close" {
                            let startDate = DateFormate.dateConverstion(dateStr: SaveAddressClass.getBranchDetailsArray[0].StarDateTime!)
                           // let currentDate = DateFormate.dateConverstion(dateStr: SaveAddressClass.getBranchDetailsArray[0].CurrentDateTime!)
                            if startDate > self.currentDate {
                                self.miniOrderLbl.text = "\(String(format:"\(String(describing: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Restaurant opens next at", value: "", table: nil)))) %@", DateFormate.dateConverstionToString(date: startDate)))"
                            }else{
                                self.miniOrderLbl.text = "\(String(format:"\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Restaurant opens next at", value: "", table: nil))!) %@ \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "tomorrow", value: "", table: nil))!)", DateFormate.dateConverstionToString(date: startDate)))"
                            }
                            self.proceedToPayBtn.isEnabled = false
                            self.proceedToPayBtn.layer.opacity = 0.5
                            self.miniOrderHeightConstraint.constant = 25
                        }else{
                            self.proceedToPayBtn.isEnabled = true
                            self.miniOrderHeightConstraint.constant = 0
                            self.proceedToPayBtn.layer.opacity = 1.0
                        }
                        if AppDelegate.getDelegate().dictionary_FinalDiscount.count > 0 || AppDelegate.getDelegate().dictionary_FinalAmount.count > 0{
                            self.priceCalucaltion()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                ANLoader.hide()
                            }
                        }else{
                            self.promotionCancelBtn_Method()
                            
                            if self.whereObj != 3 || self.whereObj != 2 {
                                self.whereObj = 0
                                if isUserLogIn() == true {
                                    self.getPromotionsHistoryService()
                                }else{
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                        ANLoader.hide()
                                    }
                                }
                                
                            }else{
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    ANLoader.hide()
                                }
                            }
                        }
                    }else{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            ANLoader.hide()
                        }
                        self.vatPercentageLbl.text = "\( (AppDelegate.getDelegate().filePath?.localizedString(forKey: "VAT", value: "", table: nil))!) (0%)"
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Branch Details not found!", value: "", table: nil))!)
                        self.emptyCartView.isHidden = false
                    }
                    if SaveAddressClass.getBranchDetailsArray[0].UpdateSeverity! == 2{
                        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
                        let obj = mainStoryBoard.instantiateViewController(withIdentifier: "UpdateAppVC") as! UpdateAppVC
                        obj.modalPresentationStyle = .overCurrentContext
                        obj.modalTransitionStyle = .crossDissolve
                        self.present(obj, animated: false, completion: nil)
                        return
                    }
                }
            }){ (error) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
                SaveAddressClass.getBranchDetailsArray.removeAll()
                self.proceedToPayBtn.isEnabled = false
                self.proceedToPayBtn.layer.opacity = 0.5
                self.miniOrderLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Online orders not accepted", value: "", table: nil))!
                self.miniOrderHeightConstraint.constant = 25
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            self.noNetWork = (Bundle.main.loadNibNamed("NoNetworkView", owner: self, options: nil)?.first as? NoNetworkView)!
            self.noNetWork.delegate = self
            self.noNetWork.frame = CGRect(x:0,y:10,width:self.view.frame.width,height:self.view.frame.height-20)
            self.view.addSubview(self.noNetWork)
        }
    }
    func getPromotionsHistoryService()  {
        
        let dic:[String:Any] = ["BranchId":SaveAddressClass.getBranchDetailsArray[0].BranchId! as Any,"DeviceToken":"\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))","UserId":"\(UserDef.getUserId())","BrandId":SaveAddressClass.getBranchDetailsArray[0].BrandId! as Any]
        //let dic:[String:Any] = ["BranchId":0,"DeviceToken":0,"UserId":0,"BrandId":0]
        OrderApiRouter.getPromotionsService(dic: dic,isLoader:false,isUIDisable:true, success: { (data) in
           
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            if(data.Status == false){
                Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.Message)
            }else{
                SaveAddressClass.getPromotionsArray.removeAll()
                DispatchQueue.main.async {
                    for Data in data.Data!{
                        //print(data)
                        SaveAddressClass.getPromotionsArray.append(Data)
                    }
                    self.highestDiscount_Calc()
                }
            }
        }){ (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }

    @IBAction func mapBtn_tapped(_ sender: Any) {
        // Location Manager
        locationManager = Location()
        locationManager.needToDisplayAlert = true
        locationManager.delegate = self
    }
    @IBAction func orderTypeBtn_tapped(_ sender: Any) {
        if AppDelegate.getDelegate().appLanguage == "English"{
            let startPoint = CGPoint(x: orderTypeBtn.frame.origin.x+40, y: 238 - myScrollView.contentOffset.y)
            self.orderTypeView.frame =  CGRect(x: 0, y: 0, width: 150, height: 80)
            popover.show(self.orderTypeView, point: startPoint)
            itmesTableView.reloadData()
        }else{
            let startPoint = CGPoint(x: ExpectedTimeMainView.frame.origin.x+55, y: 238 - myScrollView.contentOffset.y)
            self.orderTypeView.frame =  CGRect(x: 0, y: 0, width: 150, height: 80)
            popover.show(self.orderTypeView, point: startPoint)
            itmesTableView.reloadData()
        }
    }
    @IBAction func promotionCancelBtn_tapped(_ sender: Any) {
        self.promotionCodeLbl_HeightConstraint.constant = self.promotionMainView.frame.size.height-20
        if self.promotionCancelBtn.titleLabel?.text == "x" {
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.promotionCancelBtn.setTitle(">", for: .normal)
            }else{
                self.promotionCancelBtn.setTitle("<", for: .normal)
            }
            self.promotionCancelBtn_Method()
        }else{
            if self.promotionCodeLbl.text == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Apply Promo Code", value: "", table: nil))! || self.promotionCodeLbl.text == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Not Available", value: "", table: nil))!{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    self.promotionCancelBtn.setTitle(">", for: .normal)
                }else{
                    self.promotionCancelBtn.setTitle("<", for: .normal)
                }
            }else{
                self.promotionCancelBtn.setTitle("x", for: .normal)
            }
            self.ApplyPromationBtn_Tapped(0)
        }
    }
    func promotionCancelBtn_Method() {
        couponDiscoun = 0.0
        promoAmount = 0.0
        promoParcenatge = 0.0
        promocode = ""
        
        self.promotionCodeLbl_HeightConstraint.constant = self.promotionMainView.frame.size.height-20
        self.promotionCodeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Apply Promo Code", value: "", table: nil))!
        self.promotionSubLbl.text = ""
        priceCalucaltion()
    }
    
    @IBAction func PickUpBtn_tapped(_ sender: Any) {
        orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Pick Up", value: "", table: nil))!
        OrderTypeId = 2
        popover.dismiss()
        priceCalucaltion()
    }
    @IBAction func DeleveryBtn_Tapped(_ sender: Any) {
        orderTypeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery", value: "", table: nil))!
        OrderTypeId = 3
        popover.dismiss()
        priceCalucaltion()
    }
    @IBAction func ItemPlusBtn_tapped(sender: UIButton) {
        //print(sender.tag)
        var qty:Int = SaveAddressClass.getOrderArray[sender.tag].Quantity!
        var TotalAmount: Double = SaveAddressClass.getOrderArray[sender.tag].TotalAmount!
        var originalTotalAmount: Double = SaveAddressClass.getOrderArray[sender.tag].OriginalTotalAmount!
        originalTotalAmount = originalTotalAmount / Double(qty)
        TotalAmount = TotalAmount / Double(qty)
        qty = qty + 1
        TotalAmount = TotalAmount * Double(qty)
        originalTotalAmount = originalTotalAmount * Double(qty)
        
        let Sql = "UPDATE OrderTable SET Quantity = '\(qty)',TotalAmount = '\(TotalAmount)',originalTotalAmount = '\(originalTotalAmount)' where OrderId ='\(SaveAddressClass.getOrderArray[sender.tag].OrderId!)'"
        let inserOrdertBool = DBObject.InsertOrderString(insertSql: Sql)
        if inserOrdertBool == true {
            self.priceCalucaltion()
        }
    }
    @IBAction func ItemMinusBtn_tapped(sender: UIButton) {
        //print(sender.tag)
        var qty:Int = SaveAddressClass.getOrderArray[sender.tag].Quantity!
        if  qty == 1 {
            let Sql = "DELETE FROM OrderTable where OrderId ='\(SaveAddressClass.getOrderArray[sender.tag].OrderId!)'"
            let inserOrdertBool = DBObject.InsertOrderString(insertSql: Sql)
            if inserOrdertBool == true {
                let Sql1 = "DELETE FROM AdditionalTable where OrderId ='\(SaveAddressClass.getOrderArray[sender.tag].OrderId!)'"
                let inserOrdertBool1 = DBObject.InsertOrderString(insertSql: Sql1)
                if inserOrdertBool1 == true {
                    self.priceCalucaltion()
                }
            }
        }else{
            var TotalAmount: Double = SaveAddressClass.getOrderArray[sender.tag].TotalAmount!
            var originalTotalAmount: Double = SaveAddressClass.getOrderArray[sender.tag].OriginalTotalAmount!
            originalTotalAmount = originalTotalAmount / Double(qty)
            TotalAmount = TotalAmount / Double(qty)
            qty = qty - 1
            TotalAmount = TotalAmount * Double(qty)
            originalTotalAmount = originalTotalAmount * Double(qty)
            
            let Sql = "UPDATE OrderTable SET Quantity = '\(qty)',TotalAmount = '\(TotalAmount)',originalTotalAmount = '\(originalTotalAmount)'  where OrderId ='\(SaveAddressClass.getOrderArray[sender.tag].OrderId!)'"
            let inserOrdertBool = DBObject.InsertOrderString(insertSql: Sql)
            if inserOrdertBool == true {
                self.priceCalucaltion()
            }
        }
    }
    func priceCalucaltion() {
        let count = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable")
        if (count > 0){
            if let tabItems = self.tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[2]
                tabItem.badgeValue = "\(count)"
            }
            self.emptyCartView.isHidden = true
            
            if count > 1{
                TotalQtyStr =  "\(count) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Items", value: "", table: nil))!)"
            }else {
                TotalQtyStr = "\(count) \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Item", value: "", table: nil))!)"
            }
            
            amount = DBObject.geTotalOrderAmount(Sql:"SELECT sum(TotalAmount) FROM OrderTable")
            self.itemTotalLbl.text = "\(amount.withCommas())"
            self.serviceChargeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Charge", value: "", table: nil))!
            deliveryCharge = 0.0
            if SaveAddressClass.getBranchDetailsArray.count > 0 {
                if OrderTypeId == 3 {
                    deliveryCharge = SaveAddressClass.getBranchDetailsArray[0].DeliveryCharges!
                    self.serviceChargeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Delivery Charge", value: "", table: nil))!
                }else{
                    self.serviceChargeLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Service Charge", value: "", table: nil))!
                }
               // print(SaveAddressClass.getBranchDetailsArray[0].MinOrderCharges!)
               // print(amount)
                if SaveAddressClass.getBranchDetailsArray[0].MinOrderCharges! > amount {
                    self.proceedToPayBtn.isEnabled = false
                    self.proceedToPayBtn.layer.opacity = 0.5
                    self.miniOrderLbl.text = "\(String(format:"\(String(describing: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Subtotal must be greater than", value: "", table: nil))!)) \(SaveAddressClass.getBranchDetailsArray[0].MinOrderCharges!-1) SAR"))"
                    self.miniOrderHeightConstraint.constant = 25
                }else{
                    self.proceedToPayBtn.isEnabled = true
                    self.miniOrderHeightConstraint.constant = 0
                    self.proceedToPayBtn.layer.opacity = 1.0
                }
                minOrderCharge = SaveAddressClass.getBranchDetailsArray[0].MinOrderCharges!
            }else{
                self.proceedToPayBtn.isEnabled = false
                self.proceedToPayBtn.layer.opacity = 0.5
                if firstTime == false {
                    self.miniOrderHeightConstraint.constant = 25
                    self.miniOrderLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Online orders not accepted", value: "", table: nil))!
                }else{
                    self.miniOrderHeightConstraint.constant = 0
                }
            }
            
            // Offer Amount
            let OriginalTotalAmount:Double = DBObject.geTotalOrderAmount(Sql:"SELECT sum(OriginalTotalAmount) FROM OrderTable")
            let discountAmount:Double = OriginalTotalAmount - amount
            if discountAmount != 0 {
                self.offerDiscountLbl.text = "- \(discountAmount.withCommas())"
                self.offerDiscountTextHeightConstraint.constant = 30
            }else{
                self.offerDiscountLbl.text = "- 0.00"
                self.offerDiscountTextHeightConstraint.constant = 0
            }
            self.deliveryChargeLbl.text = "+ \(deliveryCharge.withCommas())"
            //self.vat = amount * 0.05
            //self.vat = ((amount + deliveryCharge) * (SaveAddressClass.getStoresArray[0].VatPercentage!/100.00))
            if SaveAddressClass.getStoresArray.count > 0{
                self.vat = ((amount + deliveryCharge) * (SaveAddressClass.getStoresArray[0].VatPercentage!/100.00))
            }else{
                self.vat = ((amount + deliveryCharge) * (SaveAddressClass.getBranchDetailsArray[0].VatPercentage!/100.00))
            }
            self.vatAmountLbl.text = "+ \(self.vat.withCommas())"
            let subTotal = amount + vat + deliveryCharge
            subTotalAmount.text = "\(subTotal.withCommas())"
            
            if promoAmount > 0 {
                couponDiscoun = promoAmount > (amount+self.vat+deliveryCharge) ? (amount+self.vat+deliveryCharge) : promoAmount
            }else if promoParcenatge > 0 {
                couponDiscoun = (amount+vat+deliveryCharge) * (promoParcenatge/100.00)
            }
            
            if maxAmount <= couponDiscoun{
                couponDiscoun = maxAmount
            }
            self.couponDiscountLbl.text = "- \(couponDiscoun.withCommas())"

 
            if subTotal < minSpend {
                if minOrderCharge > amount {
                    self.proceedToPayBtn.isEnabled = false
                    self.proceedToPayBtn.layer.opacity = 0.5
                    let MinOrderCharges = SaveAddressClass.getBranchDetailsArray[0].MinOrderCharges! - 1.00
                    self.miniOrderLbl.text = "\(String(format:"\(String(describing: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Cart amount must be greater than", value: "", table: nil))!)) %@ SAR", MinOrderCharges.withCommas()))"
                    self.miniOrderHeightConstraint.constant = 25
                }else{
                    if promoAmount > 0 || promoParcenatge > 0 {
                        if self.miniOrderHeightConstraint.constant == 0 {
                            self.proceedToPayBtn.isEnabled = false
                            self.proceedToPayBtn.layer.opacity = 0.5
                            self.miniOrderLbl.text = "\(String(format:"\(String(describing: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Promotion applied, cart amount must be greater than", value: "", table: nil))!)) %@ SAR", minSpend.withCommas()))"
                            self.miniOrderHeightConstraint.constant = 25
                        }
                    }
                }
            }
            
            if promocode.isEmpty != true{
                self.promotionCodeLbl.text = promocode.capitalized
            }
            if self.promotionCodeLbl.text != (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Apply Promo Code", value: "", table: nil))!{
                self.promotionCancelBtn.setTitle("x", for: .normal)
            }
            amount = (amount+self.vat+deliveryCharge) - couponDiscoun
            
            if AppDelegate.getDelegate().appLanguage == "English"{
                self.totalAmountLbl.text = "SAR \(amount.withCommas())"
            }else{
                self.totalAmountLbl.text = "\(amount.withCommas()) SAR"
            }
            _ = DBObject.GetOrder(getSql: "SELECT * FROM OrderTable")
            self.itmesTableView.reloadData()
        }else{
            if let tabItems = self.tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[2]
                tabItem.badgeValue = nil
            }
            self.emptyCartView.isHidden = false
        }
    }
    @IBAction func cardPaymentBtn_tapped(_ sender: Any) {
        PaymentMode = 2
        let normalImg = UIImage(named: "paymentNormal")
        let selectImg = UIImage(named: "paymentSelect")
        self.cardPaymentBtn.setImage(selectImg, for: .normal)
        self.cashPaymentBtn.setImage(normalImg, for: .normal)
    }
    @IBAction func cashPaymentBtn_tapped(_ sender: Any) {
        PaymentMode = 1
        let normalImg = UIImage(named: "paymentNormal")
        let selectImg = UIImage(named: "paymentSelect")
        self.cardPaymentBtn.setImage(normalImg, for: .normal)
        self.cashPaymentBtn.setImage(selectImg, for: .normal)
    }
    @objc func getCurrentDateAndTime(){
        
        //print(SaveAddressClass.getBranchDetailsArray[0])
        let startDate = DateFormate.dateConverstion(dateStr: SaveAddressClass.getBranchDetailsArray[0].StarDateTime!)
        
        if isChangeTime == false {
           self.ExpetectedTimeCalculatMethod()
        }else{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd MMM yyyy hh:mm a"
            dateFormatterGet.locale = Locale(identifier: "EN")
            let calendar = Calendar.current
            let date = calendar.date(byAdding: .minute, value: 0, to: currentDate)
            let expectedDate = dateFormatterGet.date(from: self.expectedTimeLbl.text!)
            if expectedDate!.compare(date!) == .orderedSame || expectedDate!.compare(date!) == .orderedAscending {
                isChangeTime = false
                self.ExpetectedTimeCalculatMethod()
            }
        }
        
        if SaveAddressClass.getBranchDetailsArray[0].StoreStatus! == "Close" {
            if startDate > self.currentDate {
                self.miniOrderLbl.text = "\(String(format:"\(String(describing: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Restaurant opens next at", value: "", table: nil)))) %@", DateFormate.dateConverstionToString(date: startDate)))"
            }else{
                self.miniOrderLbl.text = "\(String(format:"\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Restaurant opens next at", value: "", table: nil))!) %@ \((AppDelegate.getDelegate().filePath?.localizedString(forKey: "tomorrow", value: "", table: nil))!)", DateFormate.dateConverstionToString(date: startDate)))"
            }
            self.proceedToPayBtn.isEnabled = false
            self.proceedToPayBtn.layer.opacity = 0.5
            self.miniOrderHeightConstraint.constant = 25
            return
        }
        
        if startDate > self.currentDate {
            self.miniOrderLbl.text = "\(String(format:"\(String(describing: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Orders closed for today", value: "", table: nil)))) %@", DateFormate.dateConverstionToString(date: startDate)))"
            self.proceedToPayBtn.isEnabled = false
            self.proceedToPayBtn.layer.opacity = 0.5
            self.miniOrderHeightConstraint.constant = 25
            return
        }

    }
    
    func ExpetectedTimeCalculatMethod() {
        if SaveAddressClass.getBranchDetailsArray.count > 0 {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd MMM yyyy hh:mm a"
            dateFormatterGet.locale = Locale(identifier: "EN")
            let calendar = Calendar.current
            let date = calendar.date(byAdding: .minute, value: 0, to: currentDate)
            self.expectedTimeLbl.text = dateFormatterGet.string(from:date!)
            popover.dismiss()
        }else{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd MMM yyyy hh:mm a"
            dateFormatterGet.locale = Locale(identifier: "EN")
            
            let calendar = Calendar.current
            let date = calendar.date(byAdding: .minute, value: 0, to: Date())
            self.expectedTimeLbl.text = dateFormatterGet.string(from:date!)
            popover.dismiss()
        }
    }
    @IBAction func changeExpTimeBtn_tapped(_ sender: Any) {
        self.datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        self.datePicker.setDate(currentDate, animated: true)
        self.datePicker.setValue(UIColor.DarkGreen, forKey: "textColor")
        var dateComponents = DateComponents()
        
        if SaveAddressClass.getBranchDetailsArray[0].FutureOrderDay! == 0{
            dateComponents.day = 1
        }else{
            dateComponents.day = SaveAddressClass.getBranchDetailsArray[0].FutureOrderDay!
        }
        let maximumDays = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        self.datePicker.maximumDate = maximumDays
        
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .minute, value: 0, to: currentDate)
        self.datePicker.minimumDate = date!
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            let startPoint = CGPoint(x: ExpectedTimeMainView.frame.origin.x+180, y: 238 - myScrollView.contentOffset.y)
            self.ChangeTimeView.frame =  CGRect(x: 0, y: 0, width: 280, height: 280)
            popover.show(self.ChangeTimeView, point: startPoint)
        }else{
            let startPoint = CGPoint(x: ExpectedTimeMainView.frame.origin.x+50, y: 238 - myScrollView.contentOffset.y)
            self.ChangeTimeView.frame =  CGRect(x: 0, y: 0, width: 280, height: 280)
            popover.show(self.ChangeTimeView, point: startPoint)
        }
    }
    
    @IBAction func cancelDatePickerBtn_tapped(_ sender: Any) {
        popover.dismiss()
    }
    @IBAction func DoneDatePickerBtn_tapped(_ sender: Any) {
        isChangeTime = true
        self.datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd MMM yyyy hh:mm a"
        dateFormatterGet.locale = Locale(identifier: "EN")

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.locale = Locale(identifier: "EN")
        self.expectedTimeLbl.text = dateFormatterGet.string(from:self.datePicker.date)
        popover.dismiss()
    }
    @IBAction func proceedToPayBtn_tapped(_ sender: Any) {
        if(isUserLogIn() == false){
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let loginVCObj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            loginVCObj.loginFrom = 1
            self.navigationController?.pushViewController(loginVCObj, animated: true)
            return
        }
        
        let count = DBObject.getCountOrder(Sql:"SELECT sum(Quantity) FROM OrderTable")
        if count > 0{
            whereObj = 3
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            var obj = CheckOutVC()
            obj = mainStoryBoard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:(AppDelegate.getDelegate().filePath?.localizedString(forKey: "Add Items to Cart", value: "", table: nil))!)
        }
        
        
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    // no network
    func TryAgain() {
        DispatchQueue.main.async {
        self.noNetWork.isHidden = true
        self.proceedToPayBtn_tapped(0)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Apply Promation
    @IBAction func ApplyPromationBtn_Tapped(_ sender: Any){
        if isUserLogIn() == true {
            whereObj = 2
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let promotionVC = mainStoryBoard.instantiateViewController(withIdentifier: "PromotionsVC") as! PromotionsVC
            promotionVC.promoDelegate = self
            promotionVC.obj_Offer = 0
            self.navigationController?.pushViewController(promotionVC, animated: true)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
            let loginVCObj = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            loginVCObj.loginFrom = 1
            self.navigationController?.pushViewController(loginVCObj, animated: true)
        }
    }
    
    //MARK: - Calculating High Discount
    func highestDiscount_Calc() {
        let promoArrayDict = SaveAddressClass.getPromotionsArray
        var highestPercentage: Double = 0.0
        amount = DBObject.geTotalOrderAmount(Sql:"SELECT sum(TotalAmount) FROM OrderTable")
        //self.vat = amount * 0.05
        //self.vat = ((amount + deliveryCharge) * (SaveAddressClass.getStoresArray[0].VatPercentage!/100.00))
        if SaveAddressClass.getStoresArray.count > 0{
            self.vat = ((amount + deliveryCharge) * (SaveAddressClass.getStoresArray[0].VatPercentage!/100.00))
        }else{
            self.vat = ((amount + deliveryCharge) * (SaveAddressClass.getBranchDetailsArray[0].VatPercentage!/100.00))
        }
        
        for dic in promoArrayDict {
            if (amount+vat+deliveryCharge) >= Double(dic.MinSpend) && dic.TodayAvailable == true {
                if dic.DiscountType == 1{
                    if Double(dic.DiscountAmt) > highestDiscount{
                        AppDelegate.getDelegate().dictionary_FinalAmount = [dic]
                        if (amount+vat+deliveryCharge) > Double(dic.DiscountAmt) {
                            highestDiscount = Double(dic.DiscountAmt) > highestDiscount ? Double(dic.DiscountAmt) : highestDiscount
                        }else{
                            
                            let fmount = (amount+vat+deliveryCharge) - Double(dic.DiscountAmt)
                            highestDiscount = fmount > highestDiscount ? fmount : highestDiscount
                        }
                    }
                    
                }else if dic.DiscountType == 2 {
                    if Double(dic.DiscountAmt) > highestPercentage {
                        AppDelegate.getDelegate().dictionary_FinalDiscount = [dic]
                        // highestPercentage = dic.DiscountAmt > highestPercentage ? dic.DiscountAmt : highestPercentage
                        let fmount = (amount+vat+deliveryCharge) * (dic.DiscountAmt/100.00)
                        highestPercentage = fmount > dic.MaxAmount ? dic.MaxAmount : fmount
                    }
                }
            }
        }
        
        if AppDelegate.getDelegate().dictionary_FinalDiscount.count > 0 || AppDelegate.getDelegate().dictionary_FinalAmount.count > 0 {
            if highestDiscount > highestPercentage{
                if AppDelegate.getDelegate().dictionary_FinalDiscount.count > 0{
                    self.promotionCodeLbl.text = AppDelegate.getDelegate().dictionary_FinalAmount[0].PromotionCode
                    promocode = AppDelegate.getDelegate().dictionary_FinalAmount[0].PromotionCode
                    promoAmount =  AppDelegate.getDelegate().dictionary_FinalAmount[0].DiscountAmt
                    maxAmount = AppDelegate.getDelegate().dictionary_FinalAmount[0].MaxAmount
                    minSpend = AppDelegate.getDelegate().dictionary_FinalAmount[0].MinSpend
                }
            }else{
                if AppDelegate.getDelegate().dictionary_FinalDiscount.count > 0{
                    self.promotionCodeLbl.text = AppDelegate.getDelegate().dictionary_FinalDiscount[0].PromotionCode
                    promocode = AppDelegate.getDelegate().dictionary_FinalDiscount[0].PromotionCode

                    promoParcenatge = AppDelegate.getDelegate().dictionary_FinalDiscount[0].DiscountAmt
                    maxAmount = AppDelegate.getDelegate().dictionary_FinalDiscount[0].MaxAmount
                    minSpend = AppDelegate.getDelegate().dictionary_FinalDiscount[0].MinSpend
                }
            }
        }
        self.priceCalucaltion()
    }
}
extension CartVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(SaveAddressClass.getOrderArray.count)
        return SaveAddressClass.getOrderArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell
        if AppDelegate.getDelegate().appLanguage == "English"{
            cell.ItemNameLbl.text = SaveAddressClass.getOrderArray[indexPath.row].ItemName_En!.capitalized
        }else{
            cell.ItemNameLbl.text = SaveAddressClass.getOrderArray[indexPath.row].ItemName_Ar!.capitalized
        }
        
        cell.qtyLbl.text = "\(SaveAddressClass.getOrderArray[indexPath.row].Quantity!)"
        if SaveAddressClass.getOrderArray[indexPath.row].Comments! == ""{
            cell.noteLbl.text = ""
        }else{
            cell.noteLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Note", value: "", table: nil))!):\(SaveAddressClass.getOrderArray[indexPath.row].Comments!)"
        }
        if SaveAddressClass.getOrderArray[indexPath.row].DiscountPercentage! > 0 {
            // Line Draw horizentaly discount price
            let attributeString = NSMutableAttributedString(string: "SAR \(SaveAddressClass.getOrderArray[indexPath.row].OriginalTotalAmount!.withCommas())")
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
            cell.originalPriceLbl?.attributedText = attributeString
            cell.priceConstraintHeight.constant = 25
        }else{
            cell.originalPriceLbl?.text = nil
            cell.priceConstraintHeight.constant = 30
        }
        cell.ItemPriceLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "SAR", value: "", table: nil))!) \(SaveAddressClass.getOrderArray[indexPath.row].TotalAmount!.withCommas())"
        
        // selected store image
        let imgStr:NSString = "\(url.ItemImg.imgPath())\(SaveAddressClass.getOrderArray[indexPath.row].ItemImg! )" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let URLString = URL.init(string: urlStr as String)
        cell.ItemImg.kf.indicatorType = .activity
        cell.ItemImg.kf.setImage(with: URLString)
        cell.ItemImg.layer.cornerRadius = 10
        cell.ItemMinusBtn.tag = indexPath.row
        cell.ItemMinusBtn.addTarget(self, action: #selector(ItemMinusBtn_tapped(sender:)), for: .touchUpInside)
        cell.ItemPlusBtn.tag = indexPath.row
        cell.ItemPlusBtn.addTarget(self, action: #selector(ItemPlusBtn_tapped(sender:)), for: .touchUpInside)
        self.tableViewHieghtConstrains.constant = tableView.contentSize.height - 5
        self.ItemViewHeightConstraint.constant = tableView.contentSize.height - 5
        return cell
    }
}
extension CartVC : LocationDelegate {
    func didFailedLocation(error: String) {
        //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message: error)
        let alert = UIAlertController(title: "Allow Location Access", message: "The Chef needs access to your location. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)

                   // Button to Open Settings
                   alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                       guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                           return
                       }
                       if UIApplication.shared.canOpenURL(settingsUrl) {
                           UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                               print("Settings opened: \(success)")
                           })
                       }
                   }))
                   alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                   self.present(alert, animated: true, completion: nil)
    }
    func didUpdateLocation(latitude: Double?, longitude: Double?) {
        apiLatitude = Double(latitude!)
        apiLongtitude = Double(longitude!)
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(apiLatitude),\(apiLongtitude)&zoom=14&views=traffic&q=\(SaveAddressClass.getBranchDetailsArray[0].Latitude!),\(SaveAddressClass.getBranchDetailsArray[0].Longitude!)")!, options: [:], completionHandler: nil)
        }else {
            print("Can't use comgooglemaps://")
            UIApplication.shared.open(URL(string:"http://maps.google.com/maps?q=\(apiLatitude),\(apiLongtitude)&zoom=14&views=traffic&q=\(SaveAddressClass.getBranchDetailsArray[0].Latitude!),\(SaveAddressClass.getBranchDetailsArray[0].Longitude!)")!, options: [:], completionHandler: nil)
        }
    }
}
