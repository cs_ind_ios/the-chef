//
//  FilterVC.swift
//  Chef
//
//  Created by RAVI on 17/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

protocol AddFilterVCDelegate {
    func didTapAction(dic:[Int],distance:Int)
}

class FilterVC: UIViewController {
    
    @IBOutlet weak var endKMLbl: UILabel!
    @IBOutlet weak var startKMLbl: UILabel!
    @IBOutlet weak var doneTextBtn: UIButton!
    @IBOutlet weak var allTextBtn: UIButton!
    @IBOutlet weak var foodLocationTextLbl: UILabel!
    @IBOutlet weak var cuisinesCollectionView: UICollectionView!
    @IBOutlet weak var `switch`: UISwitch!
    @IBOutlet weak var distanceSlider: UISlider!
    @IBOutlet weak var distanceLbl: UILabel!
    var addFilterVCDelegate: AddFilterVCDelegate!
    var selectIndex:Int!
    var sliderCurrentValue = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startKMLbl.text = "1 KM"
        endKMLbl.text = "40 KM"

        self.tabBarController?.tabBar.isHidden = true
    doneTextBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Done", value: "", table: nil))!, for: .normal)
    allTextBtn.setTitle((AppDelegate.getDelegate().filePath?.localizedString(forKey: "All", value: "", table: nil))!, for: .normal)
        foodLocationTextLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Food Location", value: "", table: nil))!
        cuisinesCollectionView.delegate = self
        cuisinesCollectionView.dataSource = self
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            cuisinesCollectionView.semanticContentAttribute = .forceLeftToRight
            distanceSlider.semanticContentAttribute = .forceLeftToRight
        }else{
            cuisinesCollectionView.semanticContentAttribute = .forceRightToLeft
            distanceSlider.semanticContentAttribute = .forceRightToLeft
        }
        
        cuisinesCollectionView.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "filterCVHeaderCell", value: "", table: nil))!, bundle: nil), forSupplementaryViewOfKind:UICollectionView.elementKindSectionHeader, withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "filterCVHeaderCell", value: "", table: nil))!)

        cuisinesCollectionView.dataSource = self as UICollectionViewDataSource
        sliderCurrentValue = AppDelegate.getDelegate().selectDistance
        
        let layout = cuisinesCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.sectionHeadersPinToVisibleBounds = true
        
        if AppDelegate.getDelegate().selectDistance > 0{
            distanceLbl.text = "\(AppDelegate.getDelegate().selectDistance) KM" as String
            distanceSlider.value = Float(AppDelegate.getDelegate().selectDistance)
        }

        let count = AppDelegate.getDelegate().selectFilters.count
        if count > 0{
            for i in 0..<cuisinesCollectionView.numberOfSections - 1{
                for j in 0..<cuisinesCollectionView.numberOfItems(inSection: i + 1) {
                    for k in 0..<count{
                        if AppDelegate.getDelegate().selectFilters[k] == SaveAddressClass.getFilters[i].Filters[j].FilterId{
                            SaveAddressClass.getFilters[i].Filters[j].Status = true
                            self.cuisinesCollectionView.reloadData()
                        }
                    }
                }
            }
        }else{
            for i in 0..<cuisinesCollectionView.numberOfSections - 1 {
                for j in 0..<cuisinesCollectionView.numberOfItems(inSection: i + 1) {
                    SaveAddressClass.getFilters[i].Filters[j].Status = false
                    self.cuisinesCollectionView.reloadData()
                }
            }
        }
        let vendorCount = AppDelegate.getDelegate().selectVendor.count
        if vendorCount > 0{
            for i in 0..<cuisinesCollectionView.numberOfItems(inSection: 0){
                for k in 0..<vendorCount{
                    if AppDelegate.getDelegate().selectVendor[k] == SaveAddressClass.getVendorTypes[i].Id!{
                        SaveAddressClass.getVendorTypes[i].Status = true
                        self.cuisinesCollectionView.reloadData()
                    }
                }
            }
        }else{
            for i in 0..<cuisinesCollectionView.numberOfItems(inSection: 0){
                SaveAddressClass.getVendorTypes[i].Status = false
                self.cuisinesCollectionView.reloadData()
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        dismiss(animated: false, completion: nil)
    }
    @IBAction func distanceSliderValueChanged(_ sender: UISlider) {
        let x = Int(round(sender.value))
        distanceLbl.text = "\(x) KM"
        distanceLbl.center = setUISliderThumbValueWithLabel(slider: sender)
        sliderCurrentValue = Int(distanceSlider.value)
        if distanceSlider.value > 1 && distanceSlider.value < distanceSlider.maximumValue{
            distanceLbl.isHidden = false
        }else{
            distanceLbl.isHidden = true
        }
    }
    func setUISliderThumbValueWithLabel(slider: UISlider) -> CGPoint {
        let slidertTrack : CGRect = slider.trackRect(forBounds: slider.bounds)
        let sliderFrm : CGRect = slider .thumbRect(forBounds: slider.bounds, trackRect: slidertTrack, value: slider.value)
        return CGPoint(x: sliderFrm.origin.x + slider.frame.origin.x + 18, y: slider.frame.origin.y + 43)
    }
    @IBAction func filterBtn_Tapped(_ sender: UIButton) {
        for i in 0..<cuisinesCollectionView.numberOfSections - 1 {
            for j in 0..<cuisinesCollectionView.numberOfItems(inSection: i + 1) {
                for k in 0..<AppDelegate.getDelegate().selectFilters.count{
                    if AppDelegate.getDelegate().selectFilters[k] != SaveAddressClass.getFilters[i].Filters[j].FilterId{
                        SaveAddressClass.getFilters[i].Filters[j].Status = false
                        self.cuisinesCollectionView.reloadData()
                    }
                }
            }
        }
        dismiss(animated: true, completion: nil)
    }
    @IBAction func allBtn_Tapped(_ sender: Any) {
        for i in 0..<cuisinesCollectionView.numberOfSections - 1 {
            for j in 0..<cuisinesCollectionView.numberOfItems(inSection: i + 1) {
                cuisinesCollectionView.selectItem(at: IndexPath(row: j, section: i), animated: false, scrollPosition: [])
                SaveAddressClass.getFilters[i].Filters[j].Status = true
                self.cuisinesCollectionView.reloadData()
            }
        }
    }
    @IBAction func doneBtn_Tapped(_ sender: UIButton) {
        AppDelegate.getDelegate().selectFilters.removeAll()
        for i in 0..<cuisinesCollectionView.numberOfSections - 1 {
            for j in 0..<cuisinesCollectionView.numberOfItems(inSection: i + 1) {
                if SaveAddressClass.getFilters[i].Filters[j].Status == true{
                    AppDelegate.getDelegate().selectFilters.append(SaveAddressClass.getFilters[i].Filters[j].FilterId!)
                }
            }
        }
        AppDelegate.getDelegate().selectVendor.removeAll()
        for i in 0..<cuisinesCollectionView.numberOfItems(inSection: 0){
            if SaveAddressClass.getVendorTypes[i].Status == true{
                AppDelegate.getDelegate().selectVendor.append(SaveAddressClass.getVendorTypes[i].Id!)
            }
        }
        if sliderCurrentValue == 0{
            sliderCurrentValue = 40
        }
        AppDelegate.getDelegate().selectDistance = sliderCurrentValue
        self.addFilterVCDelegate!.didTapAction(dic: AppDelegate.getDelegate().selectFilters, distance: sliderCurrentValue)
        //print(AppDelegate.getDelegate().selectFilters)
        //print(AppDelegate.getDelegate().selectDistance)
        dismiss(animated: true, completion: nil)
    }
}
extension FilterVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return SaveAddressClass.getFilters.count + 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return SaveAddressClass.getVendorTypes.count
        }else{
            return SaveAddressClass.getFilters[section-1].Filters.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cuisinesCollectionView.dequeueReusableCell(withReuseIdentifier: "filterCVCell", for: indexPath) as! filterCVCell
        cell.NameLbl.numberOfLines = 2
        if indexPath.section == 0{
            cell.cellView.layer.cornerRadius = 20
            cell.cellView.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
            cell.cellView.layer.borderWidth = 1
            if AppDelegate.getDelegate().appLanguage == "English"{
               cell.NameLbl.text = SaveAddressClass.getVendorTypes[indexPath.row].Name_En!
            }else{
                cell.NameLbl.text = SaveAddressClass.getVendorTypes[indexPath.row].Name_Ar!
            }
            if SaveAddressClass.getVendorTypes[indexPath.row].Status == true {
                cell.cellView.backgroundColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
                cell.NameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }else{
                cell.cellView.backgroundColor = .clear
                cell.NameLbl.textColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
            }
        }else{
            cuisinesCollectionView.allowsMultipleSelection = true
            cell.cellView.layer.cornerRadius = 20
            cell.cellView.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
            cell.cellView.layer.borderWidth = 1
            if AppDelegate.getDelegate().appLanguage == "English"{
               cell.NameLbl.text = SaveAddressClass.getFilters[indexPath.section - 1].Filters[indexPath.row].FilterName_En
            }else{
                cell.NameLbl.text = SaveAddressClass.getFilters[indexPath.section - 1].Filters[indexPath.row].FilterName_Ar
            }
            
            if SaveAddressClass.getFilters[indexPath.section - 1].Filters[indexPath.row].Status == true {
                cell.cellView.backgroundColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
                cell.NameLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }else{
                cell.cellView.backgroundColor = .clear
                cell.NameLbl.textColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
            }
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if SaveAddressClass.getVendorTypes[indexPath.row].Status == true{
                SaveAddressClass.getVendorTypes[indexPath.row].Status = false
            }else{
                for i in 0..<SaveAddressClass.getVendorTypes.count{
                    if SaveAddressClass.getVendorTypes[i].Status == true{
                        SaveAddressClass.getVendorTypes[i].Status = false
                    }
                }
                SaveAddressClass.getVendorTypes[indexPath.row].Status = SaveAddressClass.getVendorTypes[indexPath.row].Status == true ? false : true
            }
            self.cuisinesCollectionView.reloadData()
        }else{
            SaveAddressClass.getFilters[indexPath.section - 1].Filters[indexPath.row].Status = SaveAddressClass.getFilters[indexPath.section - 1].Filters[indexPath.row].Status == true ? false : true
            self.cuisinesCollectionView.reloadData()
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = self.cuisinesCollectionView.bounds
        let screenWidth = screenSize.width
        return CGSize(width: screenWidth/3, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reusableview = UICollectionReusableView()
        let firstheader: filterCVHeaderCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "filterCVHeaderCell", value: "", table: nil))!, for: indexPath) as! filterCVHeaderCell
        if indexPath.section == 0{
            if AppDelegate.getDelegate().appLanguage == "English"{
                firstheader.headerTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Vendor Types", value: "", table: nil))!
            }else{
                firstheader.headerTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Vendor Types", value: "", table: nil))!
            }
        }else{
            if AppDelegate.getDelegate().appLanguage == "English"{
                firstheader.headerTitleLbl.text = SaveAddressClass.getFilters[indexPath.section - 1].FilterTypeName_En
            }else{
                firstheader.headerTitleLbl.text = SaveAddressClass.getFilters[indexPath.section - 1].FilterTypeName_Ar
            }
        }
        
        
        reusableview = firstheader
        return reusableview
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            return CGSize(width:collectionView.frame.size.width, height:50)
    }
}
