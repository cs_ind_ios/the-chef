//
//  RateVC.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

protocol RateVCDelegate {
    func didTapRating(rating: Double)
}

class RateVC: UIViewController {
    
    var rateVCDelegate: RateVCDelegate!
    var rateDouble = Double()
    var OrderId = Int()
    var StoreName = String()
    var StoreImage = String()
    
    @IBOutlet weak var mainView: CustomView!
    @IBOutlet weak var ratingTitleLbl: UILabel!
    @IBOutlet weak var storeName: UILabel!
    //@IBOutlet weak var storeImge: UIImageView!
    @IBOutlet weak var floatRatingView: FloatRatingView!
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var commentsTV: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        
        // Rating View
        floatRatingView.delegate = self
        floatRatingView.contentMode = UIView.ContentMode.scaleAspectFit
        floatRatingView.type = .wholeRatings
        floatRatingView.rating = rateDouble
        
        if rateDouble == 1 || rateDouble == 1{
            ratingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
        }else if rateDouble == 3 || rateDouble == 4 {
            ratingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
        }else if rateDouble == 5 {
            ratingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
        }
        
        commentsTV.delegate = self
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            commentsTV.textAlignment = .left
            commentsTV.semanticContentAttribute = .forceLeftToRight
        }else{
            commentsTV.textAlignment = .right
            commentsTV.semanticContentAttribute = .forceRightToLeft
        }
        
        // Text View
        let borderGray = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        self.commentsTV.layer.borderColor = borderGray.cgColor
        self.commentsTV.layer.borderWidth = 1.0
        self.commentsTV.layer.cornerRadius = 5.0
        self.commentsTV.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Good Taste", value: "", table: nil))!
        self.commentsTV.textColor = UIColor.lightGray
        
        // selected store Icon
        
        
//        // selected store image
//        let imgStr:NSString = "\(url.storeImg.imgPath())\(StoreImage)" as NSString
//        let charSet = CharacterSet.urlFragmentAllowed
//        let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
//        let URLString = URL.init(string: urlStr as String)
//        storeImge.kf.indicatorType = .activity
//        storeImge.kf.setImage(with: URLString)
        storeName.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate Our food", value: "", table: nil))!) - \(StoreName.capitalized)"
        
    }
    override func viewWillLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.mainView.shadow = true
    }
    @IBAction func backBtn_tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func rateBtn_tapped(_ sender: Any) {
        var comment:String = self.commentsTV.text
        if comment == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Good Taste", value: "", table: nil))! {
            comment = ""
        }
        ANLoader.showLoading("Loading..", disableUI: true)
        
        let jsonDic:[String : Any] = ["UserId":"\(UserDef.getUserId())", "OrderId":"\(String(describing: OrderId))", "CommandType":"3", "Rating":Int(self.floatRatingView.rating), "RatingCommand":comment]
        
        OrderApiRouter.favouriteOrder(dic: jsonDic, success: { (data) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            if data?.status == false{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                }
                return
            }else{
                //Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                self.dismiss(animated: true, completion: nil)
                
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showToastAlert(on: self, message:data!.message!)
                }else{
                    Alert.showToastAlert(on: self, message:data!.messageAr!)
                }
            }
        }){ (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension RateVC: FloatRatingViewDelegate {
    // MARK: FloatRatingViewDelegate
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        rateDouble = floatRatingView.rating
        if rateDouble == 1 || rateDouble == 1{
            ratingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
        }else if rateDouble == 3 || rateDouble == 4 {
            ratingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
        }else if rateDouble == 5 {
            ratingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
        }
    }
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        rateDouble = floatRatingView.rating
        //rateVCDelegate.didTapRating(rating: rateDouble)
        if rateDouble == 1 || rateDouble == 1{
            ratingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
        }else if rateDouble == 3 || rateDouble == 4 {
            ratingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
        }else if rateDouble == 5 {
            ratingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
        }
    }
}
extension RateVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Good Taste", value: "", table: nil))!
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let maxLength = 250
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
}
