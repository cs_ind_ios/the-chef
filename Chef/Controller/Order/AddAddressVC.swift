//
//  AddAddressVC.swift
//  Chef
//
//  Created by RAVI on 15/05/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class AddAddressVC: UIViewController {

    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var phoneNumTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var addAddressBtn: UIButton!
    
    var addressId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addAddressBtn_Tapped(_ sender: Any) {
        ANLoader.hide()
        self.view.endEditing(true)
        if (self.validInputParams()==false){
            return
        }
        let userId = "\(String(describing: UserDefaults.standard.value(forKey: "UserId")!))"
        //let addId = "\(String(describing: addressId!))"
        let addId = "\(String(describing: 0))"
        let dic = ["UserId":userId, "AddressId":addId, "HouseNo":"", "HouseName":self.nameTF.text!, "LandMark":self.cityTF.text!, "AddressType":"", "Address":self.addressTF.text!, "Latitude": "", "Longitude": "", "IsActive":"true"]
        //print(dic)
        ANLoader.showLoading("Loading...", disableUI: true)
        if(Connectivity.isConnectedToInternet()){
            AccountApiRouter.saveAddress(dic: dic) { (data) in
                if data!.status == false{
                   // print(data!.message!)
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                    }else{
                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                    }
                    DispatchQueue.main.async {
                        ANLoader.hide()
                    }
                }else{
                    DispatchQueue.main.async {
                        ANLoader.hide()
                    }
                    NotificationCenter.default.post(name: .didReceiveData, object: nil)
                    self.navigationController?.popViewController(animated: false)
                    //self.locViewHeightConst.constant = 195
                }
            }
        }else{
            DispatchQueue.main.async {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: Title, message: internetFail)
        }
    }
    func validInputParams() -> Bool {
        if self.nameTF.text == nil || (self.nameTF.text == ""){
            Alert.showAlert(on: self, title: "Name", message: Name)
            return false
        }
        if self.phoneNumTF.text == nil || (self.phoneNumTF.text == ""){
            Alert.showAlert(on: self, title: "Mobile Number", message: Mobile)
            return false
        }
        if self.addressTF.text == nil || (self.addressTF.text == ""){
            Alert.showAlert(on: self, title: "Address", message: FullAddress)
            return false
        }
        if self.cityTF.text == nil || (self.cityTF.text == ""){
            Alert.showAlert(on: self, title: "City", message: City)
            return false
        }
        return true
    }
}
