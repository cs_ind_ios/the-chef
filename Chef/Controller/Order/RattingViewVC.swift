//
//  RattingViewVC.swift
//  Chef
//
//  Created by Devbox on 24/01/20.
//  Copyright © 2020 Creative Solutions. All rights reserved.
//

import UIKit

protocol RattingViewVCVCDelegate {
    func didTapRating(vendorRating: Double,vendorComment:String,driverRating: Double,driverComment:String)
}

class RattingViewVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var storeName: UILabel!
    
    @IBOutlet weak var vendorRatingTitleLbl: UILabel!
    @IBOutlet weak var vendorRatingView: FloatRatingView!
    @IBOutlet weak var vendorcommentsTV: UITextView!
    
    @IBOutlet weak var driverRatingTitleLbl: UILabel!
    @IBOutlet weak var driverRatingView: FloatRatingView!
    @IBOutlet weak var drivercommentsTV: UITextView!
    @IBOutlet weak var driverMainViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var noThanksBtn: UIButton!
    
    var rattingViewVCVCDelegate: RattingViewVCVCDelegate!
    var vendorRate = Double()
    var driverRate = Double()
    var OrderId = Int()
    var StoreName = String()
    var StoreImage = String()
    var orderType = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
                
        if orderType == 2{
            driverMainViewHeight.constant = 0
        }else{
            driverMainViewHeight.constant = 293
        }
        
        // Rating View
        vendorRatingView.delegate = self
        vendorRatingView.contentMode = UIView.ContentMode.scaleAspectFit
        vendorRatingView.type = .wholeRatings
        vendorRatingView.rating = vendorRate
        
        driverRatingView.delegate = self
        driverRatingView.contentMode = UIView.ContentMode.scaleAspectFit
        driverRatingView.type = .wholeRatings
        driverRatingView.rating = driverRate
        
        self.vendorcommentsTV.delegate = self
        self.drivercommentsTV.delegate = self
        
        if AppDelegate.getDelegate().appLanguage == "English"{
            vendorcommentsTV.textAlignment = .left
            vendorcommentsTV.semanticContentAttribute = .forceLeftToRight
            drivercommentsTV.textAlignment = .left
            drivercommentsTV.semanticContentAttribute = .forceLeftToRight
        }else{
            vendorcommentsTV.textAlignment = .right
            vendorcommentsTV.semanticContentAttribute = .forceRightToLeft
            drivercommentsTV.textAlignment = .right
            drivercommentsTV.semanticContentAttribute = .forceRightToLeft
        }
        
        // Text View
        let borderGray = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        self.vendorcommentsTV.layer.borderColor = borderGray.cgColor
        self.vendorcommentsTV.layer.borderWidth = 1.0
        self.vendorcommentsTV.layer.cornerRadius = 5.0
        self.vendorcommentsTV.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Good Taste", value: "", table: nil))!
        self.vendorcommentsTV.textColor = UIColor.lightGray

        self.drivercommentsTV.layer.borderColor = borderGray.cgColor
        self.drivercommentsTV.layer.borderWidth = 1.0
        self.drivercommentsTV.layer.cornerRadius = 5.0
        self.drivercommentsTV.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Good Taste", value: "", table: nil))!
        self.drivercommentsTV.textColor = UIColor.lightGray
        
        storeName.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Rate Our food", value: "", table: nil))!) - \(StoreName.capitalized)"
        
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func noThanksBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func submitBtn_Tapped(_ sender: Any) {
        var vendorComment:String = self.vendorcommentsTV.text
        if vendorComment == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Good Taste", value: "", table: nil))! {
            vendorComment = ""
        }
        var driverComment:String = self.drivercommentsTV.text
        if driverComment == (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Good Taste", value: "", table: nil))! {
            driverComment = ""
        }
        var commandType = "3"
        if orderType == 2{
            commandType = "3"
        }else{
            commandType = "4"
        }
        ANLoader.showLoading("Loading..", disableUI: true)
        let jsonDic:[String : Any] = ["UserId":"\(UserDef.getUserId())", "OrderId":"\(String(describing: OrderId))",
            "CommandType":"\(commandType)",
            "Rating":Int(self.vendorRatingView.rating),
            "DriverRating":Int(self.driverRatingView.rating),
            "RatingCommand":"\(vendorComment)",
            "DriverRatingCommand":"\(driverComment)"]
        
        OrderApiRouter.favouriteOrder(dic: jsonDic, success: { (data) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            if data?.status == false{
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.message!)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data!.messageAr!)
                }
                return
            }else{
                self.dismiss(animated: true, completion: nil)
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showToastAlert(on: self, message:data!.message!)
                }else{
                    Alert.showToastAlert(on: self, message:data!.messageAr!)
                }
            }
        }){ (error) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
        }
    }
}
extension RattingViewVC: FloatRatingViewDelegate {
    // MARK: FloatRatingViewDelegate
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        if ratingView == vendorRatingView{
            vendorRate = vendorRatingView.rating
            if vendorRate == 1 || vendorRate == 2{
                vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
            }else if vendorRate == 3 || vendorRate == 4 {
                vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
            }else if vendorRate == 5 {
                vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
            }
        }else{
            driverRate = driverRatingView.rating
            if driverRate == 1 || driverRate == 2{
                driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
            }else if driverRate == 3 || driverRate == 4 {
                driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
            }else if driverRate == 5 {
                driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
            }
        }
    }
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        if ratingView == vendorRatingView{
            vendorRate = vendorRatingView.rating
            if vendorRate == 1 || vendorRate == 2{
                vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
            }else if vendorRate == 3 || vendorRate == 4 {
                vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
            }else if vendorRate == 5 {
                vendorRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
            }
        }else{
            driverRate = driverRatingView.rating
            if driverRate == 1 || driverRate == 2{
                driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What disappointed you", value: "", table: nil))!
            }else if driverRate == 3 || driverRate == 4 {
                driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What can we improve", value: "", table: nil))!
            }else if driverRate == 5 {
                driverRatingTitleLbl.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "What did you like the best", value: "", table: nil))!
            }
        }
    }
}
extension RattingViewVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Good Taste", value: "", table: nil))!
            textView.textColor = UIColor.lightGray
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let maxLength = 250
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
}
