//
//  LiveTrackingVC.swift
//  Cheff Vendor
//
//  Created by Devbox on 27/01/20.
//  Copyright © 2020 Creative Solutions. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class LiveTrackingVC: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var expectedTimeLbl: UILabel!
    @IBOutlet weak var driverReachTimeLbl: UILabel!
    @IBOutlet weak var callNowBtn: UIButton!
    
    var orderID = Int()
    var expectTime = String()
    var detailsArray = [TrackDriverModel]()
    
    var locationManager = CLLocationManager()
    var latitute = Double()
    var longitute = Double()
    var SwiftTimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(getOrderTrackingDetrails), name: NSNotification.Name(rawValue: "getLiveTrackingDetails"), object: nil)

        mapView.delegate = self
    }
    @objc func getOrderTrackingDetrails(){
           NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "getLiveTrackingDetails"), object: nil)
           DispatchQueue.main.async {
              self.getServiceList()
           }
       }
    override func viewWillAppear(_ animated: Bool) {
        self.getServiceList()
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "getLiveTrackingDetails"), object: nil)
        SwiftTimer.invalidate()
    }
    @objc func applicationDidEnterBackground(application:UIApplication)  {
        print("Background")
        SwiftTimer.invalidate()
    }
    @objc func applicationEnteredForeground(application:UIApplication)  {
        print("Foreground")
        startTimer()
    }
    func startTimer() {
        self.SwiftTimer.invalidate()
        SwiftTimer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(autoRefreshMethods), userInfo: nil, repeats: false);
    }
    @objc func autoRefreshMethods(){
         self.getServiceList()
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: Call Now Button
    @IBAction func callNowBtn_Tapped(_ sender: Any) {
        if detailsArray.count > 0{
            if let url = URL(string: "tel://\(detailsArray[0].MobileNo)") {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }else{
            //Alert.showToastAlert(on: self, message: "Something wrong")
        }
    }
    //MARK: Get Service
    func getServiceList() {
        let dic:[String:Any] = ["OrderId":"\(orderID)"]
        OrderApiRouter.TrackDriverService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.Message)
                }else{
                    Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.MessageAr)
                }
                self.detailsArray.removeAll()
                self.AllData()
            }else{
                self.detailsArray.removeAll()
                self.detailsArray = [data.Data]
                self.AllData()
                self.locationDetails()
            }
            //self.SwiftTimer.invalidate()
            self.startTimer()
        }){ (error) in
            Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:error)
            self.detailsArray.removeAll()
        }
    }
    //MARK: All Data
    func AllData(){
        nameLbl.text = detailsArray[0].FullName
        expectedTimeLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Expected Time", value: "", table: nil))!): \(expectTime)"
        
        // Image
        img.kf.indicatorType = .activity
        let ImgStr:NSString = "\(url.driverImg.imgPath())\(detailsArray[0].ProfileImage)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = ImgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let url = URL.init(string: urlStr as String)
        img.kf.setImage(with: url)
    }
    //MARK: Location Details
    func locationDetails(){
        self.mapView.clear()
        let userLocation = CLLocation(latitude: Double(detailsArray[0].UserLatitude)!, longitude: Double(detailsArray[0].UserLongitude)!)
        let driverLocation = CLLocation(latitude: Double(detailsArray[0].Latitude)!, longitude: Double(detailsArray[0].Longitude)!)
        createMarker(titleMarker: "Store", iconMarker: #imageLiteral(resourceName: "store-42"), latitude: Double(detailsArray[0].StoreLatitude)!, longitude: Double(detailsArray[0].StoreLongitude)!)
        createMarker(titleMarker: "Home", iconMarker: #imageLiteral(resourceName: "User-42"), latitude: Double(detailsArray[0].UserLatitude)!, longitude: Double(detailsArray[0].UserLongitude)!)
        createDriverMarker(titleMarker: "Driver", iconMarker: #imageLiteral(resourceName: "direction_marker"), latitude: Double(detailsArray[0].Latitude)!, longitude: Double(detailsArray[0].Longitude)!)
        latitute = Double(detailsArray[0].Latitude)!
        longitute = Double(detailsArray[0].Longitude)!
        drawPath(startLocation: driverLocation, EndLocation: userLocation, color: #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1))
        getEstimationTime(startLocation: driverLocation, EndLocation: userLocation)
        initGoogleMaps()
    }
    //MARK: Map Details
    func initGoogleMaps() {
        let camera = GMSCameraPosition.camera(withLatitude:latitute, longitude: longitute, zoom: 17.0)
        let mappView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mappView.isMyLocationEnabled = true
        //mapView.settings.myLocationButton = true
        self.mapView.camera = camera
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
    }
    func createDriverMarker(titleMarker:String, iconMarker:UIImage , latitude: CLLocationDegrees, longitude:CLLocationDegrees){
      CATransaction.begin()
      CATransaction.setAnimationDuration(0.5)
      let marker = GMSMarker()
          marker.position = CLLocationCoordinate2DMake(latitude,longitude)
          marker.title = titleMarker
          marker.icon = iconMarker
          marker.map = mapView
          let rotation:Double = Double(detailsArray[0].Rotation)!
          marker.rotation = rotation
      CATransaction.commit()
    }
    func createMarker(titleMarker:String, iconMarker:UIImage , latitude: CLLocationDegrees, longitude:CLLocationDegrees){
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude,longitude)
        marker.title = titleMarker
        marker.icon = iconMarker
        marker.map = mapView
    }
    func drawPath(startLocation:CLLocation, EndLocation:CLLocation, color:UIColor){
        let origin = "\(startLocation.coordinate.latitude),\(startLocation.coordinate.longitude)"
        let destination = "\(EndLocation.coordinate.latitude),\(EndLocation.coordinate.longitude)"
        OrderApiRouter.LocationGetService(url: "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyAFtfKh3zbXIHcZgG1h0nWI5drMMBOtFN8", success: { (Data) in
           // print(Data)
            let Totaldata = Data as! NSDictionary
            let data = Totaldata["routes"] as! [NSDictionary]
            for route in data{
                let routeOverViewPolyline = route["overview_polyline"] as! NSDictionary
                let points = routeOverViewPolyline["points"]
                let path = GMSPath.init(fromEncodedPath: points! as! String)
                let polyLine = GMSPolyline.init(path: path)
                polyLine.strokeWidth = 4
                polyLine.strokeColor = color
                polyLine.map = self.mapView
            }
        }) { (error) in
            print(error)
        }
    }
    func getEstimationTime(startLocation:CLLocation, EndLocation:CLLocation){
        let origin = "\(startLocation.coordinate.latitude),\(startLocation.coordinate.longitude)"
        let destination = "\(EndLocation.coordinate.latitude),\(EndLocation.coordinate.longitude)"
        OrderApiRouter.LocationGetService(url: "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(origin)&destinations=\(destination)&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&key=AIzaSyAFtfKh3zbXIHcZgG1h0nWI5drMMBOtFN8", success: { (Data) in
            let Totaldata = Data as! NSDictionary
            let data = Totaldata["rows"] as! [NSDictionary]
            for route in data{
                let elements = route["elements"] as! [NSDictionary]
                for durationIn in elements{
                    let duration = durationIn["duration_in_traffic"] as! NSDictionary
                    let text = duration["text"] as! String
                    self.driverReachTimeLbl.text = "\((AppDelegate.getDelegate().filePath?.localizedString(forKey: "Enjoy your food in", value: "", table: nil))!): \(text)"
                }
            }
        }) { (error) in
            print(error)
        }
    }
    // MARK: GMSMapview Delegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        mapView.isMyLocationEnabled = true
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        mapView.isMyLocationEnabled = true
        if (gesture){
            mapView.selectedMarker = nil
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.isMyLocationEnabled = true
        return false
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("coordinate \(coordinate)")
    }
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapView.isMyLocationEnabled = true
        mapView.selectedMarker = nil
        return false
    }
}
