//
//  LocationAutocompleteVC.swift
//  Chef
//
//  Created by Creative Solutions on 12/18/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationAutocompleteVCDelegate {
    func didTapAction(latitude:Double,longitude:Double,MainAddress:String, TotalAddress:String)
}

class LocationAutocompleteVC: UIViewController {
    
    var locationAutocompleteVCDelegate:LocationAutocompleteVCDelegate!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var isSearch : Bool = false
    var isCancel:Bool = false
    var countryCode = "SA"
    
    var currentLat: Double!
    var currentLong: Double!
    var currentLocationAddress: String!
    var locationSearch: [LocationSearchResult] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
    }
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func searchPlaces(sender: NSObject) {
        tableView.isHidden = true
        //print(searchBar.text!)
        var language = "en"
        if AppDelegate.getDelegate().appLanguage == "English"{
            language = "en"
        }else{
            language = "ar"
        }
        UserModuleService.LocationGetService(url: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchBar.text!)&components=country:\(countryCode)&language=\(language)&key=AIzaSyAFtfKh3zbXIHcZgG1h0nWI5drMMBOtFN8", success: { (Data) in
            //print(Data)
            self.locationSearch.removeAll()
            let Totaldata = Data as! NSDictionary
            let data = Totaldata["predictions"] as! NSArray
            for i in 0..<(data ).count {
                let dic = ((data)[i] as! NSDictionary)["structured_formatting"] as! NSDictionary
                self.locationSearch.append(LocationSearchResult(mainText: dic["main_text"] as! String, secondaryText: dic["secondary_text"] == nil ? "" : dic["secondary_text"] as! String, id: ((data )[i] as! NSDictionary)["place_id"] as! String))

            }
            if self.searchBar.text! != "" {
                self.tableView.reloadData()
                self.tableView.isHidden = false
            }
        }) { (error) in
            print(error)
        }
    }
}
//MARK: TableView Delegate Methods
extension LocationAutocompleteVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationSearch.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "LocationAddressTVCell", bundle: nil), forCellReuseIdentifier: "LocationAddressTVCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationAddressTVCell", for: indexPath) as! LocationAddressTVCell
        let selectedItem = locationSearch[indexPath.row]
        cell.titleLbl.text = selectedItem.mainText
        cell.addressLbl.text = selectedItem.secondaryText
        self.currentLocationAddress = selectedItem.secondaryText
        tableView.layoutIfNeeded()
        cell.selectionStyle = .none
        tableViewHeightConstraint.constant = tableView.contentSize.height
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = locationSearch[indexPath.row]
        UserModuleService.LocationGetService(url: "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(selectedItem.id!)&key=AIzaSyAFtfKh3zbXIHcZgG1h0nWI5drMMBOtFN8", success: { (Data) in
            let Totaldata = Data as! NSDictionary
            let data = Totaldata["result"] as! NSDictionary
            let coordinates = ((data )["geometry"] as! NSDictionary)["location"] as! NSDictionary
            self.currentLat = (coordinates["lat"] as! Double)
            self.currentLong = (coordinates["lng"] as! Double)
            self.locationAutocompleteVCDelegate.didTapAction(latitude: self.currentLat!, longitude: self.currentLong!, MainAddress: "\(String(describing: selectedItem.mainText!))", TotalAddress: "\(String(describing: selectedItem.mainText!)) \(String(describing: selectedItem.secondaryText!))")
        }){ (error) in
            print(error)
        }
        locationSearch.removeAll()
        dismiss(animated: true, completion: nil)
    }
}
//MARK: UISearchbar delegate
extension LocationAutocompleteVC: UISearchBarDelegate{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if isCancel == true{
            isSearch = false
            tableView.isHidden = false
        }else{
            isSearch = true;
        }
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchBar.text == ""{
            isSearch = false;
        }else{
            isSearch = true;
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false;
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = true;
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text!.count > 0 {
            isSearch = true
            searchItems(searchText: searchText)
        }else{
            isCancel = true
            isSearch = false
            searchItems(searchText: "")
        }
        self.tableView.reloadData()
    }
    func searchItems(searchText:String) {
        if searchText != "" {
            tableView.isHidden = false
            searchPlaces(sender: searchBar)
            self.tableView.reloadData()
        }else{
            tableView.isHidden = true
        }
    }
}
