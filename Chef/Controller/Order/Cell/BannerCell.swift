//
//  BannerCell.swift
//  Chef
//
//  Created by Creative Solutions on 12/2/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class BannerCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource{
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    var SwiftTimer = Timer()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if AppDelegate.getDelegate().appLanguage == "English"{
            bannerCollectionView.semanticContentAttribute = .forceLeftToRight
        }else{
            bannerCollectionView.semanticContentAttribute = .forceRightToLeft
        }
        self.bannerCollectionView.delegate = self
        self.bannerCollectionView.dataSource = self
        let nib = UINib(nibName: "BannerCollectionViewCell", bundle: nil)
        self.bannerCollectionView.register(nib, forCellWithReuseIdentifier: "BannerCollectionViewCell")
        SwiftTimer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true);
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    @objc func scrollToNextCell(){
        if SaveAddressClass.getStoreBannerArray.count > 0 {
            //get cell size
            let cellSize = CGSize(width: bannerCollectionView.frame.width, height: bannerCollectionView.frame.height)
    
            //get current content Offset of the Collection view
            let contentOffset = bannerCollectionView.contentOffset;
            if bannerCollectionView.contentSize.width <= bannerCollectionView.contentOffset.x + cellSize.width
            {
                bannerCollectionView.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y  , width: cellSize.width, height: cellSize.height), animated: true)
            } else {
                bannerCollectionView.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width , y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SaveAddressClass.getStoreBannerArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
        let imgStr:NSString = "\(url.storeImg.imgPath())\(SaveAddressClass.getStoreBannerArray[indexPath.row].BannerImage!)" as NSString
               let charSet = CharacterSet.urlFragmentAllowed
               let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
               let URLString = URL.init(string: urlStr as String)
               cell.bannerImg.kf.indicatorType = .activity
               cell.bannerImg.kf.setImage(with: URLString!)
        return cell
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>){
      if SaveAddressClass.getStoreBannerArray.count > 0 {
      if scrollView == bannerCollectionView{
          targetContentOffset.pointee = scrollView.contentOffset
          var indexes = self.bannerCollectionView.indexPathsForVisibleItems
          indexes.sort()
          var index = indexes.first!
          if scrollView.panGestureRecognizer.translation(in: scrollView.superview).x > 0 {
              //print("left")
              index.row = index.row - 1
          } else {
              //print("right")
              index.row = index.row + 1
          }
          index.row = index.row > 0 ? index.row:0
          if SaveAddressClass.getStoreBannerArray.count > index.row {
              self.bannerCollectionView.scrollToItem(at: index, at: .right, animated: true )
          }else{
              DispatchQueue.main.async {
                  let indexPath = IndexPath(item: 0, section: 0)
                  self.bannerCollectionView.scrollToItem(at: indexPath, at: [], animated: false )
              }
          }
      }
    }
  }
}
