//
//  LocationAddressTVCell.swift
//  Chef
//
//  Created by Devbox on 18/12/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class LocationAddressTVCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
