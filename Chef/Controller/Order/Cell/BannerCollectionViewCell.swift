//
//  BannerCollectionViewCell.swift
//  Chef
//
//  Created by Creative Solutions on 12/2/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bannerImg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
