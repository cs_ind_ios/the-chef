//
//  SearchCell.swift
//  Chef
//
//  Created by Creative Solutions on 12/2/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBtn: CustomButton!
    @IBOutlet weak var searchBtnWidthConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
