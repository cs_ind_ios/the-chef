//
//  ManageCardsVC.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class ManageCardsVC: UIViewController {
    var checkoutProvider: OPPCheckoutProvider?
    var transaction: OPPTransaction?
    
    // MARK: - Outlets
    @IBOutlet weak var manageCardTV: UITableView!
    var saveCardArray = [GetSaveCardDetailsModel]()
  
    // Add Card Details Status
    var token:String?
    var paymentBrand:String?
    var last4Digits:String?
    var holder:String?
    var expiryMonth:String?
    var expiryYear:String?

    // MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.manageCardTV.register(UINib(nibName: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ManageCardTVCell", value: "", table: nil))!, bundle: nil), forCellReuseIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ManageCardTVCell", value: "", table: nil))!)
        self.manageCardTV.delegate = self
        self.manageCardTV.dataSource = self
        self.manageCardTV.separatorStyle = .none // Remove Lines between cells
        self.tabBarController?.tabBar.isHidden = true
        //DispatchQueue.main.async {
            self.getAllSaveCardDetails()
        //}
    }
    override func viewDidDisappear(_ animated: Bool) {
        ANLoader.hide()
    }
    func getAllSaveCardDetails()  {
        let dic:[String:Any] = [
            "userId":"\(UserDef.getUserId())"
        ]
        
        PaymentModuleServices.getAllSaveCardDetailsService(dic: dic, success: { (data) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
            }
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showToastAlert(on: self, message:data.Message)
                }else{
                    Alert.showToastAlert(on: self, message:data.MessageAr)
                }
                self.saveCardArray.removeAll()
                self.manageCardTV.reloadData()
            }else{
                self.saveCardArray = data.Data!
                self.manageCardTV.reloadData()
                DispatchQueue.main.async {
                    ANLoader.hide()
                }
            }
            
        }){ (error) in
            DispatchQueue.main.async {
                ANLoader.hide()
            }
            self.saveCardArray.removeAll()
            self.manageCardTV.reloadData()
            Alert.showToastAlert(on: self, message:error)

        }
    }
    
    
    //MARK:- Button Actions
    @IBAction func backBtn_Tapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func TimeStamp() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyyMMMddHHmmssSSS"
        dateFormatter.locale = Locale(identifier: "EN")
        //Specify your format that you want
        return dateFormatter.string(from: date)
    }
    @IBAction func cardDeleteBtn_Tapped(_ sender: UIButton) {
        let alert = UIAlertController(title: title, message: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Are you sure delete card?", value: "", table: nil))!, preferredStyle: .alert)
        let action1 = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "YES", value: "", table: nil))!, style: .default) { (action:UIAlertAction) in
            
            let dic:[String:Any] = [
                "userId":"\(UserDef.getUserId())",
                "registrationId": "\(self.saveCardArray[sender.tag].Token)"
            ]
            PaymentModuleServices.getDeleteCardService(dic: dic, success: { (data) in
                if(data.Status == false){
                    if AppDelegate.getDelegate().appLanguage == "English"{
                        Alert.showToastAlert(on: self, message:data.Message)
                    }else{
                        Alert.showToastAlert(on: self, message:data.MessageAr)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.getAllSaveCardDetails()
                    }
                }
                
            }){ (error) in
                Alert.showToastAlert(on: self, message:error)
            }
        }
        
        let action2 = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "NO", value: "", table: nil))!, style: .default) { (action:UIAlertAction) in
        }
        alert.addAction(action2)
        alert.addAction(action1)
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func addNewCardBtn_Tapped(_ sender: UIButton) {
        ANLoader.showLoading("Loading..", disableUI: true)
        let userDetails = UserDef.getUserProfileDic(key: "userDetails")
        
        let merchantTransactionId = "\(UserDef.getUserId())iOS\(self.TimeStamp())"
        let dic:[String:Any] = [
            "amount": "10", 
            "shopperResultUrl": Config.urlScheme,
            "isCardRegistration": "true",
            "merchantTransactionId": merchantTransactionId,
            "customerEmail":"\((userDetails["Email"] as? String)!)",
            "userId":"\(UserDef.getUserId())",
            "notificationUrl":Config.asyncPaymentCompletedNotificationKey
        ]
        
        PaymentModuleServices.getPaymentCheckOutService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showToastAlert(on: self, message:data.Message)
                }else{
                    Alert.showToastAlert(on: self, message:data.MessageAr)
                }
            }else{
                DispatchQueue.main.async {
                    let checkoutID = data.Data![0].id
                    
                    //                        guard let checkoutID:String = data.Data![0].id else {
                    //                            Utils.showResult(presenter: self, success: false, message: "Checkout ID is empty")
                    //                            return
                    //                        }
                    
                    self.checkoutProvider = self.configureCheckoutProvider(checkoutID: checkoutID)
                    self.checkoutProvider?.delegate = self
                    self.checkoutProvider?.presentCheckout(forSubmittingTransactionCompletionHandler: { (transaction, error) in
                        DispatchQueue.main.async {
                            self.handleTransactionSubmission(transaction: transaction, error: error)
                        }
                    }, cancelHandler: nil)
                }
                
            }
            
        }){ (error) in
            Alert.showToastAlert(on: self, message:error)
        }
    }
}
extension ManageCardsVC: OPPCheckoutProviderDelegate{
    // MARK: - OPPCheckoutProviderDelegate methods
    
    // This method is called right before submitting a transaction to the Server.
    func checkoutProvider(_ checkoutProvider: OPPCheckoutProvider, continueSubmitting transaction: OPPTransaction, completion: @escaping (String?, Bool) -> Void) {
        // To continue submitting you should call completion block which expects 2 parameters:
        // checkoutID - you can create new checkoutID here or pass current one
        // abort - you can abort transaction here by passing 'true'
        completion(transaction.paymentParams.checkoutID, false)
    }
    
    // MARK: - Payment helpers
    
    func handleTransactionSubmission(transaction: OPPTransaction?, error: Error?) {
        guard let transaction = transaction else {
            Utils.showResult(presenter: self, success: false, message: error?.localizedDescription)
            return
        }
        
        self.transaction = transaction
        if transaction.type == .synchronous {
            // If a transaction is synchronous, just request the payment status
            
            self.requestPaymentStatus()
        } else if transaction.type == .asynchronous {
            // If a transaction is asynchronous, SDK opens transaction.redirectUrl in a browser
            // Subscribe to notifications to request the payment status when a shopper comes back to the app
            NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveAsynchronousPaymentCallback), name: Notification.Name(rawValue: Config.asyncPaymentCompletedNotificationKey), object: nil)
            
        } else {
            Utils.showResult(presenter: self, success: false, message: "Invalid transaction")
        }
    }
    
    func configureCheckoutProvider(checkoutID: String) -> OPPCheckoutProvider? {
        let provider = OPPPaymentProvider.init(mode: .live)
        let checkoutSettings = Utils.configureCheckoutSettings()
        //checkoutSettings.storePaymentDetails = .prompt
        return OPPCheckoutProvider.init(paymentProvider: provider, checkoutID: checkoutID, settings: checkoutSettings)
    }
    
    func requestPaymentStatus() {
        guard let resourcePath = self.transaction?.resourcePath else {
            Utils.showResult(presenter: self, success: false, message: "Resource path is invalid")
            return
        }
        
        self.transaction = nil
        
        let dic:[String:Any] = [
            "resourcePath": resourcePath, "userId":"\(UserDef.getUserId())"
        ]
        
        PaymentModuleServices.getAddCardStatusService(dic: dic, success: { (data) in
            if(data.Status == false){
                if AppDelegate.getDelegate().appLanguage == "English"{
                    Alert.showToastAlert(on: self, message:data.Message)
                }else{
                    Alert.showToastAlert(on: self, message:data.MessageAr)
                }
                
            }else{
                DispatchQueue.main.async {
                    self.getAllSaveCardDetails()
                }

                /*for dic in data.Data! {
                    for dic1 in dic.JArray {
                        if dic1.Key == "id" {
                            self.token = dic1.Value
                        }else if dic1.Key == "paymentBrand" {
                            self.paymentBrand = dic1.Value
                        }else if dic1.Key == "last4Digits" {
                            self.last4Digits = dic1.Value
                        }else if dic1.Key == "holder" {
                            self.holder = dic1.Value
                        }else if dic1.Key == "expiryMonth" {
                            self.expiryMonth = dic1.Value
                        }else if dic1.Key == "expiryYear" {
                            self.expiryYear = dic1.Value
                        }
                        
                    }
                }
                
                
                
                        DispatchQueue.main.async {
                            let dic:[String:Any] = [
                                "last4Digits": self.last4Digits!,
                                "cardHolder": self.holder!,
                                "expireYear": self.expiryYear!,
                                "expireMonth": self.expiryMonth!,
                                "cardBrand": self.paymentBrand!,
                                "token": self.token!,
                                "userId":"\(UserDef.getUserId())"
                            ]

                            PaymentModuleServices.insertSaveCardService(dic: dic, success: { (data) in
                                if(data.StatusCode == "0"){
                                    if AppDelegate.getDelegate().appLanguage == "English"{
                                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.StatusMessage)
                                    }else{
                                        Alert.showAlert(on: self, title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Title", value: "", table: nil))!, message:data.StatusMessageAr)
                                    }
                                    
                                }else{
                                    self.getAllSaveCardDetails()

                                    if AppDelegate.getDelegate().appLanguage == "English"{
                                        Alert.showToastAlert(on: self, message:data.StatusMessage)
                                    }else{
                                        Alert.showToastAlert(on: self, message:data.StatusMessageAr)
                                    }
                                }

                            }){ (error) in
                                Alert.showToastAlert(on: self, message:error)
                            }
                        }*/

            }
            
            
            
        }){ (error) in
            Alert.showToastAlert(on: self, message:error)
        }
     
    }
    
    // MARK: - Async payment callback
    
    @objc func didReceiveAsynchronousPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: Config.asyncPaymentCompletedNotificationKey), object: nil)
        self.checkoutProvider?.dismissCheckout(animated: true) {
            DispatchQueue.main.async {
                self.requestPaymentStatus()
            }
        }
    }
}

extension ManageCardsVC : UITableViewDelegate,UITableViewDataSource{
    //MARK:- TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return saveCardArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "ManageCardTVCell", value: "", table: nil))!, for: indexPath) as! ManageCardTVCell
        let dic = saveCardArray[indexPath.row]
        cell.bankNameLbl.text = dic.CardHolder
        cell.cardNumberLbl.text = "**** **** **** \(dic.Last4Digits)"
        cell.expiryLbl.text = "\(dic.ExpireMonth)/\(dic.ExpireYear)"
        if dic.CardBrand == "VISA" {
            cell.card_ImgView.image = UIImage(named: "visa")
            
        }else{
            cell.card_ImgView.image = UIImage(named: "")
            
        }
        cell.backGroundView.layer.cornerRadius = 10
        cell.backGroundView.layer.masksToBounds = true
        
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(cardDeleteBtn_Tapped(_:)), for:.touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let maskPathAll = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: [.topLeft,.topRight,.bottomLeft,.bottomRight], cornerRadii: CGSize(width: 15.0, height: 15.0))
        let shapeLayerAll = CAShapeLayer()
        shapeLayerAll.frame = cell.bounds
        shapeLayerAll.path = maskPathAll.cgPath
    }
}
