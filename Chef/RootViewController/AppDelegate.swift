//
//  AppDelegate.swift
//  Chef
//
//  Created by Creative Solutions on 4/16/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import SQLite3
import UserNotifications // Push Notification
import Firebase
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var typeOfServirceInt:Int = 0
    var window: UIWindow?
    var db: OpaquePointer?
    var activeOrder:Bool = true
    var selectFilters = [Int]()
    var selectVendor = [Int]()
    var selectDistance = Int()
    var homeScreen:Bool = false
    var appLanguage:String?
    var myLanguage:String?
    var filePath: Bundle?
    var isFirst:Bool = true
    var popUpAlert:Bool = false

    var dictionary_FinalAmount = [GetPromotions]()
    var dictionary_FinalDiscount = [GetPromotions]()
    
    static func getDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        // Make sure that URL scheme is identical to the registered one
        if url.scheme?.caseInsensitiveCompare(Config.urlScheme) == .orderedSame {
            // Send notification to handle result in the view controller.
            NotificationCenter.default.post(name: Notification.Name(rawValue: Config.asyncPaymentCompletedNotificationKey), object: nil)
            return true
        }
        return false
    }
    func application(_ application: UIApplication, shouldAllowExtensionPointIdentifier extensionPointIdentifier: UIApplication.ExtensionPointIdentifier) -> Bool {
        if (extensionPointIdentifier == UIApplication.ExtensionPointIdentifier.keyboard) {
            return false
        }
        return true
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //AIzaSyBYKS-s42W5rnvgaJPjXvuP6PrE7Atfng4
        //GMSPlacesClient.provideAPIKey("AIzaSyA3k068n19sujgNgTe7z6VSL6YXXqVGMuQ")
        //GMSServices.provideAPIKey("AIzaSyA3k068n19sujgNgTe7z6VSL6YXXqVGMuQ")
        GMSPlacesClient.provideAPIKey("AIzaSyAFtfKh3zbXIHcZgG1h0nWI5drMMBOtFN8")
        GMSServices.provideAPIKey("AIzaSyAFtfKh3zbXIHcZgG1h0nWI5drMMBOtFN8")
        
        self.CreateSQLiteDatabase()
        self.getFilter()
        StoreReviewHelper.incrementAppOpenedCount()
        
       // ModelController.methodStatusBarColorChange()
       // application.statusBarStyle = .lightContent
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        // Device Language Control UIDesign
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        if(isDeviceToken() == false){
            UserDef.methodForSaveStringObjectValue("-1", andKey: "DeviceToken")
        }
        
        getDevcieLangauge()
        registerForPushNotifications() // Push Notification
        
        FirebaseApp.configure() // Firebase
        Fabric.with([Crashlytics.self])
        return true
    }
    
    // MARK: Push Notifications
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            DispatchQueue.main.async(execute: {
                guard settings.authorizationStatus == .authorized else { return }
                UIApplication.shared.registerForRemoteNotifications()
            })
        }
    }
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        UserDefaults.standard.set(token, forKey: "DeviceToken")
        Crashlytics.sharedInstance().setObjectValue("\(token)", forKey: "Device Token")
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    // MARK: End Push Notifications
    func getDevcieLangauge() {
        let app = UIApplication.shared.delegate as? AppDelegate
        let defaults = UserDefaults.standard
        var language = Bundle.main.preferredLocalizations[0]
        language = language.components(separatedBy: "-")[0]
        if !UserDefaults.standard.bool(forKey: "HasLaunchedOnce") {
            UserDefaults.standard.set(true, forKey: "HasLaunchedOnce")
            UserDefaults.standard.synchronize()
            
            if (language == "en") {
                let myBundle = Bundle.main
                let myImage = myBundle.path(forResource: "en", ofType: "lproj")
                app?.filePath = Bundle(path: myImage ?? "")
                app?.myLanguage = "English"
                appLanguage = "English"
                defaults.set("English", forKey: "Language")
                defaults.synchronize()
            } else {
                let myBundle = Bundle.main
                let myImage = myBundle.path(forResource: "ar", ofType: "lproj")
                app?.filePath = Bundle(path: myImage ?? "")
                app?.myLanguage = "Arabic"
                appLanguage = "Arabic"
                defaults.set("Arabic", forKey: "Language")
                defaults.synchronize()
            }
            tabBarController()
            
        }else{
            let laguageSelect = UserDefaults.standard.string(forKey: "Language")
            if (laguageSelect == "Arabic") {

                let myBundle = Bundle.main
                let myImage = myBundle.path(forResource: "ar", ofType: "lproj")
                app?.filePath = Bundle(path: myImage ?? "")
                app?.myLanguage = "Arabic"
                appLanguage = "Arabic"
                defaults.set("Arabic", forKey: "Language")
                defaults.synchronize()
            } else {
                let myBundle = Bundle.main
                let myImage = myBundle.path(forResource: "en", ofType: "lproj")
                app?.filePath = Bundle(path: myImage ?? "")
                app?.myLanguage = "English"
                appLanguage = "English"
                defaults.set("English", forKey: "Language")
                defaults.synchronize()
            }
            tabBarController()
        }
    }
    func getFilter() {
        if SaveAddressClass.getFilterCategories.count == 0 {
            if let fileUrl = Bundle.main.url(forResource: "filter", withExtension: "plist"),
                let data = try? Data(contentsOf: fileUrl) {
                if let result = try? PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [[String: Any]] { // [String: Any] which ever it is
                    for filterList in result! {
                        let filterDic  = FilterCategories(
                            FilterTypeId:  filterList["FilterTypeId"] is NSNull ? 0 : filterList["FilterTypeId"] as! Int,
                            FilterTypeName_En: filterList["FilterTypeName_En"] is NSNull ? "" : filterList["FilterTypeName_En"] as! String,
                            FilterTypeName_Ar: filterList["FilterTypeName_En"] is NSNull ? "" : filterList["FilterTypeName_En"] as! String,
                            SelectionType: filterList["SelectionType"] is NSNull ? "" : filterList["SelectionType"] as! String,
                            Filters:self.filterFunc(inputArray:  filterList["Filters"] as! Array<AnyObject> ) as! [FilterList]
                        )
                    SaveAddressClass.getFilterCategories.append(filterDic)
                    }
                }
            }
        }
    }
    // Filter MVC
    func filterFunc(inputArray:Array<AnyObject>) -> Array<AnyObject> {
        var filterArray = [FilterList]()
        
        for filterList in inputArray as! [Dictionary<String, Any>] {
            let filterDic = FilterList(
                FilterId : filterList["FilterId"] is NSNull ? 0 : filterList["FilterId"] as! Int,
                FilterName_En : filterList["FilterName_En"] is NSNull ? "" : filterList["FilterName_En"] as! String,
                FilterName_Ar : filterList["FilterName_En"] is NSNull ? "" : filterList["FilterName_En"] as! String,
                IsSelect: filterList["IsSelect"] is NSNull ? false : filterList["IsSelect"] as! Bool
            )
            filterArray.append(filterDic)
        }
        return filterArray
    }
    
    func tabBarController() {
        if appLanguage == "English"{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let mainVC : UITabBarController = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            let navigationController = UINavigationController(rootViewController: mainVC)
            self.window!.rootViewController = navigationController
            navigationController.setNavigationBarHidden(true, animated: false)
        }else{
            let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: "Main_Ar", bundle: nil)
            let mainVC : UITabBarController = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            let navigationController = UINavigationController(rootViewController: mainVC)
            
            self.window!.rootViewController = navigationController
            navigationController.setNavigationBarHidden(true, animated: false)
        }
    }
    func viewController(){
        // Override point for customization after application launch.
        let mainStoryBoard:UIStoryboard = UIStoryboard.init(name: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Main", value: "", table: nil))!, bundle: nil)
        let mainVC : LoginVC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let navigationController = UINavigationController(rootViewController: mainVC)
        self.window!.rootViewController = navigationController
        navigationController.setNavigationBarHidden(true, animated: false)
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0

    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func CreateSQLiteDatabase()  {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("CoffeeStation.sqlite")
        print(fileURL.path)
        
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        
        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS OrderTable (OrderId INTEGER PRIMARY KEY AUTOINCREMENT, BranchId INTEGER, CategoryId INTEGER, ItemId INTEGER,ItemImg TEXT, ItemName_En TEXT, ItemName_Ar TEXT, SizeId INTEGER, Size_En TEXT, Size_Ar TEXT, SizePrice REAL, Quantity INTEGER, TotalAmount REAL, Comments TEXT, OriginalSizePrice REAL, OriginalTotalAmount REAL, DiscountPercentage REAL)", nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error creating table: \(errmsg)")
        }
        
        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS AdditionalTable (Id INTEGER PRIMARY KEY AUTOINCREMENT, OrderId INTEGER, AdditionalId INTEGER, AddtionalName_En TEXT, AddtionalName_Ar TEXT, AddPrice REAL, Quantity INTEGER, AddTotalPrice REAL, OriginalAddPrice REAL, OriginalAddTotalPrice REAL)", nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error creating table: \(errmsg)")
        }
        
    }
}
// MARK: Push Notifications
extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        NotificationCenter.default.post(name: Notification.Name(rawValue:"getOrderTrackingDetails"), object: nil)
        NotificationCenter.default.post(name: Notification.Name(rawValue:"getLiveTrackingDetails"), object: nil)

        
        let alert = UIAlertController(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: Title, value: "", table: nil))!, message: ((notification.request.content.userInfo["aps"] as? [AnyHashable : Any])?["alert"] as! String), preferredStyle: .alert)
        let action = UIAlertAction(title: (AppDelegate.getDelegate().filePath?.localizedString(forKey: "Ok", value: "", table: nil))!, style: .cancel, handler: nil)
        alert.addAction(action)
        window?.rootViewController?.present(alert, animated: true)
        
//        print(notification.request.content.userInfo)
//        print(notification.request.content.userInfo["PnType"] as Any)
//
//        let PnType : Int? = notification.request.content.userInfo["PnType"] as? Int
//        switch PnType {
//        case 1:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedNotificationData"), object: nil )
//
//        case 2:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedRequestData"), object: nil )
//
//        case 3:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedRequestData"), object: nil )
//
//        case 4:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedRequestData"), object: nil )
//
//        case 5:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedRequestData"), object: nil )
//
//        case 6:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedRequestData"), object: nil )
//
//        case 7:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedHistoryData"), object: nil )
//
//        default:
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReceviedNotificationData"), object: nil )
//
//        }
        
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
       /* if(isUserLogIn() == true){
            // 1
            let userInfo = response.notification.request.content.userInfo
            let aps = userInfo["aps"] as! [String: AnyObject]
            print(aps)
            let identifier = "NotificationVC"
            
            //            let PnType: Int = response.notification.request.content.userInfo["PnType"] as! Int
            //            switch PnType {
            //            case 1:
            //                identifier = "NotificationVC"
            //
            //            case 2:
            //                identifier = "RequestsListVC"
            //
            //            case 3:
            //                identifier = "RequestsListVC"
            //
            //            case 4:
            //                identifier = "RequestsListVC"
            //
            //            case 5:
            //                identifier = "RequestsListVC"
            //
            //            case 6:
            //                identifier = "RequestsListVC"
            //
            //            case 7:
            //                identifier = "HistoryVC"
            //
            //            default:
            //                identifier = "NotificationVC"
            //            }
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let leftMenuVC: LeftMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
            let centerVC: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: identifier)
            let centerNavVC = UINavigationController(rootViewController: centerVC)
            
            //  Set the Panel controllers with just two lines of code
            let rootController: FAPanelController = window?.rootViewController as! FAPanelController
            rootController.center(centerNavVC).left(leftMenuVC)
            
        }*/
        
        
        
        //        PN TYPES
        //        1    Registration Successful    التسجيل ناجح
        //        2    New Request    طلب جديد
        //        3    Bid Placed    عرض السعر
        //        4    Bid Accepted    قبلت المزايدة
        //        5    Final Price Placed    السعر النهائي يوضع
        //        6    Final Price Accepted    السعر النهائي مقبول
        //        7    Invoice Created    الفاتورة التي تم إنشاؤها
        
        completionHandler()
    }
    
    
}

