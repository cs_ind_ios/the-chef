//
//  AccountModel.swift
//  Chef
//
//  Created by RAVI on 19/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class DisplayProfileModel: NSObject,NSCoding, Mappable {
    var successDic : ProfileData?
    var failure : String?
    
    class func newInstance(map: Map) -> Mappable?{
        return DisplayProfileModel()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        successDic <- map["Success"]
        failure <- map["Failure"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        successDic = aDecoder.decodeObject(forKey: "Success") as? ProfileData
        failure = aDecoder.decodeObject(forKey: "Failure") as? String
    }
    @objc func encode(with aCoder: NSCoder) {
        if successDic != nil{
            aCoder.encode(successDic, forKey: "Success")
        }
        if failure != nil{
            aCoder.encode(failure, forKey: "Failure")
        }
    }
}

class ProfileData: NSObject,NSCoding, Mappable {
    
    var email:String?
    var familyName:String?
    var fullName:String?
    var gender:String?
    var language:String?
    var mobile:String?
    var nickName:String?
    var userId:Int?
    
    class func newInstance(map: Map) -> Mappable?{
        return ProfileData()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map) {
        
        email <- map["Email"]
        familyName <- map["FamilyName"]
        fullName <- map["FullName"]
        gender <- map["Gender"]
        language <- map["Language"]
        mobile <- map["Mobile"]
        nickName <- map["NickName"]
        userId <- map["UserId"]
        
    }
    
    @objc required init(coder aDecoder: NSCoder) {
        //print(successDic!)
        email = aDecoder.decodeObject(forKey: "Email") as? String
        familyName = aDecoder.decodeObject(forKey: "FamilyName") as? String
        fullName = aDecoder.decodeObject(forKey: "FullName") as? String
        gender = aDecoder.decodeObject(forKey: "Gender") as? String
        language = aDecoder.decodeObject(forKey: "Language") as? String
        mobile = aDecoder.decodeObject(forKey: "Mobile") as? String
        nickName = aDecoder.decodeObject(forKey: "NickName") as? String
        userId = aDecoder.decodeObject(forKey: "UserId") as? Int
        
        
    }
    
    @objc func encode(with aCoder: NSCoder) {
        
        if email != nil{
            aCoder.encode(email, forKey: "Email")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "UserId")
        }
        if familyName != nil {
            aCoder.encode(familyName, forKey: "FamilyName")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "FullName")
        }
        if gender != nil{
            aCoder.encode(gender, forKey: "Gender")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "Mobile")
        }
        if nickName != nil{
            aCoder.encode(nickName, forKey: "NickName")
        }
        if language != nil{
            aCoder.encode(language, forKey: "Language")
        }
        
    }
    
    
    
    
}
