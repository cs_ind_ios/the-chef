//
//  LoginModel.swift
//  Chef
//
//  Created by RAVI on 19/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class LoginModel: NSObject,NSCoding, Mappable {
    var successDic : LoginModelData?
    //  var failure : String?
    var status : Bool?
    var message : String?
    var messageAr:String?
    
    class func newInstance(map: Map) -> Mappable?{
        return LoginModel()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        successDic <- map["Data"]
        //failure <- map["Failure"]
        status <- map["Status"]
        message <- map["Message"]
        messageAr <- map["MessageAr"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        successDic = aDecoder.decodeObject(forKey: "Data") as? LoginModelData
        status = aDecoder.decodeObject(forKey: "Status") as? Bool
        message = aDecoder.decodeObject(forKey: "Message") as? String
        messageAr = aDecoder.decodeObject(forKey: "MessageAr") as? String
    }
    @objc func encode(with aCoder: NSCoder) {
        
        successDic == nil ? aCoder.encode("", forKey: "Data"):aCoder.encode(successDic, forKey: "Data")
        status == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(status, forKey: "Status")
        message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(message, forKey: "Message")
        messageAr == nil ? aCoder.encode("", forKey: "MessageAr"):aCoder.encode(message, forKey: "MessageAr")
    }
}

class LoginModelData: NSObject,NSCoding, Mappable {
    
    var email:String?
    var familyName:String?
    var fullName:String?
    var gender:String?
    var language:String?
    var mobile:String?
    var nickName:String?
    var userId:Int?
    
    class func newInstance(map: Map) -> Mappable?{
        return LoginModelData()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map) {
        
        email <- map["Email"]
        familyName <- map["FamilyName"]
        fullName <- map["FullName"]
        gender <- map["Gender"]
        language <- map["Language"]
        mobile <- map["Mobile"]
        nickName <- map["NickName"]
        userId <- map["UserId"]
        
    }
    
    @objc required init(coder aDecoder: NSCoder) {
        //print(successDic!)
        email = aDecoder.decodeObject(forKey: "Email") as? String
        familyName = aDecoder.decodeObject(forKey: "FamilyName") as? String
        fullName = aDecoder.decodeObject(forKey: "FullName") as? String
        gender = aDecoder.decodeObject(forKey: "Gender") as? String
        language = aDecoder.decodeObject(forKey: "Language") as? String
        mobile = aDecoder.decodeObject(forKey: "Mobile") as? String
        nickName = aDecoder.decodeObject(forKey: "NickName") as? String
        userId = aDecoder.decodeObject(forKey: "UserId") as? Int
        
        
    }
    
    @objc func encode(with aCoder: NSCoder) {
        
        email == nil ? aCoder.encode("", forKey: "Email"):aCoder.encode(email, forKey: "Email")
        familyName == nil ? aCoder.encode("", forKey: "FamilyName"):aCoder.encode(familyName, forKey: "FamilyName")
        userId == nil ? aCoder.encode("", forKey: "UserId"):aCoder.encode(userId, forKey: "UserId")
        fullName == nil ? aCoder.encode("", forKey: "FullName"):aCoder.encode(fullName, forKey: "FullName")
        gender == nil ? aCoder.encode("", forKey: "Gender"):aCoder.encode(gender, forKey: "Gender")
        mobile == nil ? aCoder.encode("", forKey: "Mobile"):aCoder.encode(mobile, forKey: "Mobile")
        nickName == nil ? aCoder.encode("", forKey: "NickName"):aCoder.encode(nickName, forKey: "NickName")
        language == nil ? aCoder.encode("", forKey: "Language"):aCoder.encode(language, forKey: "Language")
        
        
    }
    
    
}

