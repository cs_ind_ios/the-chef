//
//  PaymentModel.swift
//  CheckClick
//
//  Created by Creative Solutions on 2/20/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
struct GetCheckOutModel : Decodable {
    var Status : Bool
    var Message : String
    var MessageAr : String
    var Data : [GetDataCheckOutModel]?
}
struct GetDataCheckOutModel : Decodable {
    var id : String
}

// Payment Status
struct GetPaymentStatusModel : Decodable {
    var Status : Bool
    var Message : String
    var MessageAr : String
    var Data : [GetPaymentStatusDataModel]?
}
struct GetPaymentStatusDataModel : Decodable {
    var customPayment : String
    var registrationId : String?
    var paymentBrand : String
    var card : GetCardModel?
}
struct GetCardModel : Decodable {
    var last4Digits : String
    var holder : String?
    var expiryMonth : String
    var expiryYear : String

}
struct GetSaveCardResponseModel : Decodable {
    var StatusCode : String
    var StatusMessage : String
    var StatusMessageAr : String

}


struct GetSaveCardModel : Decodable {
    var Status : Bool
    var Message : String
    var MessageAr : String
    var Data : [GetSaveCardDetailsModel]?
}
struct GetSaveCardDetailsModel : Decodable {
     var Id : Int
    var Token : String
    var Last4Digits : String
    var CardHolder : String
    var ExpireMonth : String
    var ExpireYear : String
    var CardBrand : String
}


struct GetDeleteCardModel : Decodable {
    var Status : Bool
    var Message : String
    var MessageAr : String

}


// Add Card Status
struct GetAddCardStatusModel : Decodable {
    var Status : Bool
    var Message : String
    var MessageAr : String

    //var Data : [GetAddCardStatusDataModel]?
}
struct GetAddCardStatusDataModel : Decodable {
    var JArray : [GetJArrayDataModel]
}
struct GetJArrayDataModel : Decodable {
    var Key : String
    var Value : String
}
