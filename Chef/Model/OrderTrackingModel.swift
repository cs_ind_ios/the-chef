//
//  OrderTrackingModel.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
import Foundation
import ObjectMapper
class OrderTrackingModel: NSObject, NSCoding, Mappable {
    
    var successDic : [OrderTrackingDetails]?
    var status : Bool?
    var message : String?
    var messageAr:String?
    class func newInstance(map: Map) -> Mappable?{
        return OrderTrackingModel()
    }
    
    
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        successDic <- map["Data"]
        status <- map["Status"]
        message <- map["Message"]
        messageAr <- map["MessageAr"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        successDic = aDecoder.decodeObject(forKey: "Data") as? [OrderTrackingDetails]
        status = aDecoder.decodeObject(forKey: "Status") as? Bool
        message = aDecoder.decodeObject(forKey: "Message") as? String
        messageAr = aDecoder.decodeObject(forKey: "MessageAr") as? String
    }
    @objc func encode(with aCoder: NSCoder) {
        successDic == nil ? aCoder.encode("", forKey: "Data"):aCoder.encode(successDic, forKey: "Data")
        status == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(status, forKey: "Status")
        message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(message, forKey: "Message")
        messageAr == nil ? aCoder.encode("", forKey: "MessageAr"):aCoder.encode(message, forKey: "MessageAr")
        
    }
}

class OrderTrackingDetails: NSObject,NSCoding,Mappable {
    var OrderMain : Dictionary<String, Any>?
    var OrderItems : [Dictionary<String, Any>]?
    var TrackingDetails : [Dictionary<String, Any>]?
    
    class func newInstance(map: Map) -> Mappable?{
        return OrderTrackingDetails()
    }
    
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        OrderMain <- map["OrderMain"]
        OrderItems <- map["OrderItems"]
        TrackingDetails <- map["TrackingDetails"]
        
    }
    @objc required init(coder aDecoder: NSCoder) {
        OrderMain = aDecoder.decodeObject(forKey: "OrderMain") as? Dictionary<String, Any>
        OrderItems = aDecoder.decodeObject(forKey: "OrderItems") as? [Dictionary<String, Any>]
        TrackingDetails = aDecoder.decodeObject(forKey: "TrackingDetails") as? [Dictionary<String, Any>]
        
    }
    
    @objc func encode(with aCoder: NSCoder) {
        OrderMain == nil ? aCoder.encode("", forKey: "OrderMain"):aCoder.encode(OrderMain, forKey: "OrderMain")
        OrderItems == nil ? aCoder.encode("", forKey: "OrderItems"):aCoder.encode(OrderItems, forKey: "OrderItems")
        TrackingDetails == nil ? aCoder.encode("", forKey: "TrackingDetails"):aCoder.encode(OrderItems, forKey: "TrackingDetails")
        
    }
}

class OrderMainModel{
    var OrderId:Int?
    var BranchId:Int?
    var BranchName_En:String?
    var BranchName_Ar:String?
    var BrandId:Int?
    var BrandName_En:String?
    var BrandName_Ar:String?
    var SubTotal:Double?
    var VatCharges:Double?
    var VatPercentage:Double?
    var TotalPrice:Double?
    var PaymentMode:String?
    var PaymentType:Int?
    var InvoiceNo:String?
    var OrderMode_En:String?
    var OrderMode_Ar:String?
    var OrderType:Int?
    var OrderStatus:String?
    var ExpectedTime:String?
    var DeliveryCharges:Double?
    var BrandLogo:String?
    var BranchLatitude:Double?
    var BranchLongitude:Double?
    var BranchAddress:String?
    var BranchMobileNo:String?
    var IsFavourite:Bool?
    var Rating:Float?
    var BrandImage:String?
    var TotalItemsQty:Int?
    var CouponAmount:Double?
    var UserAddress:String?
    var UserLatitude:Double?
    var UserLongitude:Double?

    init(OrderId:Int, BranchId:Int, BranchName_En:String, BranchName_Ar:String, BrandId:Int, BrandName_En:String, BrandName_Ar:String, SubTotal:Double, VatCharges:Double, VatPercentage:Double, TotalPrice:Double, PaymentMode:String, PaymentType:Int, InvoiceNo:String, OrderMode_En:String, OrderMode_Ar:String, OrderType:Int, OrderStatus:String, ExpectedTime:String, DeliveryCharges:Double, BrandLogo:String, BranchLatitude:Double, BranchLongitude:Double, BranchAddress:String, BranchMobileNo:String,IsFavourite:Bool,Rating:Float, BrandImage:String,TotalItemsQty:Int,CouponAmount:Double,UserAddress:String,UserLatitude:Double,UserLongitude:Double) {
        self.OrderId = OrderId
        self.BranchId = BranchId
        self.BranchName_En = BranchName_En
        self.BranchName_Ar = BranchName_Ar
        self.BrandId = BrandId
        self.BrandName_En = BrandName_En
        self.BrandName_Ar = BrandName_Ar
        self.SubTotal = SubTotal
        self.VatCharges = VatCharges
        self.VatPercentage = VatPercentage
        self.TotalPrice = TotalPrice
        self.PaymentMode = PaymentMode
        self.PaymentType = PaymentType
        self.InvoiceNo = InvoiceNo
        self.OrderMode_En = OrderMode_En
        self.OrderMode_Ar = OrderMode_Ar
        self.OrderType = OrderType
        self.OrderStatus = OrderStatus
        self.ExpectedTime = ExpectedTime
        self.DeliveryCharges = DeliveryCharges
        self.BrandLogo = BrandLogo
        self.BranchLatitude = BranchLatitude
        self.BranchLongitude = BranchLongitude
        self.BranchAddress = BranchAddress
        self.BranchMobileNo = BranchMobileNo
        self.IsFavourite = IsFavourite
        self.Rating = Rating
        self.BrandImage = BrandImage
        self.TotalItemsQty = TotalItemsQty
        self.CouponAmount = CouponAmount
        self.UserAddress = UserAddress
        self.UserLatitude = UserLatitude
        self.UserLongitude = UserLongitude
    }
}
class OrderItemsModel{
    var ItemId:Int?
    var ItemName_En:String?
    var ItemName_Ar:String?
    var ItemPrice:Double?
    var Quantity:Int?
    var ItemSizeName_En:String?
    var ItemSizeName_Ar:String?
    var SizeId:Int?
    var ItemImage:String?
    var itemcomment:String?
    var AdditionalItems:[AdditionalItemsModel]!
    init(ItemId:Int,ItemName_En:String, ItemName_Ar:String, ItemPrice:Double, Quantity:Int, ItemSizeName_En:String, ItemSizeName_Ar:String,SizeId:Int,ItemImage:String,itemcomment:String,AdditionalItems:[AdditionalItemsModel]) {
        self.ItemId = ItemId
        self.ItemName_En = ItemName_En
        self.ItemName_Ar = ItemName_Ar
        self.ItemPrice = ItemPrice
        self.Quantity = Quantity
        self.ItemSizeName_En = ItemSizeName_En
        self.ItemSizeName_Ar = ItemSizeName_Ar
        self.SizeId = SizeId
        self.ItemImage = ItemImage
        self.itemcomment = itemcomment
        self.AdditionalItems = AdditionalItems
    }
    
}
class AdditionalItemsModel{
    var AddItemId:Int?
    var AddItem_En:String?
    var AddItem_Ar:String?
    var AddItemPrice:Double?
    var AddQuantity:Int?
    init (AddItemId:Int,AddItem_En:String,AddItem_Ar:String,AddItemPrice:Double,AddQuantity:Int){
        self.AddItemId = AddItemId
        self.AddItem_En = AddItem_En
        self.AddItem_Ar = AddItem_Ar
        self.AddItemPrice = AddItemPrice
        self.AddQuantity = AddQuantity
    }
    
    
}
class TrackingOrderModel{
    var TrackingTime:String?
    init(TrackingTime:String){
        self.TrackingTime = TrackingTime
    }
    
}
