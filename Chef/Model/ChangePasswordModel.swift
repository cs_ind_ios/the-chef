//
//  ChangePasswordModel.swift
//  Chef
//
//  Created by RAVI on 19/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class ChangePasswordModel: NSObject,NSCoding, Mappable {
    
    var staus : Bool?
    var message : String?
    var messageAr:String?
    
    class func newInstance(map: Map) -> Mappable?{
        return ChangePasswordModel()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map) {
       // print(map["Staus"])
        staus <- map["Staus"]
        message <- map["Message"]
        messageAr <- map["MessageAr"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        
        staus = aDecoder.decodeObject(forKey: "Status") as? Bool
        message = aDecoder.decodeObject(forKey: "Message") as? String
        messageAr = aDecoder.decodeObject(forKey: "MessageAr") as? String
        
    }
    func encode(with aCoder: NSCoder) {
        
        staus == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(staus, forKey: "Status")
        message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(message, forKey: "Message")
        messageAr == nil ? aCoder.encode("", forKey: "MessageAr"):aCoder.encode(message, forKey: "MessageAr")
        
    }
    
}

class UpdateLanguageModel: NSObject,NSCoding, Mappable {
    
    var Status : Bool?
    var Message : String?
    var MessageAr:String?
    
    class func newInstance(map: Map) -> Mappable?{
        return UpdateLanguageModel()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map) {
        Status <- map["Status"]
        Message <- map["Message"]
        MessageAr <- map["MessageAr"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        
        Status = aDecoder.decodeObject(forKey: "Status") as? Bool
        Message = aDecoder.decodeObject(forKey: "Message") as? String
        MessageAr = aDecoder.decodeObject(forKey: "MessageAr") as? String
        
    }
    func encode(with aCoder: NSCoder) {
        
        Status == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(Status, forKey: "Status")
        Message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(Message, forKey: "Message")
        MessageAr == nil ? aCoder.encode("", forKey: "MessageAr"):aCoder.encode(MessageAr, forKey: "MessageAr")
        
    }
    
}

