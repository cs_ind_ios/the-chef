//
//  Promotions.swift
//  Chef
//
//  Created by Devbox on 04/09/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation

struct GetPromotionsModel : Decodable {
    var Status : Bool
    var Message : String
    var Data : [GetPromotions]?
}

struct GetPromotions : Decodable {
    var PromotionId: Int
    var PromoType: Int
    var DiscountType: Int
    var DiscountAmt: Double
    var Image: String
    var PromoTitle_En: String
    var PromoTitle_Ar: String
    var Desc_En: String
    var Desc_Ar: String
    var MaxAmount: Double
    var MinSpend: Double
    var Msg_En: String
    var Msg_Ar: String
    var ExpireDays: Int
    var ReferelId: Int
    var TodayAvailable: Bool
    var DayOftheWeek: String
    var PromotionCode: String
    var PromotionDays: [GetPromotionsDay]
    var StoreInfo: [StoreInfo]?
    var CategoryPromotion: [CategoryPromotion]?
}

struct GetPromotionsDay : Decodable {
    var DayName: String
}

struct StoreInfo : Decodable {
    var BrandId: Int
    var BrandNameEn: String
    var BrandNameAr: String
    var BranchId: Int
    var BranchNameEn: String
    var BranchNameAr: String
}

struct CategoryPromotion : Decodable {
    var Id: Int
    var CategoryNameEn: String
    var CategoryNameAr: String
    var CategoryImageEn: String
    var CategoryImageAr: String
    var Items: [Items]
}

struct Items : Decodable {
    var Id: Int
    var ItemNameEn: String
    var ItemNameAr: String
    var ItemImage: String
}


struct ResponseModel : Decodable {
    var Status : Bool
}

//Selection
struct DropDownModel : Decodable {
    var ID : String
    var Name : String
}
