//
//  OrderTableModel.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
public class OrderTable{
    var OrderId : Int?
    var BranchId : Int?
    var CategoryId : Int?
    var ItemId : Int?
    var ItemImg : String?
    var ItemName_En : String?
    var ItemName_Ar : String?
    var SizeId : Int?
    var Size_En : String?
    var Size_Ar : String?
    var SizePrice : Double?
    var Quantity : Int?
    var TotalAmount : Double?
    var Comments : String?
    var OriginalSizePrice : Double?
    var OriginalTotalAmount : Double?
    var DiscountPercentage : Double?
    init(OrderId : Int, BranchId : Int, CategoryId : Int, ItemId : Int, ItemImg:String, ItemName_En : String, ItemName_Ar : String, SizeId : Int, Size_En : String, Size_Ar : String, SizePrice : Double, Quantity : Int, TotalAmount : Double, Comments : String, OriginalSizePrice : Double, OriginalTotalAmount : Double, DiscountPercentage : Double) {
        self.OrderId = OrderId
        self.BranchId = BranchId
        self.CategoryId = CategoryId
        self.ItemId = ItemId
        self.ItemImg = ItemImg
        self.ItemName_En = ItemName_En
        self.ItemName_Ar = ItemName_Ar
        self.SizeId = SizeId
        self.Size_En = Size_En
        self.Size_Ar = Size_Ar
        self.SizePrice = SizePrice
        self.Quantity = Quantity
        self.TotalAmount = TotalAmount
        self.Comments = Comments
        self.OriginalSizePrice = OriginalSizePrice
        self.OriginalTotalAmount = OriginalTotalAmount
        self.DiscountPercentage = DiscountPercentage
    }
    
}
public class AdditionalTable{
    var Id : Int?
    var OrderId : Int?
    var AdditionalId : Int?
    var AddtionalName_En : String?
    var AddtionalName_Ar : String?
    var AddPrice : Double?
    var Quantity : Int?
    var AddTotalPrice : Double?
    var OriginalAddPrice : Double?
    var OriginalAddTotalPrice : Double?
    
    init(Id : Int, OrderId : Int, AdditionalId : Int, AddtionalName_En : String, AddtionalName_Ar : String, AddPrice : Double, Quantity : Int,AddTotalPrice : Double, OriginalAddPrice : Double, OriginalAddTotalPrice : Double) {
        self.Id = Id
        self.OrderId = OrderId
        self.AdditionalId = AdditionalId
        self.AddtionalName_En = AddtionalName_En
        self.AddtionalName_Ar = AddtionalName_Ar
        self.AddPrice = AddPrice
        self.Quantity = Quantity
        self.AddTotalPrice = AddTotalPrice
        self.OriginalAddPrice = OriginalAddPrice
        self.OriginalAddTotalPrice = OriginalAddTotalPrice
        
    }
    
}
