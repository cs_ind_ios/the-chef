//
//  Address.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
public class Address{
    
    var AddressId:Int?
    var HouseNo:String?
    var AddressName:String?
    var LandMark:String?
    var Address:String?
    var AddressType:String?
    var IsDeleted:Bool?
    var Latitude:Double?
    var Longitude:Double?
    
    init(AddressId:Int, HouseNo:String, AddressName:String, LandMark:String, Address:String, AddressType:String, IsDeleted:Bool, Latitude : Double, Longitude : Double) {
        self.AddressId = AddressId
        self.HouseNo = HouseNo
        self.AddressName = AddressName
        self.LandMark = LandMark
        self.Address = Address
        self.AddressType = AddressType
        self.IsDeleted = IsDeleted
        self.Latitude = Latitude
        self.Longitude = Longitude
    }
}
