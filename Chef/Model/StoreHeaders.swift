//
//  StoreHeaders.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
class HeaderNames{
    static var headesArray = [String]()
    class func headerNames() -> [String] {
        headesArray.removeAll()
        if SaveAddressClass.getStoreBannerArray.count >= 1 {
            // self.headesArray.insert("Banners", at: 0)
            self.headesArray.append("Banners")
        }
        if SaveAddressClass.getStorePromotedArray.count >= 1 {
            //self.headesArray.insert("Popular", at: 1)
            self.headesArray.append("Promoted")
            
        }
        if SaveAddressClass.getStorePopularArray.count >= 1 {
            //self.headesArray.insert("Popular", at: 1)
            self.headesArray.append("Popular")
            
        }
        
        if SaveAddressClass.getStoreAround3KmArray.count >= 1 {
            // self.headesArray.insert("Around 3KM", at: 2)
            self.headesArray.append("Around 3KM")
            
        }
        if SaveAddressClass.getStoreNewArray.count >= 1 {
            // self.headesArray.insert("New on Caffee Station", at: 4)
            self.headesArray.append("New on Caffee Station")
            
        }
        //self.headesArray.append("offer")
        self.headesArray.append("Looking something else?")
        self.headesArray.append("More CAFE")
        
        return self.headesArray
    }
}
