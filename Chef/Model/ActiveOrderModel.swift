//
//  ActiveOrderModel.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
import Foundation
import ObjectMapper
class ActiveOrderModel: NSObject, NSCoding, Mappable {
    
    var status : Bool?
    var message : String?
    var successDic : [Dictionary<String, Any>]?
    
    class func newInstance(map: Map) -> Mappable?{
        return ActiveOrderModel()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        status <- map["Status"]
        message <- map["Message"]
        successDic <- map["Data"]
        
    }
    @objc required init(coder aDecoder: NSCoder) {
        status = aDecoder.decodeObject(forKey: "Status") as? Bool
        message = aDecoder.decodeObject(forKey: "Message") as? String
        successDic = aDecoder.decodeObject(forKey: "Data") as? [Dictionary<String, Any>]
        
    }
    @objc func encode(with aCoder: NSCoder) {
        status == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(status, forKey: "Status")
        message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(message, forKey: "Message")
        successDic == nil ? aCoder.encode("", forKey: "Data"):aCoder.encode(successDic, forKey: "Data")
        
    }
}
class ActiveOrderModelArray {
    var OrderId : Int?
    var OrderStatus : String?
    var BrandName : String?
    var BrandImage : String?
    init(OrderId : Int, OrderStatus : String, BrandName : String, BrandImage : String) {
        self.OrderId = OrderId
        self.OrderStatus = OrderStatus
        self.BrandName = BrandName
        self.BrandImage = BrandImage
    }
}
