//
//  MainMenuItems.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
import Foundation
import ObjectMapper

class MainMenuItems: NSObject,NSCoding, Mappable {
    
    var successDic : MenuDetailsDic?
    var status : Bool?
    var message : String?
    var messageAr : String?
    class func newInstance(map: Map) -> Mappable?{
        return MainMenuItems()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        successDic <- map["Data"]
        status <- map["Status"]
        message <- map["Message"]
        messageAr <- map["MessageAr"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        successDic = aDecoder.decodeObject(forKey: "Data") as? MenuDetailsDic
        status = aDecoder.decodeObject(forKey: "Status") as? Bool
        message = aDecoder.decodeObject(forKey: "Message") as? String
        messageAr = aDecoder.decodeObject(forKey: "MessageAr") as? String
    }
    @objc func encode(with aCoder: NSCoder) {
        successDic == nil ? aCoder.encode("", forKey: "Data"):aCoder.encode(successDic, forKey: "Data")
        status == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(status, forKey: "Status")
        message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(message, forKey: "Message")
        messageAr == nil ? aCoder.encode("", forKey: "MessageAr"):aCoder.encode(messageAr, forKey: "MessageAr")
    }
}
class MenuDetailsDic: NSObject,NSCoding,Mappable {
    
    var menuDetails : [Dictionary<String, Any>]?
    class func newInstance(map: Map) -> Mappable?{
        return MenuDetailsDic()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        menuDetails <- map["MenuItems"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        menuDetails = aDecoder.decodeObject(forKey: "MenuItems") as? [Dictionary<String, Any>]
    }
    @objc func encode(with aCoder: NSCoder) {
        menuDetails == nil ? aCoder.encode("", forKey: "MenuItems"):aCoder.encode(menuDetails, forKey: "MenuItems")
    }
}

class GetCategoryMenu {
    var CategoryId : Int?
    var CategoryName_En : String?
    var CategoryName_Ar : String?
    var CatImage : String?
    var IsOpen : Bool?
    var MenuItems : [GetItemsMenu]!
    
    init(CategoryId: Int, CategoryName_En:String, CategoryName_Ar:String, CatImage:String, IsOpen:Bool,  MenuItems:[GetItemsMenu] ) {
        self.CategoryId = CategoryId
        self.CategoryName_En = CategoryName_En
        self.CategoryName_Ar = CategoryName_Ar
        self.CatImage = CatImage
        self.IsOpen = IsOpen
        self.MenuItems = MenuItems
    }
}

class GetItemsMenu {
    
    var ItemId : Int?
    var ItemName_En : String?
    var ItemName_Ar : String?
    var ItemDesc_En : String?
    var ItemDesc_Ar : String?
    var ItemImage : String?
    var Price : Double?
    init(ItemId:Int, ItemName_En:String, ItemName_Ar:String, ItemDesc_En:String, ItemDesc_Ar:String, ItemImage:String, Price:Double) {
        self.ItemId = ItemId
        self.ItemName_En = ItemName_En
        self.ItemName_Ar = ItemName_Ar
        self.ItemDesc_En = ItemDesc_En
        self.ItemDesc_Ar = ItemDesc_Ar
        self.ItemImage = ItemImage
        self.Price = Price
    }  
}
