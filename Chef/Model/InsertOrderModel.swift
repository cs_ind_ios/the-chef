//
//  InsertOrderModel.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
import Foundation
import ObjectMapper

class InsertOrder: NSObject, NSCoding, Mappable {
    
    var successDic : Dictionary<String, Any>?
    var status : Bool?
    var message : String?
    var messageAr:String?
    class func newInstance(map: Map) -> Mappable?{
        return InsertOrder()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        successDic <- map["Data"]
        status <- map["Status"]
        message <- map["Message"]
        messageAr <- map["MessageAr"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        successDic = aDecoder.decodeObject(forKey: "Data") as? Dictionary<String, Any>
        status = aDecoder.decodeObject(forKey: "Status") as? Bool
        message = aDecoder.decodeObject(forKey: "Message") as? String
        messageAr = aDecoder.decodeObject(forKey: "MessageAr") as? String
    }
    @objc func encode(with aCoder: NSCoder) {
        successDic == nil ? aCoder.encode("", forKey: "Data"):aCoder.encode(successDic, forKey: "Data")
        status == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(status, forKey: "Status")
        message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(message, forKey: "Message")
        messageAr == nil ? aCoder.encode("", forKey: "MessageAr"):aCoder.encode(message, forKey: "MessageAr")
    }
}

class InsertMainOrder{
    var UserId : Int?
    var BranchId : Int?
    var BrandId : Int?
    var AddressID : Int?
    var TotalPrice : Double?
    var SubTotal : Double?
    var VatCharges : Double?
    var VatPercentage : Double?
    var OrderType : Int?
    var Version : String?
    var Devicetoken : String?
    var ExpectedTime : String?
    var PaymentMode : Int?
    
    init(UserId : Int, BranchId : Int, BrandId : Int, AddressID : Int, TotalPrice : Double, SubTotal : Double, VatCharges : Double, VatPercentage : Double,  OrderType : Int, Version : String, Devicetoken : String, ExpectedTime : String, PaymentMode : Int) {
        self.UserId = UserId
        self.BranchId = BranchId
        self.AddressID = AddressID
        self.TotalPrice = TotalPrice
        self.SubTotal = SubTotal
        self.VatCharges = VatCharges
        self.VatPercentage = VatPercentage
        self.OrderType = OrderType
        self.Version = Version
        self.Devicetoken = Devicetoken
        self.ExpectedTime = ExpectedTime
        self.PaymentMode = PaymentMode
    }
}

class InsertOrderItems{
    var ItemId : Int?
    var Quantity : Int?
    var SizeId : Int?
    var ItemPrice : Double?
    var Comments : String?
    var AdditionalItems : [InsertAdditionalItems]!
    init(ItemId : Int, Quantity : Int, SizeId : Int, ItemPrice : Double, Comments : String, AdditionalItems : [InsertAdditionalItems]) {
        self.ItemId = ItemId
        self.Quantity = Quantity
        self.SizeId = SizeId
        self.ItemPrice = ItemPrice
        self.Comments = Comments
        self.AdditionalItems = AdditionalItems
    }
}

class InsertAdditionalItems{
    var AddItemId : Int?
    var AddItemPrice : Double?
    init(AddItemId : Int, AddItemPrice : Double) {
        self.AddItemId = AddItemId
        self.AddItemPrice = AddItemPrice
    }
}
