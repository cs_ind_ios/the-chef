//
//  CancelOrderModel.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
import Foundation
import ObjectMapper
class CancelOrderModel: NSObject, NSCoding, Mappable {
    
    var status : Bool?
    var message : String?
    var messageAr:String?
    class func newInstance(map: Map) -> Mappable?{
        return CancelOrderModel()
    }
    
    
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        status <- map["Status"]
        message <- map["Message"]
        messageAr <- map["MessageAr"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        status = aDecoder.decodeObject(forKey: "Status") as? Bool
        message = aDecoder.decodeObject(forKey: "Message") as? String
        messageAr = aDecoder.decodeObject(forKey: "MessageAr") as? String
    }
    @objc func encode(with aCoder: NSCoder) {
        status == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(status, forKey: "Status")
        message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(message, forKey: "Message")
        messageAr == nil ? aCoder.encode("", forKey: "MessageAr"):aCoder.encode(message, forKey: "MessageAr")
        
    }
}
