//
//  ItemDetails.swift
//  Chef
//
//  Created by RAVI on 22/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation

public class ItemDetails {
    
    var ItemId : Int?
    var ItemName_En : String?
    var ItemName_Ar : String?
    var ItemDesc_En : String?
    var ItemDesc_Ar : String?
    var ItemImage : String?
    var Price : Double?
    
    var CategoryId : Int?
    init(ItemId:Int, ItemName_En:String, ItemName_Ar : String, ItemDesc_En : String, ItemDesc_Ar : String, ItemImage : String, Price : Double, CategoryId : Int) {
        self.ItemId = ItemId
        self.ItemName_En = ItemName_En
        self.ItemName_Ar = ItemName_Ar
        self.ItemDesc_En = ItemDesc_En
        self.ItemDesc_Ar = ItemDesc_Ar
        self.ItemImage = ItemImage
        self.Price = Price
        self.CategoryId = CategoryId
    }
}
