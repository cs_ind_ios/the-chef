//
//  LiveTrackModel.swift
//  Chef
//
//  Created by Devbox on 31/01/20.
//  Copyright © 2020 Creative Solutions. All rights reserved.
//

import Foundation

//Track Driver
struct TrackDriverDetailsModel : Decodable {
    var Status : Bool
    var Message : String
    var MessageAr:String
    var Data : TrackDriverModel
}
struct TrackDriverModel : Decodable {
    var DriverId : Int
    var FullName : String
    var Latitude : String
    var Longitude : String
    var MobileNo : String
    var FullAddress : String
    var ProfileImage : String
    var VehicleNumber : String
    var OnlineStatus : Bool
    var OrderId : Int
    var UserLatitude : String
    var UserLongitude : String
    var StoreLatitude : String
    var StoreLongitude : String
    var CurrentDateTime : String
    var OrderStatus : String
    var Rotation : String
}
