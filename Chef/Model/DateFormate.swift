//
//  DateFormate.swift
//  Chef
//
//  Created by RAVI on 22/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation

class DateFormate: NSObject {
    
    static func dateConverstion(dateStr:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //2018-08-28T01:00:00
        dateFormatter.locale = Locale(identifier: "EN")
        let date  = dateFormatter.date(from: dateStr)
        return date!
    }
    
    static func dateConverstion2(dateStr:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS" //2018-08-28T01:00:00
        dateFormatter.locale = Locale(identifier: "EN")
        let date  = dateFormatter.date(from: dateStr)
        return date!
    }
    
    static func dateConverstionToString(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a" //01:00 AM
        dateFormatter.locale = Locale(identifier: "EN")
        let date  = dateFormatter.string(from: date)
        return date
    }
}
