//
//  OrderHistoryModel.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
import Foundation
import ObjectMapper
class OrderHistoryModel: NSObject, NSCoding, Mappable {
    
    var successDic : [OrderHistoryDetails]?
    var status : Bool?
    var message : String?
    var messageAr:String?
    class func newInstance(map: Map) -> Mappable?{
        return OrderHistoryModel()
    }
    
    
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        successDic <- map["Data"]
        status <- map["Status"]
        message <- map["Message"]
        messageAr <- map["MessageAr"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        successDic = aDecoder.decodeObject(forKey: "Data") as? [OrderHistoryDetails]
        status = aDecoder.decodeObject(forKey: "Status") as? Bool
        message = aDecoder.decodeObject(forKey: "Message") as? String
        messageAr = aDecoder.decodeObject(forKey: "MessageAr") as? String
    }
    @objc func encode(with aCoder: NSCoder) {
        successDic == nil ? aCoder.encode("", forKey: "Data"):aCoder.encode(successDic, forKey: "Data")
        status == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(status, forKey: "Status")
        message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(message, forKey: "Message")
        messageAr == nil ? aCoder.encode("", forKey: "MessageAr"):aCoder.encode(message, forKey: "MessageAr")
        
    }
}

class OrderHistoryDetails: NSObject,NSCoding,Mappable {
    var OrderMain : Dictionary<String, Any>?
    var OrderItems : [Dictionary<String, Any>]?
    var Rows : Int?

    class func newInstance(map: Map) -> Mappable?{
        return OrderHistoryDetails()
    }
    
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        OrderMain <- map["OrderMain"]
        OrderItems <- map["OrderItems"]
        Rows <- map["Rows"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        OrderMain = aDecoder.decodeObject(forKey: "OrderMain") as? Dictionary<String, Any>
        OrderItems = aDecoder.decodeObject(forKey: "OrderItems") as? [Dictionary<String, Any>]
        Rows = aDecoder.decodeObject(forKey: "Rows") as? Int

    }
    
    @objc func encode(with aCoder: NSCoder) {
        OrderMain == nil ? aCoder.encode("", forKey: "OrderMain"):aCoder.encode(OrderMain, forKey: "OrderMain")
        OrderItems == nil ? aCoder.encode("", forKey: "OrderItems"):aCoder.encode(OrderItems, forKey: "OrderItems")
        Rows == nil ? aCoder.encode("", forKey: "Rows"):aCoder.encode(Rows, forKey: "Rows")

    }
}

class OrderHistory{
    var OrderId:Int?
    var BranchId:Int?
    var BranchName_En:String?
    var BranchName_Ar:String?
    var BrandId:Int?
    var BrandName_En:String?
    var BrandName_Ar:String?
    var SubTotal:Double?
    var VatCharges:Double?
    var VatPercentage:Double?
    var TotalPrice:Double?
    var PaymentMode:String?
    var PaymentType:Int?
    var InvoiceNo:String?
    var OrderDate:String?
    var OrderMode_En:String?
    var OrderMode_Ar:String?
    var OrderType:Int?
    var OrderStatus:String?
    var ExpectedTime:String?
    var ItemsNames:String?
    var Qunatity:Int?
    var BranchAddress:String?
    var IsFavourite:Bool?
    var Rating:Float?
    var BrandLogo:String?
    
    init(OrderId:Int, BranchId:Int, BranchName_En:String, BranchName_Ar:String, BrandId:Int, BrandName_En:String, BrandName_Ar:String, SubTotal:Double, VatCharges:Double, VatPercentage:Double, TotalPrice:Double, PaymentMode:String, PaymentType:Int, InvoiceNo:String, OrderDate:String, OrderMode_En:String, OrderMode_Ar:String, OrderType:Int, OrderStatus:String, ExpectedTime:String, ItemsNames:String, Qunatity:Int, BranchAddress:String, IsFavourite:Bool,Rating:Float,BrandLogo:String) {
        self.OrderId = OrderId
        self.BranchId = BranchId
        self.BranchName_En = BranchName_En
        self.BranchName_Ar = BranchName_Ar
        self.BrandId = BrandId
        self.BrandName_En = BrandName_En
        self.BrandName_Ar = BrandName_Ar
        self.SubTotal = SubTotal
        self.VatCharges = VatCharges
        self.VatPercentage = VatPercentage
        self.TotalPrice = TotalPrice
        self.PaymentMode = PaymentMode
        self.PaymentType = PaymentType
        self.InvoiceNo = InvoiceNo
        self.OrderDate = OrderDate
        self.OrderMode_En = OrderMode_En
        self.OrderMode_Ar = OrderMode_Ar
        self.OrderType = OrderType
        self.OrderStatus = OrderStatus
        self.ExpectedTime = ExpectedTime
        self.ItemsNames = ItemsNames
        self.Qunatity = Qunatity
        self.BranchAddress = BranchAddress
        self.IsFavourite = IsFavourite
        self.Rating = Rating
        self.BrandLogo = BrandLogo
    }
}
