//
//  FilterModel.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation

public class FilterCategories{
    
    var FilterTypeId:Int?
    var FilterTypeName_En : String?
    var FilterTypeName_Ar : String?
    var SelectionType : String?
    
    var Filters = [FilterList]()
    
    init(FilterTypeId : Int, FilterTypeName_En : String, FilterTypeName_Ar: String, SelectionType: String, Filters : [FilterList] ) {
        self.FilterTypeId = FilterTypeId
        self.FilterTypeName_En = FilterTypeName_En
        self.FilterTypeName_Ar = FilterTypeName_Ar
        self.SelectionType = SelectionType
        self.Filters = Filters
    }
}
class FilterList{
    var FilterId:Int?
    var FilterName_En : String?
    var FilterName_Ar : String?
    var IsSelect : Bool?
    
    init(FilterId : Int, FilterName_En : String, FilterName_Ar : String, IsSelect : Bool) {
        self.FilterId = FilterId
        self.FilterName_En = FilterName_En
        self.FilterName_Ar = FilterName_Ar
        self.IsSelect = IsSelect
    }
}
class MappingBrands {
    static var mainArr = [GetStores]()
    class func Brands() -> [GetStores] {
        mainArr.removeAll()
        // Binding the stores
        for bindingStoreDic in SaveAddressClass.getStoresArray {
            var isDuplicateBool = false
            
            for i in 0..<self.mainArr.count {
                if self.mainArr[i].BrandId == bindingStoreDic.BrandId {
                    isDuplicateBool = true
                    let count = self.mainArr[i].BindingCount! + 1
                    if  self.mainArr[i].Distance! < bindingStoreDic.Distance! &&  self.mainArr[i].StoreStatus! == "Open" {
                        self.mainArr[i].BindingCount = count
                        let dic = self.mainArr[i]
                        self.mainArr.remove(at: i)
                        //self.mainArr.append(dic)
                        self.mainArr.insert(dic, at: i)
                    }else{
                        if bindingStoreDic.StoreStatus! == "Open" {
                            bindingStoreDic.BindingCount = count
                            self.mainArr.remove(at: i)
                            // self.mainArr.append(bindingStoreDic)
                            self.mainArr.insert(bindingStoreDic, at: i)
                        }
                    }
                }
            }
            if isDuplicateBool == false {
                self.mainArr.append(bindingStoreDic)
            }
        }
        let sortedArray = mainArr.sorted(by: { (one, two) -> Bool in
            return one.StoreStatus! > two.StoreStatus!
        })
        return sortedArray
    }
    class func SearchBrands() -> [GetStores] {
        mainArr.removeAll()
        // Binding the stores
        for bindingStoreDic in SaveAddressClass.getSearchStoresArray {
            var isDuplicateBool = false
            
            for i in 0..<self.mainArr.count {
                if self.mainArr[i].BrandId == bindingStoreDic.BrandId {
                    isDuplicateBool = true
                    let count = self.mainArr[i].BindingCount! + 1
                    if  self.mainArr[i].Distance! < bindingStoreDic.Distance! &&  self.mainArr[i].StoreStatus! == "Open" {
                        self.mainArr[i].BindingCount = count
                        let dic = self.mainArr[i]
                        self.mainArr.remove(at: i)
                        //self.mainArr.append(dic)
                        self.mainArr.insert(dic, at: i)
                    }else{
                        if bindingStoreDic.StoreStatus! == "Open" {
                            bindingStoreDic.BindingCount = count
                            self.mainArr.remove(at: i)
                            // self.mainArr.append(bindingStoreDic)
                            self.mainArr.insert(bindingStoreDic, at: i)
                        }
                    }
                }
            }
            if isDuplicateBool == false {
                self.mainArr.append(bindingStoreDic)
            }
        }
        let sortedArray = mainArr.sorted(by: { (one, two) -> Bool in
            return one.StoreStatus! > two.StoreStatus!
        })
        return sortedArray
    }
}
class SortingFilters{
    static var selectFilters: [String] = []
    class func sortingFilters(array:[GetStores]) -> [GetStores] {
        selectFilters.removeAll()
       // print(array)
        
        // Sorting Store list
        for filterSelectList in SaveAddressClass.getFilterCategories{
            for filterSelectDic in filterSelectList.Filters{
                if filterSelectDic.IsSelect == true {
                    let Filter = "\(String(describing: filterSelectDic.FilterName_En!))"
                    selectFilters.append(Filter)
                }
            }
        }
        // Sorting only one
        var  sortedArray = [GetStores]()
        let Relevance = self.selectFilters.filter { ($0 == "Relevance") }
        if Relevance.count > 0 || self.selectFilters.count == 0 {
            sortedArray = array.sorted(by: { (one, two) -> Bool in
                return one.Distance! < two.Distance!
            })
        }
        let Rating = self.selectFilters.filter { ($0 == "Rating") }
        //print(Rating)
        if Rating.count > 0 {
            sortedArray = array.sorted(by: { (one, two) -> Bool in
                return one.Rating! > two.Rating!
            })
        }
        let Price = self.selectFilters.filter { ($0 == "Price") }
       // print(Price)
        if Price.count > 0 {
            sortedArray = array.sorted(by: { (one, two) -> Bool in
                return one.Rating! > two.Rating!
            })
        }
        // Filter
        let Offers = self.selectFilters.filter { ($0 == "Discount") }
        if Offers.count > 0 {
            sortedArray = sortedArray.filter { ($0.DiscountAmt! > 0) }
        }
        // Sorting Store Status
        var mainSortedArray = [GetStores]()
        let storeStatus = self.selectFilters.filter { ($0 == "Open Restaurants") }
        if storeStatus.count > 0 {
            mainSortedArray = sortedArray.filter { ($0.StoreStatus! == "Open") }
        }else{
            mainSortedArray = sortedArray.sorted(by: { (one, two) -> Bool in
                return one.StoreStatus! > two.StoreStatus!
            })
        }
        SaveAddressClass.getStoresArray.removeAll()
        return mainSortedArray
    }
}
