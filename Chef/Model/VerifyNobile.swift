//
//  VerifyNobile.swift
//  Chef
//
//  Created by RAVI on 19/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class VerifyMobile: NSObject,NSCoding, Mappable{
    var successDic : SuccessOTP?
    var message : String?
    var messageAr:String?
    var status : Bool?
    
    class func newInstance(map: Map) -> Mappable?{
        return VerifyMobile()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        successDic <- map["Data"]
        status <- map["Status"]
        message <- map["Message"]
        messageAr <- map["MessageAr"]
    }
    @objc required init(coder aDecoder: NSCoder) {
       // print(successDic!)
        successDic = aDecoder.decodeObject(forKey: "Data") as? SuccessOTP
    }
    @objc func encode(with aCoder: NSCoder) {
        successDic == nil ? aCoder.encode("", forKey: "Data"):aCoder.encode(successDic, forKey: "Data")
    }
}
class SuccessOTP: NSObject,NSCoding, Mappable {
    var otp : Int?
    class func newInstance(map: Map) -> Mappable?{
        return SuccessOTP()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map) {
        //print(map["OTP"])
        otp <- map["OTP"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        otp = aDecoder.decodeObject(forKey: "OTP") as? Int
    }
    func encode(with aCoder: NSCoder) {
        if otp != nil{
            aCoder.encode(otp, forKey: "OTP")
        }
    }
}
class ForgotOTP: NSObject,NSCoding, Mappable {
    var otp : Int?
    class func newInstance(map: Map) -> Mappable?{
        return ForgotOTP()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map) {
        //print(map["Success"])
        otp <- map["Success"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        otp = aDecoder.decodeObject(forKey: "Success") as? Int
    }
    func encode(with aCoder: NSCoder) {
        if otp != nil{
            aCoder.encode(otp, forKey: "OTP")
        }
    }
}
class ForgotPasswordModel: NSObject,NSCoding, Mappable {
    var successDic : LoginModelData?
    var status : Bool?
    var message : String?
    var messageAr:String?
    class func newInstance(map: Map) -> Mappable?{
        return ForgotPasswordModel()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        successDic <- map["Data"]
        status <- map["Status"]
        message <- map["Message"]
        messageAr <- map["MessageAr"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        successDic = aDecoder.decodeObject(forKey: "Data") as? LoginModelData
        status = aDecoder.decodeObject(forKey: "Status") as? Bool
        message = aDecoder.decodeObject(forKey: "Message") as? String
        messageAr = aDecoder.decodeObject(forKey: "MessageAr") as? String
    }
    func encode(with aCoder: NSCoder) {
        successDic == nil ? aCoder.encode("", forKey: "Data"):aCoder.encode(successDic, forKey: "Data")
        status == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(status, forKey: "Status")
        message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(message, forKey: "Message")
        messageAr == nil ? aCoder.encode("", forKey: "MessageAr"):aCoder.encode(message, forKey: "MessageAr")
    }
}
