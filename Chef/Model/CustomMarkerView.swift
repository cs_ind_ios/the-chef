//
//  CustomMarkerView.swift
//  CheckClick
//
//  Created by Creative Solutions on 2/25/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
class CustomMarkerView: UIView {
    var brandImgName:String!
    var storeName:String!
    var subStoreName:String!
    var storeDescription:String!
    var servePeople:Int!
    var distance: Double!
    var borderColor: UIColor!
    
    init(frame: CGRect, brandImgName:String, storeName: String, subStoreName:String, storeDescription:String, servePeople:Int, distance: Double, borderColor: UIColor, tag: Int) {
        super.init(frame: frame)
        self.brandImgName = brandImgName
        self.storeName = storeName
        self.subStoreName = subStoreName
        self.storeDescription = storeDescription
        self.servePeople = servePeople
        self.distance = distance
        self.borderColor = borderColor
        self.tag = tag
        setupViews()
    }
    
    func setupViews() {
        let customViewOne = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 150))
        customViewOne.backgroundColor = UIColor.clear
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 110))
        
        customView.layer.cornerRadius = 6
        customView.backgroundColor = UIColor.white
//        customView.layer.borderColor = borderColor?.cgColor
//        customView.layer.borderWidth = 1
//        customView.clipsToBounds=true
        
        let BrandImg = UIImageView(frame: CGRect(x: 5, y: 13, width: 84, height: 84))
        //BrandImg.image = UIImage(named: brandImgName!)
        let imgStr:NSString = "\(url.storeImg.imgPath())\(brandImgName!)" as NSString
        let charSet = CharacterSet.urlFragmentAllowed
        let urlStr : NSString = imgStr.addingPercentEncoding(withAllowedCharacters: charSet)! as NSString
        let URLString = URL.init(string: urlStr as String)
       // print(URLString!)
        BrandImg.kf.indicatorType = .activity
        BrandImg.kf.setImage(with: URLString!)
        BrandImg.layer.cornerRadius = 8
        BrandImg.clipsToBounds = true
        customView.addSubview(BrandImg)
        
        let storeLbl = UILabel(frame: CGRect(x: 97, y: 8, width: 125, height: 25))
        storeLbl.text = storeName
        storeLbl.numberOfLines = 2
        storeLbl.textAlignment = .left
        //storeLbl.lineBreakMode = NSLineBreakMode.byWordWrapping
        storeLbl.font=UIFont.systemFont(ofSize: 13)
        storeLbl.adjustsFontSizeToFitWidth = true
        customView.addSubview(storeLbl)
        
        let subStoreLbl = UILabel(frame: CGRect(x: 97, y: 29, width: 185, height: 21))
        subStoreLbl.text = subStoreName
        subStoreLbl.textAlignment = .left
        subStoreLbl.font=UIFont.systemFont(ofSize: 13)
        subStoreLbl.adjustsFontSizeToFitWidth = true
        subStoreLbl.textColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
        customView.addSubview(subStoreLbl)
        
        let storeDecLbl = UILabel(frame: CGRect(x: 97, y: 50, width: 175, height: 30))
        storeDecLbl.numberOfLines = 2
        storeDecLbl.text = storeDescription
        storeDecLbl.textAlignment = .left
        storeDecLbl.font=UIFont.systemFont(ofSize: 13)
        storeDecLbl.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        //storeDecLbl.adjustsFontSizeToFitWidth = true
        customView.addSubview(storeDecLbl)
        
        let serveLbl = UILabel(frame: CGRect(x: 115, y: 80.5, width: 125, height: 21))
        serveLbl.text = "can serve 1 to \(servePeople!) people"
        serveLbl.textAlignment = .left
        serveLbl.font=UIFont.systemFont(ofSize: 13)
        serveLbl.adjustsFontSizeToFitWidth = true
        serveLbl.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        serveLbl.minimumScaleFactor = 0.5
        customView.addSubview(serveLbl)
        
        let disctanceLbl = UILabel(frame: CGRect(x: 225, y: 8, width: 70, height: 21))
        disctanceLbl.text = "\(String(format: "%.2f", distance!))KM"
        disctanceLbl.textAlignment = .right
        disctanceLbl.font=UIFont.systemFont(ofSize: 13)
        disctanceLbl.adjustsFontSizeToFitWidth = true
        disctanceLbl.textColor = #colorLiteral(red: 0.4392156863, green: 0.6392156863, blue: 0.8862745098, alpha: 1)
        disctanceLbl.minimumScaleFactor = 0.5
        customView.addSubview(disctanceLbl)
        
        let eatImg = UIImageView(frame: CGRect(x: 97, y: 82, width: 15, height: 18))
        eatImg.image = UIImage(named: "eat")
        customView.addSubview(eatImg)
        
        let cardImg = UIImageView(frame: CGRect(x: 246, y: 84, width: 15.5, height: 14))
        cardImg.image = UIImage(named: "visa")
        customView.addSubview(cardImg)
        
        let cashImg = UIImageView(frame: CGRect(x: 266.5, y: 84, width: 15.5, height: 14))
        cashImg.image = UIImage(named: "visa")
        customView.addSubview(cashImg)
        
        
        let downArrowImg = UIImageView(frame: CGRect(x: 130, y: 90, width: 40, height: 40))
        downArrowImg.image = UIImage(named: "down white")
        customViewOne.addSubview(downArrowImg)
        
        
        customViewOne.addSubview(customView)
        self.addSubview(customViewOne)
        //self.addSubview(lbl)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
