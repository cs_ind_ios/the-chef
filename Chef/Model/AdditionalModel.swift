//
//  AdditionalModel.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit
import Foundation
import ObjectMapper

class AdditionlsModel: NSObject, NSCoding, Mappable {
    
    var successDic : AdditionalsDic?
    var status : Bool?
    var message : String?
    var messageAr:String?
    
    class func newInstance(map: Map) -> Mappable?{
        return AdditionlsModel()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        successDic <- map["Data"]
        status <- map["Status"]
        message <- map["Message"]
        messageAr <- map["MessageAr"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        successDic = aDecoder.decodeObject(forKey: "Data") as? AdditionalsDic
        status = aDecoder.decodeObject(forKey: "Status") as? Bool
        message = aDecoder.decodeObject(forKey: "Message") as? String
        messageAr = aDecoder.decodeObject(forKey: "MessageAr") as? String
    }
    @objc func encode(with aCoder: NSCoder) {
        successDic == nil ? aCoder.encode("", forKey: "Data"):aCoder.encode(successDic, forKey: "Data")
        status == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(status, forKey: "Status")
        message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(message, forKey: "Message")
        messageAr == nil ? aCoder.encode("", forKey: "MessageAr"):aCoder.encode(message, forKey: "MessageAr")
    }
}
class AdditionalsDic: NSObject,NSCoding,Mappable {
    
    var PricesAndAdditionals : [AdditionalItemsPrice]?
    class func newInstance(map: Map) -> Mappable?{
        return AdditionalsDic()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        PricesAndAdditionals <- map["PricesAndAdditionals"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        PricesAndAdditionals = aDecoder.decodeObject(forKey: "PricesAndAdditionals") as? [AdditionalItemsPrice]
    }
    @objc func encode(with aCoder: NSCoder) {
        PricesAndAdditionals == nil ? aCoder.encode("", forKey: "PricesAndAdditionals"):aCoder.encode(PricesAndAdditionals, forKey: "PricesAndAdditionals")
    }
}

class AdditionalItemsPrice: NSObject,NSCoding,Mappable {
    
    // var ItemSizePreiceDic : [AddItemsSizes]?
    var ItemSizesAndPrices : [Dictionary<String, Any>]?
    class func newInstance(map: Map) -> Mappable?{
        return AdditionalItemsPrice()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        //ItemSizePreiceDic <- map["ItemSizePrice"]
        ItemSizesAndPrices <- map["ItemSizePrice"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        //ItemSizePreiceDic = aDecoder.decodeObject(forKey: "ItemSizePrice") as? [AddItemsSizes]
        ItemSizesAndPrices = (aDecoder.decodeObject(forKey: "ItemSizePrice") as?  [Dictionary<String, Any>])!
    }
    @objc func encode(with aCoder: NSCoder) {
        //ItemSizePreiceDic == nil ? aCoder.encode("", forKey: "ItemSizePrice"):aCoder.encode(ItemSizePreiceDic, forKey: "ItemSizePrice")
        ItemSizesAndPrices == nil ? aCoder.encode("", forKey: "ItemSizePrice"):aCoder.encode(ItemSizesAndPrices, forKey: "ItemSizePrice")
    }
}

class AddItemsSizes: NSObject,NSCoding,Mappable{
    
    var ItemSizeName_En : String?
    var Itemprice : Double?
    var addgrp : [AddGroups]?
    var priceId:Int?

    class func newInstance(map: Map) -> Mappable?{
        return AddItemsSizes()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        ItemSizeName_En <- map["ItemSizeName_En"]
        Itemprice <- map["Itemprice"]
        addgrp <- map["addgrp"]
        priceId <- map["priceId"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        ItemSizeName_En = aDecoder.decodeObject(forKey: "ItemSizeName_En") as? String
        Itemprice = aDecoder.decodeObject(forKey: "Itemprice") as? Double
        addgrp = aDecoder.decodeObject(forKey: "addgrp") as? [AddGroups]
        priceId = aDecoder.decodeObject(forKey: "addgrp") as? Int
    }
    @objc func encode(with aCoder: NSCoder) {
        ItemSizeName_En == nil ? aCoder.encode("", forKey: "ItemSizeName_En"):aCoder.encode(ItemSizeName_En, forKey: "ItemSizeName_En")
        Itemprice == nil ? aCoder.encode("", forKey: "Itemprice"):aCoder.encode(Itemprice, forKey: "Itemprice")
        addgrp == nil ? aCoder.encode("", forKey: "addgrp"):aCoder.encode(addgrp, forKey: "addgrp")
        priceId == nil ? aCoder.encode("", forKey: "priceId"):aCoder.encode(priceId, forKey: "priceId")
    }
}

class AddGroups: NSObject,NSCoding,Mappable{
    
    var AddGrpName_En : String?
    var AddGrpId : Int?
    var additems : [AddDetails]?
    
    class func newInstance(map: Map) -> Mappable?{
        return AddGroups()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        AddGrpName_En <- map["AddGrpName_En"]
        AddGrpId <- map["AddGrpId"]
        additems <- map["additems"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        AddGrpName_En = aDecoder.decodeObject(forKey: "AddGrpName_En") as? String
        AddGrpId = aDecoder.decodeObject(forKey: "AddGrpId") as? Int
        additems = aDecoder.decodeObject(forKey: "additems") as? [AddDetails]
    }
    @objc func encode(with aCoder: NSCoder) {
        AddGrpName_En == nil ? aCoder.encode("", forKey: "AddGrpName_En"):aCoder.encode(AddGrpName_En, forKey: "AddGrpName_En")
        AddGrpId == nil ? aCoder.encode("", forKey: "AddGrpId"):aCoder.encode(AddGrpId, forKey: "AddGrpId")
        additems == nil ? aCoder.encode("", forKey: "additems"):aCoder.encode(additems, forKey: "additems")
    }
}

class AddDetails: NSObject,NSCoding,Mappable{
    
    var AddtionalName_en : String?
    var AdditionalId : Int?
    var addprice : [AddPrice]?

    class func newInstance(map: Map) -> Mappable?{
        return AddDetails()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        AddtionalName_en <- map["AddtionalName_en"]
        AdditionalId <- map["AdditionalId"]
        addprice <- map["addprice"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        AddtionalName_en = aDecoder.decodeObject(forKey: "AddtionalName_en") as? String
        AdditionalId = aDecoder.decodeObject(forKey: "AdditionalId") as? Int
        addprice = aDecoder.decodeObject(forKey: "addprice") as? [AddPrice]
    }
    @objc func encode(with aCoder: NSCoder) {
        AddtionalName_en == nil ? aCoder.encode("", forKey: "AddtionalName_en"):aCoder.encode(AddtionalName_en, forKey: "AddtionalName_en")
        AdditionalId == nil ? aCoder.encode("", forKey: "AdditionalId"):aCoder.encode(AdditionalId, forKey: "AdditionalId")
        addprice == nil ? aCoder.encode("", forKey: "addprice"):aCoder.encode(addprice, forKey: "addprice")
    }
}
class AddPrice: NSObject,NSCoding,Mappable{
    var price : Double?
    
    class func newInstance(map: Map) -> Mappable?{
        return AddPrice()
    }
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        price <- map["addprice"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        price = aDecoder.decodeObject(forKey: "addprice") as? Double
    }
    @objc func encode(with aCoder: NSCoder) {
        price == nil ? aCoder.encode("", forKey: "addprice"):aCoder.encode(price, forKey: "addprice")
    }
}

class GetItemSizesAndAdditionals {
    var priceId : Int?
    var ItemSizeName_En : String?
    var ItemSizeName_Ar : String?
    var Itemprice : Double?
    var isSelect : Bool?
    
    //var AdditionalGroup : [GetAdditionalGroup]!
    init(priceId: Int, ItemSizeName_En : String, ItemSizeName_Ar : String, Itemprice : Double, isSelect : Bool) {
        self.priceId = priceId
        self.ItemSizeName_En = ItemSizeName_En
        self.ItemSizeName_Ar = ItemSizeName_Ar
        self.Itemprice = Itemprice
        self.isSelect = isSelect
    }
}

class GetAdditionalGroup {
    var PriceId : Int?
    var AddGrpId : Int?
    var AddGrpName_En : String?
    var AddGrpName_Ar : String?
    var MinSelection : Int?
    var MaxSelection : Int?
    var getAdditionls : [getAdditionls]!
    init(PriceId:Int, AddGrpId: Int, AddGrpName_En : String, AddGrpName_Ar : String,  MinSelection : Int, MaxSelection : Int, getAdditionls : [getAdditionls]) {
        self.PriceId = PriceId
        self.AddGrpId = AddGrpId
        self.AddGrpName_En = AddGrpName_En
        self.AddGrpName_Ar = AddGrpName_Ar
        self.MinSelection = MinSelection
        self.MaxSelection = MaxSelection
        self.getAdditionls = getAdditionls
    }
}

class getAdditionls{
    var AdditionalId : Int?
    var AddtionalName_en : String?
    var AddtionalName_Ar : String?
    var Addprice : Double?
    var isSelect : Bool?
    var MinSelection : Int?
    var MaxSelection : Int?
    var Quantity : Int?
    var AddTotalPrice : Double?
    
    init(AdditionalId: Int, AddtionalName_en : String, AddtionalName_Ar : String, Addprice : Double, isSelect : Bool, MinSelection : Int, MaxSelection : Int, Quantity : Int, AddTotalPrice : Double) {
        self.AdditionalId = AdditionalId
        self.AddtionalName_en = AddtionalName_en
        self.AddtionalName_Ar = AddtionalName_Ar
        self.Addprice = Addprice
        self.isSelect = isSelect
        self.MinSelection = MinSelection
        self.MaxSelection = MaxSelection
        self.Quantity = Quantity
        self.AddTotalPrice = AddTotalPrice
    }
}
