//
//  SaveAddress.swift
//  Chef
//
//  Created by RAVI on 19/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import UIKit
import ObjectMapper

class SaveAddress: NSObject,NSCoding, Mappable {
    
    var successDic : [Dictionary<String, Any>]?
    var status : Bool?
    var message : String?
    var messageAr:String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return SaveAddress()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map) {
       // print(map["Status"])
        //print(map["Data"])
        successDic <- map["Data"]
        status <- map["Status"]
        message <- map["Message"]
        messageAr <- map["MessageAr"]
        
    }
    @objc required init(coder aDecoder: NSCoder) {
        
        successDic = aDecoder.decodeObject(forKey: "Data") as? [Dictionary<String, Any>]
        status = aDecoder.decodeObject(forKey: "Status") as? Bool
        message = aDecoder.decodeObject(forKey: "Message") as? String
        messageAr = aDecoder.decodeObject(forKey: "MessageAr") as? String
        
    }
    func encode(with aCoder: NSCoder) {
        
        successDic == nil ? aCoder.encode("", forKey: "Data"):aCoder.encode(successDic, forKey: "Data")
        status == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(status, forKey: "Status")
        message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(message, forKey: "Message")
        messageAr == nil ? aCoder.encode("", forKey: "MessageAr"):aCoder.encode(message, forKey: "MessageAr")
        
        
    }
}

class StaticAdd{
    
    static var addDetails = [String:Any]()
}
