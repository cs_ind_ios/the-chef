//
//  LocationSearchResult.swift
//  Chef
//
//  Created by Devbox on 18/12/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation

class LocationSearchResult {
    
    var mainText,secondaryText,id: String!
    
    init(mainText: String,secondaryText: String,id: String) {
        self.mainText = mainText
        self.secondaryText = secondaryText
        self.id = id
    }
}
