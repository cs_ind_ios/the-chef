//
//  StoreInformationModel.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import Foundation
import ObjectMapper

class StoreInfromationModel: NSObject,NSCoding, Mappable {
    
    var successDic :StoreInfoDic?
    var status : Bool?
    var message : String?
    var messageAr :String?
    class func newInstance(map: Map) -> Mappable?{
        return StoreInfromationModel()
    }
    
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        successDic <- map["Data"]
        status <- map["Status"]
        message <- map["Message"]
        messageAr <- map["MessageAr"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        successDic = aDecoder.decodeObject(forKey: "Data") as? StoreInfoDic
        status = aDecoder.decodeObject(forKey: "Status") as? Bool
        message = aDecoder.decodeObject(forKey: "Message") as? String
        messageAr = aDecoder.decodeObject(forKey: "MessageAr") as? String
    }
    @objc func encode(with aCoder: NSCoder) {
        successDic == nil ? aCoder.encode("", forKey: "Data"):aCoder.encode(successDic, forKey: "Data")
        status == nil ? aCoder.encode("", forKey: "Status"):aCoder.encode(status, forKey: "Status")
        message == nil ? aCoder.encode("", forKey: "Message"):aCoder.encode(message, forKey: "Message")
        messageAr == nil ? aCoder.encode("", forKey: "MessageAr"):aCoder.encode(messageAr, forKey: "MessageAr")
        
    }
}
class StoreInfoDic: NSObject,NSCoding,Mappable {
    
    var StoresDetails : [Dictionary<String, Any>]?
    var StoreCategories : [Dictionary<String, Any>]?
    var FilterCategories : [Dictionary<String, Any>]?
    var BannerDetails : [Dictionary<String, Any>]?
    var VendorTypes : [Dictionary<String, Any>]?
    var TotalRows : Int?
    
    class func newInstance(map: Map) -> Mappable?{
        return StoreInfoDic()
    }
    
    required init?(map: Map){}
    private override init(){}
    func mapping(map: Map) {
        StoresDetails <- map["StoresDetails"]
        StoreCategories <- map["StoreCategories"]
        FilterCategories <- map["FilterCategory"]
        BannerDetails <- map["BannerResult"]
        VendorTypes <- map["VendorTypes"]
        TotalRows <- map["TotalRows"]
    }
    @objc required init(coder aDecoder: NSCoder) {
        StoresDetails = aDecoder.decodeObject(forKey: "StoresDetails") as? [Dictionary<String, Any>]
        StoreCategories = aDecoder.decodeObject(forKey: "StoreCategories") as? [Dictionary<String, Any>]
        FilterCategories = aDecoder.decodeObject(forKey: "FilterCategory") as? [Dictionary<String, Any>]
        BannerDetails = aDecoder.decodeObject(forKey: "BannerResult") as? [Dictionary<String, Any>]
        VendorTypes = aDecoder.decodeObject(forKey: "VendorTypes") as? [Dictionary<String, Any>]
        TotalRows = aDecoder.decodeObject(forKey: "VendorTypes") as? Int
    }
    
    @objc func encode(with aCoder: NSCoder) {
        StoreCategories == nil ? aCoder.encode("", forKey: "StoreCategories"):aCoder.encode(StoreCategories, forKey: "StoreCategories")
        StoresDetails == nil ? aCoder.encode("", forKey: "StoresDetails"):aCoder.encode(StoresDetails, forKey: "StoresDetails")
        FilterCategories == nil ? aCoder.encode("", forKey: "FilterCategory"):aCoder.encode(FilterCategories, forKey: "FilterCategory")
        BannerDetails == nil ? aCoder.encode("", forKey: "BannerResult"):aCoder.encode(BannerDetails, forKey: "BannerResult")
        VendorTypes == nil ? aCoder.encode("", forKey: "VendorTypes"):aCoder.encode(VendorTypes, forKey: "VendorTypes")
        TotalRows == nil ? aCoder.encode("", forKey: "TotalRows"):aCoder.encode(TotalRows, forKey: "TotalRows")
    }
    
}
class GetBanners{
    
    var BannerName_En:String?
    var BannerName_Ar:String?
    var BannerImage:String?
    var StoreDetails:[GetStores]!
    init(BannerName_En:String, BannerName_Ar:String, BannerImage:String, StoreDetails:[GetStores]) {
        self.BannerName_En = BannerName_En
        self.BannerName_Ar = BannerName_Ar
        self.BannerImage = BannerImage
        self.StoreDetails = StoreDetails
        
    }
    
}
class GetStores{
    var AvgPreparationTime:Int?
    var BranchId:Int?
    var BranchName_En:String?
    var BranchName_Ar:String?
    var DeliveryCharges:Double?
    var Address:String?
    var MinOrderCharges:Double?
    
    var EmailId:String?
    var MobileNo:String?
    var OrderTypes:String?
    
    var Rating:String?
    var RatingCount:Int?
    
    var StoreLogo_En:String?
    var StoreImage_En:String?
    
    var StoreStatus:String?
    var StoreType:String?
    
    var TakeAwayDistance:Int?
    var DeliveryDistance:Double?
    
    var StarDateTime:String?
    var EndDateTime:String?
    var CurrentDateTime : String?
    
    var IsPromoted:Bool?
    var Popular:Bool?
    var DiscountAmt:Int?
    var IsNew:Bool?
    
    var Latitude:Double?
    var Longitude:Double?
    var Distance:Double?
    
    var BrandId : Int?
    var BindingCount : Int?
    var BrandName_En:String?
    var BrandName_Ar:String?
    var PaymentType:String?
    var BranchDescription_En:String?
    var StoreType_Ar:String?
    var BranchDescription_Ar:String?
    
    var Serving:Int?
    var FutureOrderDay:Int?
    
    var UpdatesAvailable : Int?
    var UpdateSeverity : Int?
    
    var VatPercentage:Double?
    
    
    init(AvgPreparationTime:Int, BranchId:Int, BranchName_En:String, BranchName_Ar:String, DeliveryCharges:Double, Address:String, MinOrderCharges:Double, EmailId:String, MobileNo:String, OrderTypes:String, Rating:String, RatingCount:Int, StoreLogo_En:String, StoreImage_En:String, StoreStatus:String, StoreType:String, TakeAwayDistance:Int, DeliveryDistance:Double, StarDateTime:String, EndDateTime:String, CurrentDateTime:String, IsPromoted:Bool, Popular:Bool, DiscountAmt:Int, IsNew:Bool, Latitude:Double, Longitude:Double, Distance:Double, BrandId:Int, BindingCount : Int, BrandName_En:String, BrandName_Ar:String, PaymentType:String,BranchDescription_En:String,Serving:Int,FutureOrderDay:Int,StoreType_Ar:String,BranchDescription_Ar:String, UpdatesAvailable:Int, UpdateSeverity:Int, VatPercentage:Double) {
        
        self.AvgPreparationTime = AvgPreparationTime
        self.BranchId = BranchId
        self.BranchName_En = BranchName_En
        self.BranchName_Ar = BranchName_Ar
        self.DeliveryCharges = DeliveryCharges
        self.Address = Address
        
        self.MinOrderCharges = MinOrderCharges
        self.EmailId = EmailId
        self.MobileNo = MobileNo
        self.OrderTypes = OrderTypes
        self.Rating = Rating
        self.RatingCount = RatingCount
        
        self.StoreLogo_En = StoreLogo_En
        self.StoreImage_En = StoreImage_En
        
        self.StoreStatus = StoreStatus
        self.StoreType = StoreType
        
        self.TakeAwayDistance = TakeAwayDistance
        self.DeliveryDistance = DeliveryDistance
        
        self.StarDateTime = StarDateTime
        self.EndDateTime = EndDateTime
        self.CurrentDateTime = CurrentDateTime
        
        self.IsPromoted = IsPromoted
        self.Popular = Popular
        self.DiscountAmt = DiscountAmt
        self.IsNew = IsNew
        
        self.Latitude = Latitude
        self.Longitude = Longitude
        self.Distance = Distance
        
        self.BrandId = BrandId
        self.BindingCount = BindingCount
        self.BrandName_En = BrandName_En
        self.BrandName_Ar = BrandName_Ar
        self.PaymentType = PaymentType
        self.BranchDescription_En = BranchDescription_En
        self.BranchDescription_Ar = BranchDescription_Ar
        self.StoreType_Ar = StoreType_Ar
        self.Serving = Serving
        self.FutureOrderDay = FutureOrderDay
        
        self.UpdateSeverity = UpdateSeverity
        self.UpdatesAvailable = UpdatesAvailable
        
        self.VatPercentage = VatPercentage
    }
    
}
class GetStoresCategory{
    var StoreCatId:Int?
    var CatetgoryName_En:String?
    var CatetgoryName_Ar:String?
    var Description_En:String?
    var Description_Ar:String?
    var Image_En:String?
    var Image_Ar:String?
    
    init(StoreCatId:Int, CatetgoryName_En:String, CatetgoryName_Ar:String, Description_En:String, Description_Ar:String, Image_En:String, Image_Ar:String) {
        self.StoreCatId = StoreCatId
        self.CatetgoryName_En = CatetgoryName_En
        self.CatetgoryName_Ar = CatetgoryName_Ar
        self.Description_En = Description_En
        self.Description_Ar = Description_Ar
        self.Image_En = Image_En
        self.Image_Ar = Image_Ar
    }
}
class GetFilterCategory{
    var FilterTypeId:Int?
    var FilterTypeName_En:String?
    var FilterTypeName_Ar:String?
    var Filters:[GetAdditionalFilters]!
    
    init(FilterTypeId:Int, FilterTypeName_En:String, FilterTypeName_Ar:String, Filters:[GetAdditionalFilters]) {
        self.FilterTypeId = FilterTypeId
        self.FilterTypeName_En = FilterTypeName_En
        self.FilterTypeName_Ar = FilterTypeName_Ar
        self.Filters = Filters
    }
}
class GetAdditionalFilters{
    var FilterId:Int?
    var FilterName_En:String?
    var FilterName_Ar:String?
    var Status:Bool

    init (FilterId:Int, FilterName_En:String, FilterName_Ar:String, Status:Bool){
        self.FilterId = FilterId
        self.FilterName_En = FilterName_En
        self.FilterName_Ar = FilterName_Ar
        self.Status = Status
    }
}
class GetVendorTypes{
    var Id:Int?
    var Name_En:String?
    var Name_Ar:String?
    var Status:Bool

    init (Id:Int, Name_En:String, Name_Ar:String, Status:Bool){
        self.Id = Id
        self.Name_En = Name_En
        self.Name_Ar = Name_Ar
        self.Status = Status
    }
}
