//
//  SaveAddressClass.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
class SaveAddressClass{
    static var saveAddressDic = [String:Any]()
    static var getAddressArray = [Address]()
    
    // Stores
    static var getStoresArray = [GetStores]()
    static var getStoreBannerArray = [GetBanners]()
    static var getStorePromotedArray = [GetStores]()
    static var getStorePopularArray = [GetStores]()
    static var getStoreAround3KmArray = [GetStores]()
    static var getStoreNewArray = [GetStores]()
    static var selectStoreDic = [String:Any]()
    static var selectStoreArray = [GetStores]()
    static var selectBannerArray = [GetBanners]()
    static var selectBannersStoreArray = [GetStores]()
    
    static var getSaveStoreArray = [GetStores]()
    static var getBranchDetailsArray = [GetStores]()
    
        // Store Catergory
    static var getStoreCatArray = [GetStoresCategory]()
    static var SelectSearchCatDic = [String:Any]()
    
      // Menu
    static var getMenuDetails = [GetCategoryMenu]()
    static var getSelectItemDetails = [ItemDetails]()
    static var getItemSize = [GetItemSizesAndAdditionals]()
    static var getAdditionals = [GetAdditionalGroup]()
    
    // FilterCategories
    static var getFilterCategories = [FilterCategories]()
    static var getFilters = [GetFilterCategory]()
    static var getVendorTypes = [GetVendorTypes]()
    
    // Search
    static var getSearchStoresArray = [GetStores]()
    
    // Order
    static var getOrderArray = [OrderTable]()
    static var getAdditionalsArray = [AdditionalTable]()
    static var getOrderHistoryArray = [OrderHistory]()
    static var getOrderTrackingMainArray = [OrderMainModel]()
    static var getOrderTrackingItemsArray = [OrderItemsModel]()
    static var getActiveOrderArray = [ActiveOrderModelArray]()
    
    //Promotions
    static var getPromotionsArray = [GetPromotions]()
    
    
}
