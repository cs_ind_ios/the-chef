//
//  ModelController.swift

//This class belongs to contain common methods.
//In this way we will reduce writing of reusable code.

import UIKit

class ModelController: NSObject {
  
   
    
//Method for get Build Version and Build Number
    class func methodForApplicationVersion() ->String{
        
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
        let version = nsObject as! String
        
        let buildNumber: AnyObject? =  Bundle.main.infoDictionary?["CFBundleVersion"] as AnyObject?
        let number = buildNumber as! String

        let stringApplicationVersion = "v"+version+"("+number+")"
        return stringApplicationVersion
    }
    

    
//Method for show alert into this aaplication
    class func showAlert(_ alertTitle: String, andMessage alertMessage: String, withController view: UIViewController){
        
        let alert: UIAlertView = UIAlertView(title:alertTitle, message:alertMessage, delegate:nil, cancelButtonTitle:nil, otherButtonTitles:"OK")
        
        alert.show()
    }
    
//Method for check validation of email
    class func validateEmail(_ strEmail: String) -> Bool {
        //Regular Expresions
        let kRegexEmailValidate = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", kRegexEmailValidate)
        if emailTest.evaluate(with: strEmail) == false {
            return false
        }
        else {
            return true
        }
    }

//Method for change StatusBar Color
    class func methodStatusBarColorChange() {
        
        UIApplication.shared.isStatusBarHidden =  false
        let statusBar:UIView = UIApplication.shared.value(forKey: "statusBar")as!UIView
            statusBar.backgroundColor = UIColor(red: 34.0/255.0, green: 72.0/255.0, blue: 95.0/255.0, alpha: 1.0)
    }
    
}
