//
//  Validations.swift
//  CheckClick Vendor
//
//  Created by Creative Solutions on 12/21/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

class Validations: NSObject {
    static func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    static func isPasswordValid(password : String) -> Bool{
        if (password.count >= 4) && (password.count <= 15){
            return true
            }
        return false
    }
    static func isNameValid(name : String) -> Bool{
        if (name.count >= 2) && (name.count <= 50){
            return true;
        }
        return false;
    }
    static func isCharCount(name : String) -> Bool{
        if (name.count >= 2) && (name.count <= 50){
            return true
        }
        return false
    }
    static func isZipCount(zip : String) -> Bool{
        if zip.count == 6{
            return true
        }
        return false
    }
    static func isOTPValid(otp : String) -> Bool{
        if otp.count == 4{
            return true
        }
        return false
    }
    static func isMobileNumberValid(mobile : String) -> Bool{
        if mobile.count == 9{
            return true
        }
        return false
    }
    static func isMobileNoValid(mobile : String, country : String) -> Bool{
        
        if country == "+973" {
          if mobile.count == 8{
                return true
           }
        }else{
            if mobile.count == 9{
               return true
            }
        }
        return false
    }
    static func isMobileNumberValidWithCountryCode(mobile : String) -> Bool{
        if mobile.count == 14{
            return true
        }
        return false
    }
    static func isMobileNumberAddressValid(mobile : String) -> Bool{
        if mobile.count >= 9 ||  mobile.count <= 12{
            return true
        }
        return false
    }
    static func isSTCMobileNumberValid(mobile : String) -> Bool{
        if mobile.count == 12{
            return true
        }
        return false
    }
}
