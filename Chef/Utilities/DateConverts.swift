//
//  DateConverts.swift
//  CheckClick Vendor
//
//  Created by Creative Solutions on 1/2/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
/// Date Format type
enum DateFormatType: String {
    case time = "HH:mm:ss"
    case date = "dd-MM-yyyy"
    case dateR = "yyyy-MM-dd"
    case ddMMM = "dd MMM"
    case ddMMMYYYY = "dd MMM YYYY"
    case timeA = "hh:mm a"

    case yy = "yy"

    /// Date with hours
    case dateNTimeA = "dd-MM-yyyy hh:mm a"
    case dateNTimeSS = "yyyy-MM-dd'T'Hh:mm:ss"

}

class DateConverts: NSObject {
    class func convertDateToString(date: Date, dateformatType:DateFormatType) -> String{
        let date = Date()
        let dateformat = DateFormatter()
        dateformat.dateFormat = dateformatType.rawValue
        dateformat.locale = Locale(identifier: "EN")
        let dateStr = dateformat.string(from: date)
       // print(dateStr)
        return dateStr
    }
    class func convertStringToDate(date: String, dateformatType:DateFormatType) -> Date{
        let dateformat = DateFormatter()
        dateformat.dateFormat = dateformatType.rawValue
        dateformat.locale = Locale(identifier: "EN")
        return dateformat.date(from: date)! 
    }
    class func convertStringToStringDates(inputDateStr: String, inputDateformatType:DateFormatType, outputDateformatType:DateFormatType) -> String{
        let dateformat = DateFormatter()
        //Input
        dateformat.dateFormat = inputDateformatType.rawValue
        dateformat.locale = Locale(identifier: "EN")
        let inputDate = dateformat.date(from: inputDateStr)
        //Output
        dateformat.dateFormat = outputDateformatType.rawValue
        dateformat.locale = Locale(identifier: "EN")
        return dateformat.string(from: inputDate!)
    }
    class func convertddMMMYYFormate(inputDateStr: String, inputDateformatType:DateFormatType) -> String{
        let dateformat = DateFormatter()
        //Input
        dateformat.dateFormat = inputDateformatType.rawValue
        dateformat.locale = Locale(identifier: "EN")
        let inputDate = dateformat.date(from: inputDateStr)
        //Output
        dateformat.dateFormat = "dd MMM"
        
        let dateformat1 = DateFormatter()
        dateformat1.dateFormat = "yy"

        
        return "\(dateformat.string(from: inputDate!))'\(dateformat1.string(from: inputDate!))"
    }
}
