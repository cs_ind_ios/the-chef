//
//  CustomButton.swift
//  CheckClick Vendor
//
//  Created by Creative Solutions on 12/20/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

@IBDesignable
class CustomButton: UIButton {
    @IBInspectable var borderWidth : CGFloat = 0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor : UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var cornerRadis : CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadis
            self.layer.masksToBounds = true
        }
    }
    
    @IBInspectable var shadow : Bool = false{
        didSet{
            if shadow == true{
                self.layer.shadowPath =
                    UIBezierPath(roundedRect: self.bounds,
                                 cornerRadius: self.layer.cornerRadius).cgPath
                self.layer.shadowColor = UIColor.gray.cgColor
                self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                self.layer.shadowOpacity = 1.0
                self.layer.shadowRadius = 1.0
                self.layer.masksToBounds = false
            }
        }
    }
}
