//
//  Constants.swift
//  CheckClick Vendor
//
//  Created by Creative Solutions on 12/19/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation

let Defaults = UserDefaults.standard
struct UserDefaultsKeys {
    static let APP_OPENED_COUNT = "APP_OPENED_COUNT"
}
// Text
let Title = "The Chef"
let Cancel = "Cancel"
let Ok = "Ok"
let Logout = "Logout"

// Font
let FontBook = "Gotham-Book"
let FontMedium = "Gotham-Medium"
let FontHelveticaBold = "Helvetica"

// Signup
let Email = "Please enter Email"
let EmailValidation = "Please use Email format (example - abc@abc.com)"
let YourName = "Please enter Name"
let ValidCharacters = "Should be 2 - 50 characters"

// Password
let Password = "Please enter Password"
let OldPassword = "Please enter Old Password"
let ConfirmPassword = "Please enter Confirm Password"
let VerifyPassword = "Password not matched, Please enter correct password"
let PasswordValidation = "Password must be 4 - 8 characters"
let NewPassword = "Please enter New Password"
let InvalidPassword = "Invalid old Password!"
let PasswordSuccess = "Password changed Successfully"
let RetypePassword = "Please enter Retype Password"
let RetypePasswordValid  = "Please enter Correct Re-Type Password"

// Phone Number
let PhoneNo = "Please enter phone number"
let ValidPhoneNo = "Please enter valid phone number"
let ActivationCode = "Please enter Activation Code"
let IvalidActiveCode = "Please enter valid OTP"

//Seetings
let S_CP = "Change Password"
let S_CPN = "Change Phone Number"
let S_CEM = "Change E-Mail"
let S_CL = "Change Language"
let S_PN = "Push Notification"
let S_TC = "Terms and Conditions"
let S_logout = "Logout"
let PNHeader = "Receive this notification on this device for the following"
let NewEmailHeader = "By verifiing a new e-mail address, %@ will no longer be associated with your account"
let NewPhoneNoHeader = "By verifiing a new phone number, %@ will no longer be associated with your account"

let internetFail = "No Internet"
let HouseNo = "Please enter house/flat no"
let Landmark = "Please enter landmark"
let AddressName = "Please enter address name"
let Login = "Please Login into your Account"
let Name = "Please enter your Name"
let Mobile = "Please enter Mobile Number"
let FullAddress = "Please enter your Address"
let City = "Please enter your City"




//Arabic

// Text
let Title_Ar = "تطبيق الشيف"
let Cancel_Ar = "إلغاء"
let Ok_Ar = "تم"
let Logout_Ar = "تسجيل خروج"

// Signup
let Email_Ar = "الرجاء ادخال البريد الالكتروني"
let EmailValidation_Ar = "الرجاء اختيار صيغة البريد الالكتروني. مثال ( abc@abc.com)"
let YourName_Ar = "الرجاء ادخال اسم"
let ValidCharacters_Ar = "يجب أن تكون من ٢ - ٥٠ حرف"

// Password
let Password_Ar = "الرجاد ادخال كلمة المرور"
let ConfirmPassword_Ar = "الرجاء إدخال تأكيد كلمة المرور"
let VerifyPassword_Ar = "كلمة المرور غير مطابقة، الرجاء ادخال كلمة مرور صحيحة"
let PasswordValidation_Ar = "يجب أن تكون كلمة المرور من ٤ - ٨ أحرف"
let NewPassword_Ar = "الرجاء ادخال كلمة المرور الجديدة"
let OldPassword_Ar = "الرجاء ادخال كلمة المرور القديمة"
let InvalidPassword_Ar = "Invalid old Password!"
let PasswordSuccess_Ar = "تم تغيير كلمة المرور بنجاح"
let RetypePassword_Ar = "الرجاء إدخال كلمة المرور المعاد كتابتها"
let RetypePasswordValid_Ar  = "الرجاء ادخال كلمة المرور المعاد كتابتها بشكل صحيح"

// Phone Number
let PhoneNo_Ar = "الرجاء ادخال رقم الهاتف"
let ValidPhoneNo_Ar = "الرجاد ادخال رقم هاتف صحيح"
let ActivationCode_Ar = "الرجاء ادخال رمز التفعيل"
let IvalidActiveCode_Ar = "الرجاء ادخال كلمة المرور الصالحه لمره واحده بشكل صحيح"


let internetFail_Ar = "لا يوجد إتصال بالشبكة"
let HouseNo_Ar = "الرجاء ادخال رقم المنزل/الشقة"
let Landmark_Ar = "الرجاء ادخال معلم معروف"
let AddressName_Ar = "الرجاد ادخال اسم العنوان"
let Login_Ar = "Please Login into your Account"
let Name_Ar = "Please enter your Name"
let Mobile_Ar = "Please enter Mobile Number"
let FullAddress_Ar = "Please enter your Address"
let City_Ar = "Please enter your City"

