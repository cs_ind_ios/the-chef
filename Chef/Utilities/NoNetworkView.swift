//
//  NoNetworkView.swift
//  Werkabee
//
//  Created by Kirti Rai on 21/08/17.
//  Copyright © 2017 Flexsin. All rights reserved.
//

import UIKit

protocol NoNetworkViewDelegate
{
    func TryAgain()
}

class NoNetworkView: UIView {

    var delegate : NoNetworkViewDelegate?
    
    @IBAction func tryagainBtnClicked(_ sender: Any) {
        
        if let temp = self.delegate
        {
            //print(temp)
            delegate?.TryAgain()
        }else{
            print("optional value contains nill value")
        }
    }
}
