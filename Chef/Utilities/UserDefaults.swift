//
//  UserDefaults.swift
//  CheckClick Vendor
//
//  Created by Creative Solutions on 1/2/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit

func isUserLogIn() -> Bool {
    return UserDefaults.standard.value(forKey: "UserId") == nil ? false : true
}

func isSelectCategory() -> Bool {
    return UserDefaults.standard.value(forKey: "Category") == nil ? false : true
}
func isLocationArray() -> Bool {
    return UserDefaults.standard.value(forKey: "LocationDetails") == nil ? false : true
}
func isDeviceToken() -> Bool {
    return UserDefaults.standard.value(forKey: "DeviceToken") == nil ? false : true
}
func getDeviceLanguage() -> String {
    var language = Bundle.main.preferredLocalizations[0]
    language = language.components(separatedBy: "-")[0]
    if (language == "en") {
         return "English"
    }
    return "Arabic"
}

// remove from UserDefault
func removeFromUserDefaultForKey(key:String){
    UserDefaults.standard.removeObject(forKey: key)
    UserDefaults.standard.synchronize()
    
}
