//
//  CustomImageView.swift
//  CheckClick Vendor
//
//  Created by Creative Solutions on 7/10/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class ImageCornerRectView: UIImageView {
    @IBInspectable var cornerRadius: CGFloat = 2
    @IBInspectable var borderWidth: CGFloat = 0.0
    @IBInspectable var borderColor: UIColor? = UIColor.lightGray
    @IBInspectable var BGColor: Bool = true
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor?.cgColor
        layer.borderWidth = borderWidth
        layer.masksToBounds = true
        if BGColor == true{
           backgroundColor = UIColor.hexColor(000000, aplpha: 0.08)
        }
        
    }
    
}


@IBDesignable
class ImageTopCornerRectView: UIImageView {
    @IBInspectable var cornerRadius: CGFloat = 2
    @IBInspectable var borderWidth: CGFloat = 0.0
    @IBInspectable var borderColor: UIColor? = UIColor.lightGray
    @IBInspectable var BGColor: Bool = true
    override func layoutSubviews() {
        //layer.cornerRadius = cornerRadius
        layer.borderColor = borderColor?.cgColor
        layer.borderWidth = borderWidth
        layer.masksToBounds = true
        if BGColor == true{
            self.layer.cornerRadius = CGFloat(cornerRadius)
            self.clipsToBounds = true
            self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            backgroundColor = UIColor.hexColor(000000, aplpha: 0.08)
        }
        
    }
    
}
