//
//  UIColorExt.swift
//  CoffeeStation
//
//  Created by Creative Solutions on 7/26/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

extension UIColor{
    
    class var lightRedColor: UIColor {
        return self.init(red: 242.0/255.0, green: 62.0/255.0, blue: 89.0/255.0, alpha: 1.0)
    }
    class var lightBackColor: UIColor {
        return self.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.5)
    }
    class var DarkGreen: UIColor {
        return self.init(red: 34.0/255.0, green: 72.0/255.0, blue: 95.0/255.0, alpha: 1.0)
    }
    
    class var BlueColor: UIColor {
        return self.init(red: 42.0/255.0, green: 163.0/255.0, blue: 251.0/255.0, alpha: 1.0)
    }
    
    class var LigtGrayColor: UIColor {
        return self.init(red: 218.0/255.0, green: 218.0/255.0, blue: 221.0/255.0, alpha: 1.0)
    }
    class var Background: UIColor {
        return self.hexColor(0xfdff54, aplpha: 1.0)
    }
    
    class func hexColor(_ hexColorNumber:UInt32, aplpha:CGFloat) -> UIColor {
        let red = (hexColorNumber & 0xFF0000) >> 16
        let green = (hexColorNumber & 0x00FF00) >> 8
        let blue = (hexColorNumber & 0x0000FF)
        
        return UIColor (red: CGFloat(red)/255, green: CGFloat(green)/255, blue: CGFloat(blue)/255, alpha: aplpha)
        
    }
    
     class func hexStringToUIColor (hex:String, aplpha:CGFloat) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: aplpha
        )
    }
}


