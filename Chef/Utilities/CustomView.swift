//
//  CustomView.swift
//  CheckClick Vendor
//
//  Created by Creative Solutions on 12/20/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import UIKit

@IBDesignable
class CustomView: UIView {
        @IBInspectable var borderWidth : CGFloat = 0{
            didSet{
                self.layer.borderWidth = borderWidth
                
            }
        }
        @IBInspectable var borderColor : UIColor = UIColor.clear{
            didSet{
                self.layer.borderColor = borderColor.cgColor
            }
        }
        @IBInspectable var cornerRadis : CGFloat = 0{
            didSet{
                self.layer.cornerRadius = cornerRadis
                self.layer.masksToBounds = true
            }
        }
        
        @IBInspectable var shadow : Bool = false{
            didSet{
                if shadow == true{
                    
                    self.layer.shadowPath =
                        UIBezierPath(roundedRect: self.bounds,
                                     cornerRadius: self.layer.cornerRadius).cgPath

                    self.layer.shadowColor = UIColor.black.cgColor
                    self.layer.shadowOpacity = 0.4
                    self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
                    self.layer.shadowRadius = 1
                    self.layer.masksToBounds = false
                }
            }
        }
}

@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}

@IBDesignable
class CardViewAllSides: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    @IBInspectable var shadowSize: CGFloat = 0.5
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: bounds.width + shadowSize,
                                                   height: self.bounds.height + shadowSize))
        
        //let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        
        
    }
    
}

@IBDesignable
class HeaderCardView: UIView {
    
    @IBInspectable var shadowSize: CGFloat = 3.0
    @IBInspectable var shadowColor: UIColor? = UIColor.lightGray
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        let shadowPath = UIBezierPath(rect: CGRect(x: -2,
                                                   y: self.bounds.height/2,
                                                   width: bounds.width+4,
                                                   height: self.bounds.height/2 + shadowSize))
        
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        
    }
    
}
@IBDesignable
class TopCardView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 0.0
    @IBInspectable var shadowSize: CGFloat = 2.0
    @IBInspectable var shadowColor: UIColor? = UIColor.lightGray
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        let shadowPath = UIBezierPath(rect: CGRect(x: -2,
                                                   y: -shadowSize,
                                                   width: bounds.width+4,
                                                   height: self.bounds.height/2))
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
        layer.cornerRadius = cornerRadius
        
    }
    
}

