//
//  OrderHistoryCell.swift
//  Chef
//
//  Created by RAVI on 30/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell {

    
    @IBOutlet weak var brandLbl: UILabel!
    @IBOutlet weak var expDateTimeLbl: UILabel!
    @IBOutlet weak var deliveryStatusLbl: UILabel!
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var brandImg: UIImageView!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var ratingWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
