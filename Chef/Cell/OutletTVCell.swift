//
//  OutletTVCell.swift
//  Chef
//
//  Created by RAVI on 22/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class OutletTVCell: UITableViewCell {

    @IBOutlet weak var storeNameLbl: UILabel!
    
    @IBOutlet weak var distanceLbl: UILabel!
    
    @IBOutlet weak var ratingLbl: UILabel!
    
    
    @IBOutlet weak var storeStatusLbl: UILabel!
    @IBOutlet weak var storeTypeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
