//
//  StoresTVCell.swift
//  Chef
//
//  Created by RAVI on 22/07/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class StoresTVCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var visitedNumbersLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var subStoreNameLbl: UILabel!
    @IBOutlet weak var ratingImg: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
