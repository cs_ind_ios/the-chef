//
//  OrderSummaryTVCell.swift
//  Chef
//
//  Created by RAVI on 26/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class OrderSummaryTVCell: UITableViewCell {

    
    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var noteLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var qtyLbl: UILabel!
    @IBOutlet weak var lineLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
