//
//  SelectAddressCell.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class SelectAddressCell: UITableViewCell {

    @IBOutlet weak var addressTitleLbl: UILabel!
    @IBOutlet weak var addressDisLbl: UILabel!
    
    @IBOutlet weak var mobileNumLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
