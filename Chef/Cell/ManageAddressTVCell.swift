//
//  ManageAddressTVCell.swift
//  Chef
//
//  Created by RAVI on 16/05/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class ManageAddressTVCell: UITableViewCell {

    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var mobileNumLbl: UILabel!    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
