//
//  OptionsCell.swift
//  Chef
//
//  Created by RAVI on 20/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class OptionsCell: UICollectionViewCell {
    
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var optionNameLbl: UILabel!
    @IBOutlet weak var optImageVIew: UIImageView!
    
}
