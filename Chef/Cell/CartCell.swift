//
//  CartCell.swift
//  Chef
//
//  Created by RAVI on 22/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {

    @IBOutlet weak var ItemNameLbl: UILabel!
    @IBOutlet weak var ItemImg: UIImageView!
    @IBOutlet weak var ItemPriceLbl: UILabel!
    @IBOutlet weak var ItemPlusBtn: UIButton!
    @IBOutlet weak var ItemMinusBtn: UIButton!
    @IBOutlet weak var qtyLbl: UILabel!
    @IBOutlet weak var originalPriceLbl: UILabel!
    @IBOutlet weak var priceConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var noteLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
