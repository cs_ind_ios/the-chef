//
//  AdditionalsCell.swift
//  Chef
//
//  Created by RAVI on 22/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

//1. call delegate method
protocol AdditionalsCellDelegate:AnyObject {
    func SelectAdditionalBtn_tapped(cell: UITableViewCell)
}

class AdditionalsCell: UITableViewCell {
    @IBOutlet weak var selectImgBtn: UIButton!
    @IBOutlet weak var selectImg: UIImageView!
    @IBOutlet weak var additionalNameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var quanitityView: CustomView!
    @IBOutlet weak var quanitityHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var originalPriceLbl: UILabel!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var quanitityLbl: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var priceConstraintY: NSLayoutConstraint!
    @IBOutlet weak var maxiQtyLbl: UILabel!
    //2. create delegate variable
    weak var delegate: AdditionalsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func SelectAdditionalBtn_tapped(sender: AnyObject) {
        //4. call delegate method
        //check delegate is not nil with `?`
        delegate?.SelectAdditionalBtn_tapped(cell: self)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
