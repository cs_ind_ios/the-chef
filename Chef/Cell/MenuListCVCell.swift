//
//  MenuListCVCell.swift
//  Chef
//
//  Created by RAVI on 17/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class MenuListCVCell: UICollectionViewCell {
    
    @IBOutlet weak var foodImg: UIImageView!
    @IBOutlet weak var foodNameLbl: UILabel!
    @IBOutlet weak var foodDescriptionLbl: UILabel!
    @IBOutlet weak var foodCostLbl: UILabel!
}
