//
//  MoreCafeTVCell.swift
//  Chef
//
//  Created by RAVI on 22/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class MoreCafeTVCell: UITableViewCell {

    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var storeTypeLbl: UILabel!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var storeIcon: UIImageView!
    @IBOutlet weak var storeCloseStatus: UILabel!
    
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
