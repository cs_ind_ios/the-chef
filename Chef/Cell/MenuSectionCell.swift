//
//  MenuSectionCell.swift
//  Chef
//
//  Created by RAVI on 22/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class MenuSectionCell: UITableViewCell {
    
    @IBOutlet weak var catNameLbl: UILabel!
    @IBOutlet weak var itemsCountLbl: UILabel!
    @IBOutlet weak var arrowImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
