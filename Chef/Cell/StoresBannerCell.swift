//
//  StoresBannerCell.swift
//  Chef
//
//  Created by RAVI on 22/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class StoresBannerCell: UICollectionViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var blackView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
extension UIImageView {
    func setBorderColor(imageview : UIImageView) {
        imageview.layer.cornerRadius = 1.0
        imageview.layer.borderColor  =  UIColor.white.cgColor
        imageview.layer.borderWidth = 0.5
        imageview.layer.masksToBounds=true
        
        imageview.layer.shadowOpacity = 0.5
        imageview.layer.shadowColor =  UIColor.white.cgColor
        imageview.layer.shadowRadius = 2.0
        imageview.layer.shadowOffset = CGSize(width:0, height: 2)
    }
}
extension UIView {
    func setCardView(view : UIView){
        view.layer.shadowOpacity = 0.3
        view.layer.shadowColor =  UIColor.darkText.cgColor
        view.layer.shadowRadius = 2.0
        view.layer.shadowOffset = CGSize(width:0, height: 1)
    }
    func setGradientColor(view: UIView) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.black.cgColor]
        gradientLayer.opacity = 0.7
        view.layer.addSublayer(gradientLayer)
    }
}

