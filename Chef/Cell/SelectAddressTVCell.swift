//
//  SelectAddressTVCell.swift
//  Chef
//
//  Created by RAVI on 19/07/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit

class SelectAddressTVCell: UITableViewCell {

    
    @IBOutlet weak var textNameLbl: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
