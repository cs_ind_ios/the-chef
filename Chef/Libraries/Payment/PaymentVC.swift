//
//  PaymentVC.swift
//  Chef
//
//  Created by Creative Solutions on 10/9/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import UIKit
import SafariServices

@available(iOS 12.1.1, *)
class PaymentVC: UIViewController, OPPCheckoutProviderDelegate {
    var transaction: OPPTransaction?
    var safariVC: SFSafariViewController?
    let provider = OPPPaymentProvider(mode: OPPProviderMode.live)
    let urlScheme = "com.creativesols.chef.payments"
    let mainColor: UIColor = UIColor.init(red: 10.0/255.0, green: 134.0/255.0, blue: 201.0/255.0, alpha: 1.0)

    var checkoutID:String = ""
    var checkoutProvider: OPPCheckoutProvider?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func GetCheckOutIdPreparationMethod(amount:Double)  {
        let userDetails = UserDef.getUserProfileDic(key: "userDetails")
        let merchantTransactionId = "\(UserDef.getUserId())iOS\(self.TimeStamp())"
        let dic:[String:Any] = [
            "amount": String(format: "%.2f", amount),
            "shopperResultUrl": urlScheme,
            "isCardRegistration": "false",
            "merchantTransactionId": merchantTransactionId,
            "customerEmail":"\((userDetails["Email"] as? String)!)",
            "userId":"\(UserDef.getUserId())",
            "notificationUrl":Config.asyncPaymentCompletedNotificationKey
        ]
        PaymentModuleServices.getPaymentCheckOutService(dic: dic, success: { (data) in
            if(data.Status == false){
                NotificationCenter.default.post(name: .paymentFailed, object: nil)
            }else{
                DispatchQueue.main.async {
                    //print(data.Data![0].id)
                    self.checkoutID = data.Data![0].id
                    let checkoutSettings = OPPCheckoutSettings()
                    let paymentRequest = OPPPaymentProvider.paymentRequest(withMerchantIdentifier: "merchant.creative-sols.TheChef", countryCode: "KSA")
                    paymentRequest.supportedNetworks = [ PKPaymentNetwork.visa,
                                                         PKPaymentNetwork.masterCard, PKPaymentNetwork.mada]

                    checkoutSettings.applePayPaymentRequest = paymentRequest
                    //  checkoutSettings.language = "ar"
                    // Set available payment brands for your shop
                    checkoutSettings.paymentBrands = ["VISA", "MASTER" , "APPLEPAY", "MADA"]
                    
                    checkoutSettings.shopperResultURL = self.urlScheme + "://payment"
                    checkoutSettings.theme.navigationBarBackgroundColor = self.mainColor
                    checkoutSettings.theme.confirmationButtonColor = self.mainColor
                    checkoutSettings.theme.accentColor = self.mainColor
                    checkoutSettings.theme.cellHighlightedBackgroundColor = self.mainColor
                    checkoutSettings.theme.sectionBackgroundColor = self.mainColor.withAlphaComponent(0.05)
                    
                    /*   if #available(iOS 11.0, *) {
                     paymentRequest.requiredShippingContactFields = Set([PKContactField.postalAddress])
                     } else {
                     paymentRequest.requiredShippingAddressFields = .postalAddress
                     } */
                    
                    
                    
                    self.checkoutProvider = OPPCheckoutProvider(paymentProvider: self.provider, checkoutID: self.checkoutID, settings: checkoutSettings)!
                    self.checkoutProvider?.delegate = self
                self.checkoutProvider?.presentCheckout(forSubmittingTransactionCompletionHandler: { (transaction, error) in
                        guard let transaction = transaction else {
                            // Handle invalid transaction, check error
                            //print(error.debugDescription)
                            NotificationCenter.default.post(name: .paymentFailed, object: nil)

                            return
                        }
                        
                        self.transaction = transaction
                        if transaction.type == .synchronous {
                            // If a transaction is synchronous, just request the payment status
                            // You can use transaction.resourcePath or just checkout ID to do it
                            self.getstatus(transaction: self.transaction!)
                            
                        } else if transaction.type == .asynchronous {
                            NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveAsynchronousPaymentCallback), name: Notification.Name(rawValue: "AsyncPaymentCompletedNotificationKey"), object: nil)
                            
                            /* self.safariVC = SFSafariViewController(url: self.transaction!.redirectURL!)
                             self.safariVC?.delegate = self;
                             self.present(self.safariVC!, animated: true, completion: nil)
                             */
                            
                        } else {
                            // Executed in case of failure of the transaction for any reason
                            print(self.transaction.debugDescription)
                            NotificationCenter.default.post(name: .paymentFailed, object: nil)

                        }
                    }, cancelHandler: {
                        // Executed if the shopper closes the payment page prematurely
                        print(self.transaction.debugDescription)
                        NotificationCenter.default.post(name: .paymentFailed, object: nil)
                    })
                    
                }
            }
            }){ (error) in
                NotificationCenter.default.post(name: .paymentFailed, object: nil)
            }
    }
    func getstatus(transaction:OPPTransaction) {
        let dic:[String:Any] = [
            "resourcePath": transaction.resourcePath!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        ]
        PaymentModuleServices.getPaymentStatusService(dic: dic, success: { (data) in
            if(data.Status == false){
                NotificationCenter.default.post(name: .paymentFailed, object: nil)

            }else{
                if data.Data![0].customPayment == "OK" {
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .paymentSuccess, object: nil)
                    }
                }else{
                    NotificationCenter.default.post(name: .paymentFailed, object: nil)
                }
            }
            
        }){ (error) in
            NotificationCenter.default.post(name: .paymentFailed, object: nil)
        }
    }
    
    
    @objc func didReceiveAsynchronousPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "AsyncPaymentCompletedNotificationKey"), object: nil)
        /*  self.safariVC?.dismiss(animated: true, completion: {
         DispatchQueue.main.async {
         self.getstatus(transaction: self.transaction!)
         }
         }) */
        
        
        
        /*  self.checkoutProvider?.dismissCheckout(animated: true, completion: {
         DispatchQueue.main.async {
         self.getstatus(transaction: self.transaction!)
         }
         }) */
        
        
        
        
        self.checkoutProvider?.dismissCheckout(animated: true) {
            DispatchQueue.main.async {
                print(self.transaction?.resourcePath as Any)
                self.getstatus(transaction: self.transaction!)
            }
        }
    }
    
    func TimeStamp() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyyMMMddHHmmssSSS" //Specify your format that you want
        return dateFormatter.string(from: date)
    }
}
extension Notification.Name {
    static let paymentFailed = Notification.Name("paymentFailed")
    static let paymentSuccess = Notification.Name("paymentSuccess")
}
