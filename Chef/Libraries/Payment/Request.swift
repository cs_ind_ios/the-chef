import UIKit

//TODO: This class uses our test integration server; please adapt it to use your own backend API.
class Request: NSObject {
    
    // Test merchant server domain
   // static let serverDomain = "http://csadms.com/CheckClickAPI/api/PaymentGet/"
  //  static let serverDomain = "http://csadms.com/mentorpayments/api/Payments/"
    
    static let serverDomain = "http://csadms.com/CheffAppAPI/api/Payments/"
    static func requestCheckoutID(amount: Double, currency: String, completion: @escaping (String?) -> Void) {
        let parameters: [String:String] = [
            "amount": String(format: "%.2f", amount),
            "shopperResultUrl": Config.urlScheme,
            "isCardRegistration": "false",
            "merchantTransactionId":"123447",
            "customerEmail":"hello@gmail.com",
            "userId":"3519",
            "notificationUrl":Config.asyncPaymentCompletedNotificationKey
        ]
        var parametersString = ""
        for (key, value) in parameters {
            parametersString += key + "=" + value + "&"
        }
        parametersString.remove(at: parametersString.index(before: parametersString.endIndex))
        
        let url = serverDomain + "NewCheckoutRequestFromSDK?"  + parametersString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let request = NSURLRequest(url: URL(string: url)!)
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                let checkoutID = json?["id"] as? String
                completion(checkoutID)
            } else {
                completion(nil)
            }
            }.resume()
    }
    static func requestCheckoutIDSaveCards(amount: Double, currency: String, completion: @escaping (String?) -> Void) {
        let parameters: [String:String] = [
            "amount": String(format: "%.2f", amount),
            "shopperResultUrl": Config.urlScheme,
            "isCardRegistration": "true",
            "merchantTransactionId":"123447",
            "customerEmail":"hello@gmail.com",
            "userId":"3519",
            "notificationUrl":Config.asyncPaymentCompletedNotificationKey
        ]
        var parametersString = ""
        for (key, value) in parameters {
            parametersString += key + "=" + value + "&"
        }
        parametersString.remove(at: parametersString.index(before: parametersString.endIndex))
        
        let url = serverDomain + "NewCheckoutRequestFromSDK?"  + parametersString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let request = NSURLRequest(url: URL(string: url)!)
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                let checkoutID = json?["id"] as? String
                completion(checkoutID)
            } else {
                completion(nil)
            }
            }.resume()
    }
    
    static func requestPaymentStatus(resourcePath: String, completion: @escaping (Bool) -> Void) {
        let url = serverDomain + "paymentstatus?" + "resourcePath=" + resourcePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let request = NSURLRequest(url: URL(string: url)!)
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                let transactionStatus = json?["customPayment"] as? String
                completion(transactionStatus == "OK")
            } else {
                completion(false)
            }
            }.resume()
    }
}
