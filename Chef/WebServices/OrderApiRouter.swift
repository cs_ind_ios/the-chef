//
//  OrderApiRouter.swift
//  CoffeeStation
//
//  Created by Creative Solutions on 7/6/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation
import ObjectMapper
class OrderApiRouter {
    // New
    class func getStoreInformation(dic : Dictionary<String, Any> , success: @escaping (_ result:StoreInfromationModel?) -> Void, onError:@escaping (_ error:String)-> Void) {
        
        APIHandler.APIHandlerRequest(url: url.StoresInformatio.path(), method: .post, param: dic, success: { (data) in
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if (json["Status"] != nil) {
                    let sdata = Mapper<StoreInfromationModel>().map(JSON: json)
                    success(sdata)
                }else{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError("\(error)")
            
        }
    }
    class func getOrderhistory(dic : Dictionary<String, Any> , success: @escaping (_ result:OrderHistoryModel?) -> Void, onError:@escaping (_ error:String)-> Void) {
        
        APIHandler.APIHandlerRequest(url: url.getOrderHistory.path(), method: .post, param: dic, success: { (data) in
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if (json["Status"] != nil) {
                    let sdata = Mapper<OrderHistoryModel>().map(JSON: json)
                    success(sdata)
                   // print(sdata!)
                }else{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
                
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError("\(error)")
            
        }
    }
    class func getOrderTracking(dic : Dictionary<String, Any> , success: @escaping (_ result:OrderTrackingModel?) -> Void, onError:@escaping (_ error:String)-> Void) {
        
        APIHandler.APIHandlerRequest(url: url.getOrderTracking.path(), method: .post, param: dic, success: { (data) in
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if (json["Status"] != nil) {
                    let sdata = Mapper<OrderTrackingModel>().map(JSON: json)
                    success(sdata)
                }else{
                     onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
                
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError("\(error)")

        }
    }
    class func cancelOrder(dic : Dictionary<String, Any> , success: @escaping (_ result:CancelOrderModel?) -> Void, onError:@escaping (_ error:String)-> Void) {
        
        APIHandler.APIHandlerRequest(url: url.cancelOrder.path(), method: .post, param: dic, success: { (data) in
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if (json["Status"] != nil) {
                    let sdata = Mapper<CancelOrderModel>().map(JSON: json)
                    success(sdata)
                }else{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
                
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError("\(error)")
            
        }
    }
    
    class func favouriteOrder(dic : Dictionary<String, Any> , success: @escaping (_ result:CancelOrderModel?) -> Void, onError:@escaping (_ error:String)-> Void) {
        
        APIHandler.APIHandlerRequest(url: url.favouriteOrder.path(), method: .post, param: dic, success: { (data) in
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if (json["Status"] != nil) {
                    let sdata = Mapper<CancelOrderModel>().map(JSON: json)
                    success(sdata)
                }else{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
                
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError("\(error)")
            
        }
    }
    class func activeOrder(dic : Dictionary<String, Any> , success: @escaping (_ result:ActiveOrderModel?) -> Void, onError:@escaping (_ error:String)-> Void) {
        
        APIHandler.APIHandlerRequest(url: url.activeOrder.path(), method: .post, param: dic, success: { (data) in
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if (json["Status"] != nil) {
                    let sdata = Mapper<ActiveOrderModel>().map(JSON: json)
                    success(sdata)
                }else{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
                
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError("\(error)")
            
        }
    }
    class func PlaceOrder(dic : Dictionary<String, Any> , success: @escaping (_ result:InsertOrder?) -> Void, onError:@escaping (_ error:String)-> Void) {
        
        APIHandler.APIHandlerRequest(url: url.insertOrder.path(), method: .post, param: dic, success: { (data) in
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if (json["Status"] != nil) {
                    let sdata = Mapper<InsertOrder>().map(JSON: json)
                    success(sdata)
                }else{
                    ANLoader.hide()
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
                
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError("\(error)")
            
        }
    }
    class func getAdditional(dic : Dictionary<String, Any> , success: @escaping (_ result:AdditionlsModel?) -> Void, onError:@escaping (_ error:String)-> Void) {
        
        APIHandler.APIHandlerRequest(url: url.getAdditions.path(), method: .post, param: dic, success: { (data) in
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if (json["Status"] != nil) {
                    let sdata = Mapper<AdditionlsModel>().map(JSON: json)
                    success(sdata)
                }else{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError("\(error)")
            
        }
    }
    class func getMainMenuItems(dic : Dictionary<String, Any> , success: @escaping (_ result:MainMenuItems?) -> Void, onError:@escaping (_ error:String)-> Void) {
        
        APIHandler.APIHandlerRequest(url: url.getMainMenu.path(), method: .post, param: dic, success: { (data) in
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                if (json["Status"] != nil) {
                    let sdata = Mapper<MainMenuItems>().map(JSON: json)
                    success(sdata)
                }else{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
                
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError("\(error)")
            
        }
    }
    class func getPromotionsService(dic:[String:Any],isLoader:Bool,isUIDisable:Bool,success: @escaping (_ result:GetPromotionsModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestOptions(url: url.getPromotionService.path(), params: param, method: .post,isLoader:isLoader,isUIDisable:isUIDisable, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(GetPromotionsModel.self, from: result)
                    success(result1)
                }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    class func TrackDriverService(dic:[String:Any],success: @escaping (_ result:TrackDriverDetailsModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.trackDriverService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                   // print(result)
                    let result1 = try JSONDecoder().decode(TrackDriverDetailsModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
        }) { (error) in
            onError(error)
        }
    }
    //Map Navigation
    class func LocationGetService(url:String,success: @escaping (_ result:Any) -> Void, onError: @escaping (_ error:String) -> Void){
        APIHandler.APIHandlerRequest(url: url, method: .get, param: [:], success: { (result) in
           // print(result)
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: JSONSerialization.ReadingOptions.allowFragments)
                success(json)
            }catch{
                onError("Conntion Failed")
            }
        }) { (error) in
            onError(error)
        }
    }
}
//    class func getPromotions(dic : Dictionary<String, Any> , success: @escaping (_ result:Promotions?) -> Void, onError:@escaping (_ error:String)-> Void) {
//
//        APIHandler.APIHandlerRequest(url: url.getPromotionService.path(), method: .post, param: dic, success: { (data) in
//            do{
//                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
//                print(json)
//                if (json["Status"] != nil) {
//                    let sdata = Mapper<Promotions>().map(JSON: json)
//                    success(sdata)
//                }else{
//                    onError("Service request failed!")
//                }
//            }catch{
//                onError("Service request failed!")
//            }
//
//        }) { (error) in
//            onError("\(error)")
//        }
//    }
