//
//  AccountApiRouter.swift
//  CoffeeStation
//
//  Created by CS on 08/06/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation
import ObjectMapper

class AccountApiRouter {
    
    //Change Password
    class func changePasswordService(dic : Dictionary<String, Any> , success: @escaping (_ result:ChangePasswordModel?) -> Void) {
    
        APIHandler.APIHandler(url: url.changePassword.path(), method: .post, param: dic) { (result) in
            //print(result)
            let data = result as! [String : Any]
            let sdata = Mapper<ChangePasswordModel>().map(JSON: data)
           // print(sdata!)
            success(sdata)
        }
    
    }
    
    //Display Profile Details
    class func displayProfileService(userId : String , success: @escaping (_ result:DisplayProfileModel?) -> Void) {
        let dic = ["UserId":userId]
        let dic1 = ["DisplayProfile":dic]
            APIHandler.APIHandler(url: url.getUserAdd.path(), method: .post, param: dic1) { (result) in
            //print(result)
                
            let data = result as! [String : Any]
            let sdata = Mapper<DisplayProfileModel>().map(JSON: data)
            //print(sdata!)
            success(sdata)
        }
    }
    
    
    // Update Profile
    class func updateProfileService(dic : Dictionary<String, Any>, success: @escaping (_ result:DisplayProfileModel?) -> Void) {
    
        APIHandler.APIHandler(url: url.getUserAdd.path(), method: .post, param: dic) { (result) in
            //print(result)
            let data = result as! [String : Any]
            let sdata = Mapper<DisplayProfileModel>().map(JSON: data)
            //print(sdata!)
            success(sdata)
        }
    }
    
    // LogOut
    
    class func logOutService(dic : Dictionary<String, Any>, success: @escaping (_ result:ChangePasswordModel?) -> Void) {
    
        APIHandler.APIHandler(url: url.LogOut.path(), method: .post, param: dic) { (result) in
           // print(result)
            let data = result as! [String : Any]
            let sdata = Mapper<ChangePasswordModel>().map(JSON: data)
            //print(sdata!)
            success(sdata)
        }
    }
    
    // Save/edit/delete Address
    
    class func saveAddress(dic : Dictionary<String, Any>, success: @escaping (_ result:SaveAddress?) -> Void) {
                
        APIHandler.APIHandler(url: url.SaveAddress.path(), method: .post, param: dic) { (result) in
            //print(result)
            let data = result as! [String : Any]
            let sdata = Mapper<SaveAddress>().map(JSON: data)
            //print(sdata!)
            success(sdata)
        }
    }
    
    // get user Address
    class func getUserAdd(userId : String, success: @escaping (_ result:SaveAddress?) -> Void) {
        let dic = ["UserId":userId]
        APIHandler.APIHandler(url: url.getUserAdd.path(), method: .post, param: dic) { (result) in
            //print(result)
            let data = result as! [String : Any]
            StaticAdd.addDetails = data
            let sdata = Mapper<SaveAddress>().map(JSON: data)
            //print(sdata!)
            success(sdata)
        }
    }
}

