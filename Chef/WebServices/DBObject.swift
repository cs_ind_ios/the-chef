//
//  DBObject.swift
//  CoffeeStation
//
//  Created by Creative Solutions on 9/27/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation
import SQLite3
import UIKit

class DBObject {
    
   static var db: OpaquePointer?
   
    
    class func DBReturen() -> OpaquePointer {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("CoffeeStation.sqlite")
        
        //print(fileURL.path)
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        
        return db!
    }
    
//MARK: Insert Order
class func InsertOrder(insertSql:[OrderTable]) -> Bool  {
    
     db = DBReturen()
    var stmt: OpaquePointer?

    
    let dic = insertSql[0]
    let Sql = "INSERT INTO OrderTable (BranchId, CategoryId, ItemId, ItemName_En, ItemName_Ar, SizeId, Size_En, Size_Ar, SizePrice, Quantity, TotalAmount, Comments, OriginalSizePrice, OriginalTotalAmount, DiscountPercentage) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"


    
    if sqlite3_prepare(db, Sql, -1, &stmt, nil) != SQLITE_OK{
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("error preparing insert: \(errmsg)")
        return false
    }

    if sqlite3_bind_int(stmt, 1, Int32(dic.BranchId!)) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding BranchId: \(errmsg)")
        return false
    }
    if sqlite3_bind_int(stmt, 2, Int32(dic.CategoryId!)) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding CategoryId: \(errmsg)")
        return false
    }
    if sqlite3_bind_int(stmt, 3, Int32(dic.ItemId!)) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding ItemId: \(errmsg)")
        return false
    }

    if sqlite3_bind_text(stmt, 4, dic.ItemName_En! , -1, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding ItemName_En: \(errmsg)")
        return false
    }
    
    if sqlite3_bind_text(stmt, 5, dic.ItemName_Ar!, -1, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding ItemName_En: \(errmsg)")
        return false
    }
    
    if sqlite3_bind_int(stmt, 6, Int32(dic.SizeId!)) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding SizeId: \(errmsg)")
        return false
    }
    
    if sqlite3_bind_text(stmt, 7, dic.Size_En!, -1, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding Size_En: \(errmsg)")
        return false
    }
    
    if sqlite3_bind_text(stmt, 8, dic.Size_Ar!, -1, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding Size_Ar: \(errmsg)")
        return false
    }
    
    if sqlite3_bind_double(stmt, 9, dic.SizePrice!) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding SizePrice: \(errmsg)")
        return false
    }
    if sqlite3_bind_int(stmt, 10, Int32(dic.Quantity!)) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding Quantity: \(errmsg)")
        return false
    }
    
    if sqlite3_bind_double(stmt, 11, dic.TotalAmount!) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding TotalAmount: \(errmsg)")
        return false
    }
    
    if sqlite3_bind_text(stmt, 12, dic.Comments!, -1, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding Comments: \(errmsg)")
        return false
    }
    if sqlite3_bind_double(stmt, 13, dic.OriginalSizePrice!) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding TotalAmount: \(errmsg)")
        return false
    }
    if sqlite3_bind_double(stmt, 14, dic.OriginalTotalAmount!) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding TotalAmount: \(errmsg)")
        return false
    }
    if sqlite3_bind_double(stmt, 15, dic.DiscountPercentage!) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure binding TotalAmount: \(errmsg)")
        return false
    }
    
    
    if sqlite3_step(stmt) != SQLITE_DONE {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("failure inserting orderTable: \(errmsg)")
        return false
    }
    
   return true
}
    
class func InsertOrderString(insertSql:String) -> Bool  {
        
        db = DBReturen()
        var stmt: OpaquePointer?
        
        if sqlite3_prepare(db, insertSql, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return false
        }
        
        
        if sqlite3_step(stmt) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure inserting orderTable: \(errmsg)")
            return false
        }
        
        return true
}
//MARK: Insert Additionals
class func InsertAdditionals(insertSql:[AdditionalTable]) -> Bool  {
        
        db = DBReturen()
        var stmt: OpaquePointer?
        
        
        let dic = insertSql[0]
        let Sql = "INSERT INTO AdditionalTable (OrderId, AdditionalId, AddtionalName_En, AddtionalName_Ar, AddPric) VALUES (?,?,?,?,?)"
        
        
        
        if sqlite3_prepare(db, Sql, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return false
        }
        
        if sqlite3_bind_int(stmt, 1, Int32(dic.OrderId!)) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding BranchId: \(errmsg)")
            return false
        }
        if sqlite3_bind_int(stmt, 2, Int32(dic.AdditionalId!)) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding CategoryId: \(errmsg)")
            return false
        }
        
        
        if sqlite3_bind_text(stmt, 3, dic.AddtionalName_En!, -1, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding ItemName_En: \(errmsg)")
            return false
        }
        
        if sqlite3_bind_text(stmt, 4, dic.AddtionalName_Ar!, -1, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding ItemName_En: \(errmsg)")
            return false
        }
        
        if sqlite3_bind_double(stmt, 5, dic.AddPrice!) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding SizePrice: \(errmsg)")
            return false
        }
    
        
        if sqlite3_step(stmt) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure inserting orderTable: \(errmsg)")
            return false
        }
        
        return true
}
    
   
    
    
class func getMaxOrderId(Sql:String) -> Int  {
        db = DBReturen()
        var stmt: OpaquePointer?
    
    if sqlite3_prepare(db, Sql, -1, &stmt, nil) != SQLITE_OK{
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("error preparing insert: \(errmsg)")
        return 0
    }
    var OrderId:Int = 0
    while(sqlite3_step(stmt) == SQLITE_ROW){
        OrderId = Int(sqlite3_column_int(stmt, 0))
    }
    
    return OrderId
}
class func getCountOrder(Sql:String) -> Int  {
        db = DBReturen()
        var stmt: OpaquePointer?
        
        if sqlite3_prepare(db, Sql, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return 0
        }
        var count:Int = 0
        while(sqlite3_step(stmt) == SQLITE_ROW){
            count = Int(sqlite3_column_int(stmt, 0))
        }
        
        return count
}
    
    class func geTotalOrderAmount(Sql:String) -> Double  {
        db = DBReturen()
        var stmt: OpaquePointer?
        
        if sqlite3_prepare(db, Sql, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return 0
        }
        var count:Double = 0
        while(sqlite3_step(stmt) == SQLITE_ROW){
            count = Double(sqlite3_column_double(stmt, 0))
        }
        
        return count
    }
class func GetOrder(getSql:String) ->Bool{
    SaveAddressClass.getOrderArray.removeAll()
    db = DBReturen()
    var stmt:OpaquePointer?
    
    if sqlite3_prepare(db, getSql, -1, &stmt, nil) != SQLITE_OK{
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("error preparing insert: \(errmsg)")
        return false
    }
  
    while(sqlite3_step(stmt) == SQLITE_ROW){
        let OrderId = sqlite3_column_int(stmt, 0)
        let BranchId = sqlite3_column_int(stmt, 1)
        let CategoryId = sqlite3_column_int(stmt, 2)
        
        let ItemId = sqlite3_column_int(stmt, 3)
        let ItemImg = String(cString: sqlite3_column_text(stmt, 4))
        let ItemName_En = String(cString: sqlite3_column_text(stmt, 5))
        let ItemName_Ar = String(cString: sqlite3_column_text(stmt, 6))

        let SizeId = sqlite3_column_int(stmt, 7)
        let Size_En = String(cString: sqlite3_column_text(stmt, 8))
        let Size_Ar = String(cString: sqlite3_column_text(stmt, 9))
        let SizePrice = sqlite3_column_double(stmt, 10)

        let Quantity = sqlite3_column_int(stmt, 11)
        let TotalAmount = sqlite3_column_double(stmt, 12)
        let Comments = String(cString: sqlite3_column_text(stmt, 13))
        let OriginalSizePrice = sqlite3_column_double(stmt, 14)
        let OriginalTotalAmount = sqlite3_column_double(stmt, 15)
        let DiscountPercentage = sqlite3_column_double(stmt, 16)

        SaveAddressClass.getOrderArray.append(OrderTable(
            OrderId: Int(OrderId),
            BranchId: Int(BranchId),
            CategoryId: Int(CategoryId),
            ItemId: Int(ItemId),
            ItemImg: String(describing: ItemImg),
            ItemName_En: String(describing: ItemName_En),
            ItemName_Ar: String(describing: ItemName_Ar),
            SizeId: Int(SizeId),
            Size_En: String(describing: Size_En),
            Size_Ar: String(describing: Size_Ar),
            SizePrice:Double(SizePrice),
            Quantity:Int(Quantity),
            TotalAmount:Double(TotalAmount),
            Comments:String(describing: Comments),
            OriginalSizePrice:Double(OriginalSizePrice),
            OriginalTotalAmount:Double(OriginalTotalAmount),
            DiscountPercentage:Double(DiscountPercentage)
            )
        )
    }
    
    return true
}
    

class func GetAdditionals(getSql:String){
        SaveAddressClass.getAdditionalsArray.removeAll()
        db = DBReturen()
        var stmt:OpaquePointer?
        
        if sqlite3_prepare(db, getSql, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return 
        }
        
        while(sqlite3_step(stmt) == SQLITE_ROW){
            let Id = sqlite3_column_int(stmt, 0)
            let OrderId = sqlite3_column_int(stmt, 1)
            let AdditionalId = sqlite3_column_int(stmt, 2)
            let AddtionalName_En = String(cString: sqlite3_column_text(stmt, 3))
            let AddtionalName_Ar = String(cString: sqlite3_column_text(stmt, 4))
            let AddPrice = sqlite3_column_double(stmt, 5)
            let Quantity = sqlite3_column_int(stmt, 6)
            let AddTotalPrice = sqlite3_column_double(stmt, 7)
            let OriginalAddPrice = sqlite3_column_double(stmt, 8)
            let OriginalAddTotalPrice = sqlite3_column_double(stmt, 9)


            SaveAddressClass.getAdditionalsArray.append(AdditionalTable(
                Id: Int(Id),
                OrderId: Int(OrderId),
                AdditionalId: Int(AdditionalId),
                AddtionalName_En: String(describing: AddtionalName_En),
                AddtionalName_Ar: String(describing: AddtionalName_Ar),
                AddPrice:Double(AddPrice),
                Quantity:Int(Quantity),
                AddTotalPrice:Double(AddTotalPrice),
                OriginalAddPrice: Double(OriginalAddPrice),
                OriginalAddTotalPrice:Double(OriginalAddTotalPrice)
                )
            

            )
        }
        
    }
    
    
    

class func GetSaveStore(getSql:String){
        SaveAddressClass.getSaveStoreArray.removeAll()
        db = DBReturen()
        var stmt:OpaquePointer?
        
        if sqlite3_prepare(db, getSql, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return
        }
        
        while(sqlite3_step(stmt) == SQLITE_ROW){
            let BrandId = sqlite3_column_int(stmt, 1)
            let BranchId = sqlite3_column_int(stmt, 2)
            let BranchName_En = String(cString: sqlite3_column_text(stmt, 3))
            let BranchName_Ar = String(cString: sqlite3_column_text(stmt, 4))
            let Address = String(cString: sqlite3_column_text(stmt, 5))
            let StoreLogo_En = String(cString: sqlite3_column_text(stmt, 6))
            let StoreImage_En = String(cString: sqlite3_column_text(stmt, 7))
            let Latitude = sqlite3_column_double(stmt, 8)
            let Longitude = sqlite3_column_double(stmt, 9)
            let MinOrderCharges = sqlite3_column_double(stmt, 10)
            let StarDateTime = String(cString: sqlite3_column_text(stmt, 11))
            let EndDateTime = String(cString: sqlite3_column_text(stmt, 12))
            let DeliveryCharges = sqlite3_column_double(stmt, 13)
            let DeliveryDistance = sqlite3_column_double(stmt, 14)
            let TakeAwayDistance = sqlite3_column_int(stmt, 15)
            let DiscountAmt = sqlite3_column_int(stmt, 16)

            
          
            SaveAddressClass.getSaveStoreArray.append(GetStores(AvgPreparationTime: 0,
                                                                BranchId: Int(BranchId),
                                                                BranchName_En: BranchName_En,
                                                                BranchName_Ar: BranchName_Ar,
                                                                DeliveryCharges: Double(DeliveryCharges),
                                                                Address: Address,
                                                                MinOrderCharges: MinOrderCharges,
                                                                EmailId: "",
                                                                MobileNo: "",
                                                                OrderTypes: "",
                                                                Rating: "",
                                                                RatingCount: 0,
                                                                StoreLogo_En: StoreLogo_En,
                                                                StoreImage_En: StoreImage_En,
                                                                StoreStatus: "",
                                                                StoreType: "",
                                                                TakeAwayDistance: Int(TakeAwayDistance),
                                                                DeliveryDistance: Double(DeliveryDistance),
                                                                StarDateTime: StarDateTime,
                                                                EndDateTime: EndDateTime,
                                                                CurrentDateTime: "",
                                                                IsPromoted: false,
                                                                Popular: false,
                                                                DiscountAmt: Int(DiscountAmt),
                                                                IsNew: false,
                                                                Latitude: Latitude,
                                                                Longitude: Longitude,
                                                                Distance: 0,
                                                                BrandId:Int(BrandId),
                                                                BindingCount: 0,
                                                                BrandName_En: "",
                                                                BrandName_Ar: "",
                                                                PaymentType: "",
                                                                BranchDescription_En: "",
                                                                Serving: 0, FutureOrderDay: 0, StoreType_Ar: "", BranchDescription_Ar: "", UpdatesAvailable: 0, UpdateSeverity: 0, VatPercentage: 0
                )
            )
            
        }
        
        //return true
    }
}
