//
//  PaymentModuleServices.swift
//  CheckClick
//
//  Created by Creative Solutions on 2/20/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
class PaymentModuleServices{

class func getPaymentCheckOutService(dic:[String:Any],success: @escaping (_ result:GetCheckOutModel) -> Void, onError: @escaping (_ error:String) -> Void){
    let param = dic
    APIHandler.Request(url: url.getPaymentCheckOutService.path(), params: param, method: .post, success: { (result) in
        do{
            let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
            do{
                let result1 = try JSONDecoder().decode(GetCheckOutModel.self, from: result)
                success(result1)
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
        }catch{
            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
        }
        
    }) { (error) in
        onError(error)
    }
}
    
    class func getPaymentStatusService(dic:[String:Any],success: @escaping (_ result:GetPaymentStatusModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.getPaymentResourcePathService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(GetPaymentStatusModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    
    class func getPaymentStatusServiceWithOptions(dic:[String:Any],isLoader:Bool, isUIDisable:Bool, success: @escaping (_ result:GetPaymentStatusModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestOptions(url: url.getPaymentResourcePathService.path(), params: param, method: .post, isLoader: isLoader, isUIDisable: isUIDisable, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
               // print(json)
                do{
                    let result1 = try JSONDecoder().decode(GetPaymentStatusModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    
    class func insertSaveCardService(dic:[String:Any],success: @escaping (_ result:GetSaveCardResponseModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.getSaveCardService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(GetSaveCardResponseModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    
    
    class func getAllSaveCardDetailsService(dic:[String:Any],success: @escaping (_ result:GetSaveCardModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.getAllSaveCardDetailsService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(GetSaveCardModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    
    class func getDeleteCardService(dic:[String:Any],success: @escaping (_ result:GetDeleteCardModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.getDeleteCardService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(GetDeleteCardModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    
    class func getAddCardStatusService(dic:[String:Any],success: @escaping (_ result:GetAddCardStatusModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.Request(url: url.getAddCardStatusService.path(), params: param, method: .post, success: { (result) in
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                //print(json)
                do{
                    let result1 = try JSONDecoder().decode(GetAddCardStatusModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
}
