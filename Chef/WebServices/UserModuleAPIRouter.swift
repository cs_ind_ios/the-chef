//
//  UserModuleAPIRouter.swift
//  Chef
//
//  Created by RAVI on 18/04/19.
//  Copyright © 2019 Creative Solutions. All rights reserved.
//

import Foundation
import ObjectMapper

class UserModuleService {
    
    // verify mobile number and email
    class func VerifyMobNumService(mobNum : String ,email:String, success: @escaping (_ result:VerifyMobile?) -> Void) {
        let dic:[String:String] = ["Mobile":mobNum,"Email":email]
        let params = ["VerifyMobile":dic]
        APIHandler.APIHandler(url: url.verifyMobileForSignUp.path(), method: .post, param: params) { (result) in
           // print(result)
            let data = result as! [String : Any]
            let rData = Mapper<VerifyMobile>().map(JSON: data)
            //print(rData!)
            success(rData)
        }
    }
    
    // Sign up service
    class func signUpService(name : String,mobNum : String,email:String,paswd:String,lang:String,otp:String,nickName:String,gender:String,userType:String,userId:String,deviceToken:String , success: @escaping (_ result:SignUpModel?) -> Void) {
        
        let model = UIDevice.current.model
        let userDic = ["FullName":name,"FamilyName":name,"NickName":nickName,"Gender":gender,"Mobile":mobNum,"Email":email,"Password":paswd,"Language":lang,"DeviceType":model,"UserType":userType,"UserId":userId,"Otp":otp];
        
        let dTooken = "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))"
        
        let dTokenDic = ["DeviceToken":dTooken]
        
        let fullDic = ["UserDetails":userDic,"UserAuthActivity":dTokenDic]
        APIHandler.APIHandler(url: url.register.path(), method: .post, param: fullDic) { (result) in
            //print(result)
            let data = result as! [String : Any]
            let sdata = Mapper<SignUpModel>().map(JSON: data)
            //print(sdata!)
            success(sdata)
        }
        
    }
    
    // Login Service
    class func loginService(mobNum : String, paswd:String, language:String, success: @escaping (_ result:LoginModel?) -> Void) {
        
        let userDic = ["Mobile":mobNum,"Password":paswd, "Language":language];
        
        let dTooken = "\(String(describing: UserDefaults.standard.value(forKey: "DeviceToken")!))"
        
        let dTokenDic = ["DeviceToken":dTooken]
        
        let fullDic = ["UserDetails":userDic,"UserAuthActivity":dTokenDic]
       // print("\(fullDic)")
        APIHandler.APIHandler(url: url.login.path(), method: .post, param: fullDic) { (result) in
            //print(result)
            let data = result as! [String : Any]
            let sdata = Mapper<LoginModel>().map(JSON: data)
            //print(sdata!)
            success(sdata)
        }
        
    }
    
    // verify mobile number for forgot password
    class func VerifyMobNumServiceForFogrotPwd(mobNum : String, success: @escaping (_ result:VerifyMobile?) -> Void) {
        
        let dic:[String:String] = ["Mobile":mobNum]
        let params = ["VerifyMobile":dic]
        
        APIHandler.APIHandler(url: url.verifyMobileForForgotPassword.path(), method: .post, param: params) { (result) in
            //print(result)
            let data = result as! [String : Any]
            let rData = Mapper<VerifyMobile>().map(JSON: data)
            //print(rData!)
            success(rData)
        }
    }
    
    //forgot password
    class func forgotPasswordService(mobNum: String, success: @escaping (_ result:ForgotOTP?) -> Void){
        let dic = ["Mobile":mobNum]
        let dic1 = ["ForgotPassword":dic]
        APIHandler.APIHandler(url: url.forgotPassword.path(), method: .post, param:dic1) { (result) in
           // print(result)
            let data = result as! [String : Any]
            let sdata = Mapper<ForgotOTP>().map(JSON: data)
            //print(sdata!)
            success(sdata)
        }
        
    }
    
    
    
    // set new password
    class func setNewPassword(mobNum: String,newPswd:String, success: @escaping (_ result:ForgotPasswordModel?) -> Void){
        
        let dic = ["Mobile":mobNum,"NewPassword":newPswd]
        let dic1 = ["SetNewPassword":dic]
        APIHandler.APIHandler(url: url.setNewPassword.path(), method: .post, param: dic1) { (result) in
           // print(result)
            let data = result as! [String : Any]
            let sdata = Mapper<ForgotPasswordModel>().map(JSON: data)
           // print(sdata!)
            success(sdata)
        }
        
    }
    
    // Update user details
    class func UpdateLanguageService(dic:[String:Any],isLoader:Bool,isUIDisable:Bool,success: @escaping (_ result:ResponseModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestOptions(url: url.updateLangaueService.path(), params: param, method: .post,isLoader:isLoader,isUIDisable:isUIDisable, success: { (result) in
            do{
                _ =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(ResponseModel.self, from: result)
                    success(result1)
                }catch{
                    onError("Conntion Failed")
                }
            }catch{
                onError("Conntion Failed")
            }
            
        }) { (error) in
            onError(error)
        }
    }
    
    class func logoutService(dic:[String:Any],isLoader:Bool,isUIDisable:Bool,success: @escaping (_ result:ResponseModel) -> Void, onError: @escaping (_ error:String) -> Void){
        let param = dic
        APIHandler.RequestOptions(url: url.logoutService.path(), params: param, method: .post,isLoader:isLoader,isUIDisable:isUIDisable, success: { (result) in
            do{
                _ =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
                do{
                    let result1 = try JSONDecoder().decode(ResponseModel.self, from: result)
                    success(result1)
                }catch{
                    onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
                }
            }catch{
                onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"something went wrong, try after some time", value: "", table: nil))!)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    class func LocationGetService(url:String,success: @escaping (_ result:Any) -> Void, onError: @escaping (_ error:String) -> Void){
        APIHandler.APIHandlerRequest(url: url, method: .get, param: [:], success: { (result) in
            print(result)
            do{
                let json =  try JSONSerialization.jsonObject(with: result, options: JSONSerialization.ReadingOptions.allowFragments)
                //print(json)
                success(json)
            }catch{
                onError("Conntion Failed")
            }
        }) { (error) in
            onError(error)
        }
    }
    
//    // verify mobile number and email
//    class func verifyEmailAndMobile(dic:[String:Any],success: @escaping (_ result:VerifyMobileAndEmailModel) -> Void, onError: @escaping (_ error:String) -> Void){
//        let param = dic
//        APIHandler.Request(url: url.verifyMobileForSignUp.path(), params: param, method: .post, success: { (result) in
//            do{
//                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
//                do{
//                    print(result)
//                    let result1 = try JSONDecoder().decode(VerifyMobileAndEmailModel.self, from: result)
//                    print(result1.Message)
//                    success(result1)
//                }catch{
//                    onError("\(json["Message"] as! String)")
//                }
//            }catch{
//                onError("Conntion Failed")
//            }
//
//        }) { (error) in
//            onError(error)
//        }
//    }
//
//    // Sign up service
//    class func signUpService(dic:[String:Any],success: @escaping (_ result:SignUpModel) -> Void, onError: @escaping (_ error:String) -> Void){
//        let param = dic
//        APIHandler.Request(url: url.register.path(), params: param, method: .post, success: { (result) in
//            do{
//                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
//                do{
//                    let result1 = try JSONDecoder().decode(SignUpModel.self, from: result)
//                    success(result1)
//                }catch{
//                    onError("\(json["Message"] as! String)")
//                }
//            }catch{
//                onError("Conntion Failed")
//            }
//
//        }) { (error) in
//            onError(error)
//        }
//    }
//    class func sigInService(dic:[String:Any],success: @escaping (_ result:SignUpModel) -> Void, onError: @escaping (_ error:String) -> Void){
//        let param = dic
//        APIHandler.Request(url: url.login.path(), params: param, method: .post, success: { (result) in
//            do{
//                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
//                do{
//                    let result1 = try JSONDecoder().decode(SignUpModel.self, from: result)
//                    success(result1)
//                }catch{
//                    onError("\(json["Message"] as! String)")
//                }
//            }catch{
//                onError("Conntion Failed")
//            }
//
//        }) { (error) in
//            onError(error)
//        }
//    }
//    class func VerifyMobNumServiceForFogrotPwd(dic:[String:Any],success: @escaping (_ result:VerifyMobileAndEmailModel) -> Void, onError: @escaping (_ error:String) -> Void){
//        let param = dic
//        APIHandler.Request(url: url.verifyMobileForForgotPassword.path(), params: param, method: .post, success: { (result) in
//            do{
//                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
//                do{
//                    let result1 = try JSONDecoder().decode(VerifyMobileAndEmailModel.self, from: result)
//                    success(result1)
//                }catch{
//                    onError("\(json["Message"] as! String)")
//                }
//            }catch{
//                onError("Conntion Failed")
//            }
//
//        }) { (error) in
//            onError(error)
//        }
//    }
//    class func changePasswordService(dic:[String:Any],success: @escaping (_ result:SignInModel) -> Void, onError: @escaping (_ error:String) -> Void){
//        let param = dic
//        APIHandler.Request(url: url.setNewPassword.path(), params: param, method: .post, success: { (result) in
//            do{
//                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
//                do{
//                    let result1 = try JSONDecoder().decode(SignInModel.self, from: result)
//                    success(result1)
//                }catch{
//                    onError("\(json["Message"] as! String)")
//                }
//            }catch{
//                onError("Conntion Failed")
//            }
//
//        }) { (error) in
//            onError(error)
//        }
//    }
//    class func getUserAddService(dic:[String:Any],success: @escaping (_ result:SignInModel) -> Void, onError: @escaping (_ error:String) -> Void){
//        let param = dic
//        APIHandler.Request(url: url.getUserAdd.path(), params: param, method: .post, success: { (result) in
//            do{
//                let json =  try JSONSerialization.jsonObject(with: result, options: []) as! [String:Any]
//                do{
//                    let result1 = try JSONDecoder().decode(SignInModel.self, from: result)
//                    success(result1)
//                }catch{
//                    onError("\(json["Message"] as! String)")
//                }
//            }catch{
//                onError("Conntion Failed")
//            }
//
//        }) { (error) in
//            onError(error)
//        }
//    }
    
}
