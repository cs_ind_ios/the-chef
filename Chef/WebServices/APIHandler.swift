//
//  APIHandler.swift
//  CoffeeStation
//
//  Created by CS on 07/06/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class APIHandler {

    class func APIHandlerRequest(url:String, method:HTTPMethod, param:Parameters , success:@escaping (_ data : Data)-> Void, onError:@escaping ( _ error:String) ->Void) {
        
        //ANLoader.showLoading("Loading...", disableUI: false)
        if(Connectivity.isConnectedToInternet() == true){
        
        let headers: HTTPHeaders = [:]
        
        let timeManage = Alamofire.SessionManager.default
        timeManage.session.configuration.timeoutIntervalForRequest = 60
        timeManage.session.configuration.timeoutIntervalForResource = 60
        print("Paramers:\(param)")
        print(url)
    
        timeManage.request(url, method: method, parameters: param , encoding: URLEncoding.default , headers: headers).responseJSON { (response ) in
           // ANLoader.hide()
            success(response.data!)
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            ANLoader.hide()
            }
            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"Connection Error! Please check the internet connection", value: "", table: nil))!)
        }
    }
    
    class func APIHandler(url:String, method:HTTPMethod, param:Parameters , success:@escaping (_ data : Any)-> Void ) {
        let headers: HTTPHeaders = [:]
        let timeManage = Alamofire.SessionManager.default
        timeManage.session.configuration.timeoutIntervalForRequest = 60
        timeManage.session.configuration.timeoutIntervalForResource = 60
        //print(param)
        //print(headers)
       // print(url)
        
        timeManage.request(url, method: method, parameters: param , encoding: URLEncoding.default , headers: headers).responseJSON { (response ) in
            switch response.result {
            case .success(let jsonData) :
                //print(jsonData)
                success(jsonData)
            case .failure(let error) :
                if error._code == NSURLErrorTimedOut {
                    print("Req Time Out")
                }
                //print(error.localizedDescription)
                return
                
            }
        }
    }
    
    class func Request(url:String,params:Parameters,method:HTTPMethod,success:@escaping(_ data : Data)->Void, onError:@escaping(_ error:String)->Void) {
        if(Connectivity.isConnectedToInternet() == true){
            ANLoader.showLoading("Loading..", disableUI: true)
            let headers: HTTPHeaders = [:]

            //let headers: HTTPHeaders = ["api_key":"1233456","Content-Type":"application/json"]
            let timeManage = Alamofire.SessionManager.default
            timeManage.session.configuration.timeoutIntervalForRequest = 60
            timeManage.session.configuration.timeoutIntervalForResource = 60
            //print(params)
            //print(url)
            timeManage.request(url, method: method, parameters: params, encoding: JSONEncoding.default ,headers: headers).responseJSON { (response) in
                success(response.data!)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                }
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
             ANLoader.hide()
            }
            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"Connection Error! Please check the internet connection", value: "", table: nil))!)
        }
    }
    
    class func                                                                                                     RequestOptions(url:String, params:Parameters, method:HTTPMethod, isLoader:Bool, isUIDisable:Bool, success:@escaping(_ data : Data)->Void,onError:@escaping(_ error:String)->Void) {
        if(Connectivity.isConnectedToInternet() == true){
            if isLoader == true {
                ANLoader.showLoading("Loading..", disableUI: isUIDisable)
            }
            let headers: HTTPHeaders = [:]
            //let headers: HTTPHeaders = ["api_key":"1233456","Content-Type":"application/json"]
            let timeManage = Alamofire.SessionManager.default
            timeManage.session.configuration.timeoutIntervalForRequest = 60
            timeManage.session.configuration.timeoutIntervalForResource = 60
            //print(params)
            //print(url)
            timeManage.request(url, method: method, parameters: params, encoding: JSONEncoding.default ,headers: headers).responseJSON { (response) in
                success(response.data!)
                if isLoader == true {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    ANLoader.hide()
                    }
                }
            }
        }else{
            if isLoader == true {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                ANLoader.hide()
                }
            }
            onError((AppDelegate.getDelegate().filePath?.localizedString(forKey:"Connection Error! Please check the internet connection", value: "", table: nil))!)
        }
    }
    
    class func APIHandlerRequestCancel(){
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }
    }
}


