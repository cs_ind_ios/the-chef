//
//  URLS.swift
//  CheckClick Vendor
//
//  Created by Creative Solutions on 12/27/18.
//  Copyright © 2018 Creative Solutions. All rights reserved.
//
import Foundation
import Alamofire

//let baseURL = "http://csadms.com/CheffAppAPI/"
let baseURL = "http://api.thechefapp.net/"
//let baseURL = "http://csadms.com/ChefAppAPITest/"

enum url: String{
    case login = "api/UserAPI/GetUserValidation"
    case register = "api/UserAPI/SaveUserDetails"
    case verifyMobileForSignUp = "api/UserAPI/GetMobileEmailVerify"
    case verifyMobileForForgotPassword = "api/UserAPI/GetMobileVerify"
    case forgotPassword = "api/UserAPI/Forgotpassword"
    case setNewPassword = "api/UserAPI/SetNewPassword"
    
    // account module
    case changePassword = "api/UserAPI/ChangePassword"
    case LogOut = "api/UserAPI/LogOut"
    case SaveAddress = "api/UserAPI/SaveUserAddress"
    case getUserAdd = "api/UserAPI/GetUserAddress"
    case trackDriverService = "api/Driver/NewGetLiveTracking"
    
    // Order Module
    case StoresInformatio = "api/StoreInformationAPI/GetStoreCatInformation"
    
    // menuItems and Additional
    case getMainMenu = "api/ManageItems/GetMenuItems"
    case getAdditions = "api/ManageItems/GetAdditionalItemsPrices"
    
    // Place Order
    case insertOrder = "api/OrderInformation/InsertOrder"
    case getOrderHistory = "api/OrderInformation/GetUserOrderHistory"
    case getOrderTracking = "api/OrderInformation/GetOrderTracking"
    case cancelOrder = "api/OrderInformation/CancelOrder" 
    case favouriteOrder = "api/OrderInformation/InsertFavRatingDeleteOrder"
    case activeOrder = "api/OrderInformation/UserActiveOrder"
    
    //Payment
    case getPaymentCheckOutService = "api/PaymentAPI/NewCheckoutRequestFromSDK"
    case getPaymentResourcePathService = "api/PaymentAPI/paymentStatus"
    case getSaveCardService = "api/PaymentAPI/SaveCard"
    case getAllSaveCardDetailsService = "api/PaymentAPI/GetAllSavedCards"
    case getDeleteCardService = "api/PaymentAPI/deleteCard"
    case getAddCardStatusService = "api/PaymentAPI/AddCardStatus"
    case getPromotionService = "api/PromotionAPI/NewgetAppPromotions"
    case updateLangaueService = "api/UserAPI/languageChange"
    case logoutService = "api/PartnerAuthenticationAPI/Logout"

    func path() ->String { return baseURL + self.rawValue }
    //img path
    case storeImg = "Uploads/StoreImages/"
    case ItemImg = "Uploads/ItemImages/"
    case CategoryImg = "Uploads/CategoryImages/"
    case driverImg = "Uploads/DriverImages/"
    
    func imgPath() -> String {
        return baseURL + self.rawValue
    }
}
class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
